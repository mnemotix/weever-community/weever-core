module.exports = function(api) {
  if(!api.env('test')){
    api.cache(false);
  }

  const plugins = [];

  return {
    plugins,
    sourceMaps: "inline",
    ignore: [
      (process.env.NODE_ENV !== 'test' ?  "**/*.test.js" : null)
    ].filter(n => n)
  };
};
