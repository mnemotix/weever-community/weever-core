# Pour installer la stack Weever

## Installer les bases de données.

La stack a besoin de 3 bases de données pour fonctionner :

- [GraphDB EE](https://graphdb.ontotext.com/documentation/enterprise/). Attention, une licence est requise pour l'édition entreprise, Mnemotix vous la fournira.
- [Elastic Search >7](https://www.elastic.co/fr/)
- Mysql (ou équivalent) pour le serveur Keycloak


## Initialiser GraphDB

- Créer un nouveau repository dans GraphDB (via le [workbench](https://URL_DE_GRAPHDB/repository/create)). Dans l'interface de création, cliquer sur "Upload Custom Ruleset" et utiliser le fichier `data/graphdb/rdfs-plus-mnx.pie`

Le nom du repository correspond à la variable d'environnement à configurer (partie suivante):

```
GRAPHDB_REPOSITORY=
```

- Créer un nouvel utilisateur à associer en LECTURE / ECRITURE à ce repository via le [workbench](https://URL_DE_GRAPHDB/users). Cet utilisateur correspond aux variables d'environnement à configurer (partie suivante):

```
GRAPHDB_USER=
GRAPHDB_PASSWORD=
```

- Importer l'ontologie via le [workbench](https://URL_DE_GRAPHDB/import#user). Cliquer sur "Upload RDF files" et charger le fichier `data/graphdb/ontology.trig`. Laisser les options d'import par défaut.

## Configurer les variables d'environnement

Les différentes variables d'environnements utilisées sont disponibles dans le template `.env.template`

## Lancer la stack

### En passant par docker-compose

Dans le dossier du docker-compose.yml :

```sh
  docker-compose up -d 
```

## Finalisation de configuration :

## Keycloak

Se connecter à Keycloak avec les identifiants disponibles dans le `.env`

Créer un nouveau Realm en important le fichier `data/keycloak/realm-luma.json` APRES avoir changé 

 - la ligne 7 :

```
"secret": "** NE PAS OUBLIER DE REMPLIR LE MÊME SECRET QUE DANS LE .env",
```

Mettre dans cette ligne la même valeur que celle configurée dans la variable `KEYCLOAK_CLIENT_SECRET` du `.env` après l'avoir générée sur [un UUID générateur](https://www.uuidgenerator.net/version4)

 - les lignes 54-58 pour régler le serveur SMTP d'envoi de mail de réinitialisation de mot de passe.


