Prefices
{
  rdf : http://www.w3.org/1999/02/22-rdf-syntax-ns#
  rdfs : http://www.w3.org/2000/01/rdf-schema#
  owl : http://www.w3.org/2002/07/owl#
  onto : http://www.ontotext.com/
  xsd : http://www.w3.org/2001/XMLSchema#
  psys : http://proton.semanticweb.org/protonsys#
  pext : http://proton.semanticweb.org/protonext#
  schema : http://schema.org/
  skos : http://www.w3.org/2004/02/skos/core#
  mnx : http://ns.mnemotix.com/ontologies/2019/8/generic-model/
  sioc: http://rdfs.org/sioc/ns#
  prov: http://www.w3.org/ns/prov#
}

Axioms
{
	<rdf:type> <rdf:type> <rdf:Property>
	<rdf:subject> <rdf:type> <rdf:Property>
	<rdf:predicate> <rdf:type> <rdf:Property>
	<rdf:object> <rdf:type> <rdf:Property>
	<rdf:first> <rdf:type> <rdf:Property>
	<rdf:rest> <rdf:type> <rdf:Property>
	<rdf:value> <rdf:type> <rdf:Property>
	<rdf:nil> <rdf:type> <rdf:List>
	<rdfs:subClassOf> <rdfs:domain> <rdfs:Class>
	<rdf:subject> <rdfs:domain> <rdf:Statement>
	<rdf:predicate> <rdfs:domain> <rdf:Statement>
	<rdf:object> <rdfs:domain> <rdf:Statement>
	<rdf:first> <rdfs:domain> <rdf:List>
	<rdf:rest> <rdfs:domain> <rdf:List>
	<rdfs:domain> <rdfs:range> <rdfs:Class>
	<rdfs:range> <rdfs:range> <rdfs:Class>
	<rdfs:subClassOf> <rdfs:range> <rdfs:Class>
	<rdf:rest> <rdfs:range> <rdf:List>
	<rdfs:comment> <rdfs:range> <rdfs:Literal>
	<rdfs:label> <rdfs:range> <rdfs:Literal>
	<rdf:Alt> <rdfs:subClassOf> <rdfs:Container>
	<rdf:Bag> <rdfs:subClassOf> <rdfs:Container>
	<rdf:Seq> <rdfs:subClassOf> <rdfs:Container>
	<rdfs:ContainerMembershipProperty> <rdfs:subClassOf> <rdf:Property>
	<rdfs:isDefinedBy> <rdfs:subPropertyOf> <rdfs:seeAlso>
	<rdf:XMLLiteral> <rdf:type> <rdfs:Datatype>
	<rdf:XMLLiteral> <rdfs:subClassOf> <rdfs:Literal>
	<rdfs:Datatype> <rdfs:subClassOf> <rdfs:Class>
	<owl:equivalentClass> <rdf:type> <owl:TransitiveProperty>
	<owl:equivalentClass> <rdf:type> <owl:SymmetricProperty>
	<owl:equivalentClass> <rdfs:subPropertyOf> <rdfs:subClassOf>
	<owl:equivalentProperty> <rdf:type> <owl:TransitiveProperty>
	<owl:equivalentProperty> <rdf:type> <owl:SymmetricProperty>
	<owl:equivalentProperty> <rdfs:subPropertyOf> <rdfs:subPropertyOf>
	<owl:inverseOf> <rdf:type> <owl:SymmetricProperty>
	<rdfs:subClassOf> <rdf:type> <owl:TransitiveProperty>
	<rdfs:subPropertyOf> <rdf:type> <owl:TransitiveProperty>
	<rdf:type> <psys:transitiveOver> <rdfs:subClassOf>
	<owl:differentFrom> <rdf:type> <owl:SymmetricProperty>
	<xsd:nonNegativeInteger> <rdf:type> <rdfs:Datatype>
	<xsd:string> <rdf:type> <rdfs:Datatype>
	<rdf:_1> <rdf:type> <rdf:Property>
	<rdf:_1> <rdf:type> <rdfs:ContainerMembershipProperty>
}

Rules
{
/////
// RDFS PLUS
/////

	Id: rdfs7

	  a b c
	  b <rdfs:subPropertyOf> d [Constraint b != d]
	------------------------------------
	  a d c


	Id: rdfs8_10

	  a <rdf:type> <rdfs:Class>
	------------------------------------
	  a <rdfs:subClassOf> a


	Id: proton_TransitiveOver

	  a <psys:transitiveOver> b
	  c a d
	  d b e
	------------------------------------
	  c a e


	Id: proton_TransProp

	  a <rdf:type> <owl:TransitiveProperty>
	------------------------------------
	  a <psys:transitiveOver> a


	Id: proton_TransPropInduct

	  a <psys:transitiveOver> a
	------------------------------------
	  a <rdf:type> <owl:TransitiveProperty>


	Id: owl_invOf

	  a b c
	  b <owl:inverseOf> d
	------------------------------------
	  c d a


	Id: owl_invOfBySymProp

	  a <rdf:type> <owl:SymmetricProperty>
	------------------------------------
	  a <owl:inverseOf> a


	Id: owl_SymPropByInverse

	  a <owl:inverseOf> a
	------------------------------------
	  a <rdf:type> <owl:SymmetricProperty>


	Id: owl_EquivClassBySubClass

	  a <rdfs:subClassOf> b [Constraint b != a]
	  b <rdfs:subClassOf> a [Cut]
	------------------------------------
	  a <owl:equivalentClass> b


	Id: owl_EquivPropBySubProp

	  a <rdfs:subPropertyOf> b [Constraint b != a]
	  b <rdfs:subPropertyOf> a [Cut]
	------------------------------------
	  a <owl:equivalentProperty> b

/////
// MNX Rules
/////

/// SKOS ENHANCEMENTS

    // Set same conceptScheme for all narrower concepts
    Id: inScheme_Transitive_Inference
      concept <skos:broaderTransitive> broader
      broader <skos:topConceptOf> scheme
    ------------------------------------
      concept <skos:inScheme> scheme

    // Set same vocabulary belonging for all narrower concepts
    Id: vocabulary_Transitive_Inference
      concept <skos:inScheme> scheme
      scheme  <mnx:schemeOf> vocabulary
    ------------------------------------
      concept <mnx:conceptOf> vocabulary

/// DELETION CASCADES

    // Delete all involvements of a deleted project contribution
    Id: involvement_project_contrib_Deletion_Inference
      involvement <rdf:type> <mnx:Involvement>
      involvement <mnx:hasProjectContribution> projectContribution
      projectContribution <mnx:hasDeletion> deletion
      ------------------------------------
      involvement <mnx:hasDeletion> deletion

    // Delete all involvements of a deleted project
    Id: involvement_project_Deletion_Inference
      involvement <rdf:type> <mnx:Involvement>
      involvement <mnx:hasProject> project
      project <mnx:hasDeletion> deletion
      ------------------------------------
      involvement <mnx:hasDeletion> deletion

    // Delete all involvements of a deleted agent
    Id: involvement_agent_Deletion_Inference
      involvement <rdf:type> <mnx:Involvement>
      involvement <mnx:hasAgent> agent
      agent <mnx:hasDeletion> deletion
      ------------------------------------
      involvement <mnx:hasDeletion> deletion

    // Delete all affiliations of a deleted person
    Id: affiliation_person_Deletion_Inference
      affiliation <rdf:type> <mnx:Affiliation>
      affiliation <mnx:hasPerson> person
      person <mnx:hasDeletion> deletion
      ------------------------------------
      affiliation  <mnx:hasDeletion> deletion

    // Delete all affiliations of a deleted organisation
    Id: affiliation_org_Deletion_Inference
      affiliation <rdf:type> <mnx:Affiliation>
      affiliation <mnx:hasOrg> organisation
      organisation <mnx:hasDeletion> deletion
      ------------------------------------
      affiliation  <mnx:hasDeletion> deletion

    // Delete all taggings of a deleted entity
    Id: tagging_entity_Deletion_Inference
      tagging <rdf:type> <mnx:Tagging>
      tagging <mnx:isTaggingOf> entity
      entity <mnx:hasDeletion> deletion
      ------------------------------------
      tagging <mnx:hasDeletion> deletion

    // Delete all taggings of a deleted concept
    Id: tagging_concept_Deletion_Inference
      tagging <rdf:type> <mnx:Tagging>
      tagging <mnx:hasConcept> concept
      concept <mnx:hasDeletion> deletion
      ------------------------------------
      tagging <mnx:hasDeletion> deletion

    // Delete all attachments of a deleted resource
    Id: attachment_resource_Deletion_Inference
      attachment <rdf:type> <mnx:Attachment>
      attachment <mnx:hasResource> resource
      resource <mnx:hasDeletion> deletion
      ------------------------------------
      attachment <mnx:hasDeletion> deletion

    // Delete all attachments of a deleted project contribution
    Id: attachment_project_contribution_Deletion_Inference
      attachment <rdf:type> <mnx:Attachment>
      attachment <mnx:hasProjectContribution> projectContribution
      attachment <mnx:hasDeletion> deletion
      ------------------------------------
      attachment <mnx:hasDeletion> deletion

    // Delete a user account of a deleted person
    Id: userAccount_person_Deletion_Inference
      person <rdf:type> <mnx:Person>
      person <mnx:hasUserAccount> userAccount
      person <mnx:hasDeletion> deletion
      ------------------------------------
      userAccount <mnx:hasDeletion> deletion

/// DEFAULT ACCESS TARGETS

    // Propagate readWrite property on entities for all Administrator group users
    Id: entity_defaultAccessTargets
      entity <rdf:type> <mnx:Entity>
    -------------------------------------
      entity <mnx:hasReadWriteAccessTarget> <mnx:user-group/AdministratorGroup>

    // Propagate readWrite property on projects for all in Editor group users
    Id: project_defaultAccessTargets
      project <rdf:type> <mnx:Project>
    -------------------------------------
      project <mnx:hasReadWriteAccessTarget> <mnx:user-group/EditorGroup>

    // Propagate readOnly property on agents for all in Contributor group users
    Id: agent_defaultAccessTargets
      agent <rdf:type> <mnx:Agent>
    -------------------------------------
      agent <mnx:hasReadWriteAccessTarget> <mnx:user-group/EditorGroup>
      agent <mnx:hasReadOnlyAccessTarget> <mnx:user-group/ContributorGroup>

    // Propagate readWrite property on SKOS vocabularies for all in Editor group users
    Id: vocabulary_defaultAccessTargets
      vocabulary <rdf:type> <mnx:Vocabulary>
    -------------------------------------
      vocabulary <mnx:hasReadWriteAccessTarget> <mnx:user-group/EditorGroup>
      vocabulary <mnx:hasReadOnlyAccessTarget> <mnx:user-group/ContributorGroup>

    // Propagate readWrite property on SKOS concepts for all in Editor group users
    Id: concept_defaultAccessTargets
      concept <rdf:type> <skos:Concept>
    -------------------------------------
      concept <mnx:hasReadWriteAccessTarget> <mnx:user-group/EditorGroup>
      concept <mnx:hasReadOnlyAccessTarget> <mnx:user-group/ContributorGroup>

    // Propagate readWrite property on SKOS schemes for all in Editor group users
    Id: scheme_defaultAccessTargets
      scheme <rdf:type> <skos:ConceptScheme>
    -------------------------------------
      scheme <mnx:hasReadWriteAccessTarget> <mnx:user-group/EditorGroup>
      scheme <mnx:hasReadOnlyAccessTarget> <mnx:user-group/ContributorGroup>

    // Propagate readWrite property on SKOS collections for all in Editor group users
    Id: collection_defaultAccessTargets
      collection <rdf:type> <skos:Collection>
    -------------------------------------
      collection <mnx:hasReadWriteAccessTarget> <mnx:user-group/EditorGroup>
      collection <mnx:hasReadOnlyAccessTarget> <mnx:user-group/ContributorGroup>

    // Propagate readWrite property on vocabularies for all in Editor group users
    Id: userAccount_selfReadWriteAccessTarget
      userAccount <rdf:type> <mnx:UserAccount>
    -------------------------------------
      userAccount <mnx:hasReadWriteAccessTarget> userAccount

    // Propagate readWrite property for the entity creator (userAccount)
    Id: creator_readWriteAccessTarget
      entity <mnx:hasCreation> creation
      creation <prov:wasAssociatedWith> user
      -------------------------------------
      entity <mnx:hasReadWriteAccessTarget> user

    // Propagate mnx:hasAccessTarget predicates of a projet to its project contributions
    Id: projectToProjectContrib_sameReadOnlyAccessTarget
      projectContribution <mnx:hasProject> project
      project <mnx:hasReadOnlyAccessTarget> accessTarget
      -------------------------------------
      projectContribution <mnx:hasReadOnlyAccessTarget> accessTarget

    Id: projectToProjectContrib_sameReadWriteAccessTarget
      projectContribution <mnx:hasProject> project
      project <mnx:hasReadWriteAccessTarget> accessTarget
      -------------------------------------
      projectContribution <mnx:hasReadWriteAccessTarget> accessTarget

    // Propagate mnx:hasReadOnlyAccessTarget predicates of an attachment to its resource
    Id: projectContribToAttachment_sameReadOnlyAccessTarget
      attachment <rdf:type> <mnx:Attachment>
      attachment <mnx:isAttachmentOf> projectContribution
      attachment <mnx:hasResource> resource
      projectContribution <mnx:hasReadOnlyAccessTarget> accessTarget
      -------------------------------------
      attachment <mnx:hasReadOnlyAccessTarget> accessTarget
      resource  <mnx:hasReadOnlyAccessTarget> accessTarget

    Id: projectContribToAttachment_sameReadWriteAccessTarget
      attachment <rdf:type> <mnx:Attachment>
      attachment <mnx:isAttachmentOf> projectContribution
      attachment <mnx:hasResource> resource
      projectContribution <mnx:hasReadWriteAccessTarget> accessTarget
      -------------------------------------
      attachment <mnx:hasReadOnlyAccessTarget> accessTarget
      resource  <mnx:hasReadWriteAccessTarget> accessTarget   // <= ReadWriteAccess user can also edit a resource that does not belongs to him.

    // Propagate mnx:hasAccessTarget predicates of an attachment to its resource
    Id: projectContribToInvolvement_sameAccessTarget
      involvement <rdf:type> <mnx:Involvement>
      involvement <mnx:hasInvolvement> projectContribution
      projectContribution <mnx:hasAccessTarget> accessTarget
      -------------------------------------
      involvement <mnx:hasReadOnlyAccessTarget> accessTarget

    // Propagate mnx:hasAccessTarget predicates of a project contribution to replying ones (commonly named memos)
    Id: projectContribToProjectContribreply_sameAccessTarget
      projectContributionReply <mnx:isReplyOf> projectContribution
      projectContribution <mnx:hasAccessTarget> accessTarget
      -------------------------------------
      projectContributionReply <mnx:hasReadOnlyAccessTarget> accessTarget

    // Propagate readWrite property on SKOS concepts for all in Editor group users
    Id: projectToProjectOutput_sameAccessTarget
      projectOutput <rdf:type> <mnx:ProjectOutput>
      project <mnx:hasProjectOutput> projectOutput
      project <mnx:hasAccessTarget> accessTarget
      -------------------------------------
      projectOutput <mnx:hasReadOnlyAccessTarget>  accessTarget

    // Propagate readWrite property on SKOS concepts for all in Editor group users
    Id: projectOutputToProjectOutput_sameAccessTarget
      projectOutput <rdf:type> <mnx:ProjectOutput>
      projectOutputRep <mnx:hasRepresentation> projectOutput
      projectOutput <mnx:hasAccessTarget> accessTarget
      -------------------------------------
      projectOutputRep <mnx:hasReadOnlyAccessTarget> accessTarget

/// USER GROUPS

    // If a user belongs to AdminstratorGroup, he should also belongs to EditorGroup.
    Id: subGroupTransitivity
      groupA <mnx:isSubGroupOf> groupB
      groupB <mnx:isSubGroupOf> groupC
    -------------------------------------
      groupA <mnx:isSubGroupOf> groupC

    // If a user belongs to AdminstratorGroup, he should also belongs to EditorGroup.
    Id: user_isTransitiveMemberOf_visitorGroup
      user <rdf:type> <mnx:UserAccount>
    -------------------------------------
      user <mnx:isTransitiveMemberOf> <mnx:user-group/VisitorGroup>

    // If a user belongs to AdminstratorGroup, he should also belongs to EditorGroup.
    Id: adminUser_inEditorUserGroup
      user <sioc:member_of> groupA
      groupA <mnx:isSubGroupOf> groupB
    -------------------------------------
      user <mnx:isTransitiveMemberOf> groupB

    // If a userAccount is ReadWrite accessTarget, he belongs to ContributorGroup
    Id: readWrite_userAccount_inContributorGroup
      project <rdf:type> <mnx:Project>
      project <mnx:hasReadWriteAccessTarget> user
      user <rdf:type> <mnx:UserAccount>
    -------------------------------------
      user <sioc:member_of> <mnx:user-group/ContributorGroup>

     // If a userAccount is ReadWrite accessTarget, he belongs to ContributorGroup
        Id: readWrite_userGroupUserAccounts_inContributorGroup
          project <rdf:type> <mnx:Project>
          project <mnx:hasReadWriteAccessTarget> group
          group <rdf:type> <mnx:UserGroup>
          user <sioc:member_of> group
        -------------------------------------
          user <sioc:member_of> <mnx:user-group/ContributorGroup>
}