/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import { promisify } from "util";
import stream from "stream";
import fs from "fs";
import os from "os";
import convertCsvToXlsx from "@aternus/csv-to-xlsx";
import got from "got";

const pipeline = promisify(stream.pipeline);

/**
 * Serves localized labels for the application under the endpoint /locales/:lang
 *
 * The locales are defined in the directory src/locales
 *
 */
export async function serveCSV({ app }) {
  app.get("/csv", async (req, res) => {
    try {
      const filename = req.query.fname;
      const tmpCvsPath = `${os.tmpdir()}/${filename}`;
      const tmpXlsPath = `${os.tmpdir()}/${filename}.xls`;

      if(fs.existsSync(tmpCvsPath)) {
        fs.rmSync(tmpCvsPath);
      }

      if(fs.existsSync(tmpXlsPath)) {
        fs.rmSync(tmpXlsPath);
      }

      const uri = req.query.uri.replace(" ", "+");

      await pipeline(got.stream(uri), fs.createWriteStream(tmpCvsPath));

      convertCsvToXlsx(tmpCvsPath, tmpXlsPath);

      res.download(tmpXlsPath, `${filename}.xls`, () => {
        fs.rmSync(tmpCvsPath);
        fs.rmSync(tmpXlsPath);
      });
    } catch (e) {
      res.status(500).send(e.message);
    }
  });
}
