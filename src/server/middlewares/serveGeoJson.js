/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {promisify} from "util";
import stream from "stream";
import fs from "fs";
import os from "os";
import ogr2ogr from "ogr2ogr";
import {logError} from "@mnemotix/synaptix.js";
import got from "got";
import {parse as wktToGeoJson} from "wellknown";

const pipeline = promisify(stream.pipeline);

/**
 * Serves localized labels for the application under the endpoint /locales/:lang
 *
 * The locales are defined in the directory src/locales
 * 
 * if you have "spawn ogr2ogr ENOENT" error you need to install `GDAL tools`
 *  > brew install gdal --HEAD
 *  > brew install GDAL
 *  > gdal-config --version     to check if installed ok 
 */
export async function serveGeoJson({app}) {
  app.get("/geojson", async (req, res) => {
    try {

      const uri = req.query.uri.replace(" ", "+");
      const filename = req.query.fname;
      const tmpFilePath = `${os.tmpdir()}/${filename}`;

      await pipeline(got.stream(uri), fs.createWriteStream(tmpFilePath));

      let data;

      if (filename.match(/\.wkt/)) {
        const wkt = fs.readFileSync(tmpFilePath);
        data = wktToGeoJson(wkt.toString());
      } else {
        ({data} = await ogr2ogr(tmpFilePath));
      }

      fs.rmSync(tmpFilePath);

      res.json(data);
    } catch (e) {
      res.status(500).send(e.message);
    }
  });
}
