/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");

const env = require("env-var");

/**
 * @param {string} distPath
 *
 * @param {string} assetsPath - Where Webpack compiles assets except Js
 * @param {string} jsAssetsPath -  Where Webpack compiles Javascript assets
 * @param {object} entries
 *
 * @param {string} [mainJsPath] - Deprecated. Use entries
 * @param {string} [htmlTemplatePath] - Deprecated. Use entries
 */
export function generateWebpackConfig({
  distPath,
  assetsPath = "a/",
  jsAssetsPath = "js/",
  entries = [],
  mainJsPath,
  htmlTemplatePath
}) {
  let isDev = process.env.NODE_ENV !== "production";
  let isProd = !isDev;
  let isCI = !!process.env.CI;

  /** mode **/
  let mode = isDev ? "development" : "production";

  /** devtool **/
  let devtool = isDev ? "eval-source-map" : false;

  // For retrocompatibility purpose.
  if (mainJsPath && htmlTemplatePath) {
    entries.push({
      name: "main",
      entry: mainJsPath,
      html: {
        template: htmlTemplatePath,
        filename: "index.html",
        excludeChunks: ["koncept"]
      }
    });
  }

  /** entry **/
  let entry = entries.reduce((acc, {name, entry}) => {
    if (isDev && env.get("HOT_RELOAD_DISABLED").asBool() !== true) {
      acc[name] = ["webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000", entry];
    } else {
      acc[name] = entry;
    }
    return acc;
  }, {});

  /** output **/
  let output = {
    path: distPath,
    filename: jsAssetsPath + (isDev ? "[name].bundle.js" : "[name].[hash].bundle.js"),
    chunkFilename: jsAssetsPath + (isDev ? "[name].chunk.js" : "[name].[hash].chunk.js"),
    publicPath: "/",
    clean: true
  };

  /** plugins **/
  let plugins = [
    ...(!isCI ? [new webpack.ProgressPlugin()] : []),
    ...entries.map(
      ({html}) =>
        new HtmlWebpackPlugin({
          inject: "body",
          ...html
        })
    ),
    ...(isDev && env.get("HOT_RELOAD_DISABLED").asBool() !== true
      ? [new webpack.HotModuleReplacementPlugin(), new ReactRefreshWebpackPlugin()]
      : []),
    ...(isProd
      ? [
          new MiniCssExtractPlugin({
            filename: "[name].[hash].css",
            chunkFilename: "[id].[hash].css"
          }),
          new CompressionPlugin({
            filename: "[path][base].gz",
            algorithm: "gzip",
            test: /\.js$|\.css$|\.html$/,
            threshold: 10240,
            minRatio: 0.8
          }),
          new CompressionPlugin({
            filename: "[path][base].br",
            algorithm: "brotliCompress",
            test: /\.(js|css|html|svg)$/,
            compressionOptions: {
              level: 11
            },
            threshold: 10240,
            minRatio: 0.8
          })
        ]
      : []),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.EnvironmentPlugin({
      FRONTEND_DEBUG: false,
      TUSD_ENDPOINT: "",
      THUMBOR_BASE_URL: "",
      NODE_ENV: "development",
      SYNAPTIX_USER_SESSION_COOKIE_NAME: "SNXID"
    })
  ];

  /** optimization **/
  let optimization = {
    splitChunks: {
      cacheGroups: {
        vendors: {
          test(module) {
            return (
              module.resource &&
              module.resource.match(/\.yarn|node_modules/) &&
              !module.resource.match(
                /weever-core|koncept-core|synaptix|@mui|\/react\/|\/react-dom\/|leaflet|@uppy|react-force-graph|three/
              )
            );
          },
          name: "vendor",
          chunks: "all",
          minSize: 0
        },
        mui: {
          test: /@mui/,
          name: "mui",
          chunks: "all",
          minSize: 0
        },
        react: {
          test: /\/react\/|\/react-dom\//,
          name: "react",
          chunks: "all",
          minSize: 0
        },
        leaflet: {
          test: /\/leaflet\/|\/@?react-leaflet\//,
          name: "leaflet",
          chunks: "all",
          minSize: 0
        },
        synaptix: {
          test: /synaptix/,
          name: "synaptix",
          chunks: "all",
          minSize: 0
        },
        uppy: {
          test: /@uppy/,
          name: "uppy",
          chunks: "all",
          minSize: 0
        },
        graph: {
          test: /react-force-graph|three/,
          name: "graph",
          chunks: "all",
          minSize: 0
        }
      }
    }
  };
  if (isProd) {
    optimization.minimizer = [
      new TerserPlugin({
        extractComments: true
      }),
      new CssMinimizerPlugin()
    ];
  }

  /** modules **/
  let webpackModule = {
    rules: [
      {
        test: /\.m?js$/,
        resolve: {
          fullySpecified: false
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: require.resolve("babel-loader"),
            options: {
              rootMode: "upward",
              plugins: [
                isDev && env.get("HOT_RELOAD_DISABLED").asBool() && require.resolve("react-refresh/babel")
              ].filter(Boolean)
            }
          }
        ]
      },
      {
        /*
         * Also, CSS dependencies in node_modules are not used by this project
         *
         */
        test: /\.css$/i,
        include: /node_modules/,
        use: [require.resolve("style-loader"), require.resolve("css-loader")]
      },
      {
        test: /\.(woff|woff2|eot|ttf)$/,
        use: [
          {
            loader: require.resolve("url-loader"),
            options: {
              limit: 4096
            }
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif|svg|ico)$/i,
        use: {
          loader: require.resolve("file-loader"),
          options: {
            name: "[name].[hash].[ext]",
            outputPath: assetsPath
          }
        }
      },
      {
        test: /\.md/,
        use: require.resolve("raw-loader")
      }
    ]
  };

  return {
    mode,
    devtool,
    entry,
    output,
    plugins,
    optimization,
    module: webpackModule,
    resolve: {
      extensions: [
        ".js",
        ".mjs",
        ".json" /* needed because some dependencies use { import './Package' } expecting to resolve Package.json */,
        ".css" /* for libraries shipping ES6 module to work */
      ],
      fallback: {
        // This is to avoid the error "BREAKING CHANGE: webpack < 5 used to include polyfills for node.js core modules by default."
        path: false,
        url: false,
        events: false
      }
    },
    cache: {
      type: "filesystem"
    },
    performance: {
      hints: false
    }
  };
}
