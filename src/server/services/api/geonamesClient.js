/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import env from "env-var";
import got from "got";
import {Model} from "@mnemotix/synaptix.js";
import ConceptDefinition from "../../datamodel/definitions/ConceptDefinition";

const cache = new Map();

class GeonamesClient {
  async get({service, params = {}} = {}) {
    const geonamesUsername = env.get("GEONAMES_USERNAME").asString();

    if (geonamesUsername) {
      params.username = geonamesUsername;

      const serializedParams = Object.entries(params)
        .reduce((acc, [key, value]) => {
          if (Array.isArray(value)) {
            for (let valueFragment of value) {
              acc.push(`${key}=${valueFragment}`);
            }
          } else {
            acc.push(`${key}=${value}`);
          }
          return acc;
        }, [])
        .join("&");

      const uri = `http://api.geonames.org/${service}?${serializedParams}`;

      if (cache.has(uri)) {
        return cache.get(uri);
      }

      let response = await got(uri, {retry: 3}).json();

      if (response) {
        cache.set(uri, response);
        return response;
      }
    }
  }

  /**
   * Fetch Geonames place by an geonamesId. Cache result to save credit.
   *
   * @param {string} id - Geonames ID
   * @param {string} [lang] - Preferred language
   * @param {number} [retry=0] - Retry count
   * @return {Model}
   */
  async getConcept({id, lang} = {lang: "fr"}) {
    const place = await this.get({
      service: "getJSON",
      params: {
        geonameId: id.replace("geonames:", ""),
        lang
      }
    });

    if (place) {
      return this._convertPlaceToConcept(place, lang);
    }
  }

  /**
   * Search for Geonames places.
   * Use Geonames /searchJSON API and restrict to "A" feature classes (http://www.geonames.org/export/codes.html)
   *
   * @param {string} qs - Query string
   * @param {string} [lang=fr] - Preferred language
   * @param {number} [limit=10] - Limit results size
   * @param {number} [retry=0] - Retry count
   * @param {boolean} [citiesOnly=false] - Filter on cities.
   * @param {array} [featureCodes] - Filter on feature codes @see https://www.geonames.org/export/codes.html
   * @return {Model[]}
   */
  async searchConcepts({qs, lang = "fr", limit = 50, citiesOnly, featureCodes, bbox} = {}) {
    let filtersParams;

    if (featureCodes) {
      filtersParams = {
        featureCode: featureCodes
      };
    } else if (citiesOnly) {
      filtersParams = {
        featureCode: ["ADM3", "ADM4", "ADM5"]
      };
    }

    if (bbox && bbox.length === 4) {
      filtersParams = {
        ...filtersParams,
        north: bbox[0],
        west: bbox[1],
        south: bbox[2],
        east: bbox[3]
      };
    }

    let {geonames: places} =
      (await this.get({
        service: "searchJSON",
        params: {
          q: qs,
          lang,
          searchlang: lang,
          maxRows: limit,
          ...filtersParams
        }
      })) || {};

    return (places || []).map((place) => this._convertPlaceToConcept(place, lang));
  }

  /**
   * @param place
   * @param lang
   * @param overrideName if true search name in alternateNames or toponymName
   * @return {{countryId}|*}
   * @private
   */
  _convertPlaceToConcept(place, lang, overrideName = false) {
    let concept = new Model({
      id: `geonames:${place.geonameId}`,
      uri: `http://www.geonames.org/${place.geonameId}`,
      type: ConceptDefinition.getNodeType(),
      props: {
        scopeNote: place.fcodeName,
        schemePrefLabel: place.countryName,
        vocabularyPrefLabel: "Geonames"
      }
    });

    if (overrideName) {
      // As name_startsWith parameter changes the "name" format,
      // we prefer to use toponymName instead.
      // @see http://www.geonames.org/export/geonames-search.html
      // <quote>
      // 'name' and 'toponymName'
      // The response returns two name attributes. The 'name' attribute is a localized name, the preferred name in the language passed in the optional 'lang'
      // parameter or the name that triggered the response in a 'startsWith' search. The attribute 'toponymName' is the main name of the toponym as displayed
      // on the google maps interface page or in the geoname file in the download. The 'name' attribute is derived from the alternate names.
      // </quote>

      // try to search for a translated name in `place.alternateNames` because `toponymName` is often not translated
      let translatedNamefound = false;
      if (place.alternateNames && lang) {
        // try to get isPreferredName first
        translatedNamefound =
          place.alternateNames.find(
            (element) => element.lang && element.lang.toLowerCase() === lang.toLowerCase() && element.isPreferredName
          ) ||
          // if isPreferredName not found take the firt in same lang
          place.alternateNames.find((element) => element.lang && element.lang.toLowerCase() === lang.toLowerCase());
      }

      if (translatedNamefound && translatedNamefound.name) {
        place.name = translatedNamefound.name;
      } else if (place.toponymName) {
        place.name = place.toponymName;
      }
    }

    concept.prefLabel = place.name;

    return concept;
  }
}

export const geonamesClient = new GeonamesClient();
