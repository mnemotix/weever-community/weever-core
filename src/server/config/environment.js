/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
export default {
  //
  // Application configuration
  //
  UUID: {
    description: "This is the application UUID.",
    defaultValue: "weever",
    defaultValueInProduction: true
  },
  APP_PORT: {
    description: "This is listening port of the application.",
    defaultValue: 3034
  },
  APP_URL: {
    description: "This is the base url of the application.",
    defaultValue: () => `http://localhost:${process.env.APP_PORT}`
  },
  SYNAPTIX_USER_SESSION_COOKIE_NAME: {
    description: "This is the session cookie name",
    defaultValue: "SNXID",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  //
  // RDF configuration
  //
  SCHEMA_NAMESPACE_MAPPING: {
    description: "The ReSource ontology schema namespace mapping",
    defaultValue: JSON.stringify({
      rdfs: "http://www.w3.org/2000/01/rdf-schema#",
      skos: "http://www.w3.org/2004/02/skos/core#",
      geonames: "https://www.geonames.org/",
      foaf: "http://xmlns.com/foaf/0.1/",
      prov: "http://www.w3.org/ns/prov#",
      sioc: "http://rdfs.org/sioc/ns#",
      dc: "http://purl.org/dc/terms/",
      mnx: "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
      rs: "https://data.lafayetteanticipations.com/ontology/resource/"
    }),
    defaultValueInProduction: true
  },
  NODES_NAMESPACE_URI: {
    description: "The ReSource nodes (or individuals) (or class instances) namespace URI",
    defaultValue: "https://data.lafayetteanticipations.com/resource/",
    defaultValueInProduction: true
  },
  NODES_PREFIX: {
    description: "The ReSource nodes or individuals) (or class instances) namespace URI",
    defaultValue: "rsd",
    defaultValueInProduction: true
  },
  NODES_NAMED_GRAPH: {
    description: "The ReSource nodes named graph URI",
    defaultValue: "https://data.lafayetteanticipations.com/resource/graph",
    defaultValueInProduction: true
  },
  //
  // Log configuration
  //
  LOG_LEVEL: {
    description: "Log level (ERROR | VERBOSE | TRACE)",
    defaultValue: "ERROR",
    defaultValueInProduction: true
  },
  LOG_FORMAT: {
    description: "Log format (json | logstash | default)",
    defaultValue: "default",
    defaultValueInProduction: true
  },
  //
  // OAuth configuration
  //
  OAUTH_BASE_URL: {
    description: "This is the OAUTH server host",
    defaultValue: "http://localhost:8181"
  },
  OAUTH_REALM: {
    description: "This is the OAUTH realm used for this app",
    defaultValue: "resource"
  },
  OAUTH_AUTH_URL: {
    description: "This is the OAUTH authentication URL",
    defaultValue: () =>
      `${process.env.OAUTH_BASE_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect/auth`,
    defaultValueInProduction: true
  },
  OAUTH_TOKEN_URL: {
    description: "This is the OAUTH token validation URL",
    defaultValue: () =>
      `${process.env.OAUTH_BASE_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect/token`,
    defaultValueInProduction: true
  },
  OAUTH_LOGOUT_URL: {
    description: "This is the OAUTH logout URL",
    defaultValue: () =>
      `${process.env.OAUTH_BASE_URL}/auth/realms/${process.env.OAUTH_REALM}/protocol/openid-connect/logout`,
    defaultValueInProduction: true
  },
  OAUTH_REALM_CLIENT_ID: {
    description: "This is the OAUTH Realm client ",
    defaultValue: "api"
  },
  OAUTH_REALM_CLIENT_SECRET: {
    description: "This is the OAUTH shared secret between this client app and OAuth2 server.",
    defaultValue: "e9a7be6b-5850-4d22-8740-a8d535e2880b",
    obfuscate: true
  },
  OAUTH_ADMIN_USERNAME: {
    description: "This is the OAUTH admin username",
    defaultValue: "admin"
  },
  OAUTH_ADMIN_PASSWORD: {
    description: "This is the OAUTH admin password",
    defaultValue: "rspwd!",
    obfuscate: true
  },
  OAUTH_ADMIN_TOKEN_URL: {
    description: "This is the OAUTH token validation URL for admin session",
    defaultValue: () => `${process.env.OAUTH_BASE_URL}/auth/realms/master/protocol/openid-connect/token`,
    defaultValueInProduction: true
  },
  OAUTH_ADMIN_API_URL: {
    description: "This is the OAUTH API admin session",
    defaultValue: () => `${process.env.OAUTH_BASE_URL}/auth/admin/realms/${process.env.OAUTH_REALM}`,
    defaultValueInProduction: true
  },
  //
  // Index configuration
  //
  ES_MASTER_URI: {
    description: "Elasticsearch URI",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  ES_CLUSTER_USER: {
    description: "Elasticsearch user name",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  ES_CLUSTER_PWD: {
    description: "Elasticsearch user password",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  INDEX_DISABLED: {
    description: "Is index disabled",
    defaultValue: "0",
    defaultValueInProduction: true
  },
  INDEX_PREFIX_TYPES_WITH: {
    description: "Prefix all index types with a prefix",
    defaultValue: "local-resource-"
  },
  //
  // RDF store configuration
  //
  RDFSTORE_ROOT_URI: {
    description: "RDF store URI",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  RDFSTORE_USER: {
    description: "RDF store user name",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  RDFSTORE_PWD: {
    description: "RDF store user password",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  RDFSTORE_REPOSITORY_NAME: {
    description: "RDF store repository",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  //
  // User groups configuration
  //
  CONTRIBUTOR_USER_GROUP_ID: {
    description: "Contributor group URI",
    defaultValue: "mnx:user-group/ContributorGroup",
    defaultValueInProduction: true
  },
  EDITOR_USER_GROUP_ID: {
    description: "Editor group URI",
    defaultValue: "mnx:user-group/EditorGroup",
    defaultValueInProduction: true
  },
  ADMIN_USER_GROUP_ID: {
    description: "Administrator URI",
    defaultValue: "mnx:user-group/AdministratorGroup",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  //
  // Access policies configuration
  //
  PUBLIC_POLICY_ID: {
    description: "Public access policy URI. Use this id to publish resources",
    defaultValue: "mnx:access-policy/PublicPolicy",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  CONFIDENTIAL_POLICY_ID: {
    description:
      "Confidential access policy URI. Use this id to hide resources from users who doesn't have at least readOnly access on.",
    defaultValue: "mnx:access-policy/ConfidentialPolicy",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  DEFAULT_POLICY_ID: {
    description: "Default access policy URI",
    defaultValue: "mnx:access-policy/PrivatePolicy",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  //
  // Geonames configuration
  //
  GEONAMES_USERNAME: {
    description: "Geonames usename to call geonames API",
    defaultValue: () => "",
    defaultValueInProduction: true
  },
  //
  // RabbitMQ configuration
  //
  RABBITMQ_HOST: {
    description: "This is RabbitMQ host.",
    defaultValue: "localhost"
  },
  RABBITMQ_PORT: {
    description: "This is RabbitMQ port.",
    defaultValue: 5672
  },
  RABBITMQ_LOGIN: {
    description: "This is RabbitMQ login.",
    defaultValue: "guest"
  },
  RABBITMQ_PASSWORD: {
    description: "This is RabbitMQ password.",
    defaultValue: "rspwd!",
    obfuscate: true
  },
  RABBITMQ_EXCHANGE_NAME: {
    description: "This is RabbitMQ exchange name.",
    defaultValue: "local-resource"
  },
  RABBITMQ_EXCHANGE_DURABLE: {
    description: "Is RabbitMQ exchange durable.",
    defaultValue: "0",
    defaultValueInProduction: true
  },
  RABBITMQ_RPC_TIMEOUT: {
    description: "RPC timeout",
    defaultValue: 10e3,
    defaultValueInProduction: true
  },
  //
  // Companion, TusD and Thumbor configuration (file processing stuff)
  //
  COMPANION_ENDPOINT: {
    description: "This is the Uppy companion endpoint to enabled server to server upload",
    defaultValue: "http://localhost:3020",
    exposeInGraphQL: true
  },
  COMPANION_GOOGLE_DRIVE_ENABLED: {
    description: "Is Google Drive files upload enabled in Uppy",
    defaultValue: "1",
    exposeInGraphQL: true
  },
  COMPANION_REMOTE_URL_ENABLED: {
    description: "Is Google Drive files upload enabled in Uppy",
    defaultValue: "1",
    exposeInGraphQL: true
  },
  TUSD_ENDPOINT: {
    description: "This is the TUS resumable uploader server endpoint",
    defaultValue: "http://localhost:1080/files/",
    exposeInGraphQL: true
  },
  THUMBOR_BASE_URL: {
    description: "This is the Thumbor (thumbnail generation) endpoint",
    defaultValue: "http://127.0.0.1:8000/unsafe",
    exposeInGraphQL: true
  },
  //
  // Features configurations
  //
  PUBLIC_EXPLORER_ENABLED: {
    description: "Enable user to access public data without to be logged in",
    defaultValue: "0",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  FILES_BIN_DISABLED: {
    description: "Disable the files bin interface",
    defaultValue: () => process.env.FILES_UPLOAD_WORKFLOW !== "BIN_WORKFLOW",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  FILES_UPLOAD_WORKFLOW: {
    description: `
    Which upload workflow to use. Options are :
     - BIN_WORKFLOW. Use this one to upload files in project level, then organize them with the file bin.
     - DIRECT_WORKFLOW (default). Use this one to upload files directy in project contributions.
    `,
    defaultValue: "DIRECT_WORKFLOW",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  KONCEPT_ENABLED: {
    description: "Enable Koncept",
    defaultValue: "1",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  },
  CONCEPT_SUBMISSION_VOCABULARY_ID: {
    description: "Enable user to submit concepts in a specific vocabulary",
    defaultValue: "",
    defaultValueInProduction: true,
    exposeInGraphQL: true
  }
};
