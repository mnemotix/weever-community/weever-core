export let commonFields = [];

export let commonMapping = {
  entityId: {
    type: "keyword",
  },
  query: {
    type: "percolator",
  },
  percoLabel: {
    type: "text",
  },
};

export let commonEntityFilter = "!bound(?hasDeletionAction)";
