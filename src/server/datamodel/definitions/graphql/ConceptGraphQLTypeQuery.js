/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GraphQLTypeQuery, getObjectResolver} from "@mnemotix/synaptix.js";
import ConceptDefinition from "../ConceptDefinition";
import {geonamesClient} from "../../../services/api/geonamesClient";

export class ConceptGraphQLTypeQuery extends GraphQLTypeQuery {
  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      [this.generateFieldName(modelDefinition)]: this.getConcept.bind(this)
    });
  }

  /**
   * @param {*} _
   * @param {id} concept id
   * @param {SynaptixDatastoreRdfSession} synaptixSession
   * @param {GraphQLResolveInfo} gqlInfos
   */
  async getConcept(_, {id, ...args}, synaptixSession, gqlInfos) {
    if (id.includes("geonames")) {
      return geonamesClient.getConcept({id, lang: synaptixSession.getLang()});
    } else {
      return getObjectResolver(ConceptDefinition, _, {id, ...args}, synaptixSession, gqlInfos);
    }
  }
}
