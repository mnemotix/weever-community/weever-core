/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {GraphQLTypeConnectionQuery, getObjectsResolver} from "@mnemotix/synaptix.js";
import ConceptDefinition from "../ConceptDefinition";
import {geonamesClient} from "../../../services/api/geonamesClient";

export class ConceptGraphQLTypeConnectionQuery extends GraphQLTypeConnectionQuery {
  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      [this.generateFieldName(modelDefinition)]: this.getConcepts.bind(this)
    });
  }

  /**
   * @param {*} _
   * @param {object} args
   * @param {SynaptixDatastoreRdfSession} synaptixSession
   * @param {GraphQLResolveInfo} gqlInfos
   */
  async getConcepts(_, args, synaptixSession, gqlInfos) {
    /** @var {Collection} */
    const localConceptsCollection = await getObjectsResolver(
      ConceptDefinition,
      _,
      args,
      synaptixSession,
      gqlInfos,
      true
    );

    const {qs, limit, after, filters = []} = args;

    if (!!qs && (filters.length === 0 || filters[0] === "hasVocabulary:*")) {
      const remoteConcepts = await geonamesClient.searchConcepts({
        qs: qs.replace(/^\^(.+)\.\*$/, "$1"),  // This is a simple regex to remove polluting boolean_prefix qs. Ex : ^suède.*
        lang: synaptixSession.getLang()
      });

      localConceptsCollection.appendObjects(remoteConcepts);
    }

    return synaptixSession.wrapObjectsIntoGraphQLConnection(localConceptsCollection, {
      limit,
      after
    });
  }
}
