import merge from "lodash/merge";
// import {SynaptixClientToolkitFrLocale} from "@synaptix/ui";
import {fr as SynaptixClientToolkitFrLocale} from "@mnemotix/synaptix-client-toolkit/lib/locales/fr";
import {locales as konceptLocales} from "@mnemotix/koncept-core/locales";

import {frLocale} from "@synaptix/ui/locales";

export const fr = merge({}, SynaptixClientToolkitFrLocale, konceptLocales.fr, frLocale, {
  ACCESS_TARGET: {
    ADD: "Ajouter un utilisateur",
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner un utilisateur...",
      NO_RESULT: "Aucun utilisateur trouvé."
    },
    CATEGORY: {
      USERACCOUNT: "Utilisateur",
      USERGROUP: "Groupe d'utilisateurs"
    },
    USER_ACCOUNT: {
      OWNER: "Propriétaire"
    },
    USER_GROUP: {
      GLOBAL_ROLE: "Rôle global"
    }
  },
  ACTIONS: {
    ADD: "Ajouter",
    ADD_AGENT: "Ajouter un acteur",
    ADD_ARTWORK: "Ajouter une œuvre",
    ADD_EXHBITION: "Ajouter une manifestation",
    ADD_IMAGES: "Ajouter des images",
    ADD_IMAGE: "Ajouter une image",
    ADD_MEMO: "Ajouter un mémo",
    ADD_ORGANIZATION: "Ajouter une organisation",
    ADD_SUB_PROJECT: "Ajouter un sous-projet",
    ADD_PARENT_PROJECT: "Ajouter un projet parent",
    ADD_PERSON: "Ajouter une personne",
    ASK_URL: "Renseigner l'URL du lien",
    BATCH_UPDATE: "Modifier en lot",
    BATCH_UPDATE_plural: "Modifier en lot ({{count}})",
    CANCEL: "Annuler",
    CHANGE_IMAGE: "Modifier l'image",
    CHANGE: "Modifier",
    CLOSE: "Fermer",
    DELETE: "Effacer",
    DOWNLOAD: "Télécharger",
    EDIT: "Editer",
    GO_BACK: "Retour",
    LOAD_DATA: "Charger les données",
    LOADING_DATA: "Chargement des données",
    FROM_TO: " du {{from}} au {{to}}",
    ONE_VALUE_ONLY: "Une seule valeur possible",
    NO_RESULT: "Aucun résultat ne correspond à votre recherche",
    NONE: "Aucun",
    PROCEED: "Valider",
    REMOVE_IMAGE: "Supprimer",
    SAVE: "Enregistrer",
    SUCCESS: "Enregistré avec succès",
    UPDATE: "Modifier",
    LOADING: "Chargement...",
    NEXT: "Suivante",
    PREVIOUS: "Précédente",
    RICH_TEXT_EDITOR: {
      DELETE_ALL: "Effacer tout le contenu",
      MENTION_BUTTON: {
        AGENT: "Mentionner un acteur",
        PROJECT: "Mentionner un projet",
        PROJECTOUTPUT: "Mentionner un object artistique",
        CONCEPT: "Taguer avec un concept"
      }
    },
    NOT_ALLOWED: "Vous n'avez pas accès à cette fonctionnalité.",
    VALIDATE: "Valider",
    CONFIRM_LIVE_FORM: "Certaines modifications n'ont pas été enregistrées, quitter quand même ?",
    ZOOM_IN: "Zoomer",
    ZOOM_OUT: "Dézoomer",
    ZOOM_RESET: "Réinitialiser",
    SEE_GRAPH: "Voir le graphe"
  },
  ACTIVITY: {
    GLOBAL_TITLE: "Activité générale de l'application",
    ENTITY_ACTIVITY: "Flux d'activité",
    TYPE: {
      DELETION: "Suppression",
      UPDATE: "Mise à jour",
      CREATION: "Création"
    },
    TITLE: {
      DELETION: "Suppression d'un élément",
      DELETION_plural: "Suppression de {{count}} éléments",
      UPDATE: "Mise à jour d'un élément",
      UPDATE_plural: "Mise à jour de {{count}} éléments",
      CREATION: "Création d'un élément",
      CREATION_plural: "Création de {{count}} éléments"
    },
    ACTION: {
      CREATION: {
        PERSON: "<person>{{person}}</person> a crée la personne <entity>{{label}}</entity>",
        PERSON_SELF_CREATED: "La personne et son compte utilisateur associé ont été créés",
        USER_GROUP: "<person>{{person}}</person> a crée le groupe d'utilisateurs <entity>{{label}}</entity>",
        USER_ACCOUNT: "Un nouveau compte utilisateur associé à la personne <entity>{{label}}</entity> a été créé",
        FILE: "<person>{{person}}</person> a importé le fichier <entity>{{label}}</entity>",
        PROJECT_CONTRIBUTION: "<person>{{person}}</person> a crée l'événement <entity>{{label}}</entity>",
        PROJECT_CONTRIBUTION_WITH_PROJECT:
          "<person>{{person}}</person> a crée l'événement <entity>{{label}}</entity> dans le projet <project>{{projectTitle}}</project>",
        PROJECT: "<person>{{person}}</person> a crée le projet <entity>{{label}}</entity>",
        ORGANIZATION: "<person>{{person}}</person> a crée l'organisation <entity>{{label}}</entity>",
        CONCEPT: "<person>{{person}}</person> a crée le concept <entity>{{label}}</entity>"
      },
      UPDATE: {
        PERSON: "<person>{{person}}</person> a mis à jour la personne <entity>{{label}}</entity>",
        USER_GROUP: "<person>{{person}}</person> a mis à jour le groupe d'utilisateurs <entity>{{label}}</entity>",
        FILE: "<person>{{person}}</person> a mis à jour le fichier <entity>{{label}}</entity>",
        PROJECT_CONTRIBUTION:
          "<person>{{person}}</person> a mis à jour l'événement <entity>{{label}}</entity> dans le projet <project>{{projectTitle}}</project>",
        PROJECT: "<person>{{person}}</person> a mis à jour le projet <entity>{{label}}</entity>",
        ORGANIZATION: "<person>{{person}}</person> a mis à jour l'organisation <entity>{{label}}</entity>"
      },
      DELETION: {
        PERSON: "<person>{{person}}</person> a supprimé une personne.",
        USER_GROUP: "<person>{{person}}</person> a supprimé un groupe d'utilisateurs.",
        USER_ACCOUNT: "<person>{{person}}</person> a supprimé un compte utilisateur.",
        FILE: "S<person>{{person}}</person> a supprimé un fichier.",
        PROJECT_CONTRIBUTION: "<person>{{person}}</person> a supprimé un événement.",
        PROJECT: "<person>{{person}}</person> a supprimé un projet.",
        ORGANIZATION: "<person>{{person}}</person> a supprimé une organisation."
      }
    },
    ENTITY: {
      FIELDS_UPDATE_plural: "Les champs <fields>{{fields}}</fields> ont été édités.",
      FIELDS_UPDATE: "Le champ <fields>{{fields}}</fields> a été édité.",
      LINKS: {
        ADDED: "Un lien <linkedComponent/> a été ajouté.",
        ADDED_plural: "Plusieurs liens <linkedComponent/> ont été ajoutés.",
        REMOVED: "Un lien <linkedComponent/> a été supprimé.",
        REMOVED_plural: "Plusieurs liens <linkedComponent/> ont été supprimé."
      },
      ACCESS_POLICY: {
        ADDED: "La confidentialité a été réglée sur <linkedComponent />."
      },
      READ_ONLY_ACCESS_TARGETS: {
        ADDED: "Le niveau d'accès en lecture seule a été ajouté pour <linkedComponent />.",
        REMOVED: "Le niveau d'accès en lecture seule a été retiré pour <linkedComponent />."
      },
      READ_WRITE_ACCESS_TARGETS: {
        ADDED: "Le niveau d'accès en modification a été ajouté pour <linkedComponent />.",
        REMOVED: "Le niveau d'accès en modification a été retiré pour <linkedComponent />."
      },
      TAGGINGS: {
        ADDED: "Le concept <linkedComponent/> a été taggué.",
        ADDED_plural: "Les concepts <linkedComponent/> ont été taggués.",
        REMOVED: "Le concept <linkedComponent/> a été retiré.",
        REMOVED_plural: "Les concepts <linkedComponent/> ont été retirés."
      }
    }
  },
  ADMIN: {
    USERS: {
      TITLE: "Comptes et utilisateurs"
    },
    MENU: {
      LABEL: "Administrateur",
      NEW_USER: "Nouvel utilisateur",
      USERS: "Utilisateurs",
      NEW_GROUP: "Nouveau groupe",
      GROUPS: "Groupes et rôles",
      ACTIVITY: "Activité"
    },
    GROUPS: {
      GLOBAL_ROLES: {
        TITLE: "Gestion des rôles",
        DESCRIPTION: "Groupes d'utilisateurs disposant de droits globaux sur les fontionnalités de Weever."
      },
      SPECIFIC_GROUPS: {
        TITLE: "Gestion des groupes d'utilisateurs",
        DESCRIPTION:
          "Utilisés dans les paramètres de projets pour donner des droits spécifiques à des groupes d'utilisateurs"
      },
      CREATE: "Créer un groupe"
    },
    GROUP: {
      NAME: "Nom du groupe",
      DESCRIPTION: "Description",
      MEMBERS: "Membres du groupe",
      ADD: "Ajouter un membre",
      LINKEDIT_ADD_LABEL: "Ajouter un membre au groupe",
      LINKEDIT_ADD_SUBLABEL: "Sélectionner un ou des membres à rajouter au groupe",
      INFORMATIVE_GROUP_MESSAGE:
        "Ce groupe d'utilisateurs est purement informatif. Les utilisateurs de ce groupe sont gérés de façon automatique et ne peuvent être modifiés dans cette interface.",
      REMOVE: {
        TITLE: "Supprimer le groupe",
        WARN_MESSAGE: "Attention, cette action est irrévocable.",
        CONFIRM_MESSAGE: 'Êtes-vous absolument sûr de vouloir supprimer le groupe "{{group}}"?'
      }
    },
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner un membre...",
      CHOOSE_ANOTHER: "Sélectionner un autre...",
      NO_RESULT: "Aucun membre trouvé."
    }
  },
  AGENT: {
    ADDRESS: {
      LABELS: {
        BIRTH: "Lieu de naissance",
        HOME: "Domicile",
        WORK: "Travail",
        OTHER: "Autre"
      },
      NEW: "Ajouter une adresse",
      TYPE: "Type"
    },
    ADDRESSES: "Adresse(s)",
    ADDRESSES_LABELS: {
      BIRTH: "Lieu de naissance",
      CITY: "Ville",
      COUNTRY: "Pays",
      HOME: "Lieu de vie",
      OTHER: "Autre lieu",
      POSTALCODE: "Code postal",
      STREET1: "Rue",
      STREET2: "Complément d'adresse",
      WORK: "Lieu de travail",
      EMAIL: "Email"
    },
    AFFILIATION: {
      ORGANIZATION: {
        ADD: "Ajouter une organisation"
      },
      ROLE: "Role",
      START_DATE: "Date de début",
      END_DATE: "Date de fin",
      AGENT_TYPE: "Type d'acteur"
    },
    AFFILIATION_NO_ROLE: "Rôle non précisé",
    AVATAR: "Avatar",
    CREATE: "Nouvel agent",
    CREATED_AT_BY: "Créé le {{date}} par ",
    EMAILS: "Courriel(s)",
    EMAIL_LABEL: "Type",
    EMAILS_LABELS: {
      MAIN: "Principal",
      OTHER: "Autre",
      PRIVATE: "Privé",
      PERSO: "Personnel"
    },
    EMAIL_NEW: "Ajouter un courriel",
    NEW_ADDRESS: "Nouvelle adresse",
    NO_ADDRESS: "Aucune",
    NO_AFFILIATION: "Aucune",
    NO_EMAILS: "Aucun",
    NO_PHONE: "Aucun",
    PHONE_LABEL: "Type",
    PHONE_NEW: "Ajouter un téléphone",
    PHONE_NUMBER: "Numéro de téléphone",
    PHONES: "Téléphone(s)",
    PHONES_LABELS: {
      FIX: "Fixe",
      MOB: "Mobile",
      OTHER: "Autre"
    },
    SUB_LABEL: "Sélectionner un ou des agents à mentionner"
  },
  APOLLO: {
    ERROR: {
      USER_NOT_ALLOWED: "Vous n'avez pas les droits pour accéder à cette page",
      MALFORMED_REQUEST: "Une erreur s'est produite.",
      SERVER_ERROR: "Une erreur s'est produite.",
      USER_NOT_EXISTS_IN_GRAPH: "Votre compte est désynchronisé, contactez un administrateur."
    }
  },
  APP_BAR: {
    HOME: "Accueil",
    BUTTONS: {
      BIN: "Chutier",
      EDIT: "Éditer",
      IMPORT: "Importer",
      KONCEPT: "Thésaurus"
    },
    MENU: {
      HELLO: "Bonjour",
      MY_PROFILE: "Mon profil",
      SIGN_OUT: "Se déconnecter",
      VISUALIZATIONS: {
        TITLE: "Visualisations",
        GLOBAL_GRAPH_ITEM: "Graphe global"
      }
    }
  },
  ARTISTIC_OBJECT: {
    AUTHORS: "Acteur(s) associé(s)",
    COLLECTION: "Collection",
    COURTESY: "Courtesy",
    CREATE: "Nouvel objet artistique",
    CREDITS: "Crédits",
    DESCRIPTION: "Description",
    DIMENSIONS: "Dimensions",
    IMAGES: "Images",
    NEW_AUTHOR: "Ajouter un acteur",
    NO_AUTHORS: "Aucun",
    NO_COLLECTION: "Aucune",
    NO_COURTESY: "Aucune",
    NO_CREDITS: "Aucun",
    NO_DESCRIPTION: "Aucune",
    NO_DIMENSIONS: "Aucune",
    NO_IMAGE: "Aucune image",
    NO_PROJECT: "Aucun",
    NO_REPRESENTATION: "Aucune",
    NO_TECHNICAL_INFO: "Aucune",
    NO_YEAR: "Aucune",
    PROJECTS: "Projets associés",
    REPRESENTATIONS: "Œuvres ou manifestations associées",
    SHORT_DESCRIPTION: "Description résumée",
    TECHNICAL_INFO: "Informations techniques",
    TITLE: "Titre",
    YEAR: "Année"
  },
  ARTWORK: {
    CREATE: "Nouvelle œuvre",
    NEW: "Ajouter une œuvre",
    SUB_LABEL: "Sélectionner une ou des œuvres à associer",
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner une œuvre...",
      CHOOSE_ANOTHER: "Sélectionner une autre ...",
      NO_RESULT: "Aucune œuvre trouvée."
    }
  },
  AUTOCOMPLETE: {
    ADD: "Ajouter {{value}}",
    PENDING_CREATION: "{{value}} (Nouveau)"
  },
  BIN: {
    ACTIONS: {
      LINK_TO_NEW_PROJECT_CONTRIBUTION: {
        DESCRIPTION: "Vous allez lier ces ressources à un nouvel évenement du projet {{project}}.",
        SUCCESS: "Les ressources ont été liées avec succès",
        TITLE: "Créer un événement"
      },
      LINK_TO_PROJECT: {
        DESCRIPTION: "Vous êtes sur le point de réattribuer un ou plusieurs éléments à un nouveau projet.",
        SUCCESS: "Les ressources ont été liées avec succès",
        TITLE: "Lier à un projet"
      },
      LINK_TO_PROJECT_CONTRIBUTION: {
        DESCRIPTION: "Vous allez lier ces ressources à un évenement du projet {{project}}.",
        SUCCESS: "Les ressources ont été liées avec succès",
        TITLE: "Lier à un événement"
      }
    },
    EMPTY_DESCRIPTION: "Le chutier est vide, excellent travail !",
    EMPTY_TITLE: "Bravo !"
  },
  BREADCRUMB: {
    ARTWORKS: "Liste des œuvres",
    EXHIBITIONS: "Liste des manifestations",
    GROUPS: "Gestion des rôles et groupes d'utilisateurs",
    ORGANIZATIONS: "Liste des organisations",
    PERSONS: "Liste des personnes",
    PROJECTS: "Liste des projets",
    RESOURCES: "Liste des ressources"
  },
  CONCEPT: {
    AUTOCOMPLETE: {
      NO_RESULT: "Aucun concept trouvé.",
      NOT_CLASSIFIED: "En vrac"
    }
  },
  CONFIRMATION_MODAL: {
    NO: "Non",
    YES: "Oui"
  },
  ENTITY: {
    CREATED_AT_BY: "Créé le {{date}} par ",
    EXTERNAL_LINK_NEW: "Ajouter un lien externe",
    EXTERNAL_LINKS_LABELS: {
      FACEBOOK: "Facebook",
      INSTAGRAM: "Instagram",
      LINKEDIN: "LinkedIn",
      OTH: "Autre site web",
      OTHER: "Autre site web",
      TWITTER: "Twitter",
      WEBSITE: "Site web"
    },
    LABEL_TYPE: "Type",
    LINK: "Lien",
    EXTERNAL_LINKS: "Lien(s) externe(s)",
    NEW_LINK: "Nouveau lien",
    NO_EXTERNAL_LINK: "Aucun",
    TAGGINGS: "Tags",
    NEW_TAGGING: "Nouveau tag",
    NO_TAGGING: "Aucun",
    ADD_TAGGING: "Ajouter un tag",
    ACCESS_POLICY: "Confidentialité",
    NO_ACCESS_POLICY: "Aucune",
    ACTIONS: {
      BATCH_UPDATE: {
        TITLE: "Modification d'un élément",
        TITLE_plural: "Modification en lot de {{count}} éléments",
        CONFIRM: "Êtes-vous sûr de vouloir modifier en lot cet élément",
        CONFIRM_plural: "Êtes-vous sûr de vouloir modifier en lot {{count}} éléments",
        SUCCESS: "Modification effectuée avec succès"
      }
    }
  },
  EXHIBITION: {
    CREATE: "Nouvelle manifestation",
    NEW: "Ajouter une manifestation",
    SUB_LABEL: "Sélectionner une ou des manifestations à associer",
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner une manifestation...",
      CHOOSE_ANOTHER: "Sélectionner un autre...",
      NO_RESULT: "Aucune manifestation trouvée."
    }
  },
  FORM_ERRORS: {
    FIELD_ERRORS: {
      EMAIL_REQUIRED: "L'adresse de courriel doit être renseignée",
      EMAIL_ALREADY_REGISTERED: "Cette adresse de courriel est déjà utilisée",
      INVALID_EMAIL: "Le format de l'adresse de courriel est invalide",
      INVALID_DATE: "Le format de la date est invalide",
      INVALID_LINK: "Le format du lien est invalide",
      PASSWORD_REQUIRED: "Le mot de passe doit être renseigné",
      PASSWORD_CONFIRMATION_REQUIRED: "La confirmation de mot de passe doit être renseignée",
      PASSWORD_TOO_SHORT: "Le mot de passe trop court",
      PASSWORDS_DO_NOT_MATCH: "La confirmation ne correspond pas au nouveau mot de passe",
      REQUIRED: "Champ obligatoire",
      REQUIRED_S: "Champ(s) obligatoire(s)",
      TOO_SHORTS: "Champ(s) trop court(s)",
      WRONG_OLD_PASSWORD: "L'ancien mot de passe est incorrect",
      TITLE_REQUIRED: "Le titre doit être renseigné",
      DESCRIPTION_REQUIRED: "La description doit être renseignée",
      START_DATE_REQUIRED: "La date de début doit être renseignée",
      FIRST_NAME_REQUIRED: "Le prénom doit être renseigné",
      LAST_NAME_REQUIRED: "Le nom de famille doit être renseigné",
      LABEL_REQUIRED: "Le label doit être renseigné",
      ROLE_REQUIRED: "Le rôle doit être renseigné",
      AGENT_REQUIRED: "Une personne ou une organisation doit être renseignée",
      NUMBER_REQUIRED: "Le numéro doit être renseigné",
      LINK_REQUIRED: "Le lien doit être renseigné"
    },
    GENERAL_ERRORS: {
      DISABLED_BECAUSE_NO_MODIFICATION: "Aucune modification n'a été faite",
      FORM_VALIDATION_ERROR: "Certains champs sont invalides.",
      INVALID_CREDENTIALS: "Ces identifiants ne sont pas valides",
      UNEXPECTED_ERROR: "Un problème non identifié nous empêche d'effectuer cette action. Réessayez plus tard",
      USER_MUST_BE_AUTHENTICATED: "Vous devez être connecté avec un compte utilisateur pour effectuer cette action",
      USER_NOT_ALLOWED: "Vous n'avez pas la persmission d'effectuer cette action"
    }
  },
  IMPORT: {
    FEATURES_PROJECTS: "Derniers projets sélectionnés :",
    RESET_PROJECTS: "Oublier les derniers projets...",
    SELECT_PROJECTS: "Lier à un/des projets",
    SELECT_EVENT: "Lier à un événement",
    CREATE_EVENT: "Créer un nouvel événement",
    UPLOAD: "Importer le fichier",
    UPLOAD_plural: "Importer {{count}} fichiers",
    UPLOADING_MESSAGE: "Import en cours : {{percentage}}%",
    PAUSE_MESSAGE: "Import en pause ({{percentage}}%)",
    FILES_SELECTED_0: "Aucun fichier séléctionné",
    FILES_SELECTED: "{{count}} fichier séléctionné",
    FILES_SELECTED_plural: "{{count}} fichiers séléctionnés",
    SELECT_ALL_FILES: "Sélectionner tous les fichiers",
    UNSELECT_ALL_FILES: "Désélectionner",
    UPLOAD_GROUP_TITLE: "Groupe d'import pour l'événement {{event}}",
    UPLOAD_GROUP_TITLE_SUBTITLE: "{{count}} fichier",
    UPLOAD_GROUP_TITLE_SUBTITLE_plural: "{{count}} fichiers",
    UPLOAD_GROUP_SUCCESS: "Fichiers importés avec succès dans l'événement",
    UPLOAD_GROUP_SEE_PROJECT: "Voir dans l'événément",
    UPLOAD_GROUP_STATUS: {
      COMPLETE: "Terminé",
      PAUSE: "En pause",
      FINALIZING: "En cours de finalisation...",
      IMPORTING: "En cours d'import..."
    }
  },
  ORGANIZATION: {
    ACTIONS: {
      REMOVE: {
        TITLE: "Supprimer l'organisation",
        WARN_MESSAGE: "Attention, cette action est irrévocable.",
        CONFIRM_MESSAGE: 'Êtes-vous absolument sûr de vouloir supprimer l\'organisation "{{organization}}"?'
      },
      BATCH_UPDATE: {
        TITLE: "Modification d'une organisation",
        TITLE_plural: "Modification en lot de {{count}} organisations",
        CONFIRM: "Êtes-vous sûr de vouloir modifier en lot cette organisation",
        CONFIRM_plural: "Êtes-vous sûr de vouloir modifier en lot {{count}} organisations",
        SUCCESS: "Modification effectuée avec succès"
      }
    },
    ACTIVITY: {
      AFFILIATIONS: {
        ADDED: "Une affiliation à la personne <linkedComponent/> a été ajoutée.",
        ADDED_plural: "Des affiliations aux personnes <linkedComponent/> ont été ajoutée.",
        REMOVED: "L'affiliation à la personne <linkedComponent/> a été retirée.",
        REMOVED_plural: "Les affiliations aux personnes <linkedComponent/> ont été retirées."
      }
    },
    AFFILIATIONS: "Membre(s) de l'organisation",
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner une organisation...",
      CHOOSE_ANOTHER: "Sélectionner un autre...",
      NO_RESULT: "Aucune personne trouvée."
    },
    CREATE: "Nouvelle organisation",
    CREATE_TITLE: "Créer une nouvelle organisation",
    CREATED_AT_BY: "Créé le {{date}} par ",
    DESCRIPTION: "Description",
    NAME: "Nom",
    NEW: "Ajouter une organisation",
    NEW_ROLE: "Nouveau rôle",
    NO_DESCRIPTION: "Aucune",
    ORGANIZATION_HEADER: "Organisation(s) associée(s)",
    SUB_LABEL: "Sélectionner une ou des organisations à associer",
    SHORT_DESCRIPTION: "Description résumée",
    FIELD_ERRORS: {
      NAME_REQUIRED: "Le nom doit être renseigné"
    }
  },
  PERSON: {
    ACTIONS: {
      REMOVE: {
        TITLE: "Supprimer la personne",
        WARN_MESSAGE: "Attention, cette action est irrévocable.",
        CONFIRM_MESSAGE: 'Êtes-vous absolument sûr de vouloir supprimer la personne "{{person}}"?'
      },
      BATCH_UPDATE: {
        TITLE: "Modification d'une personne",
        TITLE_plural: "Modification en lot de {{count}} personnes",
        CONFIRM: "Êtes-vous sûr de vouloir modifier en lot cette personne",
        CONFIRM_plural: "Êtes-vous sûr de vouloir modifier en lot {{count}} personnes",
        SUCCESS: "Modification effectuée avec succès"
      }
    },
    ACTIVITY: {
      AFFILIATIONS: {
        ADDED: "Une affiliation à l'organisation <linkedComponent/> a été ajoutée.",
        ADDED_plural: "Des affiliations aux organisations <linkedComponent/> ont été ajoutée.",
        REMOVED: "L'affiliation à l'organisation <linkedComponent/> a été retirée.",
        REMOVED_plural: "Les affiliations aux l'organisations <linkedComponent/> ont été retirées."
      }
    },
    AFFILIATIONS: "Membre d'organisation(s)",
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner une personne...",
      CHOOSE_ANOTHER: "Sélectionner une autre...",
      NO_RESULT: "Aucune personne trouvée."
    },
    BIO: "Biographie",
    BORN_ON: "Né(e) le {{date}}",
    BORN: "Né(e) le",
    CREATE: "Nouvelle personne",
    CREATE_TITLE: "Créer une nouvelle personne",
    DELETE: {
      DELETE_DEFINITELY: "SUPPRIMER DEFINITIVEMENT LA PERSONNE",
      DONE_CONFIRMATION: "Personne supprimée avec succès",
      I_UNDERSTAND: "Supprimer"
    },
    FIRST_NAME: "Prénom",
    FULL_NAME: "Nom complet",
    LAST_NAME: "Nom",
    NEW: "Ajouter une personne",
    NEW_ROLE: "Nouveau rôle",
    NO_BIOGRAPHY: "Aucune",
    ROLE: "Rôle",
    SHORT_BIO: "Biographie résumée",
    ADMIN_PANEL: {
      TITLE: "Paramètres de compte utilisateur",
      ACCOUNT_CREATED: "Compte crée , rechargement automatique de la page !",
      ACCOUNT_DISABLED: "Compte désactivé",
      ACCOUNT_ENABLED: "Compte activé",
      CREATE_ACCOUNT: "Créer un compte",
      DELETED_ACCOUNT: "Cette personne à supprimé son compte utilisateur.",
      DONT_SEND_EMAIL_NOTIFICATION: "Ne pas envoyer des notifications par mails",
      SETTINGS: "Paramètres des emails.",
      GOT_ACCOUNT: "Cette personne dispose d'un compte utilisateur.",
      NO_ACCOUNT_YET: "Cette personne ne dispose pas encore de compte utilisateur.",
      NO_ROLE: "Aucun role",
      PWD_OR_EMAIL_TOO_SHORT: "Mot de passe ou email trop cour",
      ROLES: "Rôles",
      SEND_EMAIL_NOTIFICATION: "Envoyer des notifications par mails",
      PASSWORD: "Mot de passe"
    },
    FIELD_ERRORS: {
      LAST_NAME_REQUIRED: 'Au moins un des champs "Prénom" ou "Nom de famille" doit être renseigné',
      EMAIL_REQUIRED: "Au moins une adresse mail doit être renseignée"
    }
  },
  PROFIL: {
    EDIT_MY_PROFIL: "Edition de mon profil",
    MY_PERSONAL_INFO: "mes informations personnelles",
    MY_PASSWORD: "mon mot de passe",
    MY_ACCOUNT: "mon compte",
    CHANGE_PASSWORD: {
      OLD_PASSWORD: "Ancien mot de passe",
      NEW_PASSWORD: "Nouveau mot de passe",
      CONFIRM_NEW_PASSWORD: "Confirmation du nouveau mot de passe",
      DONE_CONFIRMATION: "Mot de passe modifié avec succès"
    },
    DELETE_ACCOUNT: {
      DELETE_DEFINITELY: "SUPPRIMER LE COMPTE DEFINITIVEMENT",
      DONE_CONFIRMATION: "Compte supprimé avec succès",
      I_UNDERSTAND: "Je comprends les consequences, supprimer mon compte.",
      WARN_MESSAGE: "Cette action est irrémédiable et entraine la suppression de toutes les données.",
      PLEASE_INPUT_A: "Veuillez saisir",
      PLEASE_INPUT_B: "pour confirmer",
      CHECKSTRING: "SUPPRIMER"
    },
    ACCESS_PRIVILEDGE_ADMIN: 'Vous avez un accès "Administrateur"',
    ACCESS_PRIVILEDGE_EDITOR: 'Vous avez un accès "Éditeur"',
    ACCESS_PRIVILEDGE_CONTRIBUTOR: 'Vous avez un accès "Contributeur"'
  },
  PROJECT: {
    ACTIONS: {
      ACCESS_RIGHTS: {
        TITLE: "Droits d'accès au projet",
        READ_ONLY_TITLE: "Lecture seule",
        READ_WRITE_TITLE: "Modification"
      },
      REMOVE: {
        TITLE: "Supprimer le projet",
        WARN_MESSAGE: "Attention, cette action est irrévocable.",
        CONFIRM_MESSAGE: 'Êtes-vous absolument sûr de vouloir supprimer le projet "{{project}}"?'
      },
      BATCH_UPDATE: {
        TITLE: "Modification d'un projet",
        TITLE_plural: "Modification en lot de {{count}} projets",
        CONFIRM: "Êtes-vous sûr de vouloir modifier en lot ce projet",
        CONFIRM_plural: "Êtes-vous sûr de vouloir modifier en lot {{count}} projets",
        SUCCESS: "Modification effectuée avec succès"
      }
    },
    ARTISTS: "Artistes",
    ARTWORKS_HEADER: "Œuvres associées",
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner un projet...",
      CHOOSE_ANOTHER: "Sélectionner un autre projet...",
      NO_RESULT: "Aucun projet trouvé."
    },
    COLOR: "Couleur",
    CREATE: "Nouveau projet",
    CREATE_TITLE: "Créer un nouveau projet",
    CREATED_AT_BY: "Créé le {{date}} par ",
    DESCRIPTION: "Description",
    END_DATE: "Date d'archivage",
    END_DATE_TOOLTIP: "Date du dernier événement du projet",
    EVENTS: {
      COLUMNS: {
        END_DATE: "Date de fin",
        MEMO: "Mémo",
        MEMOS: "Mémos",
        RESOURCES: "Ressources",
        START_DATE: "Date de début",
        TITLE: "Titre"
      }
    },
    PROJECT_CONTRIBUTIONS: "Événement(s)",
    EXHBITIONS_HEADER: "Manifestations associées",
    GATHERED_AGENTS: "Acteurs recensés dans les événements",
    IMAGE: "Image",
    INVOLVEMENT_NO_ROLE: "Rôle non précisé",
    INVOLVEMENTS: "Acteurs du projet",
    NEW: "Ajouter un projet",
    NEW_PARENT: "Ajouter un projet parent",
    NO_DESCRIPTION: "Aucune",
    NO_INVOLVEMENT: "Aucun",
    NO_RELATED_ARTWORKS: "Aucune œuvre",
    NO_RELATED_EXHIBITION: "Aucune manifestion",
    NO_RELATED_SUBPROJECTS: "Aucun sous-projet",
    SHORT_DESCRIPTION: "Description résumée",
    START_DATE: "Date de début",
    START_DATE_TOOLTIP: "Date du premier événement du projet",
    SUB_LABEL: "Sélectionner un ou des projets à associer",
    SUB_PROJECTS: "Sous-projets",
    NO_PARENT_PROJECT: "Aucun",
    PARENT_PROJECT: "Projet parent",
    TITLE: "Titre",
    STATUS: "Statut",
    STATE: {
      RUNNING: "En cours",
      CLOSED: "Archivé",
      CLOSED_AT: "Archivé le {{date}}"
    },
    ACTIVITY: {
      PROJECT_CONTRIBUTIONS: {
        ADDED: "L'événement <linkedComponent/> a été ajouté.",
        ADDED_plural: "Les événements <linkedComponent/> ont été ajouté.",
        REMOVED: "L'événement <linkedComponent/> a été retiré.",
        REMOVED_plural: "Les événements <linkedComponent/> ont été retirés."
      },
      SUB_PROJECT: {
        ADDED: "Le sous-projet <linkedComponent/> a été ajouté.",
        ADDED_plural: "Les sous-projets <linkedComponent/> ont été ajouté.",
        REMOVED: "Le sous-projet <linkedComponent/> a été retiré.",
        REMOVED_plural: "Les sous-projets <linkedComponent/> ont été retirés."
      },
      PARENT_PROJECT: {
        ADDED: "Le projet parent <linkedComponent/> a été ajouté.",
        REMOVED: "Le projet parent <linkedComponent/> a été retiré."
      },
      INVOLVEMENTS: {
        ADDED: "L'acteur <linkedComponent/> a été ajouté.",
        ADDED_plural: "Les acteurs <linkedComponent/> ont été ajoutés.",
        REMOVED: "L'acteur <linkedComponent/> a été retiré.",
        REMOVED_plural: "Les acteurs <linkedComponent/> ont été retirés."
      }
    }
  },
  PROJECT_CONTRIBUTION: {
    AUTOCOMPLETE: {
      CHOOSE: "Sélectionner un événement...",
      CHOOSE_ANOTHER: "Sélectionner un autre événement...",
      NO_RESULT: "Aucun événément trouvé."
    },
    ACTIVITY: {
      ATTACHMENTS: {
        ADDED: "La ressource <linkedComponent/> a été ajoutée.",
        ADDED_plural: "Les ressources <linkedComponent/> ont été ajoutées.",
        REMOVED: "La ressource <linkedComponent/> a été retirée.",
        REMOVED_plural: "Les ressources <linkedComponent/> ont été retirées."
      },
      INVOLVEMENTS: {
        ADDED: "L'acteur <linkedComponent/> a été ajouté.",
        ADDED_plural: "Les acteurs <linkedComponent/> ont été ajoutés.",
        REMOVED: "L'acteur <linkedComponent/> a été retiré.",
        REMOVED_plural: "Les acteurs <linkedComponent/> ont été retirés."
      },
      PROJECTS: {
        ADDED: "L'événement a été attaché au projet <linkedComponent/>.",
        ADDED_plural: "L'événement a été attaché aux projets <linkedComponent/>.",
        REMOVED: "L'événement a été retiré au projet <linkedComponent/>.",
        REMOVED_plural: "L'événement a été retiré des projets <linkedComponent/>."
      },
      REPLIES: {
        ADDED: "Le mémo <linkedComponent/> a été ajouté.",
        ADDED_plural: "Les mémos <linkedComponent/> ont été ajoutés.",
        REMOVED: "Le mémo <linkedComponent/> a été retiré.",
        REMOVED_plural: "Les mémos <linkedComponent/> ont été retirés."
      }
    },
    BY: "par",
    DESCRIPTION: "Description",
    CREATE: "Nouvel événement",
    CREATED_AT: "Créé le",
    CREATED_AT_BY: "Créé le {{date}} par :",
    END_DATE: "Date de fin",
    INVOLVEMENTS: "Acteurs impliqués",
    ATTACHMENTS: "Fichier(s) attaché(s)",
    MEMOS: {
      CREATE: "Nouveau mémo",
      COLUMNS: {
        CONTENT: "Contenu"
      }
    },
    NO_DATE: "Aucune",
    NO_RELATED_INVOLVEMENTS: "Aucun",
    RESOURCES: {
      COLUMNS: {
        END_DATE: "Date de fin",
        MEMO: "Mémo",
        RESOURCES: "Ressources",
        START_DATE: "Date début",
        TITLE: "Titre"
      },
      AND_MORE: "+{{count}}"
    },
    START_DATE: "Date de début",
    TABS: {
      RESOURCES: "Ressources",
      MEMOS: "Mémos"
    },
    ACTIONS: {
      BATCH_UPDATE: {
        TITLE: "Modification d'un événement",
        TITLE_plural: "Modification en lot de {{count}} événements",
        CONFIRM: "Êtes-vous sûr de vouloir modifier en lot cet événement",
        CONFIRM_plural: "Êtes-vous sûr de vouloir modifier en lot {{count}} événements",
        SUCCESS: "L'événement a été modifié avec succès",
        SUCCESS_plural: "Les {{count}} événements ont été modifiés avec succès"
      }
    }
  },
  EXPLORE: {
    PROJECT: {
      PARENTPROJECT: "Projet parent",
      SIBLINGPROJECT: "Project frère",
      SUBPROJECT: "Sous projet",
      SEE_ALSO: {
        ASSOCIATED_PROJECTS: "Projets associés...",
        ASSOCIATED_AGENTS: "Acteurs associés..."
      }
    }
  },
  REMOTE_TABLE: {
    ACTIONS: {
      FILTERS: "Filtres",
      REMOVE: "Supprimer",
      REMOVE_CONFIRM_TEXT: "Êtes-vous sûr de vouloir supprimer cet objet ?",
      REMOVE_CONFIRM_TEXT_plural: "Êtes-vous sûr de vouloir supprimer ces {{count}} objets ?",
      REMOVE_plural: "Supprimer ({{count}})",
      REMOVE_SUCCESS: "Élements supprimés avec succès",
      SELECT_ALL_MESSAGE: "Attention, seuls les {{count}} éléments de la page sont sélectionnés.",
      SELECT_ALL_WIDE_MESSAGE: "{{count}} éléments sélectionnés",
      SELECT_ALL_WIDE: "Sélectionner l'ensemble des {{count}} éléments",
      DESELECT_ALL: "Effacer la sélection"
    },
    BODY: {
      NO_MATCH: "Aucun résultat trouvé...",
      TOOLTIP: "Sort"
    },
    FILTER: {
      ALL: "Tout",
      RESET: "Réinitialiser",
      TITLE: "Filtres"
    },
    PAGINATION: {
      DISPLAY_ROWS: "de",
      NEXT: "Page suivante",
      PREVIOUS: "Page précédente",
      ROWS_PER_PAGE: "Lignes par page:"
    },
    SELECTED_ROWS: {
      DELETE: "Supprimer",
      DELETE_ARIA: "Supprimer les colonnes sélectionnées",
      TEXT: "colonne(s) sélectionnée(s)"
    },
    TOOLBAR: {
      DOWNLOAD_CSV: "Télécharger CSV",
      FILTER_TABLE: "Filtrer",
      PRINT: "Imprimer",
      SEARCH: "Rechercher",
      VIEW_COLUMNS: "Colonnes visibles"
    },
    VIEW_COLUMNS: {
      TITLE: "Colonnes visibles",
      TITLE_ARIA: "Afficher/Cacher les colonnes du tableau"
    }
  },
  RESET_PASSWORD: {
    CONFIRM: "Envoyer le mail",
    CONFIRMATION: "Email envoyé, verifiez également votre dossier SPAM",
    CHECK_SPAM: "Verifier également votre dossier SPAM",
    SEND_RESET_TO_EMAIL: "Reinitialiser le mot de passe via le mail"
  },
  RESOURCE: {
    ACTIONS: {
      REMOVE: {
        TITLE: "Supprimer la ressource",
        WARN_MESSAGE: "Attention, cette action est irrévocable.",
        CONFIRM_MESSAGE: 'Êtes-vous absolument sûr de vouloir supprimer la ressource "{{resource}}"?'
      },
      BATCH_UPDATE: {
        TITLE: "Modification d'une ressource",
        TITLE_plural: "Modification en lot de {{count}} ressources",
        CONFIRM: "Êtes-vous sûr de vouloir modifier en lot cette ressource",
        CONFIRM_plural: "Êtes-vous sûr de vouloir modifier en lot {{count}} ressources",
        SUCCESS: "La ressource a été modifiée avec succès",
        SUCCESS_plural: "Les {{count}} ressources ont été modifiées avec succès"
      }
    },
    CREATED_AT_BY: "Versée le {{date}} par ",
    CREDITS: "Crédits",
    DESCRIPTION: "Légende",
    FILE_NAME: "Nom du fichier",
    IS_NEW: "Nouveau",
    MIME: "Type",
    MORE_DETAILS: "Détails de la ressource",
    NO_CREDITS: "Aucun",
    NO_DESCRIPTION: "Aucune",
    SHOW_CAROUSEL: "Afficher le carrousel d'image",
    SIZE: "Taille",
    ATTACH: "Attacher des ressources",
    UPLOAD: "Verser des fichiers",
    VIEWER: {
      NO_HANDLER_MESSAGE: "Cette ressource n'est pas visualisable.",
      NO_HANDLER_LINK_MESSAGE: "Télécharger le fichier et l'ouvrir sur votre ordinateur."
    },
    URL: "Lien de téléchargement"
  },
  SEARCH: {
    COLUMNS: {
      ARTISTIC_OBJECTS: {
        PICTURES_COUNT: "Images associées"
      },
      COMMON: {
        IMAGE: "Image",
        CREATED_AT: "Créé le",
        CREATED_BY: "Créé par",
        END_AT: "Fin",
        IS_PARENT_PROJECT: "Projet racine",
        PRIVACY: "Confidentialité",
        PROJECT: "Projet",
        START_AT: "Début",
        TITLE: "Titre",
        UPDATED_AT: "Modifié le",
        TAGS: "Tags"
      },
      FILES: {
        MIME: "Type",
        SIZE: "Taille"
      },
      ORGANIZATIONS: {
        NAME: "Nom",
        SHORT_NAME: "Nom court"
      },
      PERSONS: {
        FULL_NAME: "Nom complet",
        FIRST_NAME: "Prénom",
        LAST_NAME: "Nom"
      },
      PROJECTS: {
        GATHER_SUB_PROJECTS: "Regrouper les sous-projets",
        STATE: "État"
      }
    },
    TABS: {
      ARTWORKS: "Œuvres",
      EXHIBITIONS: "Manifestations",
      ORGANIZATIONS: "Organisations",
      PERSONS: "Personnes",
      PROJECTS: "Projets",
      RESOURCES: "Ressources"
    }
  },
  SIGN_IN: {
    EMAIL: "Adresse mail",
    PASSWORD: "Mot de passe",
    PASSWORD_FORGOTTEN: "Mot de passe oublié",
    REDIRECT_SIGN_UP: "Créer un compte",
    TITLE: "Se connecter",
    PUBLIC_MODE_ACCESS: "Explorer les données ouvertes"
  },
  SIGN_UP: {
    EMAIL: "Adresse mail",
    FIRST_NAME: "Prénom",
    LAST_NAME: "Nom",
    PASSWORD: "Mot de passe",
    REDIRECT_SIGN_IN: "Vous avez déjà un compte ? Se connecter.",
    SUBMIT: "Créer le compte",
    TITLE: "Créer un compte",
    VALIDATE_PASSWORD: "Confirmation du mot de passe"
  },
  TRANSLATE: {
    COPY_FROM_OTHER_LANG: "Copier la valeur de l'autre langue",
    NOT_EXISTS: "La traduction est manquante pour cette langue"
  },
  TYPENAME: {
    ARTWORK: "Œuvre",
    EXHIBITION: "Manifestation",
    ORGANIZATION: "Organisation",
    PERSON: "Personne",
    PROJECT: "Projet",
    RESOURCE: "Ressource",
    CONCEPT: "Concept",
    PROJECT_CONTRIBUTION: "Événement"
  },
  UPLOAD: {
    ADD_MORE: "Télécharger d'autres fichiers",
    POSTPROCESS_ERROR:
      "Une erreur s'est produite au niveau du traitement serveur, merci de réessayer et si le problème persiste contacter un administrateur.",
    PROCESSING: "Traitement sur le serveur",
    PROCESSING_COUNT: "Traitement sur le serveur ({{count}})",
    SHOW_UPLOADED_IN_BIN: "Voir la ressource importée dans le chutier",
    SHOW_UPLOADED_IN_BIN_plural: "Voir les {{count}} ressources importées dans le chutier",
    START: "Démarrer l'import",
    SUCCESS_DETAILS: "(Retrouvez cette ressource dans le chutier)",
    SUCCESS_DETAILS_plural: "(Retrouvez ces {{count}} ressources dans le chutier)",
    SUCCESS_MESSAGE: "Le téléchargement s'est bien déroulé."
  },
});
