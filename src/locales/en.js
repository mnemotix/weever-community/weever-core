import merge from "lodash/merge";
import {locales as konceptLocales} from "@mnemotix/koncept-core/locales";

export const en = merge({}, konceptLocales.en, {
  ACCESS_TARGET: {
    ADD: "Add a user",
    AUTOCOMPLETE: {
      CHOOSE: "Select a user...",
      NO_RESULT: "No user found."
    },
    CATEGORY: {
      USERACCOUNT: "User",
      USERGROUP: "Group of users"
    },
    USER_ACCOUNT: {
      OWNER: "Owner"
    },
    USER_GROUP: {
      GLOBAL_ROLE: "Global role"
    }
  },
  ACTIONS: {
    ADD: "Add",
    ADD_AGENT: "Add an actor",
    ADD_ARTWORK: "Add an artwork",
    ADD_IMAGES: "Add images",
    ADD_IMAGE: "Add image",
    ADD_EXHBITION: "Add an exhbition",
    ADD_MEMO: "Add memo",
    ADD_ORGANIZATION: "Add an organization",
    ADD_SUB_PROJECT: "Add a child project",
    ADD_PARENT_PROJECT: "Add a parent project",
    ADD_PERSON: "Add a person",
    ASK_URL: "Enter link URL",
    BATCH_UPDATE: "Batch update",
    CANCEL: "Cancel",
    CHANGE_IMAGE: "Change image",
    CHANGE: "Change",
    CLOSE: "Close",
    DELETE: "Delete",
    EDIT: "Edit",
    GO_BACK: "Go back",
    LOAD_DATA: "Load data",
    LOADING_DATA: "Loading data",
    FROM_TO: " from {{from}} to {{to}}",
    ONE_VALUE_ONLY: "One value only",
    NO_RESULT: "No result match your search",
    PROCEED: "Proceed",
    REMOVE_IMAGE: "Remove",
    SAVE: "Save",
    SUCCESS: "Successfully saved",
    UPDATE: "Update",
    DOWNLOAD: "Download",
    LOADING: "Loading...",
    NEXT: "Next",
    NONE: "None",
    PREVIOUS: "Previous",
    RICH_TEXT_EDITOR: {
      DELETE_ALL: "Delete all content",
      MENTION_BUTTON: {
        AGENT: "Mention an actor",
        PROJECT: "Mention a project",
        PROJECTOUTPUT: "Mention an artistic object",
        CONCEPT: "Tag with a concept"
      }
    },
    NOT_ALLOWED: "Your are not allowed to use this feature.",
    VALIDATE: "Validate",
    CONFIRM_LIVE_FORM: "Are you sure you want to leave ? You have form with unsaved changes.",
    ZOOM_IN: "Zoom in",
    ZOOM_OUT: "Zoom out",
    ZOOM_RESET: "Zoom reset",
    SEE_GRAPH: "See the graph"
  },
  ACTIVITY: {
    TYPE: {
      DELETION: "Deletion",
      UPDATE: "Update",
      CREATION: "Creation"
    },
    TITLE: {
      DELETION: "Deletion of an object",
      DELETION_plural: "Deletion of {{count}} objects",
      UPDATE: "Update of an object",
      UPDATE_plural: "Update of {{count}} objects",
      CREATION: "Creation of an object",
      CREATION_plural: "Creation of {{count}} objects"
    }
  },
  ADMIN: {
    USERS: {
      TITLE: "Accounts and users"
    },
    MENU: {
      LABEL: "Aministrator",
      NEW_USER: "New user",
      USERS: "Users",
      NEW_GROUP: "New group",
      GROUPS: "Groups and roles",
      ACTIVITY: "Activity"
    },
    GROUPS: {
      GLOBAL_ROLES: {
        TITLE: "Roles settings",
        DESCRIPTION: "User groups with global permissions on Weever features"
      },
      SPECIFIC_GROUPS: {
        TITLE: "User groups settings",
        DESCRIPTION: "User groups used within projects settings to give specific permissions"
      },
      CREATE: "Add group"
    },
    GROUP: {
      NAME: "Group's name",
      DESCRIPTION: "Description",
      MEMBERS: "Members of the group",
      ADD: "Add a new member",
      LINKEDIT_ADD_LABEL: "Add members to the group",
      LINKEDIT_ADD_SUBLABEL: "Select one or many members to add to the group",
      INFORMATIVE_GROUP_MESSAGE:
        "This user group is purely informative. Users are handled in automatic way and can't be modified in this page.",
      REMOVE: {
        TITLE: "Remove group",
        WARN_MESSAGE: "Caution, this action is irrevocable.",
        CONFIRM_MESSAGE: 'Are you sure to remove group "{{group}}"?'
      }
    },
    AUTOCOMPLETE: {
      CHOOSE: "Choose a member...",
      CHOOSE_ANOTHER: "Choose another member...",
      NO_RESULT: "No member found."
    }
  },
  AGENT: {
    ADDRESS: {
      LABELS: {
        BIRTH: "Place of birth",
        HOME: "Home",
        WORK: "Work",
        OTHER: "Other"
      },
      NEW: "Add a new adress",
      TYPE: "Type"
    },
    ADDRESSES: "Addresse(s)",
    ADDRESSES_LABELS: {
      BIRTH: "Birth place",
      CITY: "City",
      COUNTRY: "Country",
      HOME: "Home place",
      OTHER: "Other",
      POSTALCODE: "Postal code",
      STREET1: "Street",
      STREET2: "Additional address",
      WORK: "Workplace",
      EMAIL: "Email"
    },
    AFFILIATION: {
      ORGANIZATION: {
        ADD: "Add an organization"
      },
      ROLE: "Role",
      START_DATE: "Start date",
      END_DATE: "End date",
      AGENT_TYPE: "Actor type"
    },
    AFFILIATION_NO_ROLE: "Role not specified",
    AVATAR: "Avatar",
    CREATED_AT_BY: "Created at {{date}} by ",
    CREATE: "New agent",
    NEW_ADDRESS: "New address",
    EMAILS: "Email(s)",
    EMAIL_LABEL: "Type",
    EMAILS_LABELS: {
      MAIN: "Main",
      OTHER: "Other",
      PRIVATE: "Private",
      PERSO: "Private"
    },
    EMAIL_NEW: "Add a new email",
    NO_ADDRESS: "None",
    NO_AFFILIATION: "None",
    NO_EMAILS: "None",
    NO_PHONE: "None",
    PHONE_LABEL: "Type",
    PHONE_NEW: "Add a new phone number",
    PHONE_NUMBER: "Phone number",
    PHONES: "Phone(s)",
    PHONES_LABELS: {
      FIX: "Landline",
      MOB: "Mobile",
      OTHER: "Other"
    },
    SUB_LABEL: "Select one ore more agent to link"
  },
  APOLLO: {
    ERROR: {
      USER_NOT_ALLOWED: "You are not allowed to access this page.",
      MALFORMED_REQUEST: "Error, changes have not been saved !",
      SERVER_ERROR: "An error occured...",
      USER_NOT_EXISTS_IN_GRAPH: "Your account is desynchronized, contact an administrator."
    }
  },
  APP_BAR: {
    HOME: "Home",
    BUTTONS: {
      BIN: "Bin",
      EDIT: "Edit",
      IMPORT: "Import",
      KONCEPT: "Vocabulary"
    },
    MENU: {
      HELLO: "Hello",
      MY_PROFILE: "My profil",
      SIGN_OUT: "Sign out",
      VISUALIZATIONS: {
        TITLE: "Visualizations",
        GLOBAL_GRAPH_ITEM: "Global graph"
      }
    }
  },
  ARTISTIC_OBJECT: {
    AUTHORS: "Related authors",
    COLLECTION: "Collection",
    COURTESY: "Courtesy",
    CREATE: "New artistic object",
    CREDITS: "Credits",
    DESCRIPTION: "Description",
    DIMENSIONS: "Dimensions",
    IMAGES: "Images",
    NEW_AUTHOR: "Add an author",
    NO_AUTHORS: "None",
    NO_COLLECTION: "None",
    NO_COURTESY: "None",
    NO_CREDITS: "None",
    NO_DESCRIPTION: "None",
    NO_DIMENSIONS: "None",
    NO_IMAGE: "No image",
    NO_PROJECT: "None",
    NO_REPRESENTATION: "None",
    NO_TECHNICAL_INFO: "None",
    NO_YEAR: "None",
    PROJECTS: "Related projects",
    REPRESENTATIONS: "Related artwork(s) or manifestation(s)",
    SHORT_DESCRIPTION: "Short description",
    TECHNICAL_INFO: "Technical informations",
    TITLE: "Title",
    YEAR: "Year"
  },
  ARTWORK: {
    CREATE: "New artwork",
    NEW: "Add an artwork",
    SUB_LABEL: "Select one or more artworks to link",
    AUTOCOMPLETE: {
      CHOOSE: "Choose an artwork...",
      CHOOSE_ANOTHER: "Choose another artwork...",
      NO_RESULT: "No artwork found."
    }
  },
  AUTOCOMPLETE: {
    ADD: "Add {{value}}",
    PENDING_CREATION: "{{value}} (New)"
  },
  BIN: {
    ACTIONS: {
      LINK_TO_NEW_PROJECT_CONTRIBUTION: {
        DESCRIPTION: "You are going to link these resources to a new event in the project {{project}}",
        SUCCESS: "Resources have been successfully linked",
        TITLE: "Create an event"
      },
      LINK_TO_PROJECT: {
        DESCRIPTION: "You are about to reassign one or more items to a new project.",
        SUCCESS: "Resources have been successfully linked",
        TITLE: "Link to a project"
      },
      LINK_TO_PROJECT_CONTRIBUTION: {
        DESCRIPTION: "You are going to link these resources to a project event {{project}}.",
        SUCCESS: "Resources have been successfully linked",
        TITLE: "Link to an event"
      }
    },
    EMPTY_DESCRIPTION: "The bin is empty, excellent work!",
    EMPTY_TITLE: "Well done !"
  },
  BREADCRUMB: {
    ARTWORKS: "List of artworks",
    EXHIBITIONS: "List of exhibitions",
    GROUPS: "List of roles and user groups",
    ORGANIZATIONS: "List of organizations",
    PERSONS: "List of persons",
    PROJECTS: "List of projects",
    RESOURCES: "List of resources"
  },
  CONCEPT: {
    AUTOCOMPLETE: {
      NO_RESULT: "No concept found.",
      NOT_CLASSIFIED: "Not classified"
    }
  },
  CONFIRMATION_MODAL: {
    NO: "No",
    YES: "Yes"
  },
  ENTITY: {
    CREATED_AT_BY: "Created at {{date}} by ",
    EXTERNAL_LINK_NEW: "Add an external link",
    EXTERNAL_LINKS_LABELS: {
      FACEBOOK: "Facebook",
      INSTAGRAM: "Instagram",
      LINKEDIN: "Linkedin",
      OTH: "Other website",
      OTHER: "Other website",
      TWITTER: "Twitter",
      WEBSITE: "Website"
    },
    LABEL_TYPE: "Type",
    LINK: "Link",
    EXTERNAL_LINKS: "External links",
    NEW_LINK: "New link",
    NO_EXTERNAL_LINK: "None",
    TAGGINGS: "Tags",
    NEW_TAGGING: "New tag",
    NO_TAGGING: "None",
    ADD_TAGGING: "Add tag",
    ACCESS_POLICY: "Access policy",
    NO_ACCESS_POLICY: "None",
    ACTIONS: {
      BATCH_UPDATE: {
        TITLE: "Update element",
        TITLE_plural: "Batch update {{count}} elements",
        CONFIRM: "Are you sure to update that element",
        CONFIRM_plural: "Are you sure to batch update {{count}} elements",
        SUCCESS: "Successfully updated"
      }
    }
  },
  EXHIBITION: {
    CREATE: "New exhibition",
    NEW: "Add an exhibition",
    SUB_LABEL: "Select one or more exhibition to link",
    AUTOCOMPLETE: {
      ADD: "Add {{value}}",
      PENDING_CREATION: "{{value}} (New)"
    }
  },
  FORM_ERRORS: {
    FIELD_ERRORS: {
      EMAIL_ALREADY_REGISTERED: "This email was already registered",
      INVALID_EMAIL: "The email format is invalid",
      INVALID_DATE: "The date format is invalid",
      INVALID_LINK: "The link form is invalid",
      PASSWORD_TOO_SHORT: "Password too short",
      PASSWORDS_DO_NOT_MATCH: "Password confirmation does not match with new password",
      REQUIRED: "Required field",
      REQUIRED_S: "Required fields",
      TOO_SHORTS: "Too short fields",
      EMAIL_REQUIRED: "The email is required",
      PASSWORD_REQUIRED: "The password is required",
      PASSWORD_CONFIRMATION_REQUIRED: "The password confirmation is required",
      WRONG_OLD_PASSWORD: "Old password is incorrect",
      TITLE_REQUIRED: "The title is required",
      DESCRIPTION_REQUIRED: "The description is required",
      START_DATE_REQUIRED: "The start date is required",
      FIRST_NAME_REQUIRED: "The first name is required",
      LAST_NAME_REQUIRED: "The last name is required",
      LABEL_REQUIRED: "The label is required",
      ROLE_REQUIRED: "The role is required",
      AGENT_REQUIRED: "The agent is required",
      NUMBER_REQUIRED: "The number is required",
      LINK_REQUIRED: "The link is required"
    },
    GENERAL_ERRORS: {
      DISABLED_BECAUSE_NO_MODIFICATION: "Disabled because no modication was done",
      FORM_VALIDATION_ERROR: "Some fields are not valid",
      INVALID_CREDENTIALS: "This credentials are not valid",
      UNEXPECTED_ERROR: "An exexpected error happened, please try later ",
      USER_MUST_BE_AUTHENTICATED: "You should be connected to do this action",
      USER_NOT_ALLOWED: "You do not have permission for this action"
    }
  },
  IMPORT: {
    FEATURES_PROJECTS: "Latest selected projects :",
    RESET_PROJECTS: "Clear latest projects...",
    SELECT_PROJECTS: "Link to a/several projects",
    SELECT_EVENT: "Link to an event",
    CREATE_EVENT: "Create a new event",
    UPLOAD: "Import file",
    UPLOAD_plural: "Import {{count}} files",
    UPLOADING_MESSAGE: "Importing : {{percentage}}%",
    PAUSE_MESSAGE: "Import in pause ({{percentage}}%)",
    FILES_SELECTED_0: "No selected file",
    FILES_SELECTED: "{{count}} selected file",
    FILES_SELECTED_plural: "{{count}} selected files",
    SELECT_ALL_FILES: "Select all files",
    UNSELECT_ALL_FILES: "Unselect",
    UPLOAD_GROUP_TITLE: "Event {{event}} import group",
    UPLOAD_GROUP_TITLE_SUBTITLE: "{{count}} file",
    UPLOAD_GROUP_TITLE_SUBTITLE_plural: "{{count}} files",
    UPLOAD_GROUP_SUCCESS: "File successfully imported in event",
    UPLOAD_GROUP_SEE_PROJECT: "See files in event",
    UPLOAD_GROUP_STATUS: {
      COMPLETE: "Complete",
      PAUSE: "In pause",
      FINALIZING: "Finalizing...",
      IMPORTING: "Importing..."
    }
  },
  ORGANIZATION: {
    ACTIONS: {
      BATCH_UPDATE: {
        TITLE: "Update organization",
        TITLE_plural: "Batch update {{count}} organizations",
        CONFIRM: "Are you sure to update that organization",
        CONFIRM_plural: "Are you sure to batch update {{count}} organizations",
        SUCCESS: "Successfully updated"
      }
    },
    AFFILIATIONS: "Member of organization(s)",
    CREATE: "New organization",
    CREATE_TITLE: "Create a new organization",
    CREATED_AT_BY: "Created at {{date}} by ",
    DESCRIPTION: "Description",
    NAME: "Name",
    NEW: "Add an organization",
    NEW_ROLE: "New role",
    NO_DESCRIPTION: "None",
    ORGANIZATION_HEADER: "Related organization",
    SHORT_DESCRIPTION: "Short description",
    SUB_LABEL: "Select one or more organizations to link",
    FIELD_ERRORS: {
      NAME: "Name is required"
    }
  },
  PERSON: {
    ACTIONS: {
      BATCH_UPDATE: {
        TITLE: "Update person",
        TITLE_plural: "Batch update {{count}} persons",
        CONFIRM: "Are you sure to update that person",
        CONFIRM_plural: "Are you sure to batch update {{count}} persons",
        SUCCESS: "Successfully updated"
      }
    },
    REMOVE: {
      TITLE: "Remove person",
      WARN_MESSAGE: "Caution, this action is irrevocable.",
      CONFIRM_MESSAGE: 'Are you sure to remove person "{{person}}"?'
    },
    AFFILIATIONS: "Member of organization(s)",
    AUTOCOMPLETE: {
      CHOOSE: "Choose a person...",
      CHOOSE_ANOTHER: "Choose another person...",
      NO_RESULT: "No person found."
    },
    BIO: "Biography",
    BORN_ON: "Born on {{date}}",
    BORN: "Born on",
    CREATE: "New person",
    CREATE_TITLE: "Create a new person",
    DELETE: {
      DELETE_DEFINITELY: "DELETE THE PERSON DEFINITELY",
      DONE_CONFIRMATION: "Person deleted with success",
      I_UNDERSTAND: "Valider"
    },
    FIRST_NAME: "First name",
    FULL_NAME: "Full name",
    LAST_NAME: "Last name",
    NEW: "Add a person",
    NEW_ROLE: "New role",
    NO_BIOGRAPHY: "None",
    ROLE: "Role",
    SHORT_BIO: "Short biography",
    ADMIN_PANEL: {
      TITLE: "User account settings",
      ACCOUNT_CREATED: "Account created , auto reload of the page !",
      ACCOUNT_DISABLED: "Account disabled",
      ACCOUNT_ENABLED: "Account enabled",
      CREATE_ACCOUNT: "Create an account",
      DELETED_ACCOUNT: "This person deleted their user account.",
      DONT_SEND_EMAIL_NOTIFICATION: "Don't send email notification",
      SETTINGS: "Email settings.",
      GOT_ACCOUNT: "This person has a user account.",
      NO_ACCOUNT_YET: "This person does not yet have a user account.",
      NO_ROLE: "No role",
      PWD_OR_EMAIL_TOO_SHORT: "Password or email too short",
      ROLES: "Roles",
      SEND_EMAIL_NOTIFICATION: "Send email notification",
      PASSWORD: "Password"
    },
    FIELD_ERRORS: {
      LAST_NAME_REQUIRED: 'At least one field "First name" or "Last name" is required',
      EMAIL_REQUIRED: "Au least one mail address is required"
    }
  },
  PROFIL: {
    EDIT_MY_PROFIL: "Edit my profil",
    MY_PERSONAL_INFO: "my personal information",
    MY_PASSWORD: "my password",
    MY_ACCOUNT: "my account",
    CHANGE_PASSWORD: {
      OLD_PASSWORD: "Old password",
      NEW_PASSWORD: "New password",
      CONFIRM_NEW_PASSWORD: "Confirm new password",
      DONE_CONFIRMATION: "Password edited with success"
    },
    DELETE_ACCOUNT: {
      DELETE_DEFINITELY: "DELETE THE ACCOUNT DEFINITELY",
      DONE_CONFIRMATION: "Account deleted with success",
      I_UNDERSTAND: "I understand the consequences, delete my account",
      WARN_MESSAGE: "This action cannot be undone. All the data will be erased.",
      PLEASE_INPUT_A: "Please input",
      PLEASE_INPUT_B: "to confirm",
      CHECKSTRING: "DELETE"
    }
  },
  PROJECT: {
    ACTIONS: {
      ACCESS_RIGHTS: {
        TITLE: "Project access rights",
        READ_ONLY_TITLE: "Read only",
        READ_WRITE_TITLE: "Read/Write"
      },
      REMOVE: {
        TITLE: "Remove project",
        WARN_MESSAGE: "Caution, this action est irrevocable.",
        CONFIRM_MESSAGE: 'Are you absolutely sure to remove project "{{project}}"?'
      },
      BATCH_UPDATE: {
        TITLE: "Update project",
        TITLE_plural: "Batch update {{count}} projects",
        CONFIRM: "Are you sure to update that project",
        CONFIRM_plural: "Are you sure to batch update {{count}} projects",
        SUCCESS: "Successfully updated"
      }
    },
    ARTISTS: "Artists",
    ARTWORKS_HEADER: "Associated artworks",
    ATTACHMENTS: "Attached file(s)",
    AUTOCOMPLETE: {
      CHOOSE: "Select a project...",
      CHOOSE_ANOTHER: "Select another project...",
      NO_RESULT: "No project found."
    },
    COLOR: "Color",
    CREATE: "New project",
    CREATE_TITLE: "Create a new project",
    CREATED_AT_BY: "Created the {{date}} by ",
    DESCRIPTION: "Description",
    END_DATE: "Archive date",
    END_DATE_TOOLTIP: "Date of last project's event",
    PROJECT_CONTRIBUTIONS: "Event(s)",
    EVENTS: {
      COLUMNS: {
        END_DATE: "End date",
        MEMO: "Memo",
        MEMOS: "Memos",
        RESOURCES: "Resources",
        START_DATE: "Start date",
        TITLE: "Title"
      }
    },
    EXHBITIONS_HEADER: "Associated exhibitions",
    GATHERED_AGENTS: "Actors listed in the events",
    IMAGE: "Image",
    INVOLVEMENT_NO_ROLE: "Role not specified",
    INVOLVEMENTS: "Actor (s) involved",
    NEW: "Add a project",
    NEW_PARENT: "Add a parent",
    NO_DESCRIPTION: "No description",
    NO_INVOLVEMENT: "None",
    NO_RELATED_ARTWORKS: "No related artworks",
    NO_RELATED_EXHIBITION: "No related exhibitions",
    NO_RELATED_SUBPROJECTS: "No related sub-projects",
    SHORT_DESCRIPTION: "Short description",
    START_DATE: "Start date",
    START_DATE_TOOLTIP: "Date of first project's event",
    SUB_LABEL: "Select one or more projects to link",
    SUB_PROJECTS: "Sub-projects",
    NO_PARENT_PROJECT: "None",
    PARENT_PROJECT: "Parent's project",
    TITLE: "Title",
    ACCESS_PRIVILEDGE_ADMIN: 'You have an "Administrator" access',
    ACCESS_PRIVILEDGE_EDITOR: 'You have an "Editor" access',
    ACCESS_PRIVILEDGE_CONTRIBUTOR: 'You have a "Contributor" access',
    STATUS: "Status",
    STATE: {
      RUNNING: "Running",
      CLOSED: "Archived",
      CLOSED_AT: "Archived on {{date}}"
    }
  },
  PROJECT_CONTRIBUTION: {
    AUTOCOMPLETE: {
      CHOOSE: "Choose an event...",
      CHOOSE_ANOTHER: "Choose another event...",
      NO_RESULT: "No event found."
    },
    BY: "By",
    CREATE: "New event",
    CREATED_AT: "Created at",
    CREATED_AT_BY: "Created at {{date}} by ",
    END_DATE: "End date",
    INVOLVEMENTS: "Actors involved",
    MEMOS: {
      CREATE: "New memo",
      COLUMNS: {
        CONTENT: "Content"
      }
    },
    NO_DATE: "None",
    NO_RELATED_INVOLVEMENTS: "None",
    RESOURCES: {
      COLUMNS: {
        END_DATE: "End date",
        MEMO: "Memo",
        RESOURCES: "Resources",
        START_DATE: "Start date",
        TITLE: "Title"
      },
      AND_MORE: "+{{count}}"
    },
    START_DATE: "Start date",
    TABS: {
      RESOURCES: "Resources",
      MEMOS: "Memos"
    },
    ACTIONS: {
      BATCH_UPDATE: {
        TITLE: "Update event",
        TITLE_plural: "Batch update {{count}} events",
        CONFIRM: "Are you sure to update that event",
        CONFIRM_plural: "Are you sure to batch update {{count}} events",
        SUCCESS: "Event successfully updated",
        SUCCESS_plural: "{{count}} events successfully updated"
      }
    }
  },
  EXPLORE: {
    PROJECT: {
      PARENTPROJECT: "Parent project",
      SIBLINGPROJECT: "Sibling project",
      SUBPROJECT: "Sub project",
      SEE_ALSO: {
        ASSOCIATED_PROJECTS: "Associated projects...",
        ASSOCIATED_AGENTS: "Associated actors..."
      }
    }
  },
  REMOTE_TABLE: {
    ACTIONS: {
      FILTERS: "Filters",
      REMOVE: "Remove",
      REMOVE_CONFIRM_TEXT: "Are you sur to delete this item ?",
      REMOVE_CONFIRM_TEXT_plural: "Are you sur to delete this {{count}} items ?",
      REMOVE_plural: "Remove ({{count}})",
      REMOVE_SUCCESS: "Items successfully deleted",
      SELECT_ALL_MESSAGE: "Caution, only {{count}} elements are selected.",
      SELECT_ALL_WIDE_MESSAGE: "{{count}} elements selected.",
      SELECT_ALL_WIDE: "Select {{count}} elements",
      DESELECT_ALL: "Unselect all"
    },
    BODY: {
      NO_MATCH: "No match found...",
      TOOLTIP: "Sort"
    },
    FILTER: {
      ALL: "All",
      RESET: "Reset",
      TITLE: "Filters"
    },
    PAGINATION: {
      DISPLAY_ROWS: "of",
      NEXT: "Next page",
      PREVIOUS: "Previous page",
      ROWS_PER_PAGE: "Rows per page:"
    },
    SELECTED_ROWS: {
      DELETE: "Delete",
      DELETE_ARIA: "Delete selected column(s)",
      TEXT: "Selected column(s)"
    },
    TOOLBAR: {
      DOWNLOAD_CSV: "Download CSV",
      FILTER_TABLE: "Filter",
      PRINT: "Print",
      SEARCH: "Search",
      VIEW_COLUMNS: "View columns"
    },
    VIEW_COLUMNS: {
      TITLE: "Visible columns",
      TITLE_ARIA: "Hide columns of the table"
    }
  },
  RESET_PASSWORD: {
    CONFIRM: "Send email",
    CONFIRMATION: "Email sended, check also your SPAM folder",
    CHECK_SPAM: "Check your spam folder",
    SEND_RESET_TO_EMAIL: "Reset password with email"
  },
  RESOURCE: {
    ACTIONS: {
      REMOVE: {
        TITLE: "Remove resource",
        WARN_MESSAGE: "Caution, this action est irrevocable.",
        CONFIRM_MESSAGE: 'Are you absolutely sure to remove resource "{{resource}}"?'
      },
      BATCH_UPDATE: {
        TITLE: "Update resource",
        TITLE_plural: "Batch update {{count}} resources",
        CONFIRM: "Are you sure to update that resource",
        CONFIRM_plural: "Are you sure to batch update {{count}} resources",
        SUCCESS: "Resource successfully updated",
        SUCCESS_plural: "{{count}} resources successfully updated"
      }
    },
    CREATED_AT_BY: "Created at {{date}} by ",
    CREDITS: "Credits",
    DESCRIPTION: "Description",
    FILE_NAME: "File name",
    IS_NEW: "Is new",
    MIME: "Type",
    MORE_DETAILS: "Resource's details",
    NO_CREDITS: "No credits",
    NO_DESCRIPTION: "No description",
    SHOW_CAROUSEL: "Show picture's slider",
    SIZE: "Size",
    ATTACH: "Attach resources",
    UPLOAD: "Upload files",
    URL: "Download link",
    VIEWER: {
      NO_HANDLER_MESSAGE: "This resource can't be viewed here.",
      NO_HANDLER_LINK_MESSAGE: "Try to download the file and open it from your computer."
    }
  },
  SEARCH: {
    COLUMNS: {
      ARTISTIC_OBJECTS: {
        PICTURES_COUNT: "Pictures count"
      },
      COMMON: {
        IMAGE: "Image",
        CREATED_AT: "Created at",
        CREATED_BY: "Created by",
        UPDATED_AT: "Updated at",
        END_AT: "End at",
        IS_PARENT_PROJECT: "Root project",
        PRIVACY: "Privacy",
        PROJECT: "Project",
        START_AT: "Start at",
        TITLE: "Title",
        TAGS: "Tags"
      },
      FILES: {
        MIME: "Type",
        SIZE: "Size"
      },
      ORGANIZATIONS: {
        NAME: "Name",
        SHORT_NAME: "Short name"
      },
      PERSONS: {
        FULL_NAME: "Full name",
        FIRST_NAME: "First name",
        LAST_NAME: "Last name"
      },
      PROJECTS: {
        GATHER_SUB_PROJECTS: "Gather sub projects",
        STATE: "State"
      }
    },
    TABS: {
      ARTWORKS: "Artworks",
      EXHIBITIONS: "Exhibitions",
      ORGANIZATIONS: "Organizations",
      PERSONS: "Persons",
      PROJECTS: "Projects",
      RESOURCES: "Resources"
    }
  },
  SIGN_IN: {
    EMAIL: "Email",
    PASSWORD: "Password",
    PASSWORD_FORGOTTEN: "Password forgotten",
    REDIRECT_SIGN_UP: "Create an account",
    TITLE: "Title",
    PUBLIC_MODE_ACCESS: "Explore open data",
  },
  SIGN_UP: {
    EMAIL: "Email address",
    FIRST_NAME: "First name",
    LAST_NAME: "Last name",
    PASSWORD: "Password",
    REDIRECT_SIGN_IN: "You alredy have an account ? Sign in.",
    SUBMIT: "Create the account",
    TITLE: "Title",
    VALIDATE_PASSWORD: "Validate the password"
  },
  TRANSLATE: {
    COPY_FROM_OTHER_LANG: "Copy value from other lang",
    NOT_EXISTS: "Translation is missing in this language"
  },
  TYPENAME: {
    ARTWORK: "Artwork",
    EXHIBITION: "Exhibition",
    ORGANIZATION: "Organization",
    PERSON: "Person",
    PROJECT: "Project",
    RESOURCE: "Resource",
    CONCEPT: "Concept",
    PROJECT_CONTRIBUTION: "Event"
  },
  UPLOAD: {
    ADD_MORE: "Download more files",
    POSTPROCESS_ERROR:
      "An error has occurred in server processing, please try again and if the problem persists contact an administrator.",
    PROCESSING: "Processing on server",
    PROCESSING_COUNT: "Processing on server ({{count}})",
    SHOW_UPLOADED_IN_BIN: "Display the uploaded ressource in the bin",
    SHOW_UPLOADED_IN_BIN_plural: "Display the {{count}} imported ressource in the bin",
    START: "Start import",
    SUCCESS_DETAILS: "(Found this ressource in the bin)",
    SUCCESS_DETAILS_plural: "(Found this {{count}} ressources in the bin)",
    SUCCESS_MESSAGE: "Download succesfull"
  }
});
