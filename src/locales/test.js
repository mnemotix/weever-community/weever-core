// import {frLocale, enLocale} from '@synaptix/ui';

export const SUIlocales = {
  fr: {
    COMMON: {
      NAME: "Nom",
      TITLE: "Titre",
      IMAGE: "Image",
      TAGS: "Tags",
      CREATED_AT: "Créé le",
      CREATED_BY: "Créé par",
      UPDATED_AT: "Modifié le",
      ACCESS_POLICY: "Confidentialité",
      END_DATE: "Date de fin",
      MEMO: "Mémo",
      MEMOS: "Mémos",
      RESOURCES: "Ressources",
      START_DATE: "Date de début",
    }
  },
  en: {
    COMMON: {
      NAME: "Name",
      IMAGE: "Image",
      TITLE: "Title",
      TAGS: "Tags",
      CREATED_AT: "Created at",
      CREATED_BY: "Created by",
      UPDATED_AT: "Updated at",
      ACCESS_POLICY: "Access policy",
      END_DATE: "End date",
      MEMO: "Memo",
      MEMOS: "Memos",
      RESOURCES: "Resources",
      START_DATE: "Start date",
    }
  }
};
