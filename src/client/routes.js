export const ROUTES = {
  INDEX: "/",
  ADMIN: "/admin",
  ADMIN_GROUPS: "/admin/groups",
  ADMIN_GROUP: "/admin/group/:id",
  ADMIN_USERS: "/admin/users",
  ADMIN_USER_CREATE: "/admin/user/create/new",
  ADMIN_GROUP_CREATE: "/admin/groups/create",
  SIGN_IN: "/",
  SIGN_UP: "/signup",
  PROFIL_EDIT: "/profil/edit",
  PROFIL_PASSWORD_FORGOTTEN: "/resetpassword",

  EXPLORE: "/explore/projects",

  PROJECTS: "/projects",
  PROJECT: "/projects/:id",
  EXPLORE_PROJECTS: "/explore/projects",
  EXPLORE_PROJECT: "/explore/projects/:id",
  PROJECT_EDIT: "/projects/edit/:id",
  PROJECT_CREATE: "/projects/create/new",
  PROJECT_CONTRIBUTION: "/project-contributions/:id",
  PROJECT_CONTRIBUTION_MEMOS: "/project-contributions/:id/memos",
  PROJECT_CONTRIBUTION_CREATE: "/project-contributions/create/new/:id",
  PROJECT_CONTRIBUTION_EDIT: "/project-contributions/edit/:id",

  RESOURCES: "/resources",
  RESOURCE: "/resources/:id",
  EXPLORE_RESOURCES: "/explore/resources",
  EXPLORE_RESOURCE: "/explore/resources/:id",

  RESOURCE_EDIT: "/resources/edit/:id",
  PERSONS: "/persons",
  PERSON: "/persons/:id",
  PERSON_EDIT: "/persons/edit/:id",
  PERSON_CREATE: "/persons/create/new",
  EXPLORE_PERSON: "/explore/persons/:id",
  EXPLORE_PERSONS: "/explore/persons",

  EXPLORE_ORGANIZATIONS: "/explore/organizations",
  EXPLORE_ORGANIZATION: "/explore/organizations/:id",
  ORGANIZATIONS: "/organizations",
  ORGANIZATION: "/organizations/:id",
  ORGANIZATION_EDIT: "/organizations/edit/:id",
  ORGANIZATION_CREATE: "/organizations/create/new",

  IMPORT: "/import/:id?",
  BIN: "/bin",
  KONCEPT: "/koncept",
  ACTIVITY: "/activity",

  NOT_FOUND: "/404-not-found",

  VISUALIZATIONS_GLOBAL_GRAPH: "/visualizations/global-graph"
};
