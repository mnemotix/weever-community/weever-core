/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import {launchKoncept as launchKonceptBase} from "@mnemotix/koncept-core";
import {theme as defaultTheme} from "./theme";

/**
 * @param {object} possibleTypes
 * @param {[Extension]} [extensions]
 * @param {Theme} [theme]
 * @param {[MentionDefinition]} [extraMentionsDefinitions]
 * @param {Component} [AppBarTitleComponent]
 * @param {Component} [SplashScreenComponent]
 */
export function launchKoncept({
  possibleTypes,
  extensions = [],
  theme,
  extraMentionsDefinitions = [],
  AppBarTitleComponent,
  SplashScreenComponent
} = {}) {
  if (!theme) {
    theme = defaultTheme;
  }

  launchKonceptBase({
    name: "koncept",
    possibleTypes,
    theme,
    extensions: [],
    locationBaseUrl: "koncept",
    AppBarTitleComponent,
    SplashScreenComponent
  });
}
