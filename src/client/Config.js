const Config = {
  colors: {
    darkBlue: "#2a3c46",
    dark: "#000000",
    white: "white",
    whiteGrey: "rgba(235, 235, 235, 0.2)",
    weeverGrey: "#a8a8a8",
    whiteOpacity: "rgba(255, 255, 255, 0.3)",
    weeverGreyOpacity: "rgba(168, 168, 168, 0.3)"
  },
  explore: {
    spacing: 3
  },
  theme: {breakpoint: "sm"}
};
export default Config;
