/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Suspense, useState} from "react";
import {
  useLoggedUser,
  ExtensionsContext,
  getExtensionTopRoutes,
  EnvironmentContext,
  getEnvironment,
  PageNotFound,
  LoadingSplashScreen,
  RichTextMentionDefinitionsContext,
  FloatingButtonDialContextProvider,
  ConceptMentionDefinition,
  SignUp,
  SignIn,
  FragmentSwitch
} from "@synaptix/ui";

import {Route, Switch} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import loadable from "@loadable/component";
import {useQuery, gql} from "@apollo/client";
import {ROUTES} from "./routes";
import EnvVars from "../server/config/environment";
import {ProjectsMentionDefinition} from "./components/routes/Project/ProjectsMentionDefinition";
import {AgentsMentionDefinition} from "./components/routes/Agent/AgentsMentionDefinition";
import {DefaultLayout} from "./components/layouts/DefaultLayout";
import {default as ExplorePersons} from "./components/routes/Explore/Person/Persons";

const AdminGroups = loadable(() => import("./components/routes/Admin/UserGroup/UserGroups"));
const AdminGroupEdit = loadable(() => import("./components/routes/Admin/UserGroup/UserGroupEdit"));
const AdminUsers = loadable(() => import("./components/routes/Admin/UserAccount/UserAccounts"));
const AdminUserCreate = loadable(() => import("./components/routes/Admin/UserAccount/UserAccountCreate"));

const Search = loadable(() => import("./components/routes/Search/Search"));
const Project = loadable(() => import("./components/routes/Project/Project"));

const Explore = loadable(() => import("./components/routes/Explore/Explore"));

const ProjectEdit = loadable(() => import("./components/routes/Project/ProjectEdit"));
const ProjectContribution = loadable(() => import("./components/routes/ProjectContribution/ProjectContribution"));
const ProjectContributionEdit = loadable(() =>
  import("./components/routes/ProjectContribution/ProjectContributionEdit")
);
const ProjectContributionCreate = loadable(() =>
  import("./components/routes/ProjectContribution/ProjectContributionCreate")
);
const Resource = loadable(() => import("./components/routes/Resource/Resource"));
const ResourceEdit = loadable(() => import("./components/routes/Resource/ResourceEdit"));
const Person = loadable(() => import("./components/routes/Person/Person"));
const PersonEdit = loadable(() => import("./components/routes/Person/PersonEdit"));
const Organization = loadable(() => import("./components/routes/Organization/Organization"));
const OrganizationEdit = loadable(() => import("./components/routes/Organization/OrganizationEdit"));
const Import = loadable(() => import("./components/routes/Import/Import"));
const Bin = loadable(() => import("./components/routes/Bin/Bin"));
const ProfilEdit = loadable(() => import("./components/routes/Profil/ProfilEdit"));
const ProfilResetPassword = loadable(() => import("./components/routes/Profil/ProfilResetPassword"));
const Activity = loadable(() => import("./components/routes/Activity/GlobalActivity"));
const GlobalGraph = loadable(() => import("./components/routes/Visualizations/GlobalGraph"));

const gqlEnvironmentQuery = gql`
  query EnvironmentQuery {
    environment {
      ${Object.entries(EnvVars)
        .filter(([variable, {exposeInGraphQL}]) => exposeInGraphQL === true)
        .map(([variable]) => variable)}
    }
  }
`;

const defaultMentionsDefinitions = [AgentsMentionDefinition, ConceptMentionDefinition, ProjectsMentionDefinition];

/**
 * @param extensions
 * @param {[MentionDefinition]} [extraMentionsDefinitions]
 * @param {Component} [AppBarTitleComponent]
 * @return {*}
 * @constructor
 */
export default function Weever({
  extensions,
  extraMentionsDefinitions,
  AppBarTitleComponent,
  SplashScreenComponent = LoadingSplashScreen
} = {}) {
  let mentionsDefinitions = defaultMentionsDefinitions;

  if (extraMentionsDefinitions) {
    mentionsDefinitions = [].concat(defaultMentionsDefinitions, extraMentionsDefinitions);
  }

  const {data: {environment} = {}, loading: envLoading} = useQuery(gqlEnvironmentQuery);
  const {isLogged, isAdmin, isContributor, isEditor, loading} = useLoggedUser();
  const ExtensionsRoutes = getExtensionTopRoutes({
    extensions,
    props: {
      isAdmin,
      isContributor,
      isEditor
    }
  });

  // context for the button to display in the speed dial floating button used in mobile
  const [contextFloatingButtonDialValues, contextFloatingButtonDialSetValues] = useState([]);

  const publicExplorerEnabled = getEnvironment(environment, "PUBLIC_EXPLORER_ENABLED", {isBoolean: true});
  const filesBinDisabled = getEnvironment(environment, "FILES_BIN_DISABLED", {isBoolean: true});

  return (
    <FloatingButtonDialContextProvider
      contextFloatingButtonDialValues={contextFloatingButtonDialValues}
      contextFloatingButtonDialSetValues={contextFloatingButtonDialSetValues}>
      <EnvironmentContext.Provider value={environment}>
        <ExtensionsContext.Provider value={extensions}>
          <RichTextMentionDefinitionsContext.Provider value={mentionsDefinitions}>
            <Choose>
              <When condition={loading || envLoading}>
                <SplashScreenComponent />
              </When>

              <Otherwise>
                <Switch>
                  <If condition={publicExplorerEnabled}>
                    <Route path={ROUTES.EXPLORE} component={Explore} />
                  </If>
                  <Route>
                    <Choose>
                      <When condition={isLogged}>
                        <Suspense fallback={<SplashScreenComponent />}>
                          <DefaultLayout TitleComponent={AppBarTitleComponent}>
                            <FragmentSwitch>
                              {/* Put extensions routes in first place to override following ones in case...*/}
                              {ExtensionsRoutes}

                              <Route path={ROUTES.NOT_FOUND} component={PageNotFound} />

                              <Route exact path={ROUTES.PROFIL_EDIT} component={ProfilEdit} />

                              <Route exact path={ROUTES.IMPORT} component={Import} />

                              <If condition={!filesBinDisabled}>
                                <Route exact path={ROUTES.BIN} component={Bin} />
                              </If>

                              <Route
                                exact
                                path={ROUTES.PROJECT_CONTRIBUTION_CREATE}
                                component={ProjectContributionCreate}
                              />
                              <Route
                                exact
                                path={ROUTES.PROJECT_CONTRIBUTION_EDIT}
                                component={ProjectContributionEdit}
                              />
                              <Route exact path={ROUTES.RESOURCE_EDIT} component={ResourceEdit} />
                              <Route exact path={ROUTES.PERSON_EDIT} component={PersonEdit} />
                              <Route exact path={ROUTES.PERSON_CREATE} component={PersonEdit} />
                              <Route exact path={ROUTES.ORGANIZATION_CREATE} component={OrganizationEdit} />
                              <Route exact path={ROUTES.ORGANIZATION_EDIT} component={OrganizationEdit} />

                              <If condition={isEditor}>
                                <Route exact path={ROUTES.PROJECT_CREATE} component={ProjectEdit} />
                              </If>

                              <Route exact path={ROUTES.PROJECT_EDIT} component={ProjectEdit} />

                              <If condition={isAdmin}>
                                <Route exact path={ROUTES.ADMIN_GROUPS} component={AdminGroups} />
                                <Route exact path={ROUTES.ADMIN_GROUP} component={AdminGroupEdit} />
                                <Route exact path={ROUTES.ADMIN_GROUP_CREATE} component={AdminGroupEdit} />
                                <Route exact path={ROUTES.ADMIN_USER_CREATE} component={AdminUserCreate} />
                                <Route exact path={ROUTES.ADMIN_USERS} component={AdminUsers} />
                                <Route exact path={ROUTES.ACTIVITY} component={Activity} />
                              </If>

                              <Route exact path={ROUTES.PROJECT} component={Project} />
                              <Route exact path={ROUTES.RESOURCE} component={Resource} />
                              <Route exact path={ROUTES.PERSON} component={Person} />
                              <Route exact path={ROUTES.ORGANIZATION} component={Organization} />
                              {/* Be sure to add the following route AFTER ROUTES.PROJECT_CONTRIBUTION_EDIT to avoid conflict */}
                              {/* Be sure to NOT ADD "exact" property, because of sub routes existence (Memos) */}
                              <Route path={ROUTES.PROJECT_CONTRIBUTION} component={ProjectContribution} />

                              <Route exact path={ROUTES.VISUALIZATIONS_GLOBAL_GRAPH} component={GlobalGraph} />

                              <Route exact path={ROUTES.EXPLORE_PERSONS} component={ExplorePersons} />

                              <Route component={Search} />
                            </FragmentSwitch>
                          </DefaultLayout>
                        </Suspense>
                      </When>

                      <Otherwise>
                        <Switch>
                          <Route exact path={ROUTES.PROFIL_PASSWORD_FORGOTTEN} component={ProfilResetPassword} />
                          <Route exact path={ROUTES.SIGN_UP}>
                            <SignUp to={ROUTES.SIGN_IN} />
                          </Route>
                          <Route>
                            <SignIn
                              toSignUp={ROUTES.SIGN_UP}
                              toForget={ROUTES.PROFIL_PASSWORD_FORGOTTEN}
                              formatedRouteToExplorer={formatRoute(ROUTES.EXPLORE)}
                            />
                          </Route>
                        </Switch>
                      </Otherwise>
                    </Choose>
                  </Route>
                </Switch>
              </Otherwise>
            </Choose>
          </RichTextMentionDefinitionsContext.Provider>
        </ExtensionsContext.Provider>
      </EnvironmentContext.Provider>
    </FloatingButtonDialContextProvider>
  );
}
