export function addOpacityToHexaColor(hex, alpha) {
  return `${hex}${Math.floor(alpha * 255)
    .toString(16)
    .padStart(2, 0)}`;
}






/**
 * return a JSON stringify with cyclic object references removed
 * @param {*} data
 */
export function jsonStringify(data) {
  const getCircularReplacer = () => {
    const seen = new WeakSet();
    return (key, value) => {
      if (typeof value === "object" && value !== null) {
        if (seen.has(value)) {
          return;
        }
        seen.add(value);
      }
      return value;
    };
  };
  
  return JSON.stringify(data, getCircularReplacer());
}

/**
 *  replace all occurence à `find` inside `str` by `replace``
 * @param {*} str source string
 * @param {*} find  string to find
 * @param {*} replace
 */
export function replaceAll(str, find, replace) {
  function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
  }
  if (str == null) {
    return null;
  }
  return str.replace(new RegExp(escapeRegExp(find), "g"), replace);
}

// remove all html tags
export function removeAllHtmlTag(str) {
  if (str == null) {
    return null;
  }
  // return str.replace(/<[^>]+>/g, replace);
  return str.replace(/(<([^>]+)>)/gi, "");
}

// remove spaces, new line & tabs
export function removeAllSpaceAndNewLine(str) {
  if (str == null) {
    return null;
  }
  return str.replace(/[\n\r\s\t]+/g, "");
}
/**
 * used to set NULL instead of <p><br/></p> inside description field for example after a user enter input inside it and then remove all content
 * so translated tooltip can be showed
 * @param {*} str
 */
export function normalizeHtmlInput(str) {
  let val = removeAllSpaceAndNewLine(removeAllHtmlTag(str));
  return val?.length < 1 ? "" : str;
}

/**
 * compare if objA and objB have same id ( id, tempId) or same jsonStringify
 * @param objA
 * @param objB
 */
export function isSameObject(objA, objB) {
  if (objA.id) {
    return objB.id === objA.id;
  } else if (objA.tempId) {
    return objB.tempId === objA.tempId;
  } else {
    return jsonStringify(objB) === jsonStringify(objA);
  }
}

/**
 * Add and element to an array of element
 * update the element if exist
 * @param {*} arrayOfElements
 * @param {*} elemToAdd
 */
export function addUniqueElemToArray(arrayOfElements, elemToAdd) {
  let newArr = [];
  let alreadyAdded = false; // add data only once
  for (let i in arrayOfElements) {
    let item = arrayOfElements[i];

    if (item.id === elemToAdd.id || (item.tempId && item.tempId === elemToAdd.tempId)) {
      alreadyAdded = true;
      newArr.push(elemToAdd);
    } else {
      newArr.push(item);
    }
  }
  if (!alreadyAdded) {
    newArr.push(elemToAdd);
  }
  return newArr;
}

// convert an object to array
// old school implementatio for IE
export function convertObjectToArray(obj) {
  var arr = [];
  for (var i in obj) {
    if (obj.hasOwnProperty(i)) {
      arr.push({index: i, value: obj[i]});
    }
  }
  return arr;
}

/**
 * search in an array of object the element with e[fieldname] === fieldValue and return his index
 * @param {*} arr
 * @param {*} fieldName
 * @param {*} fieldValue
 */
export function indexOfObjInArr(arr, fieldName, fieldValue) {
  return arr.findIndex((el) => el[fieldName] === fieldValue);
}

import smoothscroll from "smoothscroll-polyfill";
smoothscroll.polyfill();
// scrool to the anchor with id , offset is pixel offset
export function scrollToAnchor(id, offset = 0) {
  try {
    const anchor = document.getElementById(id);
    if (anchor) {
      const offsetTop = anchor.getBoundingClientRect().top + window.pageYOffset;
      window.scroll({
        top: offsetTop - offset,
        behavior: "smooth"
      });
    } else {
      console.log("anchor not found for", id);
    }
  } catch (error) {
    console.error("error in scrollToAnchor", error);
  }
}

/**
 * chunk / cut an array into subarray of size length
 * @param {array} array  to chunk
 * @param {int} size   number of items to put in each chunks
 *
 * @param {boolean} avoidFewElementsInLast : if true concat the two last chunks  if the length of the last chunk one is smaller than size,
 * for exemple (with size=4 & data.length = 17) we want to display in explore/projects elements alternativly in row then column
 * so we get 4 items in row, 4 in column , 4 in row, 1 in column. so this last element is alone and we get better UI if it's add to the previous chunk
 * so if avoidFewElementsInLast=true we get 4 items, 4 items , 5items
 *
 * @returns array
 */
export function chunkArray(array, size, avoidFewElementsInLast = false) {
  const chunked_arr = [];
  let copied = [...array]; // ES6 destructuring
  const numOfChild = Math.ceil(copied.length / size); // Round up to the nearest integer
  for (let i = 0; i < numOfChild; i++) {
    chunked_arr.push(copied.splice(0, size));
  }

  if (avoidFewElementsInLast) {
    // add the content of the last chunk(z) to the one before(y) if length < size

    let z = chunked_arr.slice(-1)[0] || [];
    let y = chunked_arr.slice(-2, -1)[0] || [];
    let rest = chunked_arr.slice(0, chunked_arr.length - 2);

    if (z?.length < size) {
      y = [...y, ...z];
      rest.push(y);
    } else {
      rest = [...rest, y, z];
    }

    return rest;
  } else {
    return chunked_arr;
  }
}


export function createGqlFilters({filterOnProject = null, filterOnProjectContribution = null, filterByExistingAttachment = null, filterByExistingProject = null, filterOnCreator = null}) {

  let gqlFilters = [];

  if (filterOnProjectContribution?.id) {
    gqlFilters.push(`hasProjectContribution.id:${filterOnProjectContribution.id}`);
  } else if (filterOnProject?.id) {
    gqlFilters.push(`hasProject.id:${filterOnProject?.id}`);
  }

  if (typeof filterByExistingAttachment === "boolean") {
    gqlFilters.push(`hasAttachment ${!!filterByExistingAttachment ? "=" : "!="} *`);
  }

  if (typeof filterByExistingProject === "boolean") {
    gqlFilters.push(`hasProject ${!!filterByExistingProject ? "=" : "!="} *`);
  }

  if (filterOnCreator?.id) {
    gqlFilters.push(`hasCreatorPerson.id:${filterOnCreator?.id}`);
  }

  return gqlFilters
}

//theme.typography.pxToRem
export function pxToRem(px) {
  return `${px * 1.375 / 22}rem`;
}
