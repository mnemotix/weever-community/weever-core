export const globalSx = {
  flexCenter: {
    display: "flex",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  chipsContainer: {
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
    "& > *": {
      margin: 1
    }
  },
  empty: {
    color: "text.emptyHint"
  },
  commaAfter: {
    marginRight: 1,
    "&:not(:last-of-type)::after": {
      content: '","'
    }
  },
  width100: {width: '100%'},

  imageCard: (theme) => ({
    flexShrink: 0,
    minWidth: theme.spacing(6),
    height: theme.spacing(12),
    marginRight: theme.spacing(0.5),
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center center",
    backgroundColor: theme.palette.grey[200],
    display: "flex",
  }),
  bigImageCard: (theme) => ({
    minWidth: `${theme.spacing(16)}px ! important`,
    height: `${theme.spacing(30)}px ! important`,
  }),
}

 