/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {useResponsive, useEnvironment, LanguageChooserMenu, getUserAuthenticationService} from "@synaptix/ui";

import {Helmet} from "react-helmet";
import {grey} from "@mui/material/colors";
import {
  Avatar,
  AppBar,
  Box,
  Badge,
  Button as MuiButton,
  Container,
  useScrollTrigger,
  Divider,
  IconButton,
  Menu,
  MenuItem,
  Slide,
  Toolbar,
  Typography
} from "@mui/material";

import {
  AccountCircle as AccountCircleIcon,
  Notifications as NotificationsIcon,
  CloudUpload as ImportIcon,
  Mood as MoodIcon,
  Visibility as BinIcon,
  FilterVintage as EditIcon,
  Explore as ExploreIcon,
  AccountTree as KonceptIcon
} from "@mui/icons-material";

import Config from "../../Config";
import {useApolloClient} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {Version} from "../widgets/Version";
import {ROUTES} from "../../routes";
import {AdminMenuLink} from "../../components/routes/Admin/AdminMenuLink";
import favicon from "../../../../assets/mnx-favicon.png";
import logo from "../../../../assets/mnx-logo.png";
import {VisualizationsMenu} from "../routes/Visualizations/VisualizationsMenu";

const classes = {
  toolbar: {
    paddingRight: "24px" // keep right padding when drawer closed
  },
  appBarSpacer: (theme) => ({
    height: theme.spacing(8)
  }),
  appBar: (theme) => ({
    zIndex: theme.zIndex.drawer + 1
  }),
  contentBody: (theme) => ({
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    minHeight: "90vh",
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      paddingLeft: `${theme.spacing(2)} ! important`,
      paddingRight: `${theme.spacing(2)} ! important`
    }
  }),
  footer: {
    width: "100%",
    padding: 1,
    marginTop: 6,
    background: grey["800"],
    color: "white"
  },
  version: {
    textAlign: "right",
    display: "block"
  },
  toolbarIcon: (theme) => ({
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  }),
  toolbarButton: {
    margin: 1
  },
  title: {
    flexGrow: 1
  },
  titleText: (theme) => ({
    fontVariant: "petite-caps",
    fontSize: theme.typography.pxToRem(26)
  }),
  logo: (theme) => ({
    width: theme.spacing(8),
    height: "auto",
    verticalAlign: "middle",
    padding: theme.spacing(1, 2, 1, 0)
  }),
  mood: (theme) => ({
    margin: theme.spacing(0, 1)
  }),
  avatar: (theme) => ({
    height: theme.spacing(4),
    width: theme.spacing(4)
  })
};

/**
 * @param {Component} [TitleComponent]
 * @param children
 * @return {*}
 * @constructor
 */
export function DefaultLayout({TitleComponent, children}) {
  const {t, i18n} = useTranslation();
  const publicExplorerEnabled = useEnvironment("PUBLIC_EXPLORER_ENABLED", {isBoolean: true});
  const konceptEnabled = useEnvironment("KONCEPT_ENABLED", {isBoolean: true});
  const filesBinDisabled = useEnvironment("FILES_BIN_DISABLED", {isBoolean: true});

  const apolloClient = useApolloClient();
  const {isTablet} = useResponsive();
  const [anchorEl, setAnchorEl] = useState(null);
  const userAuthenticationService = getUserAuthenticationService({apolloClient});
  let {user, isAdmin, isContributor, isEditor, isLogged} = userAuthenticationService.useLoggedUser();

  const {logout} = userAuthenticationService.useLogout();

  const trigger = useScrollTrigger({threshold: 0});

  const menuRoutes = [
    {
      to: formatRoute(ROUTES.IMPORT),
      icon: <ImportIcon />,
      label: t("APP_BAR.BUTTONS.IMPORT"),
      renderIf: isContributor
    },
    {
      to: formatRoute(ROUTES.BIN),
      icon: <BinIcon />,
      label: t("APP_BAR.BUTTONS.BIN"),
      renderIf: isContributor && !filesBinDisabled
    },
    {
      to: formatRoute(ROUTES.INDEX),
      icon: <EditIcon />,
      label: t("APP_BAR.BUTTONS.EDIT"),
      renderIf: isContributor
    },
    {
      to: formatRoute(ROUTES.KONCEPT),
      icon: <KonceptIcon />,
      label: t("APP_BAR.BUTTONS.KONCEPT"),
      renderIf: isContributor && konceptEnabled,
      target: "_blank"
    },
    {
      to: formatRoute(ROUTES.EXPLORE),
      icon: <ExploreIcon />,
      label: t("SIGN_IN.PUBLIC_MODE_ACCESS"),
      renderIf: publicExplorerEnabled,
      color: "secondary",
      target: "_blank"
    }
  ];

  return (
    <Box>
      <Slide appear={false} direction="down" in={!trigger}>
        <AppBar sx={classes.appBar}>
          <Toolbar sx={classes.toolbar}>
            <Typography component="h1" variant="h6" color="inherit" noWrap sx={classes.title}>
              <Link
                to={ROUTES.INDEX}
                style={{color: "inherit", textDecoration: "none", display: "flex", alignItems: "center"}}>
                {TitleComponent || (
                  <>
                    <Helmet titleTemplate={"%s - Weever"}>
                      <meta charSet="utf-8" />
                      <title>{t("APP_BAR.HOME")}</title>
                      <link rel="icon" type="image/png" href={favicon} sizes="32x32" />
                    </Helmet>
                    <Box component="img" sx={classes.logo} src={logo} alt={"Logo Mnémotix"} />
                    <Box component="span" sx={classes.titleText}>
                      Weever
                    </Box>
                  </>
                )}
              </Link>
            </Typography>

            {isTablet ? renderCompactMenu() : renderMenu()}

            <IconButton color="inherit">
              <Badge badgeContent={null} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>

            <If condition={isLogged}>
              <div>
                <IconButton color="inherit" onClick={(e) => setAnchorEl(e.currentTarget)}>
                  <Avatar src={user.avatar} sx={classes.avatar}>
                    <AccountCircleIcon />
                  </Avatar>
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  keepMounted
                  open={!!anchorEl}
                  onClose={() => setAnchorEl(null)}>
                  <MenuItem disabled key="WCMenuItem1">
                    {t("APP_BAR.MENU.HELLO")} {user.firstName} <MoodIcon sx={classes.mood} /> !
                  </MenuItem>

                  <MenuItem disabled dense key="WCMenuItem2">
                    <Choose>
                      <When condition={isAdmin}>{t("PROFIL.ACCESS_PRIVILEDGE_ADMIN")}</When>
                      <When condition={isEditor}>{t("PROFIL.ACCESS_PRIVILEDGE_EDITOR")}</When>
                      <When condition={isContributor}>{t("PROFIL.ACCESS_PRIVILEDGE_CONTRIBUTOR")}</When>
                    </Choose>
                  </MenuItem>

                  <MenuItem component={Link} to={formatRoute(ROUTES.PROFIL_EDIT)} key="WCMenuItem3">
                    {t("APP_BAR.MENU.MY_PROFILE")}
                  </MenuItem>

                  <If condition={isAdmin}>
                    <MenuItem key="WCMenuItem4">
                      <AdminMenuLink />
                    </MenuItem>
                    <MenuItem component={Link} to={formatRoute(ROUTES.ACTIVITY)}>
                      {t("ADMIN.MENU.ACTIVITY")}
                    </MenuItem>
                  </If>

                  <MenuItem key="WCMenuItem5">
                    <VisualizationsMenu />
                  </MenuItem>

                  <MenuItem
                    key="WCMenuItem6"
                    onClick={async () => {
                      await logout();
                      await apolloClient.clearStore();
                    }}>
                    {t("APP_BAR.MENU.SIGN_OUT")}
                  </MenuItem>
                </Menu>
              </div>
              <LanguageChooserMenu i18n={i18n} />
            </If>
          </Toolbar>
        </AppBar>
      </Slide>
      <Box component="main">
        <Box sx={classes.appBarSpacer} />
        <Container maxWidth="xl" sx={classes.contentBody}>
          {children}
        </Container>
        <Box component="footer" sx={classes.footer}>
          <Version sx={classes.version} />
        </Box>
      </Box>
    </Box>
  );

  function renderDivider(key) {
    return (
      <MenuItem disabled key={"wcdivider_" + key}>
        <Box style={{width: "100%"}}>
          <Divider />
        </Box>
      </MenuItem>
    );
  }

  function renderCompactMenu() {
    return menuRoutes
      .filter(({renderIf}) => renderIf)
      .map(({to, icon, label, color, target}, index) => (
        <IconButton
          key={index}
          color={color || "inherit"}
          sx={classes.toolbarButton}
          component={Link}
          to={to}
          target={target}>
          {icon}
        </IconButton>
      ));
  }

  function renderMenu() {
    return menuRoutes
      .filter(({renderIf}) => renderIf)
      .map(({to, icon, label, color, target}, index) => (
        <MuiButton
          key={index}
          color={color || "inherit"}
          startIcon={icon}
          sx={classes.toolbarButton}
          component={Link}
          to={to}
          target={target}>
          {label}
        </MuiButton>
      ));
  }
}
