import {useEffect} from "react";
import AnchorLink from "react-anchor-link-smooth-scroll";
import {Box} from "@mui/material";

// just display an horizontal time line with date, on clic scroll to anchor
// all elements from start to currentViewedId will be colored in green
export function HorizontalTimelineDate({timeline, currentViewedId, onDateClick}) {

  useEffect(() => {
    if (currentViewedId) {
      setTimeout(() => {
        var element = document.getElementById("link" + currentViewedId);

        if (element) {
          element.scrollIntoView({behavior: "smooth", block: "nearest", inline: "center"});
        }
      }, 250);
    }
  }, [currentViewedId]);

  /*
  in v1 ( loading all project contrib) , i used currentViewedIndex, now we used paginating loading so cannot use index anymore,
  i use currentViewId and found his index in all dates
  */
  let currentViewedIdIndex = timeline.findIndex(el => el["id"] === currentViewedId);

  return (
    <Box sx={classes.flexOverflowX}>
      {timeline.map(({id, page, index, render}) => {
        const viewed = currentViewedIdIndex >= index;

        return (
          <Box key={id} id={"link" + id} sx={classes.flexRCE}>
            {index > 0 && index < timeline.length && (
              <Box key={id + "separator"} sx={classes.flexColEnd}>
                <Box sx={[classes.verticalBar, viewed && classes.verticalBarColored]} />
              </Box>
            )}
            <Box sx={classes.flexColEnd} onClick={() => onDateClick(page, id)}>
              <AnchorLink
                offset="120"
                href={"#" + id}
                sx={[classes.noLinkStyle, classes.datePadding, viewed && classes.viewed]}>
                {viewed ? <b>{render}</b> : render}
              </AnchorLink>
            </Box>
          </Box>
        );
      })}
    </Box>
  );
}



const classes = {

  datePadding: {
    paddingLeft: 5,
    paddingRight: 5
  },
  noLinkStyle: {
    color: "inherit",
    textDecoration: "none",
    cursor: "pointer"
  },
  flexRCE: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center"
  },
  flexColEnd: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  verticalBar: {
    border: `1px solid rgba(0, 0, 0, 0.25)`,
    height: "1px",
    width: "50px",
    marginRight: 10,
    marginLeft: 10,
    marginBottom: "8px"
  },
  verticalBarColored: (theme) => ({
    border: `1px solid ${theme.palette.secondary.main}`
  }),
  flexOverflowX: {
    display: "flex",
    width: "100%",
    overflowX: "auto",
    padding: theme.spacing(1.5)
  },
  viewed: {
    color: "secondary.main"
  }
};
