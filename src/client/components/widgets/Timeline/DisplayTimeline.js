/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {useDebounce, useResponsive} from "@synaptix/ui";
import {Paper, Box, Typography} from "@mui/material";
import {Timeline, TimelineItem, TimelineSeparator, TimelineConnector, TimelineContent, TimelineOppositeContent, TimelineDot} from '@mui/lab';
import {Link} from "react-router-dom";
import {Waypoint} from "react-waypoint";
import {HorizontalTimelineDate} from "./HorizontalTimelineDate";
import {globalSx} from "../../layouts/styles";
import Config from "../../../Config";


/***
 * to preserve display of timeline add to each new project
 * export const theme = createMuiTheme({
    overrides: { MuiTimelineItem: { missingOppositeContent: { "&:before": { display: "none" } } } }
   }) 
 * already in core theme but need to be in extended package like re-source 
 */



/**
 *  display 2 things : 
 *  1) an horizontal chronological line with date (on click, scroll to corresponding element)
 *  2) corresponding elements to date, displayed vertically
 * 
 * @param horizontalElements : id & view for each date who are in horizontal timeline scroller 
 * @param horizontalElementSx : class to add sticky css for example
 * @param {[ {id,render} ] } verticalElements : id & view for each elements displayed vertically 
 * @param onDateClick : callback function use with a date is clicked to load data if necessary and go to corresponding anchor 
 * @param onScrollUp :  callback function called when scroll up, to check if data need to be loaded with pagination
 * @param onScrollDown : callback function called when scroll down, to check if data need to be loaded with pagination
 * @param publicMode : true/false
 */
export function DisplayTimeline({horizontalElements, horizontalElementSx, verticalElements, onDateClick, onScrollUp, onScrollDown, publicMode = false}) {
  const {isDesktop, isTablet} = useResponsive();

  // to track which contrib is on the top of viewable page, used to color current date in horizontal timeline
  const [currentViewedId, setCurrentViewedId] = useState(0);

  const [lastEntered, setLastEntered] = useState(0);
  const [lastLeaved, setLastLeaved] = useState(0);

  // use debounce to avoid crazy browser scrolling up / down
  // so debouncedSearchTerm get a new value after last setCurrentViewedId is set and keep same value for 500ms
  const debouncedCurrentID = useDebounce(currentViewedId, 300);

  function _renderContent({id, page, renderContent, disableLoadOnScroll}, index) {
    return (<Waypoint
      key={page + '+' + id}
      onEnter={() => {
        if (index < lastEntered) {
          // si on monte
          setCurrentViewedId(id);
        }
        setLastEntered(index);
      }}
      onLeave={() => {
        if (index > lastLeaved) {
          // si on descend si on n'est pas sur le dernier                 
          setCurrentViewedId(verticalElements[index + 1] ? verticalElements[index + 1].id : verticalElements[index].id);
          if (!disableLoadOnScroll) {
            onScrollDown(parseInt(page), index);
          }
        } else {
          // on monte 
          if (!disableLoadOnScroll) {
            onScrollUp(parseInt(page), index);
          }
        }
        setLastLeaved(index);
      }}
    >
      {renderContent}
    </Waypoint>)
  }

  return (
    <Box sx={classes.displayTimelineRoot}>

      {!publicMode &&
        <Paper sx={[horizontalElementSx, classes.paperBottomMargin]}>
          <HorizontalTimelineDate
            timeline={horizontalElements}
            currentViewedId={debouncedCurrentID}
            onDateClick={(page, id) => {
              onDateClick(page, id, () => {
                setCurrentViewedId(id);
              })
            }}
          />
        </Paper>
      }

      <Box sx={[isDesktop && publicMode && globalSx.flexCenter, globalSx.width100]}>
        <Timeline align={isDesktop && publicMode && !isTablet ? "alternate" : 'left'}
          sx={[
            isDesktop && publicMode ? globalSx.flexCenter : classes.displayTimelineRoot,
            publicMode ? fixWidthSx : globalSx.width100
          ]}>
          {verticalElements.map((item, index) => {
            const {renderItemTitle, generateItemRoute} = item;

            return (
              <TimelineItem key={index} sx={publicMode ? fixWidthSx : globalSx.width100}>
                {!!renderItemTitle && publicMode &&
                  <TimelineOppositeContent sx={timelineOppositeContentSx}>
                    <Typography variant="body2" color="textSecondary">
                      {renderItemTitle}
                    </Typography>
                  </TimelineOppositeContent>
                }
                <TimelineSeparator>
                  <TimelineDot />
                  <TimelineConnector />
                </TimelineSeparator>

                <TimelineContent sx={classes.timelineContent}>
                  {!publicMode && generateItemRoute &&
                    <Link to={generateItemRoute}>
                      <Typography variant="subtitle1" color="textSecondary">
                        {renderItemTitle}
                      </Typography>
                    </Link>}
                  {_renderContent(item, index)}
                </TimelineContent>
              </TimelineItem>
            )
          })}
        </Timeline>
      </Box>

    </Box>
  );
}

const classes = {
  displayTimelineRoot: {
    display: "flex",
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexDirection: "column"
  },
  timelineContent: {
    width: "50%"
  },
  paperBottomMargin: {
    marginBottom: "85px !important"
  },
 
  
};

const fixWidthSx = (theme) => ({
  width: "75%",
  [theme.breakpoints.down("md")]: {
    width: "100%"
  }
});
const timelineOppositeContentSx = (theme) => ({
  [theme.breakpoints.down(Config.theme.breakpoint)]: {
    flex: "unset !important"
  }
});