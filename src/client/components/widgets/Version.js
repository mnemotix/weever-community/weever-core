/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Package from "../../../../package.json";
import dayjs from "dayjs";

export function Version({className} = {}) {
  let version = Package.version;
  let date = version.match(/(2020[0-9]*)/);

  return (
    <span className={className}>
      <Choose>
        <When condition={!!date}>Dernière mise à jour le {dayjs(date[0]).format("L")}</When>
        <Otherwise>Version : {version} (Core)</Otherwise>
      </Choose>
    </span>
  );
}
