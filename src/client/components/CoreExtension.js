/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Extension} from "@synaptix/ui";

/**
 * Routes are code splitted
 */
import ResourceFormContentBatch from "./routes/Resource/ResourceFormContentBatch";
import {ProjectContributionExcerptInline} from "./routes/ProjectContribution/ProjectContributionExcerptInline";
import {ProjectExcerptInline} from "./routes/Project/ProjectExcerptInline";
import {OrganizationAffiliationExcerptInline} from "./routes/Agent/Affiliation/OrganizationAffiliationExcerptInline";
import {PersonAffiliationExcerptInline} from "./routes/Agent/Affiliation/PersonAffiliationExcerptInline";
import {ProjectContributionAttachmentExcerptInline} from "./routes/ProjectContribution/Attachments/ProjectContributionAttachmentExcerptInline";
import {ProjectContributionInvolvementExcerptInline} from "./routes/ProjectContribution/ProjectContributionInvolvements/ProjectContributionInvolvementExcerptInline";

export const coreExtension = new Extension({
  name: "CoreExtension",
  generateTopRoutes: ({isContributor} = {}) => <></>,
  fragments: {
    Resource: {
      FormContentBatch: {
        BeforeCommonFields: ResourceFormContentBatch
      }
    }
  },
  settings: {
    "PROJECT.ACTIVITY.COMPONENT_MAPPING": {
      PROJECT_CONTRIBUTIONS: ProjectContributionExcerptInline,
      PARENT_PROJECT: ProjectExcerptInline,
      INVOLVEMENTS: ProjectContributionInvolvementExcerptInline
    },
    "PROJECT_CONTRIBUTION.ACTIVITY.COMPONENT_MAPPING": {
      ATTACHMENTS: ProjectContributionAttachmentExcerptInline,
      PROJECTS: ProjectExcerptInline,
      REPLIES: ProjectContributionExcerptInline,
      INVOLVEMENTS: ProjectContributionInvolvementExcerptInline
    },
    "PERSON.ACTIVITY.COMPONENT_MAPPING": {
      AFFILIATIONS: OrganizationAffiliationExcerptInline
    },
    "ORGANIZATION.ACTIVITY.COMPONENT_MAPPING": {
      AFFILIATIONS: PersonAffiliationExcerptInline
    }
  }
});
