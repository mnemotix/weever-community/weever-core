/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Dashboard} from "@uppy/react";
import {useEnvironment} from "@synaptix/ui";

import "@uppy/core/dist/style.css";
import "@uppy/dashboard/dist/style.css";
import "@uppy/webcam/dist/style.css";
import "@uppy/url/dist/style.css";
import "@uppy/image-editor/dist/style.css";
/**
 * @param {object} uploadProvider
 * @param {boolean} [hideUploadButton]
 * @param {*} [width="100%"]
 * @param {*} [height=500]
 */
export function FileUploadDashboard({uploadProvider, hideUploadButton, width = "100%", height = 500} = {}) {
  let plugins = ["Webcam", "ImageEditor"];

  if (useEnvironment("COMPANION_GOOGLE_DRIVE_ENABLED", {isBoolean: true})) {
    plugins.push("GoogleDrive");
  }

  if (useEnvironment("COMPANION_REMOTE_URL_ENABLED", {isBoolean: true})) {
    plugins.push("Url");
  }

  return (
    <Dashboard
      uppy={uploadProvider}
      hideUploadButton={hideUploadButton}
      plugins={plugins}
      width={width}
      height={height}
      metaFields={[{id: "name", name: "Name", placeholder: "File name"}]}
    />
  );
}
