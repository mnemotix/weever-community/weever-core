/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect, useState} from "react";
import {getImageThumbnail, ProjectsAutocomplete, useResponsive, Button, useLocalStorage, useEnvironment, LoadingSplashScreen} from "@synaptix/ui";
import {nanoid} from "nanoid";
import {useTranslation} from "react-i18next";
import {useLazyQuery, useMutation} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {useSnackbar} from "notistack";
import {Box, Button as MuiButton, Grid, ImageList, ImageListItem, ImageListItemBar, IconButton, Typography} from "@mui/material";
import {Close as CloseIcon} from "@mui/icons-material";
import {Link, useParams} from "react-router-dom";

import {ROUTES} from "../../../../routes";
import Projects from "../../Project/Projects";
import {gqlFeaturesProjects} from "./gql/FeaturedProjects.gql";
import {gqlCreateProjectResourcesMutation} from "./gql/CreateProjectResourcesMutation.gql";
import {FileUploadDashboard} from "./FileUploadDashboard";
import {useUploadService} from "./UploadService";

const sxClasses = {
  heading: {
    marginBottom: 2
  },
  createButtonContainer: {
    textAlign: "right"
  },
  width100: {
    width: "100%"
  },
  actions: {
    marginTop: 2,
    textAlign: "center",
    "& > *": {
      marginRight: 1
    }
  },
  successMessage: {
    textAlign: "center",
    paddingLeft: 1
  },
  projectSuggestionTile: {
    cursor: "pointer",
    opacity: 0.7,
    transition: "all 0.25s",
    "&:hover": {
      opacity: 1
    }
  }
}


export default function Import({uploadEndpoint, companionEndpoint}) {
  const classes = useStyles();
  const {isDesktop} = useResponsive();
  let {id: forcedProjectId} = useParams();
  if (forcedProjectId) {
    forcedProjectId = decodeURIComponent(forcedProjectId);
  }
  const {t} = useTranslation();
  const {enqueueSnackbar, closeSnackbar} = useSnackbar();
  const [showUploadDashboard, setShowUploadDashboard] = useState(true);
  const [project, setProject] = useState(null);
  const [featuredProjects, setFeaturedProjects] = useState([]);
  const [uploadTag] = useState(nanoid());
  const [createProjectResources, {loading: creatingResources}] = useMutation(gqlCreateProjectResourcesMutation, {
    onCompleted: () => {
      setShowUploadDashboard(false);
    }
  });
  const [successSnackbarId, setSuccessSnackbarId] = useState(null);
  const [getFeaturedProjects, {data: featuredProjectsData}] = useLazyQuery(gqlFeaturesProjects);
  const {localStorage} = useLocalStorage();
  const defaultAccessPolicyId = useEnvironment("DEFAULT_POLICY_ID");

  useEffect(() => {
    (async () => {
      if (localStorage) {
        let storedFeaturedProjects = (await localStorage.getItem("featuredProjects")) || [];

        // This is the case where project id is passed in URI parameter.
        if (forcedProjectId) {
          storedFeaturedProjects = storedFeaturedProjects.filter((project) => project.id !== forcedProjectId);
          storedFeaturedProjects.unshift({id: forcedProjectId});
        }

        getFeaturedProjects({
          variables: {
            ids: storedFeaturedProjects.map(({id}) => id)
          }
        });
      }
    })();
  }, [localStorage]);

  useEffect(() => {
    return () => {
      if (successSnackbarId) {
        closeSnackbar(successSnackbarId);
      }
    };
  }, [successSnackbarId]);

  useEffect(() => {
    (async () => {
      const featuredProjects = featuredProjectsData?.projects.edges.map(({node: project}) => project);

      if (featuredProjects?.length > 0) {
        setProject(featuredProjects[0]);
      }

      if (featuredProjects?.length > 1) {
        setFeaturedProjects(featuredProjects.slice(1));
      }

      await localStorage?.setItem("featuredProjects", featuredProjects);
    })();
  }, [featuredProjectsData]);

  const {uploadProvider, waitingFiles} = useUploadService({
    endpoint: uploadEndpoint,
    companionEndpoint,
    disableResourceCreation: true
  });

  function clearSelected() {
    setProject(null);
  }

  return (
    <Choose>
      <When condition={!uploadProvider}>
        <LoadingSplashScreen />
      </When>
      <When condition={!project}>
        <Projects displayMode="grid" showSearchBar={true} onSelectProject={handleSelectProject} />
      </When>
      <Otherwise>
        <Box sx={sxClasses.heading}>
          <Grid container spacing={5} justifyContent="space-between" alignItems={"center"} direction="row-reverse">
            <Grid item xs={12} md={2} sx={sxClasses.createButtonContainer}>
              <MuiButton
                variant="contained"
                color="primary"
                component={Link}
                to={{
                  pathname: formatRoute(ROUTES.PROJECT_CREATE),
                  state: {redirectToImportAfterCreation: true}
                }}>
                {t("PROJECT.CREATE")}
              </MuiButton>
            </Grid>
            {isDesktop && (
              <Grid item xs={12} md>
                <If condition={featuredProjects.length > 0}>
                  <Typography variant="subtitle1">{t("IMPORT.FEATURES_PROJECTS")}</Typography>
                </If>
              </Grid>
            )}
            <Grid item xs={12} md container alignItems={"center"} spacing={2}>
              <Grid item xs>
                <ProjectsAutocomplete
                  selectedObject={project}
                  placeholder={!!project ? t("PROJECT.AUTOCOMPLETE.CHOOSE") : null}
                  onSelect={handleSelectProject}
                />
              </Grid>

              <If condition={project}>
                <Grid item xs>
                  <MuiButton variant="outlined" onClick={clearSelected}>
                    {t("IMPORT.RESET_PROJECTS")}
                  </MuiButton>
                </Grid>
              </If>
            </Grid>
          </Grid>

          <Grid container spacing={5} alignItems={"center"}>
            <Grid item xs={12} md={4}>
              <ImageList sx={sxClasses.width100} gap={0} cols={1} rowHeight={150}>
                {renderTile({project, cols: 1})}
              </ImageList>
            </Grid>
            {isDesktop && (
              <Grid item xs={8}>
                <ImageList sx={sxClasses.width100} gap={8} cols={5} rowHeight={125}>
                  {featuredProjects.map((project) => renderTile({project, isSuggestionTile: true}))}
                </ImageList>
              </Grid>
            )}
          </Grid>
        </Box>

        <FileUploadDashboard hideUploadButton={true} uploadProvider={uploadProvider} />

        <Box sx={sxClasses.actions}>
          <Choose>
            <When condition={showUploadDashboard}>
              <Button
                disabled={waitingFiles.length === 0}
                loading={creatingResources}
                variant="contained"
                color="primary"
                onClick={handleUpload}>
                {creatingResources ? t("UPLOAD.PROCESSING") : t("UPLOAD.START")}
              </Button>
            </When>

            <Otherwise>
              <MuiButton
                variant="contained"
                color="primary"
                onClick={() => {
                  uploadProvider.reset();
                  setShowUploadDashboard(true);
                }}>
                {t("UPLOAD.ADD_MORE")}
              </MuiButton>

              <MuiButton color="primary" component={Link} to={ROUTES.BIN}>
                {t("APP_BAR.BUTTONS.BIN")}
              </MuiButton>
            </Otherwise>
          </Choose>
        </Box>
      </Otherwise>
    </Choose>
  );

  /**
   * Handler to start the upload
   */
  async function handleUpload() {
    const {successful, error} = (await uploadProvider.upload()) || {};

    if (!successful) {
      return;
    }

    await createProjectResources({
      variables: {
        projectId: project.id,
        resourceInputs: successful.map((file) => ({
          inheritedTypename: "File",
          publicUrl: file.uploadURL,
          filename: file.name,
          mime: file.type,
          size: file.size,
          uploadTag,
          ...(defaultAccessPolicyId
            ? {
              accessPolicyInput: {
                id: defaultAccessPolicyId
              }
            }
            : {})
        }))
      }
    });

    if (successSnackbarId) {
      closeSnackbar(successSnackbarId);
    }

    const snackbarId = enqueueSnackbar(
      <Box sx={sxClasses.successMessage}>
        <div>{t("UPLOAD.SUCCESS_MESSAGE")}</div>
        <div>{t("UPLOAD.SUCCESS_DETAILS", {count: successful.length})}</div>
      </Box>,
      {
        variant: "success",
        persist: true,
        anchorOrigin: {
          vertical: "bottom",
          horizontal: "left"
        },
        action: (snackbarId) => (
          <>
            <MuiButton color="primary" component={Link} to={ROUTES.BIN}>
              {t("APP_BAR.BUTTONS.BIN")}
            </MuiButton>
            <IconButton onClick={() => closeSnackbar(snackbarId)} >
              <CloseIcon />
            </IconButton>
          </>
        )
      }
    );

    setSuccessSnackbarId(snackbarId);
    setShowUploadDashboard(false);
  }

  function renderTile({project, cols, isSuggestionTile}) {
    return (
      <ImageListItem
        key={project.id}
        sx={{tile: isSuggestionTile && classes.projectSuggestionTile}}
        cols={cols}
        {...(isSuggestionTile ? {onClick: () => handleSelectProject(project)} : null)}>
        <Choose>
          <When condition={project.image}>
            <img
              src={getImageThumbnail({
                imageUrl: project.image,
                size: "small"
              })}
              alt={project.title}
            />
          </When>
          <Otherwise>
            <Box
              sx={{
                position: "absolute",
                width: "100%",
                height: "100%",
                backgroundColor: project.color || "rgba(224,224,224,0.7)"
              }}
            />
          </Otherwise>
        </Choose>
        <ImageListItemBar title={project.title} />
      </ImageListItem>
    );
  }

  /**
   * Handler after project selection.
   *
   * - Update local state
   * - Save it in localStorage
   *
   * @param {object} project
   */
  async function handleSelectProject(project) {
    let featuredProjects = (await localStorage?.getItem("featuredProjects")) || [];
    featuredProjects = featuredProjects.filter((featuredProject) => featuredProject.id !== project.id);
    featuredProjects.unshift(project);
    await localStorage?.setItem("featuredProjects", featuredProjects.slice(0, 4));
    setFeaturedProjects(featuredProjects.slice(1));
    setProject(project);
  }
}
