/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect, useMemo, useState} from "react";
import {useEnvironment} from "@synaptix/ui";
import {Uppy, UIPlugin} from "@uppy/core";
import Tus from "@uppy/tus";
import Webcam from "@uppy/webcam";
import GoogleDrive from "@uppy/google-drive";
import ImageEditor from "@uppy/image-editor";
import Url from "@uppy/url";
import locale from "@uppy/locales/lib/fr_FR";
import {gql, useMutation} from "@apollo/client";
import {useTranslation} from "react-i18next";


export const CreateResourceMutation = gql`
  mutation CreateResourceMutation($resourceInput: FileInput!) {
    createFile(input: {objectInput: $resourceInput}) {
      createdObject {
        id
      }
    }
  }
`;

class PostProcessPlugin extends UIPlugin {
  constructor(uppy, opts) {
    super(uppy, opts);
    this.id = "PostProcessPlugin";
    this.type = "postprocessupload";
    this.postProcessFunction = opts.postProcessFunction;
  }

  install() {
    this.uppy.addPostProcessor(this.postProcessFunction);
  }

  uninstall() {
    this.uppy.removePostProcessor(this.postProcessFunction);
  }
}

function getUploadProvider({
  meta,
  restrictions,
  autoProceed,
  endpoint,
  companionEndpoint,
  googleDriveEnabled,
  remoteUrlEnabled
}) {
  let uploadProvider = new Uppy({
    meta,
    restrictions,
    autoProceed,
    locale
  });

  uploadProvider.use(Tus, {
    endpoint: endpoint || "/files",
    limit: 3, //Limit parallel upload to 3
    chunkSize: 512e3 // Limit chunk size to 512KB
  });
  uploadProvider.use(Webcam, {
    modes: ["video-audio", "video-only", "audio-only", "picture"]
  });

  if (companionEndpoint && googleDriveEnabled) {
    uploadProvider.use(GoogleDrive, {
      companionUrl: companionEndpoint
    });
  }

  if (companionEndpoint && remoteUrlEnabled) {
    uploadProvider.use(Url, {
      companionUrl: companionEndpoint
    });
  }

  uploadProvider.use(ImageEditor, {
    id: "ImageEditor",
    quality: 0.8
  });

  return uploadProvider;
}

/**
 * @param meta
 * @param restrictions
 * @param autoProceed
 * @param endpoint
 * @param {function} [getExtraResourceCreationInput]
 * @param {boolean} [disableResourceCreation]
 * @param {number} maxNumberOfFiles
 * @param {array} allowedFileTypes
 * @param {string} [companionEndpoint]
 * @param {boolean} [googleDriveEnabled]
 * @param {boolean} [remoteUrlEnabled]
 * @return {{uploadedFiles: String[], uploadProvider: Uppy.Uppy<Uppy.TypeChecking>}}
 */
export function useUploadService({
  meta,
  restrictions,
  autoProceed,
  endpoint,
  getExtraResourceCreationInput = () => {},
  disableResourceCreation,
  maxNumberOfFiles,
  allowedFileTypes,
  companionEndpoint,
  googleDriveEnabled,
  remoteUrlEnabled
} = {}) {
  const [createResource] = useMutation(CreateResourceMutation);
  const {t} = useTranslation();

  if (!endpoint) {
    endpoint = useEnvironment("TUSD_ENDPOINT");
  }

  if (!companionEndpoint) {
    companionEndpoint = useEnvironment("COMPANION_ENDPOINT");
  }

  if (googleDriveEnabled == null) {
    googleDriveEnabled = useEnvironment("COMPANION_GOOGLE_DRIVE_ENABLED", {isBoolean: true});
  }

  if (remoteUrlEnabled == null) {
    remoteUrlEnabled = useEnvironment("COMPANION_REMOTE_URL_ENABLED", {isBoolean: true});
  }

  let [uploadedFiles, setUploadedFiles] = useState(() => []);
  let [failedFiles, setFailedFiles] = useState(() => []);
  let [waitingFiles, setWaitingFiles] = useState(() => []);
  let uploadProvider = useMemo(initUploadProvider, [
    maxNumberOfFiles,
    allowedFileTypes,
    restrictions,
    disableResourceCreation,
    autoProceed,
    endpoint,
    companionEndpoint,
    googleDriveEnabled,
    remoteUrlEnabled,
    JSON.stringify(getExtraResourceCreationInput())
  ]);

  if (uploadProvider) {
    useEffect(() => {
      uploadProvider.on("complete", handleComplete);
      uploadProvider.on("postprocess-complete", handleUploadSuccess);
      uploadProvider.on("upload-error", handleUploadError);
      uploadProvider.on("file-added", handleFileAdded);
      uploadProvider.on("file-removed", handleFileRemoved);

      return () => {
        uploadProvider.off("complete", handleComplete);
        uploadProvider.off("postprocess-complete", handleUploadSuccess);
        uploadProvider.off("file-added", handleFileAdded);
        uploadProvider.off("file-removed", handleFileRemoved);
      };
    });
  }

  return {waitingFiles, failedFiles, uploadedFiles, uploadProvider};

  function initUploadProvider() {
    let uploadProvider = getUploadProvider({
      meta,
      restrictions: {
        ...restrictions,
        maxNumberOfFiles,
        allowedFileTypes
      },
      autoProceed,
      endpoint,
      companionEndpoint,
      googleDriveEnabled,
      remoteUrlEnabled
    });

    uploadProvider.use(PostProcessPlugin, {
      postProcessFunction: handleCreateResource
    });

    return uploadProvider;
  }

  function handleComplete({successful}) {
    setWaitingFiles([]);
    if (disableResourceCreation) {
      setUploadedFiles(successful);
    }
  }

  function handleUploadSuccess(file) {
    setUploadedFiles([].concat(uploadedFiles, [file]));
  }

  function handleUploadError(file) {
    setFailedFiles([].concat(failedFiles, [file]));
  }

  function handleFileAdded(file) {
    setWaitingFiles(waitingFiles.concat([file]));
  }

  function handleFileRemoved(file) {
    let indexOfFile = waitingFiles.indexOf(file);
    if (indexOfFile >= 0) {
      waitingFiles.splice(indexOfFile, 1);
      setWaitingFiles(waitingFiles);
    }
  }

  function handleCreateResource(fileIDs) {
    return Promise.all(
      fileIDs.map(async (fileID) => {
        const file = uploadProvider.getFile(fileID);
        uploadProvider.emit("postprocess-progress", file, {
          mode: "indeterminate",
          message: t("UPLOAD.PROCESSING")
        });

        let success = false;

        if (disableResourceCreation) {
          success = true;
        } else {
          try {
            if (file.uploadURL) {
              let {data} = await createResource({
                variables: {
                  resourceInput: {
                    publicUrl: file.uploadURL,
                    filename: file.name,
                    mime: file.type,
                    size: file.size,
                    ...getExtraResourceCreationInput()
                  }
                }
              });
              file.createdResource = data.createFile.createdObject;
              success = true;
            }
          } catch (e) {}
        }

        if (success) {
          uploadProvider.emit("postprocess-complete", file);
        } else {
          uploadProvider.emit("upload-error", file, new Error(t("UPLOAD.POSTPROCESS_ERROR")));
        }
      })
    );
  }
}
