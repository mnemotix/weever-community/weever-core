/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {LoadingSplashScreen,useEnvironment, Button} from '@synaptix/ui';
import {nanoid} from "nanoid";
import {FileUploadDashboard} from "./FileUploadDashboard";
import {useTranslation} from "react-i18next";
import {Button as MuiButton, DialogContent, DialogActions, Dialog} from "@mui/material";

import {useUploadService} from "./UploadService"; 
import {useMutation} from "@apollo/client";
import {gqlCreateResourceMutation} from "./gql/CreateResourceMutation.gql";
import {gqlCreateProjectResourcesMutation} from "./gql/CreateProjectResourcesMutation.gql";
import {gqlCreateProjectContributionAttachmentsMutation} from "./gql/CreateProjectContributionResourcesMutation.gql";

/**
 * @param {boolean} open
 * @param {string} [uploadEndpoint]
 * @param {string} [companionEndpoint]
 * @param {string} [attachToProjectId]
 * @param {string} [attachToProjectContributionId]
 * @param {function} onClose
 * @param {function} onResourceCreated
 */
export default function ImportDialog({
  open,
  uploadEndpoint,
  companionEndpoint,
  attachToProjectId,
  attachToProjectContributionId,
  onClose = () => { },
  onResourceCreated = () => { }
} = {}) {
  const {t} = useTranslation();
  const [uploadTag] = useState(nanoid());
  const [createResource, {loading: processingCreateResource}] = useMutation(gqlCreateResourceMutation);
  const [createProjectResources, {loading: processingcreateProjectResources}] = useMutation(
    gqlCreateProjectResourcesMutation
  );
  const [createProjectContributionAttachments, {loading: processingCreateProjectContributionAttachments}] = useMutation(
    gqlCreateProjectContributionAttachmentsMutation
  );

  const processing =
    processingCreateResource || processingcreateProjectResources || processingCreateProjectContributionAttachments;

  const [remainingCount, setRemaningCount] = useState(0);
  const {uploadProvider, waitingFiles} = useUploadService({
    endpoint: uploadEndpoint,
    companionEndpoint,
    disableResourceCreation: true
  });
  const defaultAccessPolicyId = useEnvironment("DEFAULT_POLICY_ID");

  return (
    <Dialog open={open} onClose={onClose} maxWidth={"md"} fullWidth={true}>
      <DialogContent>
        <Choose>
          <When condition={!uploadProvider}>
            <LoadingSplashScreen />
          </When>
          <Otherwise>
            <FileUploadDashboard hideUploadButton={true} uploadProvider={uploadProvider} />
          </Otherwise>
        </Choose>
      </DialogContent>
      <DialogActions>
        <MuiButton onClick={onClose}>{t("ACTIONS.CANCEL")}</MuiButton>
        <Button
          disabled={waitingFiles.length === 0}
          loading={processing}
          variant="contained"
          color="primary"
          onClick={handleUpload}>
          {processingCreateResource ? t("UPLOAD.PROCESSING_COUNT", {count: remainingCount}) : t("UPLOAD.START")}
        </Button>
      </DialogActions>
    </Dialog>
  );

  /**
   * Handler to start the upload
   */
  async function handleUpload() {
    const {successful, error} = await uploadProvider.upload();
    let resourceInputs = [];
    for (let file of successful) {
      let resourceInput = {
        inheritedTypename: "File",
        publicUrl: file.uploadURL,
        filename: file.name,
        mime: file.type,
        size: file.size,
        uploadTag
      };

      if (defaultAccessPolicyId) {
        resourceInput.accessPolicyInput = {
          id: defaultAccessPolicyId
        };
      }

      resourceInputs.push(resourceInput);
    }

    if (attachToProjectContributionId) {
      await createProjectContributionAttachments({
        variables: {
          projectContributionId: attachToProjectContributionId,
          attachmentInputs: resourceInputs.map((resourceInput) => ({
            resourceInput
          }))
        }
      });
    } else if (attachToProjectId) {
      await createProjectResources({
        variables: {
          projectId: attachToProjectId,
          resourceInputs
        }
      });
    } else {
      for (let [index, resourceInput] of Object.entries(resourceInputs)) {
        setRemaningCount(resourceInputs.length - index);

        await createResource({
          variables: {
            resourceInput
          }
        });
      }
    }

    setTimeout(onResourceCreated, 1000);
    onClose();
    uploadProvider.reset();
  }
}
