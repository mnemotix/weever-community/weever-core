/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect, useState} from "react";
import {nanoid} from "nanoid";
import {useApolloClient} from "@apollo/client";
import {Close as CancelIcon, Pause as PauseIcon, PlayArrow as PlayIcon} from "@mui/icons-material";

import {useTranslation} from "react-i18next";
import {Grid, IconButton, LinearProgress, Paper, Button as MuiButton, Box} from "@mui/material";

import {FileUploadDashboard} from "./FileUploadDashboard";
import {useUploadService} from "./UploadService";
import {FileUploadItem} from "./FileUploadItem";
import {FileUploadGroup} from "./FileUploadGroup";
import {useEnvironment, Button, ProjectContributionsAutocomplete, ProjectsAutocomplete} from '@synaptix/ui';

import {
  gqlCreateProjectContribution,
  gqlUpdateProjectContribution,
  ProjectContributionDynamicForm
} from "../../ProjectContribution";

const classes = {
  root: (theme) => ({
    padding: theme.spacing(1, 0)
  }),
  pendingUploadMessage: {
    marginLeft: 2
  },
  inDialog: {
    height: "90vh !important"
  },
  dense: (theme) => ({
    height: [theme.spacing(64), "!important"]
  }),
  inFileDashboard: (theme) => ({
    height: "100% !important",
    minHeight: theme.spacing(62)
  }),
  importSelectorPanel: {
    padding: 2
  },
  fileDashboard: {
    maxHeight: "50vh",
    overflow: "auto"
  },
  uploadButtonContainer: {
    textAlign: "right"
  },
  unselectButton: {
    display: "block",
    marginTop: 1,
    textAlign: "center"
  }
};

/**
 * @param {string} [uploadEndpoint]
 * @param {string} [companionEndpoint]
 * @param {boolean} [forceProjectContribution]
 * @param {boolean} [inDialog]
 * @param {function} [onGroupUploadSuccess]
 * @returns {JSX.Element}
 * @constructor
 */
export default function Import({
  uploadEndpoint,
  companionEndpoint,
  forceProjectContribution,
  inDialog,
  onGroupUploadSuccess
}) {
  const {t} = useTranslation();
  const [files, setFiles] = useState(new Map());
  const [selectedFiles, setSelectedFiles] = useState(new Map());
  const [uploadGroups, setUploadGroups] = useState(new Map());
  const [selectedProjects, setSelectedProjects] = useState([]);
  const [selectedProjectContribution, setSelectedProjectContribution] = useState(forceProjectContribution);
  const [pause, setPause] = useState(false);
  const [editEventDialogOpen, setEditEventDialogOpen] = useState(false);

  const apolloClient = useApolloClient();

  const [uploadTag] = useState(nanoid());

  const defaultAccessPolicyId = useEnvironment("DEFAULT_POLICY_ID");

  const {uploadProvider} = useUploadService({
    endpoint: uploadEndpoint,
    companionEndpoint,
    disableResourceCreation: true
  });

  const fileUpdateEvents = ["upload-progress", "upload-success", "thumbnail:generated", "file-added"];


  useEffect(() => {
    fileUpdateEvents.map((fileUpdateEvent) => uploadProvider.on(fileUpdateEvent, handleFileUpdated));
    return () => {
      fileUpdateEvents.map((fileUpdateEvent) => uploadProvider.off(fileUpdateEvent, handleFileUpdated));
    };
  });

  const waitingFiles = new Map(files);

  waitingFiles.forEach(({meta}, id) => {
    if (meta.uploadGroupId) {
      waitingFiles.delete(id);
    }
  });

  const uploading = Object.values(uploadProvider?.getState()?.currentUploads || {}).length > 0;

  return (
    <Box sx={[classes.root, inDialog && classes.inDialog]}>
      <Grid container spacing={4}>
        <If condition={waitingFiles.size === 0 && (!inDialog || uploadGroups.size === 0)}>
          <Grid
            item
            xs={12}
            sx={[inDialog && classes.inDialog, uploadGroups.size > 0 && classes.dense]}>
            <FileUploadDashboard uploadProvider={uploadProvider} hideUploadButton={true} height={inDialog ? "100%" : "85vh"} />
          </Grid>
        </If>

        <If condition={waitingFiles.size > 0}>
          <Grid
            container
            item
            xs={12}
            spacing={2}
            sx={[classes.fileDashboard, inDialog && classes.inDialog]}>
            {Array.from(waitingFiles.values()).map((file) => (
              <Grid key={file.id} item xs={12} sm={6} md={4} lg={3} xl={2}>
                <FileUploadItem
                  file={file}
                  checked={selectedFiles.has(file.id)}
                  uploading={uploading}
                  onRemoveFile={(file) => handleRemoveFiles([file])}
                  onToggleSelectFile={handleToggleSelectFile}
                  onFileUpdate={(file) => handleFileUpdated(file)}
                />
              </Grid>
            ))}
            <Grid item xs={12} sm={6} md={4} lg={3} xl={2}>
              <Box sx={classes.inFileDashboard}>
                <FileUploadDashboard uploadProvider={uploadProvider} hideUploadButton={true} />
              </Box>
            </Grid>
          </Grid>

          <Grid item xs={12}>
            <Paper variant="outlined" sx={classes.importSelectorPanel}>
              <Grid container spacing={3}>
                <Grid container item xs={12} spacing={2} alignItems={"flex-end"}>
                  <If condition={!forceProjectContribution?.id}>
                    <Grid item xs>
                      <ProjectsAutocomplete
                        multiple
                        placeholder={t("IMPORT.SELECT_PROJECTS")}
                        selectedObject={selectedProjects}
                        onSelect={setSelectedProjects}
                      />
                    </Grid>
                  </If>
                  <Grid item xs>
                    <ProjectContributionsAutocomplete
                      disabled={!!forceProjectContribution?.id || selectedProjects.length === 0}
                      placeholder={t("IMPORT.SELECT_EVENT")}
                      selectedObject={selectedProjectContribution}
                      onSelect={setSelectedProjectContribution}
                      projectId={JSON.stringify(selectedProjects.map(({id}) => id))}
                      creationEnabled
                      onCreate={handleCreateProjectContribution}
                    />
                  </Grid>
                </Grid>

                <Grid item xs={12} container spacing={2} alignItems="center" sx={classes.uploadButtonContainer}>
                  <Grid item xs />
                  <Grid item>
                    {t("IMPORT.FILES_SELECTED", {count: selectedFiles.size, context: `${selectedFiles.size}`})}
                  </Grid>
                  <Grid item>
                    <MuiButton
                      size="small"
                      variant="outlined"
                      onClick={() => {
                        setSelectedFiles(new Map(waitingFiles));
                      }}>
                      {t("IMPORT.SELECT_ALL_FILES")}
                    </MuiButton>
                    <If condition={selectedFiles.size > 0}>
                      <Box sx={classes.unselectButton}>
                        <MuiButton
                          size="small"
                          variant="outlined"
                          onClick={() => {
                            setSelectedFiles(new Map());
                          }}>
                          {t("IMPORT.UNSELECT_ALL_FILES")}
                        </MuiButton>
                      </Box>
                    </If>
                  </Grid>
                  <Grid item>
                    <Button
                      disabled={!(selectedFiles.size > 0 && selectedProjectContribution)}
                      variant="contained"
                      color="primary"
                      onClick={handleUpload}>
                      {uploading ? t("IMPORT.UPLOAD") : t("IMPORT.UPLOAD", {count: selectedFiles.size})}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </If>

        <If condition={uploading || (inDialog && uploadGroups.size > 0)}>
          <Grid item xs={12}  >
            <Paper variant="outlined">
              <LinearProgress
                variant="determinate"
                value={uploadProvider?.getState()?.totalProgress}
                color="secondary"
              />
              <Grid container wrap={"nowrap"} alignItems={"center"}>
                <Grid item xs sx={classes.pendingUploadMessage}>
                  {t(pause ? "IMPORT.PAUSE_MESSAGE" : "IMPORT.UPLOADING_MESSAGE", {
                    percentage: uploadProvider?.getState()?.totalProgress
                  })}
                </Grid>
                <Grid item>
                  <IconButton
                    onClick={() => {
                      if (pause) {
                        uploadProvider.resumeAll();
                      } else {
                        uploadProvider.pauseAll();
                      }
                      setPause(!pause);
                    }}>
                    {pause ? <PlayIcon /> : <PauseIcon />}
                  </IconButton>
                </Grid>
                <Grid item>
                  <IconButton
                    onClick={() => {
                      uploadProvider.cancelAll();
                      setPause(false);
                    }}>
                    <CancelIcon />
                  </IconButton>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </If>

        <If condition={uploadGroups.size > 0}>
          <Grid item xs={12}>
            {Array.from(uploadGroups.values()).map((uploadGroup) => {
              return (
                <FileUploadGroup
                  key={uploadGroup.id}
                  uploadGroup={uploadGroup}
                  files={files}
                  onRemoveFile={(file) => handleRemoveFiles([file])}
                  onToggleSelectFile={handleToggleSelectFile}
                  pause={pause}
                />
              );
            })}
          </Grid>
        </If>
      </Grid>

      <ProjectContributionDynamicForm
        projectContribution={selectedProjectContribution}
        fullForm
        formatMutationInput={({objectInput: projectContribution}) => {
          setEditEventDialogOpen(false);
          setSelectedProjectContribution(Object.assign({}, selectedProjectContribution, projectContribution));
        }}
        asDialog
        dialogOpen={editEventDialogOpen}
        onDialogClose={() => setEditEventDialogOpen(false)}
        dialogTitle={t("IMPORT.CREATE_EVENT")}
      />
    </Box>
  );

  function handleFileUpdated(updatedFile) {
    setFiles((files) => {
      const mutatedFiles = new Map(files);
      updatedFile = uploadProvider.getFile(updatedFile.id);
      mutatedFiles.set(updatedFile.id, updatedFile);
      return mutatedFiles;
    });

    if (inDialog) {
      setSelectedFiles((selectedFiles) => {
        const mutatedSelectedFiles = new Map(selectedFiles);
        const file = uploadProvider.getFile(updatedFile.id);
        mutatedSelectedFiles.set(file.id, file);
        return mutatedSelectedFiles;
      });
    }
  }

  function handleToggleSelectFile(file) {
    const mutatedSelectedFiles = new Map(selectedFiles);

    if (mutatedSelectedFiles.has(file.id)) {
      mutatedSelectedFiles.delete(file.id);
    } else {
      mutatedSelectedFiles.set(file.id, file);
    }

    setSelectedFiles(mutatedSelectedFiles);
  }

  function handleRemoveFiles(removingFiles) {
    const mutatedFiles = new Map(files);
    const mutatedSelectedFiles = new Map(selectedFiles);

    for (const file of removingFiles) {
      delete file.meta.uploadGroupId;

      if (mutatedFiles.has(file.id)) {
        mutatedFiles.delete(file.id);
      }

      if (mutatedSelectedFiles.has(file.id)) {
        mutatedSelectedFiles.delete(file.id);
      }

      uploadProvider.removeFile(file.id);
    }

    setFiles(mutatedFiles);
    setSelectedFiles(mutatedSelectedFiles);
  }

  function handleCreateProjectContribution(projectContribution) {
    setSelectedProjectContribution(projectContribution);
    setEditEventDialogOpen(true);
  }

  /**
   * Handler to start the upload
   */
  function handleUpload() {
    waitingFiles.forEach(({id}) => {
      uploadProvider.removeFile(id);
    });

    setTimeout(upload, 1000);
  }

  async function upload() {
    const mutatedUploadGroup = new Map(uploadGroups);
    const uploadGroupId = Date.now();
    const uploadGroup = {
      id: uploadGroupId,
      selectedProjects,
      selectedProjectContribution
    };

    Array.from(selectedFiles.values()).map((file) => {
      file.meta.uploadGroupId = uploadGroupId;
      delete file.preview;
      uploadProvider.addFile(file);
    });

    mutatedUploadGroup.set(uploadGroupId, uploadGroup);
    setUploadGroups(mutatedUploadGroup);

    setSelectedFiles(new Map());

    const {successful: uploadedFiles, failed: erroredFiles} = (await uploadProvider.upload()) || {};

    await finalizeUploadGroup({uploadedFiles, uploadGroup});
  }

  async function finalizeUploadGroup({uploadedFiles, uploadGroup}) {
    const {selectedProjects, selectedProjectContribution} = uploadGroup;
    const mutatedUploadGroup = new Map(uploadGroups);

    uploadGroup.finalizing = true;
    mutatedUploadGroup.set(uploadGroup.id, uploadGroup);
    setUploadGroups(mutatedUploadGroup);

    const {data: mutationResponse} = await apolloClient.mutate({
      mutation: selectedProjectContribution.id ? gqlUpdateProjectContribution : gqlCreateProjectContribution,
      variables: {
        input: {
          objectId: selectedProjectContribution.id,
          objectInput: prepareProjectContributionInput({selectedProjects, uploadedFiles, selectedProjectContribution})
        }
      }
    });

    if (!selectedProjectContribution.id) {
      selectedProjectContribution.id = mutationResponse.createProjectContribution.createdObject.id;
      setSelectedProjectContribution({...selectedProjectContribution});
    }

    completeUploadGroup({uploadGroup, selectedProjectContribution});
  }

  function completeUploadGroup({uploadGroup, selectedProjectContribution}) {
    const mutatedUploadGroup = new Map(uploadGroups);

    uploadGroup.complete = true;
    uploadGroup.selectedProjectContribution = selectedProjectContribution;
    mutatedUploadGroup.set(uploadGroup.id, uploadGroup);
    setUploadGroups(mutatedUploadGroup);

    if (onGroupUploadSuccess) {
      onGroupUploadSuccess(mutatedUploadGroup);
    }
  }

  function prepareProjectContributionInput({selectedProjects, uploadedFiles, selectedProjectContribution}) {
    let projectContributionInput = {
      projectInputs: selectedProjects.map((project) => ({
        id: project.id
      })),
      attachmentInputs: uploadedFiles.map((file) => {
        let taggingInputs;

        if (file.meta.concepts) {
          taggingInputs = file.meta.concepts.map(({id}) => ({
            conceptInput: {
              id
            }
          }));
        }
        return {
          resourceInput: {
            inheritedTypename: "File",
            publicUrl: file.uploadURL,
            filename: file.meta.customName || file.meta.name,
            mime: file.type,
            size: file.size,
            taggingInputs,
            uploadTag,
            ...(defaultAccessPolicyId
              ? {
                accessPolicyInput: {
                  id: defaultAccessPolicyId
                }
              }
              : {})
          }
        };
      })
    };

    if (!selectedProjectContribution.id) {
      projectContributionInput = Object.assign(projectContributionInput, selectedProjectContribution);
    }

    return projectContributionInput;
  }
}
