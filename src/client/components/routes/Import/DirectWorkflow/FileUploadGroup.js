import {useTranslation} from "react-i18next";
import {ExpandMore as ExpandMoreIcon} from "@mui/icons-material";

import {Grid, Accordion, AccordionSummary, AccordionDetails, Typography, Divider, Button as MuiButton} from "@mui/material";
import {Alert} from '@mui/material';
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";

import {FileUploadItem} from "./FileUploadItem";
import {CircularJauge} from "./CircularJauge";

import {ROUTES} from "../../../../routes";

const classes = {
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none"
  },
  uploadGroupHeading: (theme) => ({
    fontSize: theme.typography.pxToRem(18)
  }),
  uploadGroupSecondaryHeading: (theme) => ({
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  })
};

/**
 *
 */
export function FileUploadGroup({uploadGroup, files, onToggleSelectFile, onRemoveFile, pause} = {}) {

  const {t} = useTranslation();
  const {id, selectedProjectContribution, complete, finalizing} = uploadGroup;
  const groupFiles = Array.from(files.values()).filter(({meta}) => meta?.uploadGroupId === id);

  const groupPercentage = Math.floor(
    groupFiles.reduce((sum, {progress}) => {
      sum += progress.percentage;
      return sum;
    }, 0) / groupFiles.length
  );

  return (
    <Accordion defaultExpanded sx={classes.root}>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Grid container alignItems={"center"} spacing={2}>
          <Grid item xs>
            <Typography sx={classes.uploadGroupHeading}>
              {t("IMPORT.UPLOAD_GROUP_TITLE", {event: selectedProjectContribution.title})}
            </Typography>
          </Grid>
          <Grid item xs>
            <Typography sx={classes.uploadGroupSecondaryHeading}>
              {t("IMPORT.UPLOAD_GROUP_TITLE_SUBTITLE", {count: groupFiles.length})}
            </Typography>
          </Grid>
          <Grid item>
            <Choose>
              <When condition={complete}>{t("IMPORT.UPLOAD_GROUP_STATUS.COMPLETE")}</When>
              <When condition={finalizing}>{t("IMPORT.UPLOAD_GROUP_STATUS.FINALIZING")}</When>
              <When condition={pause}>{t("IMPORT.UPLOAD_GROUP_STATUS.PAUSE")}</When>
              <Otherwise>{t("IMPORT.UPLOAD_GROUP_STATUS.IMPORTING")}</Otherwise>
            </Choose>
          </Grid>
          <Grid item container alignItems={"center"} style={{width: "auto"}}>
            <CircularJauge percentage={groupPercentage} complete={complete} />
          </Grid>
        </Grid>
      </AccordionSummary>
      <Divider />
      <AccordionDetails>
        <Grid container spacing={2}>
          {groupFiles.map((file) => (
            <Grid key={file.id} item xs={2}>
              <FileUploadItem
                file={file}
                checked={true}
                onRemoveFile={onRemoveFile}
                onToggleSelectFile={onToggleSelectFile}
              />
            </Grid>
          ))}
          <If condition={complete}>
            <Grid item xs={12}>
              <Alert
                severity="success"
                action={
                  <MuiButton
                    color="inherit"
                    size="small"
                    component={Link}
                    target={"_blank"}
                    to={formatRoute(ROUTES.PROJECT_CONTRIBUTION, {id: selectedProjectContribution.id})}>
                    {t("IMPORT.UPLOAD_GROUP_SEE_PROJECT")}
                  </MuiButton>
                }>
                {t("IMPORT.UPLOAD_GROUP_SUCCESS")}
              </Alert>
            </Grid>
          </If>
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
}
