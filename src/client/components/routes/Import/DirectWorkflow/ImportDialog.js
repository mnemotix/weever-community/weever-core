/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Dialog, DialogContent} from "@mui/material";

import Import from "./Import";

/**
 * @param {boolean} open
 * @param {function} onClose
 * @param {string} [uploadEndpoint]
 * @param {string} [companionEndpoint]
 * @param {boolean} [forceProjectContribution]
 * @param {function} [onUploadSuccess]
 * @returns {JSX.Element}
 * @constructor
 */
export default function ImportDialog({
  open,
  onClose,
  forceProjectContribution,
  uploadEndpoint,
  companionEndpoint,
  onUploadSuccess
}) {
  return (
    <Dialog open={open} onClose={onClose} maxWidth={"xl"} fullWidth={true}>
      <DialogContent>
        <Import
          companionEndpoint={companionEndpoint}
          uploadEndpoint={uploadEndpoint}
          forceProjectContribution={forceProjectContribution}
          onGroupUploadSuccess={onUploadSuccess}
          inDialog={true}
        />
      </DialogContent>
    </Dialog>
  );
}
