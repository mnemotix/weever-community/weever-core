import {ConceptAutocomplete} from "@synaptix/ui";
import {Delete as RemoveIcon} from "@mui/icons-material";
import {useTranslation} from "react-i18next";
import {
  Box,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Checkbox,
  TextField,
  Grid
} from "@mui/material";
import numeraljs from "numeraljs";
import {CircularJauge} from "./CircularJauge";
import {getFileTypeIcon} from "./FileFormatIcons/getFileTypeIcon";

const classes = {
  media: {
    height: "250px",
    backgroundSize: "contain",
    backgroundColor: "grey[50]"
  },
  preview: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  previewIconContainer: {
    background: "white",
    height: "30%",
    width: "20%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "3px"
  }
};

const previewIconSx = (theme) => ({
  width: theme.spacing(4),
  height: theme.spacing(4),
  "& svg": {
    width: "100%",
    height: "100%"
  }
});

/**
 *
 */
export function FileUploadItem({file, checked, onRemoveFile, onToggleSelectFile, onFileUpdate = () => {}} = {}) {
  const {t} = useTranslation();
  const {preview, progress, meta} = file;
  return (
    <Card variant="outlined">
      <CardHeader
        avatar={
          <IconButton size="small" onClick={() => onRemoveFile(file)} disabled={!!progress.uploadComplete}>
            <RemoveIcon />
          </IconButton>
        }
        action={
          <Checkbox
            onChange={() => onToggleSelectFile(file)}
            checked={checked || !!file.meta?.uploadGroupId}
            disabled={!!file.meta?.uploadGroupId}
          />
        }
      />
      <Choose>
        <When condition={preview}>
          <CardMedia sx={classes.media} image={preview} title={meta.customName || meta.name} />
        </When>
        <Otherwise>{renderFilePreview()}</Otherwise>
      </Choose>

      <CardContent>
        <Grid container rowSpacing={2}>
          <Grid item xs={12}>
            <TextField
              size="small"
              name={"name"}
              value={meta.customName || meta.name}
              label={t("RESOURCE.FILE_NAME")}
              fullWidth
              disabled={!!progress.uploadStarted}
              onChange={(event) => {
                file.meta.customName = event.target.value;
                onFileUpdate(file);
              }}
              variant="standard"
            />
          </Grid>
          <Grid item xs={12}>
            <ConceptAutocomplete
              disabled={!!progress.uploadStarted}
              onSelectConcepts={(concepts) => {
                file.meta.concepts = concepts;
                onFileUpdate(file);
              }}
              selectedConcepts={meta.concepts || []}
            />
          </Grid>
        </Grid>
      </CardContent>
      <CardActions sx={(theme) => ({padding: theme.spacing(1, 2)})}>
        {numeraljs(file.size).format("0.0b")}
        <If condition={!!progress.uploadStarted}>
          <CircularJauge percentage={progress.percentage} complete={progress.uploadComplete} />
        </If>
      </CardActions>
    </Card>
  );

  function renderFilePreview() {
    const {color, Icon} = getFileTypeIcon(file.type);
    return (
      <Box sx={{...classes.media, ...classes.preview, background: color}}>
        <Box sx={classes.previewIconContainer}>
          <Icon sx={previewIconSx} />
        </Box>
      </Box>
    );
  }
}
