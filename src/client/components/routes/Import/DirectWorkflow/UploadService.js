/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect, useMemo, useState} from "react";
import {Uppy} from "@uppy/core";
import Tus from "@uppy/tus";
import Url from "@uppy/url";
import locale from "@uppy/locales/lib/fr_FR";
import {useTranslation} from "react-i18next";
import {useEnvironment} from "@synaptix/ui";

function getUploadProvider({meta, restrictions, autoProceed, endpoint, companionEndpoint}) {
  let uploadProvider = new Uppy({
    meta,
    restrictions,
    autoProceed,
    locale
  });

  uploadProvider.use(Tus, {
    endpoint: endpoint || "/files",
    limit: 3, //Limit parallel upload to 3
    chunkSize: 512e3 // Limit chunk size to 512KB
  });

  uploadProvider.use(Url, {
    companionUrl: companionEndpoint
  });

  return uploadProvider;
}

/**
 * @param meta
 * @param restrictions
 * @param autoProceed
 * @param endpoint
 * @param {function} [getExtraResourceCreationInput]
 * @param {boolean} [disableResourceCreation]
 * @param {number} maxNumberOfFiles
 * @param {array} allowedFileTypes
 * @param {string} [companionEndpoint]
 * @param {boolean} [googleDriveEnabled]
 * @param {boolean} [remoteUrlEnabled]
 * @return {{uploadedFiles: String[], uploadProvider: Uppy.Uppy<Uppy.TypeChecking>}}
 */
export function useUploadService({
  meta,
  restrictions,
  autoProceed,
  endpoint,
  getExtraResourceCreationInput = () => {},
  disableResourceCreation,
  maxNumberOfFiles,
  allowedFileTypes,
  companionEndpoint,
  remoteUrlEnabled
} = {}) {
  const {t} = useTranslation();

  if (!endpoint) {
    endpoint = useEnvironment("TUSD_ENDPOINT");
  }

  if (!companionEndpoint) {
    companionEndpoint = useEnvironment("COMPANION_ENDPOINT");
  }

  if (remoteUrlEnabled == null) {
    remoteUrlEnabled = useEnvironment("COMPANION_REMOTE_URL_ENABLED", {isBoolean: true});
  }

  let uploadProvider = useMemo(
    () =>
      getUploadProvider({
        meta,
        restrictions: {
          ...restrictions,
          maxNumberOfFiles,
          allowedFileTypes
        },
        autoProceed,
        endpoint,
        companionEndpoint,
        remoteUrlEnabled
      }),
    [
      maxNumberOfFiles,
      allowedFileTypes,
      restrictions,
      disableResourceCreation,
      autoProceed,
      endpoint,
      companionEndpoint,
      remoteUrlEnabled,
      JSON.stringify(getExtraResourceCreationInput())
    ]
  );

  if (uploadProvider) {
    useEffect(() => {
      uploadProvider.on("file-added", handleFileAdded);

      return () => {
        uploadProvider.off("file-added", handleFileAdded);
      };
    });
  }

  return {uploadProvider};

  function handleFileAdded(file) {
    uploadProvider.emit("thumbnail:request", file);
  }
}
