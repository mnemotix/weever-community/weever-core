
import {Check as SuccessIcon} from "@mui/icons-material";

import {useTranslation} from "react-i18next";
import {Box, CircularProgress, Typography} from "@mui/material";

const progressBackgroundSx = (theme) => ({
  height: [theme.spacing(4), "!important"],
  width: [theme.spacing(4), "!important"],
  color: theme.palette.grey[theme.palette.type === "light" ? 200 : 700]
})

const progressForegroundSx = (theme) => ({
  height: [theme.spacing(4), "!important"],
  width: [theme.spacing(4), "!important"],
  position: "absolute",
  left: 0
})

/**
 * @param {number} percentage completed %
 * @param {boolean} complete 
 */
export function CircularJauge({percentage, complete} = {}) {

  const {t} = useTranslation();

  return (
    <Box position="relative" display="inline-flex" sx={{position: "relative", marginLeft: "auto"}}>
      <CircularProgress
        variant="determinate"
        value={100}
        sx={progressBackgroundSx}
      />
      <CircularProgress
        variant="determinate"
        color={"secondary"}
        value={percentage}
        sx={progressForegroundSx}
      />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center">
        <Choose>
          <When condition={complete}>
            <SuccessIcon color="secondary" />
          </When>
          <Otherwise>
            <Typography
              sx={{
                fontSize: "0.55rem"
              }}
              variant="caption"
              component="div"
              color="textSecondary">{`${percentage}%`}</Typography>
          </Otherwise>
        </Choose>
      </Box>
    </Box>
  );
}
