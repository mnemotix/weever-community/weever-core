/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useState, useEffect} from "react";
import PropTypes from "prop-types";
import loadable from "@loadable/component";
import {useApolloClient} from "@apollo/client";
import {
  useEnvironment,
  useFloatingButtonDialContext,
  useResponsive,
  useExtensionFragment,
  useIsExtensionFragmentExists
} from "@synaptix/ui";
import {Button} from "@mui/material";
import {useTranslation} from "react-i18next";

const BinWorkflowImportDialog = loadable(() => import("./BinWorkflow/ImportDialog"));
const DirectWorkflowImportDialog = loadable(() => import("./DirectWorkflow/ImportDialog"));

/**
 *
 */
export function ImportDialog({...props} = {}) {
  const workflow = useEnvironment("FILES_UPLOAD_WORKFLOW");

  return workflow === "BIN_WORKFLOW" ? (
    <BinWorkflowImportDialog {...props} />
  ) : (
    <DirectWorkflowImportDialog {...props} />
  );
}

export function ImportDialogContainer({disabled = true, filterOnProject, filterOnProjectContribution, sx}) {
  const apolloClient = useApolloClient();
  const {t} = useTranslation();
  const [showImportDialog, setShowImportDialog] = useState(false);
  const {isDesktop} = useResponsive();

  const {contextFloatingButtonDialAddValues} = useFloatingButtonDialContext();
  useEffect(() => {
    if (!disabled && isDesktop) {
      contextFloatingButtonDialAddValues([
        {
          icon: "BackupIcon",
          locked: false,
          onClick: () => {
            setShowImportDialog(true);
          },
          display: "RESOURCE.UPLOAD"
        }
      ]);
    }
  }, [isDesktop]);

  return (
    <>
      <Button disabled={disabled} variant="contained" sx={sx} onClick={() => setShowImportDialog(true)}>
        {t("RESOURCE.UPLOAD")}
      </Button>
      {renderImportDialog()}
    </>
  );

  function renderImportDialog() {
    if (disabled) {
      return null;
    }

    if (useIsExtensionFragmentExists("ImportDialog")) {
      return useExtensionFragment("ImportDialog", {
        forceProjectContribution: filterOnProjectContribution,
        open: showImportDialog,
        onClose: () => {
          setShowImportDialog(false);
        },
        onUploadSuccess: () => {
          apolloClient.resetStore();
          setShowImportDialog(false);
        }
      });
    }
    return (
      <ImportDialog
        open={showImportDialog}
        onClose={() => {
          setShowImportDialog(false);
        }}
        onResourceCreated={() => {
          apolloClient.resetStore();
        }}
        attachToProjectId={filterOnProject?.id}
        attachToProjectContributionId={filterOnProjectContribution?.id}
        forceProjectContribution={filterOnProjectContribution}
        onUploadSuccess={() => {
          apolloClient.resetStore();
          setShowImportDialog(false);
        }}
      />
    );
  }
}

ImportDialogContainer.propTypes = {
  filterOnProject: PropTypes.object.isRequired,
  filterOnProjectContribution: PropTypes.object.isRequired,
  disabled: PropTypes.bool
};
