/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {RemoveEntityPanel, createLink, PageNotFound, LoadingSplashScreen} from "@synaptix/ui";
import {useParams, useHistory} from "react-router-dom";

import {Box, Breadcrumbs, Grid, Typography} from "@mui/material";
import {formatRoute} from "react-router-named-routes";
import {useSnackbar} from "notistack";
import {useTranslation} from "react-i18next";
import {useMutation, useLazyQuery} from "@apollo/client";

import {ROUTES} from "../../../routes";
import {gqlProject} from "./gql/Project.gql";
import {gqlCreateProject} from "./gql/CreateProject.gql";
import {gqlUpdateProject} from "./gql/UpdateProject.gql";
import {ProjectDynamicForm} from "./ProjectDynamicForm";
import {EntityAccessTargetsPanel} from "../Common/Entity/AccessTargets/EntityAccessTargetsPanel.js";

const classes = {
  adminContainer: {
    marginTop: 6
  }
};

export default function ProjectEdit({id} = {}) {
  const history = useHistory();
  const {t} = useTranslation();
  const params = useParams();
  const {enqueueSnackbar} = useSnackbar();

  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const [getProject, {data: {project} = {}, loading, called} = {}] = useLazyQuery(gqlProject);
  const [mutateProject, {loading: saving}] = useMutation(id ? gqlUpdateProject : gqlCreateProject, {
    onCompleted(data) {
      if (data?.createProject?.createdObject?.id) {
        id = data?.createProject.createdObject.id;
      }

      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});

      if (history.location?.state?.redirectToImportAfterCreation) {
        history.push(formatRoute(ROUTES.IMPORT, {id}));
      } else {
        history.push(formatRoute(ROUTES.PROJECT, {id}));
      }
    }
  });

  useEffect(() => {
    if (id) {
      getProject({
        variables: {
          id
        }
      });
    }
  }, [id]);

  if (id) {
    if (loading || !called) {
      return <LoadingSplashScreen />;
    }

    if (!project?.permissions?.update) {
      return <PageNotFound />;
    }
  }

  return (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <Breadcrumbs>
          {createLink({to: ROUTES.PROJECTS, text: t("BREADCRUMB.PROJECTS")})}
          <Typography color="textPrimary">{project ? project.title : t("PROJECT.CREATE_TITLE")}</Typography>
        </Breadcrumbs>
      </Grid>

      <Grid item xs={12}>
        <ProjectDynamicForm
          project={project}
          fullForm
          mutateFunction={mutateProject}
          saving={saving}
          renderRightExtraContent={renderRightExtraContent}
        />
      </Grid>
    </Grid>
  );

  function renderRightExtraContent() {
    return (
      <Grid item xs={12}>
        <If condition={project?.permissions?.delete}>
          <Box sx={classes.adminContainer}>
            <RemoveEntityPanel
              entity={project}
              panelTitle={t("PROJECT.ACTIONS.REMOVE.TITLE")}
              panelWarnMessage={t("PROJECT.ACTIONS.REMOVE.WARN_MESSAGE")}
              confirmMessage={t("PROJECT.ACTIONS.REMOVE.CONFIRM_MESSAGE", {project: project.title})}
              removeButtonLabel={t("PROJECT.ACTIONS.REMOVE.TITLE")}
              onSuccess={() => history.push(ROUTES.PROJECTS)}
              refetchQueriesAfterSuccess={["Projects"]}
            />
            <EntityAccessTargetsPanel
              entityId={project?.id}
              defaultExpanded
              panelTitle={t("PROJECT.ACTIONS.ACCESS_RIGHTS.TITLE")}
            />
          </Box>
        </If>
      </Grid>
    );
  }
}
