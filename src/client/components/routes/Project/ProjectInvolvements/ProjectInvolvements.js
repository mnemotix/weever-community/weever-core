/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ErrorBoundary, createLink, ExcerptPopover, PersonExcerpt, LoadingSplashScreen} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {List, ListItem, ListItemText, Box} from "@mui/material";

import {OrganizationExcerpt} from "../../../../components/routes/Organization/OrganizationExcerpt";
import {globalSx} from "../../../../components/layouts/styles";
import {ROUTES} from "../../../../routes";

import {gqlProjectInvolvements} from "./gql/ProjectInvolvements.gql";

const classes = {
  role: {
    marginRight: 1
  }
};

export function ProjectInvolvements(props) {
  return (
    <ErrorBoundary>
      <ProjectInvolvementsCode {...props} />
    </ErrorBoundary>
  );
}

function ProjectInvolvementsCode({projectId} = {}) {
  const {t} = useTranslation();

  const {data, loading} = useQuery(gqlProjectInvolvements, {
    variables: {
      projectId
    }
  });

  function renderItem({Component, id, route, name}) {
    return (<ExcerptPopover OnHoverDisplayComponent={<Component id={id} />}>
      {createLink({to: formatRoute(route, {id: id}), text: name})}
    </ExcerptPopover>)
  }

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <Choose>
      <When condition={data?.project.involvements.edges.length > 0}>
        <List dense disablePadding>
          {data?.project.involvements.edges.map(({node: involvement}, index) => (
            <ListItem key={index}>
              <ListItemText
                primary={<Box component="span" sx={classes.role}>{involvement.role || t("PROJECT.INVOLVEMENT_NO_ROLE")}</Box>}
                secondary={
                  <Choose>
                    <When condition={involvement.agent?.__typename === "Organization"}>
                      {renderItem({
                        Component: OrganizationExcerpt,
                        id: involvement.agent?.id,
                        imageUrl: involvement.agent?.avatar,
                        route: ROUTES.ORGANIZATION,
                        name: involvement.agent?.name,
                        role: involvement.role
                      })}
                    </When>
                    <Otherwise>
                      {renderItem({
                        Component: PersonExcerpt,
                        id: involvement.agent?.id,
                        imageUrl: involvement.agent?.avatar,
                        route: ROUTES.PERSON,
                        name: involvement.agent?.fullName,
                        role: involvement.role
                      })}
                    </Otherwise>
                  </Choose>
                }
              />
            </ListItem>
          ))}
        </List>
      </When>
      <Otherwise>
        <Box sx={globalSx.empty}>{t("PROJECT.NO_INVOLVEMENT")}</Box>
      </Otherwise>
    </Choose>
  );
}
