/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect} from "react";
import {LoadingSplashScreen, ErrorBoundary, ShowWhereIAM, PluralLinkField} from "@synaptix/ui";
import {useLazyQuery} from "@apollo/client";
import {ListItemText} from "@mui/material";
import {useTranslation} from "react-i18next";

import {ProjectInvolvementFormContent} from "./ProjectInvolvementFormContent";
import {gqlProjectInvolvements} from "./gql/ProjectInvolvements.gql";
import {projectInvolvementInputDefinition} from "./form/ProjectInvolvement.form";

export function ProjectInvolvementsEdit(props) {
  return (
    <ErrorBoundary>
      <ProjectInvolvementsEditCode {...props} />
    </ErrorBoundary>
  );
}
function ProjectInvolvementsEditCode({projectId} = {}) {
  const {t} = useTranslation();

  const [getProjectInvolvements, {data, loading}] = useLazyQuery(gqlProjectInvolvements);

  useEffect(() => {
    if (projectId) {
      getProjectInvolvements({
        variables: {
          projectId
        }
      });
    }
  }, [projectId]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <ShowWhereIAM path="ProjectInvolvementsEdit">
      <PluralLinkField
        data={data?.project}
        linkInputDefinition={projectInvolvementInputDefinition}
        renderObjectContent={(involvement) => (
          <ListItemText
            primary={
              involvement?.agent?.name || `${involvement?.agent?.firstName || ""} ${involvement?.agent?.lastName || ""}`
            }
            secondary={involvement?.role || t("AGENT.AFFILIATION_NO_ROLE")}
          />
        )}
        renderObjectForm={({object: involvement}) => <ProjectInvolvementFormContent involvement={involvement} />}
        addButtonLabel={t("ACTIONS.ADD_AGENT")}
      />
    </ShowWhereIAM>
  );
}
