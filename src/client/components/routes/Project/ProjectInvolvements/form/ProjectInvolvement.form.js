import {LinkInputDefinition, DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, string} from "yup";
import {personFormDefinition} from "../../../Person/form/Person.form";
import {organizationFormDefinition} from "../../../Organization/form/Organization.form";
import {gqlInvolvementFragment} from "../gql/ProjectInvolvements.gql";

export function getInvolvementValidationSchema({t}) {
  return object().shape({
    role: string().required(t("FORM_ERRORS.FIELD_ERRORS.ROLE_REQUIRED")),
    agent: object().required(t("FORM_ERRORS.FIELD_ERRORS.AGENT_REQUIRED"))
  });
}

export const involvementPersonLinkInputDefinition = new LinkInputDefinition({
  name: "agent",
  inputName: "agentInput",
  targetObjectFormDefinition: personFormDefinition,
  inputInheritedTypename: "Person"
});

export const involvementOrganizationLinkInputDefinition = new LinkInputDefinition({
  name: "agent",
  inputName: "agentInput",
  targetObjectFormDefinition: organizationFormDefinition,
  inputInheritedTypename: "Organization"
});

export const involvementFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["role"],
    gqlFragment: gqlInvolvementFragment,
    gqlFragmentName: "InvolvementFragment" // Precise it to avoid Apollo Error "Found 3 fragments. `fragmentName` must be provided when there is not exactly 1 fragment"
  }),
  validationSchema: getInvolvementValidationSchema
});

export const projectInvolvementInputDefinition = new LinkInputDefinition({
  name: "involvements",
  isPlural: true,
  inputName: "involvementInputs",
  targetObjectFormDefinition: involvementFormDefinition,
  nestedLinks: [involvementPersonLinkInputDefinition, involvementOrganizationLinkInputDefinition]
});
