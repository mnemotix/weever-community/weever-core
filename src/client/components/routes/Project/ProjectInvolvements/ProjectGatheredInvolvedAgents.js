/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {createLink, LoadingSplashScreen, ErrorBoundary, PersonExcerpt, ExcerptPopover} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {Box} from "@mui/material";
import {globalSx} from "../../../../components/layouts/styles";
import {ROUTES} from "../../../../routes";
import {OrganizationExcerpt} from "../../../../components/routes/Organization/OrganizationExcerpt";
import {gqlProjectGatheredInvolvedAgents} from "./gql/ProjectGatheredInvolvedAgents.gql";


/**
 *  don't add it for edition !
 * it just concat involvements in project's projectContributions
 * @param {*} projectId
 */
export function ProjectGatheredInvolvedAgents(props) {
  return (
    <ErrorBoundary>
      <ProjectGatheredInvolvedAgentsCode {...props} />
    </ErrorBoundary>
  );
}

function ProjectGatheredInvolvedAgentsCode({projectId} = {}) {
  const {t} = useTranslation();

  const {data, loading, fetchMore} = useQuery(gqlProjectGatheredInvolvedAgents, {
    variables: {
      projectId
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <Choose>
      <When condition={data?.project.gatheredInvolvedAgents.edges.length > 0}>
        {data?.project.gatheredInvolvedAgents.edges.map(({node}) => (
          <Box component="span" sx={globalSx.commaAfter} key={node.id}>
            <ExcerptPopover
              OnHoverDisplayComponent={
                node.__typename === "Person" ? <PersonExcerpt id={node.id} /> : <OrganizationExcerpt id={node.id} />
              }>
              {createLink({
                text: node.name || node.fullName || node.firstName || node.id,
                to: formatRoute(node.__typename === "Person" ? ROUTES.PERSON : ROUTES.ORGANIZATION, {id: node.id})
              })}
            </ExcerptPopover>
          </Box>
        ))}
      </When>
      <Otherwise>
        <Box sx={globalSx.empty}>{t("PROJECT.NO_INVOLVEMENT")} </Box>
      </Otherwise>
    </Choose>
  );
}
