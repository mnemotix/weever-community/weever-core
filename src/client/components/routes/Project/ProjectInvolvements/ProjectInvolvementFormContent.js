/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {oneOf} from "prop-types";
import {Grid, MenuItem, TextField as Select} from "@mui/material";
import {useTranslation} from "react-i18next";

import {LinkedObjectAutocompleteField} from "@synaptix/ui";
import {TextField} from "@synaptix/ui";
import {OrganizationsAutocomplete} from "../../Organization/OrganizationsAutocomplete";
import {PersonsAutocomplete} from "../../Person/PersonsAutocomplete";
import {OrganizationFormContent} from "../../Organization/OrganizationFormContent";
import {PersonFormContent} from "../../Person/PersonFormContent";
import {
  involvementPersonLinkInputDefinition,
  involvementOrganizationLinkInputDefinition
} from "./form/ProjectInvolvement.form";
import {useState} from "react";

export function ProjectInvolvementFormContent({sourceAgentType}) {
  const {t} = useTranslation();
  const [agentType, setAgentType] = useState(sourceAgentType || "Person");

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} md={3}>
          <Select
            select
            fullWidth
            value={agentType}
            onChange={(event) => setAgentType(event.target.value)}
            label={t("AGENT.AFFILIATION.AGENT_TYPE")}
            variant="standard">
            <MenuItem value={"Person"}>{t("TYPENAME.PERSON")}</MenuItem>
            <MenuItem value={"Organization"}>{t("TYPENAME.ORGANIZATION")}</MenuItem>
          </Select>
        </Grid>
        <Grid item xs={12} md={6}>
          <LinkedObjectAutocompleteField
            linkInputDefinition={
              agentType === "Person" ? involvementPersonLinkInputDefinition : involvementOrganizationLinkInputDefinition
            }
            creationEnabled
            renderObjectForm={() => (agentType === "Person" ? <PersonFormContent /> : <OrganizationFormContent />)}
            renderAutocomplete={({...props}) =>
              agentType === "Person" ? <PersonsAutocomplete {...props} /> : <OrganizationsAutocomplete {...props} />
            }
            required={true}
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <TextField required={true} name="role" label={t("AGENT.AFFILIATION.ROLE")} />
        </Grid>
      </Grid>
    </>
  );
}

ProjectInvolvementFormContent.propTypes = {
  sourceAgentType: oneOf(["Person", "Organization"])
};
