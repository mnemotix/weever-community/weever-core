import {Card, CardMedia, CardContent, Typography, Box} from "@mui/material";
import {AgentAvatar} from "../Explore/widgets/AgentAvatar";
import {getImageThumbnail} from "@synaptix/ui";

export function ProjectAvatar({
  project,
  imageVisible = true,
  creatorVisible = false,
  descriptionVisible = false,
  imageHeight = 100,
  titleVariant = "h5",
  descriptionVariant = "body2",
  sxClass
}) {
  const {image, color, creatorPerson, title, description, shortDescription} = project;

  return (
    <Card sx={sxClass}>
      <If condition={imageVisible}>
        <CardMedia
          component="img"
          image={image ? getImageThumbnail({imageUrl: image, size: "small"}) : null}
          height={imageHeight}
        />
      </If>
      <CardContent>
        <Box textAlign={"center"}>
          <Typography gutterBottom={descriptionVisible} variant={titleVariant} component="div">
            {title}
          </Typography>
        </Box>

        <If condition={descriptionVisible}>
          <Typography variant={descriptionVariant} color="textSecondary" component="div">
            {shortDescription || description}
          </Typography>
        </If>
      </CardContent>
      <If condition={creatorVisible && creatorPerson}>
        <AgentAvatar agent={creatorPerson} />
      </If>
    </Card>
  );
}
