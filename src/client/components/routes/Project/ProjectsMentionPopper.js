/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {getImageThumbnail, MentionPopperAC} from "@synaptix/ui";
import {Avatar, ListItemIcon, Typography} from "@mui/material";
import MenuBookIcon from "@mui/icons-material/MenuBook";

import {gqlProjectsAutocomplete} from "./gql/ProjectsAutocomplete.gql";

const classes = {
  avatar: (theme) => ({
    height: theme.spacing(2),
    width: theme.spacing(2),
    fontSize: 0.5 * theme.typography.fontSize
  }),
  listItemIcon: (theme) => ({
    minWidth: theme.spacing(2),
    marginRight: theme.spacing(1)
  })
};

/**
 * @param {string} qs
 * @param {function} onSelect
 */
export function ProjectsMentionPopper({qs, onSelect} = {}) {


  return (
    <MentionPopperAC
      qs={qs}
      gqlEntitiesQuery={gqlProjectsAutocomplete}
      gqlEntitiesConnectionPath={"projects"}
      gqlVariables={{
        first: 10
      }}
      renderEntity={project => (
        <>
          <ListItemIcon sx={classes.listItemIcon}>
            <Avatar
              src={getImageThumbnail({
                imageUrl: project.image,
                size: "small"
              })}
              sx={classes.avatar}
            >
              <MenuBookIcon />
            </Avatar>
          </ListItemIcon>
          <Typography variant="inherit" noWrap>
            {project.title}
          </Typography>
        </>
      )}
      onSelect={onSelect}
    />
  );
}
