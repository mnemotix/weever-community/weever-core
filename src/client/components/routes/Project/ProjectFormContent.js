/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Grid, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";

import {
  ColorPickerField,
  DatePickerField,
  ImagePickerField,
  TranslatableField,
  TextField,
  RichTextAreaField
} from "@synaptix/ui";

import {useUploadService} from "../Import/BinWorkflow/UploadService";
import {FileUploadDashboard} from "../Import/BinWorkflow/FileUploadDashboard";

/**
 * @param {function} [renderLeftExtraContent]
 * @param {function} [renderRightExtraContent]
 * @param {boolean} [singleColumn]
 */
export function ProjectFormContent({renderLeftExtraContent, renderRightExtraContent, singleColumn} = {}) {
  const {t} = useTranslation();
  return (
    <Grid container columnSpacing={singleColumn ? 3 : 8} alignItems="flex-start">
      <Grid item container xs={singleColumn ? 12 : 7} spacing={3}>
        <Grid item xs={12}>
          <TranslatableField required={true} FieldComponent={TextField} name="title" label={t("PROJECT.TITLE")} />
        </Grid>
        <Grid item xs={12}>
          <TranslatableField
            FieldComponent={RichTextAreaField}
            name="shortDescription"
            label={t("PROJECT.SHORT_DESCRIPTION")}
          />
        </Grid>
        <Grid item xs={12}>
          <TranslatableField FieldComponent={RichTextAreaField} name="description" label={t("PROJECT.DESCRIPTION")} />
        </Grid>

        <Grid item xs={12}>
          <ColorPickerField label={t("PROJECT.COLOR")} name="color" />
        </Grid>

        {renderLeftExtraContent?.()}
      </Grid>

      <Grid item container xs={singleColumn ? 12 : 5} spacing={3}>
        <Grid item xs={12}>
          <ImagePickerField
            name="image"
            label={t("PROJECT.IMAGE")}
            fullWidth={!singleColumn}
            useUploadService={useUploadService}
            FileUploadDashboard={FileUploadDashboard}
          />
        </Grid>
        <Grid item xs={12} container>
          <Grid item xs={12} md={6}>
            <DatePickerField name="startDate" label={t("PROJECT.START_DATE")} pickTime />
          </Grid>
          <Grid item xs={12} md={6}>
            <DatePickerField name="endDate" label={t("PROJECT.END_DATE")} pickTime />
          </Grid>
        </Grid>

        {renderRightExtraContent?.()}
      </Grid>
    </Grid>
  );
}
