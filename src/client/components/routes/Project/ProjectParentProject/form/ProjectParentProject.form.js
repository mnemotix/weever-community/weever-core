import {LinkInputDefinition} from "@mnemotix/synaptix-client-toolkit";
import {projectFormDefinition} from "../../form/Project.form";

/**
 * @type {LinkInputDefinition}
 */
export const projectParentProjectInputDefinition = new LinkInputDefinition({
  name: "parentProject",
  inputName: "parentProjectInput",
  targetObjectFormDefinition: projectFormDefinition,
  forceUpdateTarget: false
});