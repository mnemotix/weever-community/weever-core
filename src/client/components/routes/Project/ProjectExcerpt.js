import {useQuery} from "@apollo/client";
import {LoadingSplashScreen, Excerpt} from "@synaptix/ui";
import {gqlProject} from "./gql/Project.gql";

export function ProjectExcerpt({id}) {
  const {data, loading} = useQuery(gqlProject, {
    variables: {
      id
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <Excerpt
      picture={data?.project?.image}
      title={data?.project?.title}
      description={data?.project?.shortDescription || data?.project?.description}
      createdAt={data?.project?.createdAt}
      createdBy={data?.project?.createdBy}
    />
  );
}
