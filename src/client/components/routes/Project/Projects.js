/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../routes";
import {Box, Avatar, FormControlLabel, FormGroup, Switch, Typography} from "@mui/material";
import MenuBookIcon from "@mui/icons-material/MenuBook";

import {
  CollectionDataProvider,
  CollectionView,
  Button,
  useLoggedUser,
  useResponsive,
  createLink,
  getImageThumbnail,
  generateCollectionViewGenericColumns,
  EntityFormContentBatch,
  FloatingButton,
  useLocalStorage
} from "@synaptix/ui";

import {gqlProjects} from "./gql/Projects.gql";
import {gqlBatchUpdateProjects} from "./gql/BatchUpdateProject.gql";
import {projectFormDefinition} from "./form/Project.form";

const classes = {
  projectCell: {
    width: "25%",
    whiteSpace: "break-spaces"
  },
  subProjectsAction: {
    display: "inline-block",
    marginLeft: 2
  },
  subProjects: (theme) => ({
    width: theme.spacing(50),
    fontSize: theme.typography.caption.fontSize,
    marginTop: theme.spacing(0.5)
  })
};

// FIX Temporaire, je ne sais pas pourquoi gatherSubProjects reste à false dans getColumns > title > customBodyRender
// TODO
let _gatherSubProjects = false;

/**
 * @param qs
 * @param displayMode
 * @param redirectToImportAfterCreation
 * @param onSelectProject used in Bin & Import
 * @return {*}
 * @constructor
 */
export default function Projects({
  qs,
  displayMode: defaultDisplayMode,
  onSelectProject = null,
  redirectToImportAfterCreation
} = {}) {
  const {t} = useTranslation();
  const {localStorage} = useLocalStorage();
  const {isEditor, isAdmin} = useLoggedUser();
  const [gatherSubProjects, setGatherSubProjects] = useState(false);

  const [displayMode, setDisplayMode] = useState(defaultDisplayMode);
  const {isDesktop} = useResponsive();

  useEffect(() => {
    (async () => {
      if (localStorage) {
        if ((await localStorage.getItem("gatherSubProjects")) === true) {
          setGatherSubProjects(true);
          _gatherSubProjects = true;
        }

        if (!defaultDisplayMode) {
          let storedDisplayMode = await localStorage.getItem("projectsDisplayMode");

          if (storedDisplayMode) {
            setDisplayMode(storedDisplayMode);
          }
        }
      }
    })();
  }, [localStorage]);

  let availableDisplayModes = {};
  if (defaultDisplayMode) {
    availableDisplayModes[defaultDisplayMode] = {key: defaultDisplayMode};
  } else {
    availableDisplayModes = {
      table: {default: true},
      grid: {
        dataForView: (node) => node
      }
    };
  }

  const columns = getColumns({classes, t, isDesktop, gatherSubProjects});

  return (
    <>
      <CollectionDataProvider
        gqlConnectionPath={"projects"}
        gqlQuery={gqlProjects}
        gqlSortings={!!qs ? null : [{sortBy: "updatedAt", isSortDescending: true}]}
        gqlFilters={gatherSubProjects ? ["hasParentProject != *"] : null}
        gqlVariables={{gatherSubProjects}}
        qs={qs}
        searchEnabled={false}>
        <CollectionView
          collectionKey={"ProjectsList"}
          columns={columns}
          displayMode={displayMode}
          availableDisplayModes={availableDisplayModes}
          onDisplayModeChange={handleDisplayModeChange}
          onItemClick={onSelectProject}
          generateItemRoute={(node) => formatRoute(ROUTES.PROJECT, {id: node.id})}
          removalEnabled={false}
          renderRightSideActions={() => {
            return isDesktop ? (
              <Button
                disabled={!isEditor}
                to={{
                  pathname: formatRoute(ROUTES.PROJECT_CREATE),
                  state: {redirectToImportAfterCreation}
                }}>
                {t("PROJECT.CREATE")}
              </Button>
            ) : (
              <FloatingButton
                locked={!isEditor}
                to={{
                  pathname: formatRoute(ROUTES.PROJECT_CREATE),
                  state: {redirectToImportAfterCreation}
                }}
                name="AddIcon"
              />
            );
          }}
          renderLeftSideActions={() => {
            if (isDesktop) {
              return (
                <FormGroup sx={classes.subProjectsAction}>
                  <FormControlLabel
                    control={<Switch checked={gatherSubProjects} onChange={handleGatherSubProjects} value="checkedA" />}
                    label={t("SEARCH.COLUMNS.PROJECTS.GATHER_SUB_PROJECTS")}
                  />
                </FormGroup>
              );
            }
          }}
          batchEditEnabled={isEditor}
          batchEditButtonProps={{
            gqlUpdateMutation: gqlBatchUpdateProjects,
            entityFormDefinition: projectFormDefinition,
            entityFormContent: <EntityFormContentBatch formContentFragmentPrefix="Project" />,
            title: ({entityTotalCount}) => t("PROJECT.ACTIONS.BATCH_UPDATE.TITLE", {count: entityTotalCount}),
            confirmMessage: ({entityTotalCount}) =>
              t("PROJECT.ACTIONS.BATCH_UPDATE.CONFIRM", {count: entityTotalCount}),
            successMessage: ({entityTotalCount}) => t("PROJECT.ACTIONS.BATCH_UPDATE.SUCCESS", {count: entityTotalCount})
          }}
        />
      </CollectionDataProvider>
    </>
  );

  function handleGatherSubProjects() {
    const ng = !gatherSubProjects;
    setGatherSubProjects(ng);
    _gatherSubProjects = ng;
    localStorage?.setItem("gatherSubProjects", ng);
  }

  /**
   * Handler to change display mode
   */
  async function handleDisplayModeChange(mode) {
    await localStorage?.setItem("projectsDisplayMode", mode);
  }

  function getColumns() {
    let genericColumns = generateCollectionViewGenericColumns({t});

    let indexOfTags = genericColumns.findIndex(({name}) => name === "tags");

    genericColumns.splice(indexOfTags + 1, 0, {
      name: "status",
      path: "endDate",
      label: t("SEARCH.COLUMNS.PROJECTS.STATE"),
      // displayAsMainTitle:true,
      options: {
        customBodyRender: (endDate) => {
          return endDate ? t("PROJECT.STATE.CLOSED") : t("PROJECT.STATE.RUNNING");
        }
      }
    });

    return [
      {
        name: "image",
        label: " ",
        altLabel: t("SEARCH.COLUMNS.COMMON.IMAGE"),
        options: {
          customBodyRender: (image, {row: project}) => {
            return (
              <Box
                sx={{
                  my: -1
                }}>
                <Link to={formatRoute(ROUTES.PROJECT, {id: project.id})}>
                  <Avatar
                    style={{
                      backgroundColor: project.color
                    }}
                    src={getImageThumbnail({
                      imageUrl: image,
                      size: "small"
                    })}>
                    <MenuBookIcon />
                  </Avatar>
                </Link>
              </Box>
            );
          }
        }
      },
      {
        name: "title",
        label: t("SEARCH.COLUMNS.COMMON.TITLE"),
        // displayAsMainTitle:true,
        options: {
          sort: true,
          setCellProps: () =>
            isDesktop
              ? {
                  sx: classes.projectCell
                }
              : {},
          customBodyRender: (title, {row: project}) => {
            return (
              <Box sx={{minWidth: "15vw"}}>
                <Typography variant="subtitle1">
                  {createLink({
                    to: formatRoute(ROUTES.PROJECT, {id: project.id}),
                    text: title
                  })}
                </Typography>
                {_gatherSubProjects && project.subProjectsCount > 0 && (
                  <Box sx={classes.subProjects}>
                    <span>{t("PROJECT.SUB_PROJECTS")}</span> : &nbsp;
                    {(project.subProjects?.edges || []).map(({node}, index) => (
                      <span key={index}>
                        {createLink({
                          to: formatRoute(ROUTES.PROJECT, {id: node.id}),
                          text: node.title
                        })}
                        &nbsp;
                      </span>
                    ))}
                  </Box>
                )}
              </Box>
            );
          }
        }
      },
      ...genericColumns
    ];
  }
}

{
  /*

      <CollectionView
        collectionKey={"ProjectsList"}
        columns={columns}
        gqlConnectionPath={"projects"}
        gqlCountPath={"projectsCount"}
        gqlQuery={gqlProjects}
        gqlSortings={!!qs ? null : [{sortBy: "updatedAt", isSortDescending: true}]}
        gqlFilters={gatherSubProjects ? ["hasParentProject != *"] : null}
        gqlVariables={{
          gatherSubProjects
        }}
        qs={qs}
        onItemClick={onSelectProject}
        generateItemRoute={(node) => formatRoute(ROUTES.PROJECT, {id: node.id})}
        availableDisplayModes={availableDisplayModes}
        displayMode={displayMode}
        onDisplayModeChange={handleDisplayModeChange}
        removalEnabled={false}
        batchEditEnabled={isEditor}
        batchEditButtonProps={{
          gqlUpdateMutation: gqlBatchUpdateProjects,
          entityFormDefinition: projectFormDefinition,
          entityFormContent: <EntityFormContentBatch formContentFragmentPrefix="Project" />,
          title: ({entityTotalCount}) => t("PROJECT.ACTIONS.BATCH_UPDATE.TITLE", {count: entityTotalCount}),
          confirmMessage: ({entityTotalCount}) => t("PROJECT.ACTIONS.BATCH_UPDATE.CONFIRM", {count: entityTotalCount}),
          successMessage: ({entityTotalCount}) => t("PROJECT.ACTIONS.BATCH_UPDATE.SUCCESS", {count: entityTotalCount})
        }}
        renderRightSideActions={() => {
          return isDesktop ? (
            <Button
              disabled={!isEditor}
              to={{
                pathname: formatRoute(ROUTES.PROJECT_CREATE),
                state: {redirectToImportAfterCreation}
              }}>
              {t("PROJECT.CREATE")}
            </Button>
          ) : (
            <FloatingButton
              locked={!isEditor}
              to={{
                pathname: formatRoute(ROUTES.PROJECT_CREATE),
                state: {redirectToImportAfterCreation}
              }}
              name="AddIcon"
            />
          );
        }}
        renderLeftSideActions={() => {
          if (isDesktop) {
            return (
              <FormGroup sx={classes.subProjectsAction}>
                <FormControlLabel
                  control={
                    <Switch
                      checked={gatherSubProjects}
                      onChange={handleGatherSubProjects}
                      value="checkedA"
                    />
                  }
                  label={t("SEARCH.COLUMNS.PROJECTS.GATHER_SUB_PROJECTS")}
                />
              </FormGroup>
            );
          }
        }}
      />

*/
}
