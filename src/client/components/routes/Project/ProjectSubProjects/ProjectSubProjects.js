/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {createLink, LoadingSplashScreen, ErrorBoundary, ExcerptPopover} from "@synaptix/ui";
import {formatRoute} from "react-router-named-routes";
import {useQuery} from "@apollo/client";
import {Box} from "@mui/material";
import {globalSx} from "../../../layouts/styles";
import {ProjectExcerpt} from "../ProjectExcerpt";
import {gqlSubProjects} from "./gql/ProjectSubProjects.gql";
import {ROUTES} from "../../../../routes";

export function ProjectSubProjects(props) {
  return (
    <ErrorBoundary>
      <ProjectSubProjectsCode {...props} />
    </ErrorBoundary>
  );
}

/**
 * Display the sub projects of a given project
 * @param {string} projectId
 */
function ProjectSubProjectsCode({projectId} = {}) {
  const {t} = useTranslation(); 
  let data,
    loading = false;

  if (projectId) {
    ({data, loading} = useQuery(gqlSubProjects, {
      variables: {
        projectId
      }
    }));
  }

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <Choose>
      <When condition={data?.project.subProjects.edges.length > 0}>
        {data?.project.subProjects.edges.map(({node: subProject}) => (
          <Box component="span" sx={globalSx.commaAfter} key={subProject.id}>
            <ExcerptPopover OnHoverDisplayComponent={<ProjectExcerpt id={subProject.id} />}>
              {createLink({
                text: subProject.title,
                to: formatRoute(ROUTES.PROJECT, {id: subProject.id})
              })}
            </ExcerptPopover>
          </Box>
        ))}
      </When>
      <Otherwise>
        <Box sx={globalSx.empty}>{t("PROJECT.NO_RELATED_SUBPROJECTS")}</Box>
      </Otherwise>
    </Choose>
  );
}
