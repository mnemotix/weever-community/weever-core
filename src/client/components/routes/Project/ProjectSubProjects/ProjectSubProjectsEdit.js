/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {ErrorBoundary, PluralLinkField, ProjectsAutocomplete, LoadingSplashScreen} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {gqlSubProjects} from "./gql/ProjectSubProjects.gql";
import {projectSubProjectInputDefinition} from "./form/ProjectSubProjects.form";
import {ListItemText} from "@mui/material";
import {ProjectFormContent} from "../ProjectFormContent";


export function ProjectSubProjectsEdit(props) {
  return (
    <ErrorBoundary>
      <ProjectSubProjectsEditCode {...props} />
    </ErrorBoundary>
  );
}

function ProjectSubProjectsEditCode({projectId} = {}) {
  const {t} = useTranslation();
  const [getSubProjects, {data: {project} = {}, loading} = {}] = useLazyQuery(gqlSubProjects);

  useEffect(() => {
    if (projectId) {
      getSubProjects({
        variables: {
          projectId
        }
      });
    }
  }, [projectId]);

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <PluralLinkField
      data={project}
      linkInputDefinition={projectSubProjectInputDefinition}
      renderObjectContent={(project) => <ListItemText primary={project.title} />}
      renderObjectForm={() => <ProjectFormContent singleColumn />}
      renderObjectAutocomplete={({...props}) => {
        return <ProjectsAutocomplete {...props} />;
      }}
      addButtonLabel={t("ACTIONS.ADD_SUB_PROJECT")}
    />
  );
}
