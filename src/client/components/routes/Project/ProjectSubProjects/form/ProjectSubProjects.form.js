import {LinkInputDefinition} from "@mnemotix/synaptix-client-toolkit";
import {projectFormDefinition} from "../../form/Project.form";

/**
 * @type {LinkInputDefinition}
 */
export const projectSubProjectInputDefinition = new LinkInputDefinition({
  name: "subProjects",
  isPlural: true,
  inputName: "subProjectInputs",
  targetObjectFormDefinition: projectFormDefinition,
  forceUpdateTarget: false
});