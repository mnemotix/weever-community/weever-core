/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {DynamicForm, TaggingsField, AccessPolicyField, useExtensionFragment} from "@synaptix/ui";
import {Grid, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {ProjectFormContent} from "./ProjectFormContent";
import {ProjectInvolvementsEdit} from "./ProjectInvolvements/ProjectInvolvementsEdit";
 
import {ProjectParentProjectEdit} from "./ProjectParentProject/ProjectParentProjectEdit";
import {ProjectSubProjectsEdit} from "./ProjectSubProjects/ProjectSubProjectsEdit";
import {projectFormDefinition} from "./form/Project.form";

/**
 * @return {*}
 * @constructor
 */
export function ProjectDynamicForm({
  project,
  fullForm,
  mutateFunction,
  saving,
  renderRightExtraContent,
  renderLeftExtraContent
} = {}) {
  const {t} = useTranslation();

  const ProjectEditExtension = useExtensionFragment("ProjectEdit", {projectId: project?.id});

  return (
    <DynamicForm
      object={project}
      formDefinition={projectFormDefinition}
      mutateFunction={mutateFunction}
      saving={saving}
      renderFormContent={renderFormContent}
    />
  );

  function renderFormContent({renderActions}) {
    return (
      <ProjectFormContent
        renderLeftExtraContent={() => (
          <>
            <If condition={fullForm}>
              <Grid item xs={12}>
                <TaggingsField entityId={project?.id} />
              </Grid>

              <Grid item xs={12}>
                <AccessPolicyField entityId={project?.id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("PROJECT.PARENT_PROJECT")}</Typography>
                <ProjectParentProjectEdit projectId={project?.id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("PROJECT.SUB_PROJECTS")}</Typography>
                <ProjectSubProjectsEdit projectId={project?.id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("PROJECT.INVOLVEMENTS")}</Typography>

                <ProjectInvolvementsEdit projectId={project?.id} />
              </Grid>

              <Grid item xs={12}>
                {ProjectEditExtension}
              </Grid>
            </If>
            {renderLeftExtraContent?.()}
          </>
        )}
        renderRightExtraContent={() => (
          <>
            <Grid item xs={12}>
              {renderActions()}
            </Grid>
            {renderRightExtraContent?.()}
          </>
        )}
      />
    );
  }
}
