
import {RichTextViewer} from "@synaptix/ui";
import {globalSx} from "../../../../components/layouts/styles";

import {Box} from "@mui/material";

const classes = {
  imageContainer: (theme) => ({
    display: "flex",
    width: "100%",
    padding: theme.spacing(2, 0),
    minHeight: "75px",
    overflow: "auto"
  }),
  description: (theme) => ({
    flex: 1,
    width: "100%",
    display: "flex",
    flexDirection: "column",
    margin: theme.spacing(2),
    padding: theme.spacing(2),
    lineClamp: 5,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    boxOrient: "vertical",
    overflow: "hidden",
    textOverflow: "ellipsis"
  }),
  memoContainer: (theme) => ({
    padding: theme.spacing(2),
    maxWidth: `min(${theme.spacing(52)}px,100%)`,
    minWidth: `min(${theme.spacing(40)}px,100%)`,
    flex: 1,
    display: "flex",
    flexDirection: "column"
  }),
  memoTitle: {
    display: "inline-block",
    fontWeight: "bold"
  },
  memoContent: (theme) => ({
    flexGrow: 1,
    fontSize: theme.typography.fontSize * 0.9,
    display: "box",
    boxOrient: "vertical",
    overflow: "auto",
    textOverflow: "ellipsis",
    lineClamp: 5,
    textAlign: 'left',
    "& p": {
      margin: [[theme.spacing(0.2), 0]]
    }
  }),
  memoAuthor: {
    textAlign: "right"
  }
};



/**
 * Render a project contribution with date and image / memo ...
 */
export function ProjectContribution({generateThumbResourcesWithCarousel, projectContribution}) {
  return (<Box sx={classes.imageContainer}>
    {generateThumbResourcesWithCarousel(projectContribution)}
    <TimelineMemo projectContribution={projectContribution} />
  </Box>)
}

function useTimelineMemo(projectContribution) {
  if (projectContribution?.replies?.edges.length > 0) {
    return projectContribution?.replies?.edges[0]?.node;
  } else {return false}
}

function TimelineMemo({projectContribution}) {
  const memo = useTimelineMemo(projectContribution);
  if (memo) {
    return (
      <Box sx={[globalSx.imageCard, globalSx.bigImageCard, classes.memoContainer]}>
        <Box sx={classes.memoTitle}>MEMO</Box>
        <Box sx={classes.memoContent}>
          <RichTextViewer content={memo?.description || ""} />
        </Box>
        <Box sx={sx.memoAuthor}>{memo?.creatorPerson?.fullName}</Box>
      </Box>
    );
  } else {return null}
}

