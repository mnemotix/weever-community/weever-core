import {useState, useEffect} from "react";
import {LoadingSplashScreen} from "@synaptix/ui";
import get from "lodash/get";
import cloneDeep from "lodash/cloneDeep";
import dayjs from "dayjs";
import {locale} from "dayjs";
import {Box, Button as MuiButton, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery, useLazyQuery} from "@apollo/client";
import {DisplayTimeline} from "../../../../components/widgets/Timeline/DisplayTimeline";
import {convertObjectToArray, scrollToAnchor, chunkArray} from "../../../../components/utilities/tools";


import {
  gqlProjectContributionsDates,
  gqlProjectContributions
} from "../../../../components/routes/ProjectContribution/gql/ProjectContributions.gql.js";

import {classes} from "./style";

// https://github.com/apollographql/react-apollo/issues/4000

/**
 *
 * Render an horizontal timeline for a project
 *
 * 2 queries at least are runned
 * first for dates
 * second for getting project contributions paginated
 * @param gqlConnectionPath
 * @param generateItemRoute  function to generate the link to a resource
 * @param renderItem function to render each project contribution
 * @param renderItemTitle function to render each project contribution title, splitted from renderItem to be used in TimelineOppositeContent separatedly
 * @return {*}
 * @constructor
 */
export function ProjectTimeline({
  gqlConnectionPath,
  gqlCountPath,
  generateItemRoute,
  generateNodeRoute,
  renderItem,
  renderItemTitle,
  gqlVariables
} = {}) {

  if (generateNodeRoute) {
    console.log("warning, depreciated parameter : generateNodeRoute. It was renamed to generateItemRoute")
    generateItemRoute = generateNodeRoute;
  }

  const {t} = useTranslation();

  // how many items to load in one query
  const pageSize = 6;

  // force same order for date and project contribution
  const sorting = {
    sortings: [
      {
        sortBy: "startDate",
        isSortDescending: true
      }
    ]
  };

  // used to store fetched data in good index in dataContribs (cannot pass params to useLazyQuery onComplete expect resulted data)
  const [currentFetchedPage, setCurrentFetchedPage] = useState(0);
  // project-contributions are stored by pages, easier to paginate good data and avoid empty element or query many times sames elements
  const [dataContribs, setDataContribs] = useState({});
  // how many project-contributions are available server side
  const [contribsCount, setContribsCount] = useState(0);
  // how many page we need to load them
  const [pagesCount, setPagesCount] = useState(0);

  /*************************************************
   *
   *  fetch dates for horizontal timeline
   *
   *************************************************/

  // First query to get all dates
  const {data: dataDates, loading: loadingDates, error: errorDates} = useQuery(gqlProjectContributionsDates, {
    // fetchPolicy: "cache-and-network",
    fetchPolicy: "network-only",
    errorPolicy: "all",
    variables: {
      ...gqlVariables,
      ...sorting
    }
  });

  // create object key with value from 0 to pageCount and set 'pageContribs' to null
  // so in view if there is no data, we can display a div with load button etc
  function initDataContribs(data, pageCount, dates) {
    let chunked = chunkArray(dates, pageSize);
    for (let p = 0; p <= pageCount; p++) {
      if (chunked[p] && !data[p]) {
        data[p] = {
          pageContribs: false,
          from: chunked[p][0],
          to: chunked[p][chunked[p].length - 1],
          loading: false
        };
      }
    }
    return data;
  }

  // which contributions is loading ?
  function setLoadingContribsPage(page, newValue) {
    let newDC = cloneDeep(dataContribs);
    newDC[page] = {...newDC[page], loading: newValue};

    setDataContribs(newDC);
  }

  const [timelineHorizontalElements, setTimelineHorizontalElements] = useState([]);
  //when dataDates is set, recalculate HorizontalElements
  useEffect(() => {
    if (dataDates?.project) {
      let {allStartDate, timeline} = createTimeLineData();
      setTimelineHorizontalElements(timeline);
      setDataContribs(
        initDataContribs(cloneDeep(dataContribs), Math.floor(allStartDate.length / pageSize), allStartDate)
      );
    }
  }, [dataDates]);

  /***************************************************
   * fetch project contributions for vertical display
   ***************************************************/

  // useLazyQuery onComplete callback don't enable to pass optional params so we have to store them in state
  // we need an optionnal callback to be called on same case to auto scroll after fetching data
  const [useLazyQueryCB, setUseLazyQueryCB] = useState(null);

  // load first page at component init
  useEffect(() => {
    triggerFetchContribsAtPage(0);
  }, []);

  // had to use useEffect because i get async setCurrentFetchedPage, so value of currentFetchedPage was readed before setstate finish
  useEffect(() => {
    let after = currentFetchedPage > 0 ? `offset:${pageSize * (currentFetchedPage - 1) + pageSize - 1}` : null;
    getDataContribs({
      fetchPolicy: "network-only",
      errorPolicy: "all",
      variables: {
        first: pageSize,
        after,
        // limit: pageSize,
        //  offset: parsed || 0,
        ...gqlVariables,
        ...sorting
      }
    });
  }, [currentFetchedPage]);

  function triggerFetchContribsAtPage(page) {
    setLoadingContribsPage(parseInt(page), true);
    setCurrentFetchedPage(parseInt(page));
  }

  const [getDataContribs, {data}] = useLazyQuery(gqlProjectContributions);
  useEffect(() => {
    if (data) {
      const cCount = get(data, gqlCountPath);

      if (contribsCount === 0) {
        setPagesCount(Math.floor(cCount / pageSize));
        setContribsCount(cCount);
      }

      if (get(data, gqlConnectionPath).edges.length > 0) {
        // let merged = initDataContribs(cloneDeep(dataContribs), Math.floor(cCount / pageSize));
        let merged = cloneDeep(dataContribs);
        if (!merged[currentFetchedPage]) {
          merged[currentFetchedPage] = {};
        }
        merged[currentFetchedPage]["pageContribs"] = get(data, gqlConnectionPath)?.edges.map(elem => {
          return {...elem?.node, page: currentFetchedPage};
        });
        merged[currentFetchedPage]["loading"] = false;
        setDataContribs(merged);
      } else {
        setLoadingContribsPage(currentFetchedPage, false);
      }

      if (useLazyQueryCB) {
        useLazyQueryCB();
      }
    }
  }, [data]);

  // callback called on click on date from horizontalTimelLineDate
  function handleDateClick(page, id, cb) {
    let onfinish = () => {
      setTimeout(() => {
        //goToAnchor(id);
        scrollToAnchor(id, 120);
        // call the callback defined in horizontalTimelineData to scroll horizontally to the good date
        cb();
        setUseLazyQueryCB(null);
      }, 1000);
    };

    // si la donnée n'est pas chargée on charge les données puis on scroll dessus
    if (!dataContribs[page]["pageContribs"]) {
      scrollToAnchor("page-" + page, 120);
      // useLazyQueryCB sera executée dans triggerFetchContribsAtPage à la fin du chargement des données
      setUseLazyQueryCB(() => onfinish);
      // on recupere les contributions de la page de la date cliqué
      triggerFetchContribsAtPage(page);
    } else {
      onfinish();
    }
  }

  // on recupere eventuellement les contributions avant l'index cliqué
  function handleOnScrollUp(page, index) {
    // test si on fetch les données de la page précédente
    if (index <= pageSize / 3) {
      let previousPage = page - 1;

      if (previousPage > 1 && !dataContribs[previousPage]["pageContribs"]) {
        triggerFetchContribsAtPage(previousPage);
      }
    }
  }

  // on recupere eventuellement les contributions après l'index
  // vue que l'on scroll vers le bas, on cherche le prochain index à récupèrer après l'index courant,
  // exemple on a index = 20, les données des index 20 à 25 sont dispo, on récup celui de 26
  function handleOnScrollDown(page, index) {
    // test si on fetch les données de la prochaine page
    if (index > pageSize / 3) {
      let nextPage = page + 1;
      if (nextPage <= pagesCount && !dataContribs?.[nextPage]?.["pageContribs"]) {
        triggerFetchContribsAtPage(nextPage);
      }
    }
  }

  let contribs = createContribs();
  return (
    <Box sx={classes.projectTimelineRoot}>
      <DisplayTimeline
        horizontalElements={timelineHorizontalElements}
        horizontalElementSx={classes.fixed}
        verticalElements={contribs}
        onDateClick={(page, id, cb) => handleDateClick(page, id, cb)}
        onScrollDown={handleOnScrollDown}
        onScrollUp={handleOnScrollUp}
      />
    </Box>
  );

  // create dates to render
  function createTimeLineData() {
    let timeline = [];
    // used to be chunked then displayed in the button "load data from X to Y"
    let allStartDate = [];

    // to display one line only per year
    let yearsPushed = {};

    if (loadingDates || !dataDates) {
      return {allStartDate, timeline};
    }

    get(dataDates, gqlConnectionPath).edges.map(({node: projectContribution}, index) => {
      const {id, startDate, createdAt} = projectContribution;
      const _d = startDate || createdAt;
      allStartDate.push(locale() === "fr" ? dayjs(_d).format("DD/MM/YYYY") : dayjs(_d).format("YYYY/MM/DD"));
      if (_d) {
        let year = dayjs(_d).format("YYYY");

        timeline.push({
          index, // utilisé pour chargé les données ou after=index
          page: Math.floor(index / pageSize),
          id,
          render: (
            <>
              {!yearsPushed[year] &&
                <Box sx={[classes.year, classes.nowrap]}>{year}</Box>
              }
              <Box sx={classes.nowrap}>
                {locale() === "fr" ? dayjs(_d).format("DD/MM") : dayjs(_d).format("MM/DD")}
              </Box>
            </>
          )
        });
        yearsPushed[year] = true;
      }
    });
    return {allStartDate, timeline};
  }

  // return the project contributions to render
  function createContribs() {
    let projectContribs = [];
    if (!dataContribs || dataContribs.length < 1) {
      return projectContribs;
    }

    convertObjectToArray(dataContribs).map(({index: page, value}) => {
      let {pageContribs, loading, from, to} = value;
      if (pageContribs) {
        convertObjectToArray(pageContribs).map(({index, value: projectContribution}) => {
          const {id} = projectContribution;
          projectContribs.push({
            disableLoadOnScroll: false,
            id,
            page,
            index: page + "-" + index,
            generateItemRoute: generateItemRoute(projectContribution),
            renderItemTitle: renderItemTitle(projectContribution),
            renderContent: (
              <Box id={id} sx={classes.timelineLine}>
                {renderItem(projectContribution)}
              </Box>
            )
          });
        });
      } else {
        projectContribs.push({
          page,
          disableLoadOnScroll: true,
          id: "page-" + page,
          index: page,
          renderItemTitle: "",
          renderContent: (
            <Box className={classes.timelineLine}>
              <Typography variant="subtitle1">
                {!loading ? (
                  <MuiButton variant="contained" onClick={() => triggerFetchContribsAtPage(page)}>
                    {t("ACTIONS.LOAD_DATA")}
                    {from && to && t("ACTIONS.FROM_TO", {from, to})}
                  </MuiButton>
                ) : (
                  <div>
                    {t("ACTIONS.LOADING_DATA")}
                    {from && to && t("ACTIONS.FROM_TO", {from, to})}
                    <LoadingSplashScreen size={30} sx={classes.progress} />
                  </div>
                )}
              </Typography>
            </Box>
          )
        });
      }
    });
    return projectContribs;
  }
}
