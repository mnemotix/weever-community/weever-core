
import {grey} from '@mui/material/colors';

export const classes = (theme) => ({
  projectTimelineRoot: {
    flex: 1
  },
  fixed: {
    position: "sticky",
    top: "5px",
    width: "98%",
    margin: "auto",
    backgroundColor: grey["100"],
    zIndex: 10,
    paddingLeft: 5,
    paddingRight: 5
  },
  year: {
    fontSize: "1.2rem"
  },
  nowrap: {whiteSpace: "nowrap"},
  timelineLine: {
    margin: theme.spacing(2, 0, 2, 0),
    display: 'flex',
    flex: 1,
    maxWidth: '100%',
    flexDirection: 'column'
  },
  progress: {
    marginLeft: 10,
    marginRight: 10
  }
});
