/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect} from "react";
import {useParams} from "react-router-dom";
import {Breadcrumbs, Grid, Box, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {
  createLink,
  ThumbWithFullScreen,
  ExcerptPopover,
  Button,
  LoadingSplashScreen,
  useLoggedUser,
  useResponsive,
  useFloatingButtonDialContext,
  FloatingButtonDial,
  useExtensionFragment,
  Taggings,
  EntityAccessPolicy,
  useEnvironment,
  RichTextViewer,
  TranslatableDisplay,
  PageNotFound
} from "@synaptix/ui";
import {ROUTES} from "../../../routes";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";
import {ProjectExcerpt} from "./ProjectExcerpt";
import {Link} from "react-router-dom";

import {ProjectContributions} from "../ProjectContribution/ProjectContributions";
import {ProjectInvolvements} from "./ProjectInvolvements/ProjectInvolvements";
import {gqlProject} from "./gql/Project.gql";
import {ProjectGatheredInvolvedAgents} from "./ProjectInvolvements/ProjectGatheredInvolvedAgents";

import {EntityActivity} from "../Activity/EntityActivity";

import {ProjectSubProjects} from "./ProjectSubProjects/ProjectSubProjects";
import {globalSx} from "../../layouts/styles";
import {GlobalGraphButton} from "../Visualizations/GlobalGraphButton";

const classes = {
  actions: {
    mt: 2
  },
  action: {
    textAlign: "center",
    alignSelf: "center"
  },
  noImage: (theme) => ({
    width: "100%",
    color: theme.palette.text.emptyHint,
    padding: theme.spacing(15, 0),
    textAlign: "center",
    backgroundColor: theme.palette.grey[200]
  }),
  createdBy: {
    marginLeft: 0.5
  }
};

export default function Project() {
  const {t, i18n} = useTranslation();
  const {isEditor} = useLoggedUser();
  const {isDesktop} = useResponsive();
  const publicExplorerEnabled = useEnvironment("PUBLIC_EXPLORER_ENABLED", {isBoolean: true});
  const publicPolicyId = useEnvironment("PUBLIC_POLICY_ID");

  let {id} = useParams();
  id = decodeURIComponent(id);

  const {contextFloatingButtonDialSetValues} = useFloatingButtonDialContext();
  useEffect(() => {
    contextFloatingButtonDialSetValues([
      {
        icon: "AddIcon",
        link: formatRoute(ROUTES.PROJECT_CONTRIBUTION_CREATE, {id}),
        display: "PROJECT_CONTRIBUTION.CREATE"
      },
      {icon: "EditIcon", locked: !isEditor, link: formatRoute(ROUTES.PROJECT_EDIT, {id}), display: "ACTIONS.UPDATE"}
    ]);
  }, [isDesktop, isEditor]);

  const {data: {project} = {}, loading} = useQuery(gqlProject, {
    variables: {
      id
    }
  });

  const extensions = {
    afterDescription: useExtensionFragment("Project.Heading.AfterDescription", {projectId: id}),
    afterTags: useExtensionFragment("Project.Heading.AfterTags", {projectId: id}),
    headingEnd: useExtensionFragment("Project.Heading.End", {projectId: id}),
    project: useExtensionFragment("Project", {projectId: id})
  };

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (id && !project) {
    return <PageNotFound />;
  }

  return (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <Breadcrumbs>
          {createLink({to: ROUTES.PROJECTS, text: t("BREADCRUMB.PROJECTS")})}

          <If condition={project?.parentProject}>
            {createLink({
              text: project.parentProject?.title,
              to: formatRoute(ROUTES.PROJECT, {id: project?.parentProject?.id})
            })}
          </If>

          <Typography color="textPrimary">{project.title}</Typography>
        </Breadcrumbs>
      </Grid>

      <Grid item xs={12} container columnSpacing={4}>
        <Grid item xs={12} md={8}>
          <Box>
            <Typography variant="h5" gutterBottom>
              <TranslatableDisplay isTranslated={project.titleTranslated}>{project.title}</TranslatableDisplay>
            </Typography>

            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography variant="subtitle1" gutterBottom>
                  {t("PROJECT.SHORT_DESCRIPTION")}
                </Typography>

                <TranslatableDisplay isTranslated={project.shortDescriptionTranslated}>
                  <RichTextViewer content={project.shortDescription} />
                </TranslatableDisplay>
              </Grid>

              <If condition={project?.description}>
                <Grid item xs={12}>
                  <Typography variant="subtitle1" gutterBottom>
                    {t("PROJECT.DESCRIPTION")}
                  </Typography>
                  <TranslatableDisplay isTranslated={project.descriptionTranslated}>
                    <RichTextViewer content={project.description} />
                  </TranslatableDisplay>
                </Grid>
              </If>

              {extensions.afterDescription}

              <If condition={project?.taggingsCount > 0}>
                <Grid item xs={12}>
                  <Typography variant="subtitle1" gutterBottom>
                    {t("ENTITY.TAGGINGS")}
                  </Typography>
                  <Taggings entityId={id} />
                </Grid>
              </If>

              {extensions.afterTags}

              <If condition={project?.parentProject}>
                <Grid item xs={12}>
                  <Typography variant="subtitle1">{t("PROJECT.PARENT_PROJECT")} </Typography>
                  {project?.parentProject?.title ? (
                    <ExcerptPopover OnHoverDisplayComponent={<ProjectExcerpt id={project?.parentProject?.id} />}>
                      {createLink({
                        text: project?.parentProject?.title,
                        to: formatRoute(ROUTES.PROJECT, {id: project?.parentProject?.id})
                      })}
                    </ExcerptPopover>
                  ) : (
                    <Box sx={globalSx.empty}>{t("PROJECT.NO_PARENT_PROJECT")}</Box>
                  )}
                </Grid>
              </If>

              <If condition={project?.subProjectsCount > 0}>
                <Grid item xs={12}>
                  <Typography variant="subtitle1">{t("PROJECT.SUB_PROJECTS")}</Typography>
                  <ProjectSubProjects projectId={id} />
                </Grid>
              </If>

              <If condition={project?.involvementsCount > 0}>
                <Grid item xs={12}>
                  <Typography variant="subtitle1">{t("PROJECT.INVOLVEMENTS")}</Typography>
                  <ProjectInvolvements projectId={id} />
                </Grid>
              </If>

              <If condition={project?.gatheredInvolvedAgentsCount > 0}>
                <Grid item xs={12}>
                  <Typography variant="subtitle1">{t("PROJECT.GATHERED_AGENTS")}</Typography>
                  <ProjectGatheredInvolvedAgents projectId={id} />
                </Grid>
              </If>

              {extensions.headingEnd}
            </Grid>
          </Box>

          {extensions.project}
        </Grid>

        <Grid item xs={12} lg={4}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <Choose>
                <When condition={project?.image}>
                  <ThumbWithFullScreen imageUrl={project.image} backgroundColor={project.color} />
                </When>
                <Otherwise>
                  <Box sx={classes.noImage}>{t("ARTISTIC_OBJECT.NO_IMAGE")}</Box>
                </Otherwise>
              </Choose>
            </Grid>
            <Grid item xs={12}>
              {t("PROJECT.CREATED_AT_BY", {
                date: dayjs(project.createdAt).format("LL")
              })}
              <Box component="span" sx={classes.createdBy}>
                {createLink({
                  to: formatRoute(ROUTES.PERSON, {
                    id: project?.creatorPerson?.id
                  }),
                  text: Array.isArray(project?.createdBy)
                    ? [...new Set(project?.createdBy)].join(" ")
                    : project?.createdBy
                })}
              </Box>
            </Grid>

            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={12} md={4}>
                  <Typography variant="subtitle1" gutterBottom>
                    {t("PROJECT.START_DATE")}
                  </Typography>
                  {dayjs(project.startDate || project.createdAt).format("L")}
                </Grid>
                <Grid item xs={12} md={4}>
                  <div>
                    <Typography variant="subtitle1" gutterBottom>
                      {t("PROJECT.STATUS")}
                    </Typography>
                    {project.endDate
                      ? t("PROJECT.STATE.CLOSED_AT", {date: dayjs(project.endDate).format("L")})
                      : t("PROJECT.STATE.RUNNING")}
                  </div>
                </Grid>

                <Grid item xs={12} md={4}>
                  <Typography variant="subtitle1" gutterBottom>
                    {t("ENTITY.ACCESS_POLICY")}
                  </Typography>
                  <EntityAccessPolicy entityId={id} />
                </Grid>
              </Grid>
            </Grid>

            {isDesktop && (
              <Grid
                item
                xs={12}
                sx={classes.actions}
                container
                direction="row"
                justifyContent="center"
                alignItems="center"
                spacing={4}>
                <Grid item sx={classes.action}>
                  <Button disabled={!project?.permissions?.grant} to={formatRoute(ROUTES.PROJECT_EDIT, {id})}>
                    {t("ACTIONS.UPDATE")}
                  </Button>
                </Grid>

                <Grid item sx={classes.action}>
                  <GlobalGraphButton entityId={id} />
                </Grid>
                <If condition={publicExplorerEnabled && project?.accessPolicy?.id.includes(publicPolicyId)}>
                  <Grid item sx={classes.action}>
                    <Link
                      to={formatRoute(ROUTES.EXPLORE_PROJECT, {id: project.id})}
                      target={"_blank"}
                      style={classes.seePublicPage}>
                      Voir la fiche publique
                    </Link>
                  </Grid>
                </If>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={12}>
        <ProjectContributions
          projectId={project.id}
          showFloatingButton={false}
          creationEnabled={project?.permissions?.update}
          batchEditEnabled={project?.permissions?.grant}
        />
      </Grid>

      <If condition={project?.permissions?.grant}>
        <Grid item xs={12}>
          <EntityActivity entityId={id} />
        </Grid>
      </If>
      {!isDesktop && <FloatingButtonDial />}
    </Grid>
  );
}
