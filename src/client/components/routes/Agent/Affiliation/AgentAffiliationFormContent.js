/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {oneOf} from "prop-types";
import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";

import {DatePickerField, TextField, LinkedObjectAutocompleteField} from "@synaptix/ui";
import {OrganizationsAutocomplete} from "../../Organization/OrganizationsAutocomplete";
import {PersonsAutocomplete} from "../../Person/PersonsAutocomplete";
import {OrganizationFormContent} from "../../Organization/OrganizationFormContent";
import {PersonFormContent} from "../../Person/PersonFormContent";
import {
  affiliationOrganizationLinkInputDefinition,
  affiliationPersonLinkInputDefinition
} from "./form/Affiliation.form";

export function AgentAffiliationFormContent({sourceAgentType}) {

  const {t} = useTranslation();
  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <LinkedObjectAutocompleteField
            linkInputDefinition={
              sourceAgentType === "Person"
                ? affiliationOrganizationLinkInputDefinition
                : affiliationPersonLinkInputDefinition
            }
            creationEnabled
            renderObjectForm={() =>
              sourceAgentType === "Person" ? <OrganizationFormContent /> : <PersonFormContent />
            }
            renderAutocomplete={({...props}) =>
              sourceAgentType === "Person" ? (
                <OrganizationsAutocomplete {...props} />
              ) : (
                <PersonsAutocomplete {...props} />
              )
            }
            required={true}
          />
        </Grid>
        <Grid item xs={12} md={3}>
          <TextField required={true} name="role" label={t("AGENT.AFFILIATION.ROLE")} />
        </Grid>
        <Grid item xs={12} md={3}>
          <DatePickerField name="startDate" label={t("AGENT.AFFILIATION.START_DATE")} />
        </Grid>
        <Grid item xs={12} md={3}>
          <DatePickerField name="endDate" label={t("AGENT.AFFILIATION.END_DATE")} />
        </Grid>
      </Grid>
    </>
  );
}

AgentAffiliationFormContent.propTypes = {
  sourceAgentType: oneOf(["Person", "Organization"]).isRequired
};
