/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {LoadingSplashScreen,ErrorBoundary,PluralLinkField} from "@synaptix/ui";
import dayjs from "dayjs";
import {useLazyQuery} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {oneOf, string, object} from "prop-types";
import {ListItemText} from "@mui/material";
import {AgentAffiliationFormContent} from "./AgentAffiliationFormContent";
import {agentAffiliationInputDefinition} from "./form/Affiliation.form";
import {gqlAgentAffiliations} from "./gql/AgentAffiliations.gql";

export function AgentAffiliationsEdit(props) {
  return (
    <ErrorBoundary>
      <AgentAffiliationsEditCode {...props} />
    </ErrorBoundary>
  );
}

function AgentAffiliationsEditCode({agentId, agentType} = {}) {
  const {t} = useTranslation();
  const targetIsOrg = agentType === "Person";

  const [getAgentAffiliations, {data, loading}] = useLazyQuery(gqlAgentAffiliations);

  useEffect(() => {
    if (agentId) {
      getAgentAffiliations({
        variables: {
          id: agentId
        }
      });
    }
  }, [agentId]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <PluralLinkField
      data={data?.agent}
      linkInputDefinition={agentAffiliationInputDefinition}
      renderObjectContent={affiliation => (
        <ListItemText
          primary={targetIsOrg ? affiliation.organization?.name : [affiliation.person?.firstName, affiliation.person?.lastName].join(" ")}
          secondary={
            <>
              <span>{affiliation.role || t("AGENT.AFFILIATION_NO_ROLE")}</span>
              <If condition={affiliation.startDate || affiliation.endDate}>
                <span>
                  &nbsp;
                  ({affiliation.startDate ? dayjs(affiliation.startDate).format("L") : "..."}
                  &nbsp;-&nbsp;
                  {affiliation.endDate ? dayjs(affiliation.endDate).format("L") : "..."})
                </span>
              </If>
            </>
          }
        />
      )}
      renderObjectForm={({object: affiliation}) => (
        <AgentAffiliationFormContent affiliation={affiliation} sourceAgentType={agentType} />
      )}
      addButtonLabel={targetIsOrg ? t("ACTIONS.ADD_ORGANIZATION") : t("ACTIONS.ADD_PERSON")}
    />
  );
}

AgentAffiliationsEdit.propTypes = {
  agentId: string,
  agentType: oneOf(["Person", "Organization"])
};
