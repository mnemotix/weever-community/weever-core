import {LinkInputDefinition, DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, date, string} from "yup";
import {personFormDefinition} from "../../../Person/form/Person.form";
import {organizationFormDefinition} from "../../../Organization/form/Organization.form";
import {gqlAffiliationFragment} from "../gql/AgentAffiliations.gql";

export function getAffiliationValidationSchema({t}) {
  return object().shape({
    role: string().required(t("FORM_ERRORS.FIELD_ERRORS.ROLE_REQUIRED")),
    person: object()
      .nullable()
      .test("person", t("FORM_ERRORS.FIELD_ERRORS.AGENT_REQUIRED"), function (value) {
        return !!this.parent.organization || this.parent.person;
      }),
    organization: object()
      .nullable()
      .test("organization", t("FORM_ERRORS.FIELD_ERRORS.AGENT_REQUIRED"), function (value) {
        return !!this.parent.organization || this.parent.person;
      }),
    startDate: date().nullable().default(undefined),
    endDate: date().nullable().default(undefined),
  });
}

export const affiliationOrganizationLinkInputDefinition = new LinkInputDefinition({
  name: "organization",
  inputName: "orgInput",
  targetObjectFormDefinition: organizationFormDefinition
});

export const affiliationPersonLinkInputDefinition = new LinkInputDefinition({
  name: "person",
  inputName: "personInput",
  targetObjectFormDefinition: personFormDefinition
});

export const affiliationFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["role", "startDate", "endDate"],
    gqlFragment: gqlAffiliationFragment,
    gqlFragmentName: "AffiliationFragment" // Precise it to avoid Apollo Error "Found 3 fragments. `fragmentName` must be provided when there is not exactly 1 fragment"
  }),
  postProcessInitialValues: (object) => ({
    ...object,
    startDate: object.startDate === "" ? null : object.startDate, // Replace "" by null to make DatePickerField working
    endDate: object.endDate === "" ? null : object.endDate // Replace "" by null to make DatePickerField working
  }),
  validationSchema: getAffiliationValidationSchema
});

export const agentAffiliationInputDefinition = new LinkInputDefinition({
  name: "affiliations",
  isPlural: true,
  inputName: "affiliationInputs",
  targetObjectFormDefinition: affiliationFormDefinition,
  nestedLinks: [affiliationOrganizationLinkInputDefinition, affiliationPersonLinkInputDefinition]
});
