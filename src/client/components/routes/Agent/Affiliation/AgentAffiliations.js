/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {createLink, ErrorBoundary, ExcerptPopover, ShowWhereIAM, LoadingSplashScreen, PersonExcerpt} from "@synaptix/ui";
import invariant from "invariant";
import {Box, List, ListItem, ListItemText} from "@mui/material";

import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";

import {OrganizationExcerpt} from "../../../../components/routes/Organization/OrganizationExcerpt";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../../routes";
import dayjs from "dayjs";
import {gqlAgentAffiliations} from "./gql/AgentAffiliations.gql";

export function AgentAffiliations(props) {
  return (
    <ErrorBoundary>
      <AgentAffiliationsCode {...props} />
    </ErrorBoundary>
  );
}

function AgentAffiliationsCode({agentId, isPerson, className} = {}) {
  invariant(agentId, "agentId is required");

  const {t} = useTranslation();

  const {data, loading, fetchMore} = useQuery(gqlAgentAffiliations, {
    variables: {
      id: agentId
    }
  });

  return loading || !data ? (
    <LoadingSplashScreen />
  ) : (
    <ShowWhereIAM title="AgentAffiliations" path="components/routes/Agent/AgentAffiliations.js">
      <Choose>
        <When condition={data.agent.affiliations.edges.length > 0}>
          <List className={className} dense disablePadding>
            {data.agent.affiliations.edges.map(({node: affiliation}, index) => (
              <ListItem key={index}>
                <ListItemText
                  primary={
                    <Choose>
                      <When condition={isPerson}>
                        <ExcerptPopover OnHoverDisplayComponent={<OrganizationExcerpt id={affiliation.organization?.id} />}>
                          {createLink({
                            to: formatRoute(ROUTES.ORGANIZATION, {
                              id: affiliation.organization?.id
                            }),
                            text: affiliation.organization?.name
                          })}
                        </ExcerptPopover>
                      </When>
                      <Otherwise>
                        <ExcerptPopover OnHoverDisplayComponent={<PersonExcerpt id={affiliation.person?.id} />}>
                          {createLink({
                            to: formatRoute(ROUTES.PERSON, {
                              id: affiliation.person?.id
                            }),
                            text: [affiliation.person?.firstName, affiliation.person?.lastName].join(" ")
                          })}
                        </ExcerptPopover>
                      </Otherwise>
                    </Choose>
                  }
                  secondary={
                    <>
                      <Box component="span" sx={{marginRight: 1}}>{affiliation.role || t("AGENT.AFFILIATION_NO_ROLE")}</Box>
                      <If condition={affiliation.startDate || affiliation.endDate}>
                        <span>
                          ({affiliation.startDate ? dayjs(affiliation.startDate).format("L") : "..."}-
                          {affiliation.endDate ? dayjs(affiliation.endDate).format("L") : "..."})
                        </span>
                      </If>
                    </>
                  }
                />
              </ListItem>
            ))}
          </List>
        </When>
        <Otherwise>
          <Box sx={{color: "text.emptyHint"}}>{t("AGENT.NO_AFFILIATION")}</Box>
        </Otherwise>
      </Choose>
    </ShowWhereIAM>
  );
}
