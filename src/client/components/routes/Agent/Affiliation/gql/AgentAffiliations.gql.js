import {gql} from "@apollo/client";
import {gqlPersonFragment} from "@synaptix/ui";
import {gqlOrganizationFragment} from "../../../Organization/gql/Organization.gql";

export const gqlAffiliationBasicProps = ["id", "role"];

export const gqlAffiliationFragment = gql`
  fragment AffiliationFragment on Affiliation {
    id
    role
    roleTranslated
    startDate
    endDate
    person{
      ...PersonFragment
    }
    organization{
      ...OrganizationFragment
    }
  }

  ${gqlPersonFragment}
  ${gqlOrganizationFragment}
`;

export const gqlAgentAffiliationsFragment = gql`
  fragment AgentAffiliationsFragment on AgentInterface {
    affiliations {
      edges {
        node {
          ...AffiliationFragment
        }
      }
    }
  }
  
  ${gqlAffiliationFragment}
`;

export const gqlAgentAffiliations = gql`
  query AgentAffiliations($id: ID!) {
    agent(id: $id) {
      id
      ...AgentAffiliationsFragment
    }
  }

  ${gqlAgentAffiliationsFragment}
`;

