/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {createLink} from "@synaptix/ui";
import {useQuery} from "@apollo/client";
import {Skeleton} from "@mui/material";
import {formatRoute} from "react-router-named-routes";
import {gqlAffiliation} from "./gql/Affiliation.gql";
import {ROUTES} from "../../../../routes";

export function PersonAffiliationExcerptInline({id}) {
  const {data: {affiliation} = {}, loading} = useQuery(gqlAffiliation, {
    variables: {
      id,
      skipOrganization: true
    }
  });

  return loading ? (
    <Skeleton width={100} sx={{display: "inline-block"}} />
  ) : (
    createLink({
      to: formatRoute(ROUTES.PERSON, {id: affiliation?.person?.id}),
      text: affiliation?.person?.fullName
    })
  );
}
