/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ErrorBoundary,LoadingSplashScreen,PluralLinkField} from "@synaptix/ui";
import {useEffect} from "react";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {ListItemText} from "@mui/material";
import {gqlAgentPhones} from "./gql/AgentPhones.gql";
import {AgentPhonesFormContent} from "./AgentPhonesFormContent";
import {agentPhoneLinkInputDefinition} from "./form/AgentPhones.form";

export function AgentPhonesEdit(props) {
  return (
    <ErrorBoundary>
      <AgentPhonesEditCode {...props} />
    </ErrorBoundary>
  );
}

export function AgentPhonesEditCode({agentId} = {}) {
  const {t} = useTranslation();

  const [getAgentPhones, {data, loading}] = useLazyQuery(gqlAgentPhones);

  useEffect(() => {
    if (agentId) {
      getAgentPhones({
        variables: {
          id: agentId
        }
      });
    }
  }, [agentId]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <PluralLinkField
      data={data?.agent}
      linkInputDefinition={agentPhoneLinkInputDefinition}
      renderObjectContent={phone => (
        <ListItemText
          primary={t(`AGENT.PHONES_LABELS.${(phone.label || "OTHER").toUpperCase()}`)}
          secondary={phone.number}
        />
      )}
      renderObjectForm={() => <AgentPhonesFormContent />}
      addButtonLabel={t("AGENT.PHONE_NEW")}
    />
  );
}
