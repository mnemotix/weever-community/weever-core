import {gql} from "@apollo/client";


export const gqlPhoneBasicProps = ["id", "label", "number"];

export const gqlPhoneFragment = gql`
  fragment PhoneFragment on Phone{
    id
    label
    number
  }
`;

export const gqlAgentPhonesFragment = gql`
 fragment AgentPhonesFragment on AgentInterface{
   phones {
     edges {
       node {
         ...PhoneFragment
       }
     }
   }
 }
 
 ${gqlPhoneFragment}
`;

export const gqlAgentPhones = gql`
  query AgentPhones($id: ID!) {
    agent(id: $id) {
      id
      ...AgentPhonesFragment
    }
  }
  
  ${gqlAgentPhonesFragment}
`;

