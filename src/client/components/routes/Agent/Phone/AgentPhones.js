/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {ErrorBoundary, LoadingSplashScreen} from "@synaptix/ui";
import invariant from "invariant";
import {Box, List, ListItem, ListItemText} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {gqlAgentPhones} from "./gql/AgentPhones.gql";



export function AgentPhones(props) {
  return (
    <ErrorBoundary>
      <AgentPhonesCode {...props} />
    </ErrorBoundary>
  );
}
export function AgentPhonesCode({agentId, className} = {}) {
  invariant(agentId, "agentId is required");
  const {t} = useTranslation();

  const {data, loading} = useQuery(gqlAgentPhones, {
    variables: {
      id: agentId
    }
  });

  return loading || !data ? (
    <LoadingSplashScreen />
  ) : (
    <Choose>
      <When condition={data.agent.phones.edges.length > 0}>
        <List className={className} dense disablePadding>
          {data.agent.phones.edges.map(({node: phone}, index) => (
            <ListItem key={index}>
              <ListItemText
                primary={t(`AGENT.PHONES_LABELS.${(phone.label || "OTHER").toUpperCase()}`)}
                secondary={phone.number}
              />
            </ListItem>
          ))}
        </List>
      </When>
      <Otherwise>
        <Box sx={{color: "text.emptyHint"}}>{t("AGENT.NO_PHONE")}</Box>
      </Otherwise>
    </Choose>
  );
}
