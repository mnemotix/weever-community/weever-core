import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, string} from "yup";
import {gqlPhoneFragment} from "../gql/AgentPhones.gql";

export function getPhoneValidationSchema({t}) {
  return object().shape({
    number: string().required(t("FORM_ERRORS.FIELD_ERRORS.NUMBER_REQUIRED")),
    label: string().required(t("FORM_ERRORS.FIELD_ERRORS.LABEL_REQUIRED"))
  });
}

export const phoneFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["label", "number"],
    gqlFragment: gqlPhoneFragment,
    gqlFragmentName: "PhoneFragment"
  }),
  validationSchema: getPhoneValidationSchema
});
