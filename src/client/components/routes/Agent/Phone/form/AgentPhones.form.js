import {LinkInputDefinition} from "@mnemotix/synaptix-client-toolkit";
import {phoneFormDefinition} from "./Phone.form";

/**
 * @type {LinkInputDefinition}
 */
export const agentPhoneLinkInputDefinition = new LinkInputDefinition({
  name: "phones",
  isPlural: true,
  inputName: "phoneInputs",
  targetObjectFormDefinition: phoneFormDefinition
});