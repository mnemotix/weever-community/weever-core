/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect} from "react";
import {ErrorBoundary, LoadingSplashScreen, PluralLinkField} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {ListItemText} from "@mui/material";
import {gqlAgentEmails} from "./gql/AgentEmails.gql";
import {AgentEmailsFormContent} from "./AgentEmailsFormContent";
import {agentEmailLinkInputDefinition} from "./form/AgentEmails.form";

export function AgentEmailsEdit({agentId, required}) {
  return (
    <ErrorBoundary>
      <AgentEmailsEditCode agentId={agentId} required={required} />
    </ErrorBoundary>
  );
}

export function AgentEmailsEditCode({agentId, required} = {}) {
  const {t} = useTranslation();

  const [getAgentEmails, {data, loading}] = useLazyQuery(gqlAgentEmails);

  useEffect(() => {
    if (agentId) {
      getAgentEmails({
        variables: {
          id: agentId
        }
      });
    }
  }, [agentId]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <PluralLinkField
      data={data?.agent}
      label={t("AGENT.EMAILS")}
      required={required}
      linkInputDefinition={agentEmailLinkInputDefinition}
      isObjectEditable={(email) => !email.isMainEmail}
      renderObjectContent={(email) => (
        <ListItemText
          primary={t(`AGENT.EMAILS_LABELS.${(email.accountName || "OTHER").toUpperCase()}`)}
          secondary={email.email}
        />
      )}
      renderObjectForm={() => <AgentEmailsFormContent />}
      addButtonLabel={t("AGENT.EMAIL_NEW")}
    />
  );
}
