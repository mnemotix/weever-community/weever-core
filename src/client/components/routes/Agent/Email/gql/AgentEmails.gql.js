import {gql} from "@apollo/client";

export const gqlEmailBasicProps = ["id", "email", "accountName"];

export const gqlEmailFragment = gql`
  fragment EmailFragment on EmailAccount {
    id
    email
    accountName
    isMainEmail
  }
`;

export const gqlAgentEmailsFragment = gql`
  fragment AgentEmailsFragment on AgentInterface {
    emails {
      edges {
        node {
          ...EmailFragment
        }
      }
    }
  }

  ${gqlEmailFragment}
`;

export const gqlAgentEmails = gql`
  query AgentEmails($id: ID!) {
    agent(id: $id) {
      id
      ...AgentEmailsFragment
    }
  }

  ${gqlAgentEmailsFragment}
`;
