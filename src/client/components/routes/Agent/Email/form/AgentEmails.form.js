import {LinkInputDefinition} from "@mnemotix/synaptix-client-toolkit";
import {emailFormDefinition} from "./Email.form";

/**
 * @type {LinkInputDefinition}
 */
export const agentEmailLinkInputDefinition = new LinkInputDefinition({
  name: "emails",
  isPlural: true,
  inputName: "emailAccountInputs",
  targetObjectFormDefinition: emailFormDefinition,
  forceUpdateTarget: true
});
