import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, string} from "yup";
import {gqlEmailFragment} from "../gql/AgentEmails.gql";

export function getEmailValidationSchema({t}) {
  return object().shape({
    email: string()
      .email(t("FORM_ERRORS.FIELD_ERRORS.INVALID_EMAIL"))
      .required(t("FORM_ERRORS.FIELD_ERRORS.EMAIL_REQUIRED")),
    accountName: string().required(t("FORM_ERRORS.FIELD_ERRORS.LABEL_REQUIRED"))
  });
}

export const emailFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["email", "accountName"],
    gqlFragment: gqlEmailFragment,
    gqlFragmentName: "EmailFragment"
  }),
  validationSchema: getEmailValidationSchema
});
