/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {SelectField, TextField} from "@synaptix/ui";

const optionsValues = ["main", "private", "other"];

// just display only the form content
// don't take data as props to avoid using callback on field edit
export function AgentEmailsFormContent({email} = {}) {
  const {t} = useTranslation();

  // convert array of string to array of object id/name
  const options = optionsValues.map((value) => ({
    value,
    label: t(`AGENT.EMAILS_LABELS.${value.toUpperCase()}`)
  }));

  return (
    <Grid container spacing={2}>
      <Grid item md={2} xs={12}>
        <SelectField
          required={true}
          label={t("AGENT.EMAIL_LABEL")}
          name="accountName"
          id="accountName"
          options={options}
          value={email?.accountName || "main"}
        />
      </Grid>
      <Grid item md={10} xs={12}  sx={{mt:2, mb: 2}}>
        <TextField required={true} name="email" label={t("AGENT.ADDRESSES_LABELS.EMAIL")} value={email?.email || ""} />
      </Grid>
    </Grid>
  );
}
