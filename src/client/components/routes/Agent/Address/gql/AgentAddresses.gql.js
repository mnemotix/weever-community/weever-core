import {gql} from "@apollo/client";

export const gqlAddressBasicProps = ["id", "label", "postalCode", "city", "country", "street1", "street2"];

export const gqlAddressFragment = gql`
  fragment AddressFragment on Address {
    id
    label
    postalCode
    city
    cityTranslated
    country
    countryTranslated
    street1
    street2
  }
`;
export const gqlAgentAddressesFragment = gql`
  fragment AgentAddressesFragment on AgentInterface {
    addresses {
      edges {
        node {
          ...AddressFragment
        }
      }
    }
  }
  
  ${gqlAddressFragment}
`;

export const gqlAgentAddresses = gql`
  query AgentAddresses($id: ID!) {
    agent(id: $id) {
      id
      ...AgentAddressesFragment
    }
  }

  ${gqlAgentAddressesFragment}
`;

