/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {LoadingSplashScreen, ErrorBoundary} from "@synaptix/ui";
import {Box} from "@mui/material";
import invariant from "invariant";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {gqlAgentAddresses} from "./gql/AgentAddresses.gql";


export function AgentAddresses(props) {
  return (
    <ErrorBoundary>
      <AgentAddressesCode {...props} />
    </ErrorBoundary>
  );
}

function AgentAddressesCode({agentId, className} = {}) {
  invariant(agentId, "agentId is required");

  const {t} = useTranslation();

  const {data, loading, fetchMore} = useQuery(gqlAgentAddresses, {
    variables: {
      id: agentId
    }
  });

  return loading || !data ? (
    <LoadingSplashScreen />
  ) : (
    <Choose>
      <When condition={data.agent.addresses.edges.length > 0}>
        <List className={className} disablePadding dense>
          {data.agent.addresses.edges.map(({node: address}, index) => (
            <ListItem key={index}>
              <ListItemText
                primary={t(`AGENT.ADDRESSES_LABELS.${(address.label || "OTHER").toUpperCase()}`)}
                secondary={
                  <>
                    <If condition={address.street1}>
                      <>
                        {address.street1} <br />
                      </>
                    </If>

                    <If condition={address.street2}>
                      <>
                        {address.street2} <br />
                      </>
                    </If>

                    <If condition={address.postalCode || address.city}>
                      <>
                        {address.postalCode} {address.city} <br />
                      </>
                    </If>

                    <If condition={address.country}>
                      <>{address.country}</>
                    </If>
                  </>
                }
              />
            </ListItem>
          ))}
        </List>
      </When>
      <Otherwise>
        <Box sx={(theme) => ({color: theme.palette.text.emptyHint})}>
          {t("AGENT.NO_ADDRESS")}
        </Box>
      </Otherwise>
    </Choose>
  );
}
