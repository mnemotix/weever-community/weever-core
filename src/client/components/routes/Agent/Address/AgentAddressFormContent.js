/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";

import {SelectField, TextField} from "@synaptix/ui";

const optionsValues = ["work", "home", "birth"];

// just display only the form content
// don't take data as props to avoid using callback on field edit
export function AgentAddressFormContent() {
  const {t} = useTranslation();

  // convert array of string to array of object id/name
  const options = optionsValues.map(value => ({
    value,
    label: t(`AGENT.ADDRESS.LABELS.${value.toUpperCase()}`)
  }));

  return (
    <>
      <Grid container spacing={2}>        
        <Grid item xs={12} md={2}>
          <SelectField required={true} label={t("AGENT.ADDRESS.TYPE")} name="label" id="label" options={options} />
        </Grid>

        <Grid item xs={12} md={10} container spacing={2}>
          <Grid item xs={12}>
            <TextField name="street1" label={t("AGENT.ADDRESSES_LABELS.STREET1")} />
          </Grid>

          <Grid item xs={12}>
            <TextField name="street2" label={t("AGENT.ADDRESSES_LABELS.STREET2")} />
          </Grid>

          <Grid item xs={12} container spacing={2}>
            <Grid item xs={4}>
              <TextField name="postalCode" label={t("AGENT.ADDRESSES_LABELS.POSTALCODE")} />
            </Grid>

            <Grid item xs={4}>
              <TextField name="city" label={t("AGENT.ADDRESSES_LABELS.CITY")} />
            </Grid>

            <Grid item xs={4}>
              <TextField name="country" label={t("AGENT.ADDRESSES_LABELS.COUNTRY")} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
}
