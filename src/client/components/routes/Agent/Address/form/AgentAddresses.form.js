import {LinkInputDefinition} from "@mnemotix/synaptix-client-toolkit";
import {addressFormDefinition} from "./Address.form";

/**
 * @type {LinkInputDefinition}
 */
export const agentAddressLinkInputDefinition = new LinkInputDefinition({
  name: "addresses",
  isPlural: true,
  inputName: "addressInputs",
  targetObjectFormDefinition: addressFormDefinition
});