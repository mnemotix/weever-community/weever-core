import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, string} from "yup";
import {gqlAddressFragment} from "../gql/AgentAddresses.gql";

export function getAddressValidationSchema({t}) {
  return object().shape({
    label: string().required(t("FORM_ERRORS.FIELD_ERRORS.LABEL_REQUIRED"))
  });
}

export const addressFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["label", "postalCode", "city", "country", "street1", "street2"],
    gqlFragment: gqlAddressFragment,
    gqlFragmentName: "AddressFragment"
  }),
  validationSchema: getAddressValidationSchema
});
