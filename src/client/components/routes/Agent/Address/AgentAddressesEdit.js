/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {ErrorBoundary, LoadingSplashScreen, PluralLinkField} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {ListItemText} from "@mui/material";
import {gqlAgentAddresses} from "./gql/AgentAddresses.gql";
import {AgentAddressFormContent} from "./AgentAddressFormContent";
import {agentAddressLinkInputDefinition} from "./form/AgentAddresses.form";

export function AgentAddressesEdit(props) {
  return (
    <ErrorBoundary>
      <AgentAddressesEditCode {...props} />
    </ErrorBoundary>
  );
}

function AgentAddressesEditCode({agentId} = {}) {
  const {t} = useTranslation();

  const [getAgentAddresss, {data, loading}] = useLazyQuery(gqlAgentAddresses);

  useEffect(() => {
    if (agentId) {
      getAgentAddresss({
        variables: {
          id: agentId
        }
      })
    }
  }, [agentId])

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <PluralLinkField
      data={data?.agent}
      linkInputDefinition={agentAddressLinkInputDefinition}
      renderObjectContent={(address) => (
        <ListItemText
          primary={t(`AGENT.ADDRESS.LABELS.${(address.label || "OTHER").toUpperCase()}`)}
          secondary={[address.street1, address.city, address.country].filter(n => n).join(", ")}
        />
      )}
      renderObjectForm={() => <AgentAddressFormContent />}
      addButtonLabel={t("AGENT.ADDRESS.NEW")}
    />
  );
}