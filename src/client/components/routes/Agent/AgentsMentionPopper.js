/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {getImageThumbnail, MentionPopperAC} from "@synaptix/ui";

import {Box, Avatar, ListItemIcon, Typography} from "@mui/material";

import {gqlAgents} from "./gql/Agents.gql";


/**
 * @param {string} qs
 * @param {function} onSelect
 */
export function AgentsMentionPopper({qs, onSelect} = {}) {
  return (
    <MentionPopperAC
      qs={qs}
      gqlEntitiesQuery={gqlAgents}
      gqlEntitiesConnectionPath={"agents"}
      gqlVariables={{
        first: 10
      }}
      renderEntity={agent => (
        <>
          <ListItemIcon sx={(theme) => ({minWidth: theme.spacing(2), marginRight: theme.spacing(1)})}>
            <Avatar
              src={getImageThumbnail({
                imageUrl: agent.avatar,
                size: "small"
              })}
              sx={(theme) => ({
                height: theme.spacing(2),
                width: theme.spacing(2),
                fontSize: 0.5 * theme.typography.fontSize
              })}
            >
              {(agent.fullName || agent.name || "").charAt(0)}
              {(agent.lastName || "").charAt(0)}
            </Avatar>
          </ListItemIcon>
          <Typography variant="inherit" noWrap>
            {agent.fullName || agent.name}
          </Typography>
        </>
      )}
      onSelect={onSelect}
    />
  );
}
