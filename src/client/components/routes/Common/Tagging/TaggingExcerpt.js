import {useQuery} from "@apollo/client";
import {gqlTagging} from "./gql/Tagging.gql";
import {Chip, Skeleton} from "@mui/material";

export function TaggingExcerpt({id} = {}) {
  const {data: {tagging} = {}, loading} = useQuery(gqlTagging, {
    variables: {
      id
    }
  });

  return loading ? (
    <Skeleton width={100} sx={{display: "inline-block"}} />
  ) : (
    <Chip variant="outlined" size="small" label={tagging?.concept?.prefLabel || "..."} />
  );
}
