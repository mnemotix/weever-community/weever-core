import {useQuery} from "@apollo/client";
import {gqlAccessPolicy} from "./gql/AccessPolicy.gql";
import {Box, Skeleton} from "@mui/material";

export function AccessPolicyExcerpt({id} = {}) {
  const {data: {accessPolicy} = {}, loading} = useQuery(gqlAccessPolicy, {
    variables: {
      id
    }
  });

  return loading ? (
    <Skeleton width={100} sx={{display: "inline-block"}} />
  ) : (
    <Box sx={{fontWeight: "bold", display: "inline"}}>{accessPolicy?.label || "Privé"}</Box>
  );
}
