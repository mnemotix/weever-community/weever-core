import {useEffect, useState} from "react";
import {useLazyQuery} from "@apollo/client";
import {Typography, Grid, ListItemText} from "@mui/material";
import {useTranslation} from "react-i18next";

import {AccessTargetAutocomplete} from "../../../Admin/AccessTarget/AccessTargetAutocomplete";
import {PluralLinkField} from "@synaptix/ui";

import {gqlEntityAccessTargets} from "./gql/EntityAccessTargets.gql.js";
import {
  entityReadOnlyAccessTargetInputDefinition,
  entityReadWriteAccessTargetInputDefinition
} from "./form/EntityAccessTargets.form.js";
import {LoadingSplashScreen, Accordion} from '@synaptix/ui';


/**
 *
 */
export function EntityAccessTargetsPanel({entityId, panelTitle, defaultExpanded = true} = {}) {

  const {t} = useTranslation();

  const [getAccessTargets, {data: {entity} = {}, loading} = {}] = useLazyQuery(gqlEntityAccessTargets);
  const [normalizedEntity, setNormalizedEntity] = useState();

  useEffect(() => {
    if (entityId) {
      getAccessTargets({
        variables: {
          entityId
        }
      });
    }
  }, [entityId]);

  useEffect(() => {
    if (entity) {
      setNormalizedEntity(normalizeEntity(entity));
    }
  }, [entity]);

  const creatorId = entity?.creatorUserAccount?.id;

  return (
    <Accordion defaultExpanded={defaultExpanded} title={panelTitle}>
      <Choose>
        <When condition={loading}>
          <LoadingSplashScreen />
        </When>
        <Otherwise>
          <Grid container>
            <Grid xs={6} item>
              <Typography variant="subtitle1">{t("PROJECT.ACTIONS.ACCESS_RIGHTS.READ_ONLY_TITLE")}</Typography>

              {renderAccessTargetListEdit({
                linkInputDefinition: entityReadOnlyAccessTargetInputDefinition
              })}
            </Grid>
            <Grid xs={6} item>
              <Typography variant="subtitle1">{t("PROJECT.ACTIONS.ACCESS_RIGHTS.READ_WRITE_TITLE")}</Typography>

              {renderAccessTargetListEdit({
                linkInputDefinition: entityReadWriteAccessTargetInputDefinition
              })}
            </Grid>
          </Grid>
        </Otherwise>
      </Choose>
    </Accordion>
  );

  function renderAccessTargetListEdit({linkInputDefinition}) {
    return (
      <PluralLinkField
        data={normalizedEntity}
        linkInputDefinition={linkInputDefinition}
        renderObjectContent={(accessTarget) => {
          if (accessTarget.__typename === "UserGroup") {
            return (
              <ListItemText
                primary={accessTarget.label}
                secondary={accessTarget.asGlobalRole ? t("ACCESS_TARGET.USER_GROUP.GLOBAL_ROLE") : ""}
              />
            );
          } else {
            return (
              <ListItemText
                primary={accessTarget.fullName}
                secondary={
                  entity.creatorUserAccount?.id === accessTarget.id ? t("ACCESS_TARGET.USER_ACCOUNT.OWNER") : ""
                }
              />
            );
          }
        }}
        isObjectEditable={(accessTarget) => {
          if (accessTarget.__typename === "UserGroup") {
            return !accessTarget.asGlobalRole;
          } else {
            return entity.creatorUserAccount?.id !== accessTarget.id;
          }
        }}
        renderObjectAutocomplete={({...props}) => {
          return <AccessTargetAutocomplete {...props} />;
        }}
        addButtonLabel={t("ACCESS_TARGET.ADD")}
      />
    );
  }

  function normalizeEntity(entity) {
    let normalizedEntity = {};

    for (const connectionKey of ["readWriteAccessTargets", "readOnlyAccessTargets"]) {
      let userAccounts = [];
      let userGroups = [];

      const userAccountIdsInGroups = (entity[connectionKey]?.edges || []).reduce((ids, {node: accessTarget}) => {
        if (accessTarget.__typename === "UserGroup") {
          ids = ids.concat(
            ids,
            accessTarget.userAccounts.edges.map(({node}) => node.id)
          );

          userGroups.push({node: accessTarget});
        } else {
          userAccounts.push({node: accessTarget});
        }
        return ids;
      }, []);

      normalizedEntity[connectionKey] = {
        edges: [
          ...userGroups,
          ...userAccounts.filter(({node}) => creatorId === node.id || !userAccountIdsInGroups.includes(node.id))
        ]
      };
    }

    return normalizedEntity;
  }
}
