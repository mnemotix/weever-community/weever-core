import {gqlUserAccountFragment} from "../../../../Admin/UserAccount/gql/UserAccount.gql";
import {getUserAccountValidationSchema} from "../../../../Admin/UserAccount/form/UserAccount.form";
import {DynamicFormDefinition, MutationConfig, LinkInputDefinition} from "@synaptix/ui";

export const accessTargetFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["username"],
    gqlFragment: gqlUserAccountFragment,
    gqlFragmentName: "UserAccountFragment"
  }),
  validationSchema: getUserAccountValidationSchema
});

/**
 * @type {LinkInputDefinition}
 */
export const entityReadOnlyAccessTargetInputDefinition = new LinkInputDefinition({
  name: "readOnlyAccessTargets",
  inputName: "readOnlyAccessTargetInputs",
  isPlural: true,
  targetObjectFormDefinition: accessTargetFormDefinition
});

export const entityReadWriteAccessTargetInputDefinition = new LinkInputDefinition({
  name: "readWriteAccessTargets",
  inputName: "readWriteAccessTargetInputs",
  isPlural: true,
  targetObjectFormDefinition: accessTargetFormDefinition
});
