import {useQuery} from "@apollo/client";
import {gqlAccessTarget} from "./gql/AccessTarget.gql";
import {Skeleton, Box} from "@mui/material";
import {createLink} from "@synaptix/ui";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../../../routes";

export function AccessTargetExcerpt({id} = {}) {
  const {data: {accessTarget} = {}, loading} = useQuery(gqlAccessTarget, {
    variables: {
      id
    }
  });

  return loading ? (
    <Skeleton width={100} sx={{display: "inline-block"}} />
  ) : (
    <Choose>
      <When condition={accessTarget?.__typename === "UserGroup"}>
        <Box sx={{fontWeight: "bold", display: "inline"}}>{accessTarget?.label}</Box>
      </When>
      <When condition={accessTarget?.__typename === "UserAccount"}>
        {createLink({
          to: formatRoute(ROUTES.PERSON, {id: accessTarget.person?.id}),
          text: accessTarget.fullName
        })}
      </When>
    </Choose>
  );
}
