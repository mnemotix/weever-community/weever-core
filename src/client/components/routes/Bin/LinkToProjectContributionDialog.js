/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Button as MuiButton, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import {ProjectContributionsAutocomplete, Button} from '@synaptix/ui';
import {useState} from "react";
import {useTranslation} from "react-i18next";
import {useApolloClient, useMutation, gql} from "@apollo/client";
import {useSnackbar} from "notistack";
import {ErrorBoundary} from "@synaptix/ui";

/**
 * @param open
 * @param onClose
 * @param selectedResources
 * @param selectedProject
 * @return {*}
 * @constructor
 */
export function LinkToProjectContributionDialog(props) {
  return (
    <ErrorBoundary>
      <LinkToProjectContributionDialogCode {...props} />
    </ErrorBoundary>
  );
}

function LinkToProjectContributionDialogCode({open, onClose, selectedResources, selectedProject} = {}) {
  const {t} = useTranslation();
  const apolloClient = useApolloClient();
  const {enqueueSnackbar} = useSnackbar();
  const [selectedProjectContribution, setSelectedProjectContribution] = useState(null);

  const [linkResourcesToProjectContribution, {loading: savingMutation}] = useMutation(
    generateLinkResourcesToProjectContributionMutation({
      resources: selectedResources,
      projectContribution: selectedProjectContribution
    }),
    {
      onCompleted: async () => {
        enqueueSnackbar(t("BIN.ACTIONS.LINK_TO_PROJECT_CONTRIBUTION.SUCCESS"), {variant: "success"});
        await apolloClient.reFetchObservableQueries();
      }
    }
  );
   
  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{t(`BIN.ACTIONS.LINK_TO_PROJECT_CONTRIBUTION.TITLE`)}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {t(`BIN.ACTIONS.LINK_TO_PROJECT_CONTRIBUTION.DESCRIPTION`, {project: selectedProject?.title})}
        </DialogContentText>
        <ProjectContributionsAutocomplete
          projectId={selectedResources[0]?.project?.id}
          onSelect={setSelectedProjectContribution}
          selectedObject={selectedProjectContribution}
        />
      </DialogContent>
      <DialogActions>
        <MuiButton onClick={onClose} color="primary">
          {t("ACTIONS.CANCEL")}
        </MuiButton>
        <Button onClick={handleProcessAction} color="primary" autoFocus loading={savingMutation}>
          {t("ACTIONS.PROCEED")}
        </Button>
      </DialogActions>
    </Dialog>
  );

  async function handleProcessAction() {
    await linkResourcesToProjectContribution();
    onClose();
  }

  /**
   * @param resources
   * @param projectContribution
   */
  function generateLinkResourcesToProjectContributionMutation({resources, projectContribution}) {
    return gql`
      mutation LinkResourcesToProjectContribution{
        ${!projectContribution || resources.length === 0
        ? "description"
        : resources.map(
          (resource, index) => `
        updateFile${index}: updateFile(input: {objectId: "${resource.id}" objectInput: {attachmentInputs: [{projectContributionInput: {id: "${projectContribution.id}"}}]}}){
          updatedObject{
            id
            attachments{
              edges{
                node{
                  projectContribution{
                    id
                  }
                }
              }
            }
          }
        }
      `
        )
      }
      }
    `;
  }
}
