/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {Resources, SplitButton} from "@synaptix/ui";
import {Box, Button as MuiButton, Paper, Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import SentimentSatisfiedAltIcon from "@mui/icons-material/SentimentSatisfiedAlt";
import {LinkToProjectDialog} from "./LinkToProjectDialog";
import {LinkToProjectContributionDialog} from "./LinkToProjectContributionDialog";
import {LinkToNewProjectContributionDialog} from "./LinkToNewProjectContributionDialog";
import {useLoggedUser} from "@synaptix/ui";
import {formatRoute} from "react-router-named-routes";
import {ImportDialogContainer} from "../Import/ImportDialog";
import {createGqlFilters} from "../../utilities/tools";
import {ROUTES} from "../../../routes";

export default function Bin() {
  const {t} = useTranslation();
  const [selectedResources, setSelectedResources] = useState([]);
  const [actionKey, setActionKey] = useState(null);
  const [selectedProject, setSelectedProject] = useState(null);
  const [heterogeneousSelectedResources, setHeterogeneousSelectedResources] = useState(false);
  const {isEditor, isContributor} = useLoggedUser();

  const gqlFilters = createGqlFilters({filterByExistingAttachment: false, filterByExistingProject: true});

  return (
    <>
      <Paper>
        <Resources
          displayMode="table"
          gqlFilters={gqlFilters}
          onSelectResources={handleSelectedResources}
          generateItemRoute={(node) => formatRoute(ROUTES.RESOURCE, {id: node.id})}
          removalEnabled={isEditor}
          batchEditEnabled={true}
          renderActions={() => (
            <>
              <MuiButton
                variant="contained"
                disabled={selectedResources.length === 0}
                onClick={() => handleOpenDialogAction("LINK_TO_PROJECT")}>
                {t("BIN.ACTIONS.LINK_TO_PROJECT.TITLE")}
              </MuiButton>

              <ImportDialogContainer
                disabled={!isContributor}
                filterOnProject={{}}
                filterOnProjectContribution={{}}
              />
              <SplitButton
                variant="contained"
                disabled={selectedResources.length === 0 || heterogeneousSelectedResources}
                options={[
                  {
                    label: t("BIN.ACTIONS.LINK_TO_PROJECT_CONTRIBUTION.TITLE"),
                    onClick: () => handleOpenDialogAction("LINK_TO_PROJECT_CONTRIBUTION")
                  },
                  {
                    label: t("BIN.ACTIONS.LINK_TO_NEW_PROJECT_CONTRIBUTION.TITLE"),
                    onClick: () => handleOpenDialogAction("LINK_TO_NEW_PROJECT_CONTRIBUTION")
                  }
                ]}
              />
            </>
          )}
          NoResultComponent={() => (
            <Paper variant={"outlined"}>
              <Grid container justifyContent="center" alignItems="center" alignContent="center">
                <Grid item>
                  <SentimentSatisfiedAltIcon
                    fontSize="large"
                    sx={(theme) => ({
                      height: theme.spacing(20),
                      width: theme.spacing(20),
                      verticalAlign: "middle",
                      marginRight: theme.spacing(0.5),
                      color: theme.palette.grey["300"],
                      margin: [[theme.spacing(20), 0]]
                    })}
                  />
                </Grid>
                <Grid item>
                  <Box sx={(theme) => ({fontSize: theme.typography.fontSize * 3, color: theme.palette.grey["700"]})}>
                    {t("BIN.EMPTY_TITLE")}
                  </Box>
                  <Box sx={(theme) => ({fontSize: theme.typography.fontSize * 1.3})}>{t("BIN.EMPTY_DESCRIPTION")}</Box>{" "}
                </Grid>
              </Grid>
            </Paper>
          )}
        />
      </Paper>

      <LinkToProjectDialog
        onClose={handleCloseDialogAction}
        selectedProject={selectedProject}
        onSelectProject={setSelectedProject}
        selectedResources={selectedResources}
        open={actionKey === "LINK_TO_PROJECT"}
      />

      <LinkToProjectContributionDialog
        onClose={handleCloseDialogAction}
        selectedProject={selectedProject}
        selectedResources={selectedResources}
        open={actionKey === "LINK_TO_PROJECT_CONTRIBUTION"}
      />

      <LinkToNewProjectContributionDialog
        onClose={handleCloseDialogAction}
        selectedProject={selectedProject}
        selectedResources={selectedResources}
        open={actionKey === "LINK_TO_NEW_PROJECT_CONTRIBUTION"}
      />
    </>
  );

  /**
   * Handler after selecting resources.
   * - Update local state.
   * - Check that all selected resources belongs to same project
   * @param resources
   */
  function handleSelectedResources(resources) {
    setSelectedResources(resources);

    if (resources.length > 0) {
      const project = resources[0].project;

      if (project) {
        setSelectedProject(project);
        for (let resource of resources.slice(1)) {
          if (!resource.project || resource?.project?.id !== project.id) {
            return setHeterogeneousSelectedResources(true);
          }
        }
      }
    } else {
      setSelectedProject(null);
    }

    setHeterogeneousSelectedResources(false);
  }

  /**
   * Handlet to open the action dialog.
   * @param {string} actionKey - (LINK_TO_PROJECT | LINK_TO_PROJECT_CONTRIBUTION)
   */
  function handleOpenDialogAction(actionKey) {
    setActionKey(actionKey);
  }

  /**
   *
   */
  function handleCloseDialogAction() {
    setActionKey(null);
  }
}
