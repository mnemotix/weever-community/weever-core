/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Button as MuiButton, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import {Form, Formik} from "formik";
import {ProjectContributionFormContent} from "../ProjectContribution/ProjectContributionFormContent";
import {useTranslation} from "react-i18next";
import {useApolloClient, useMutation, gql} from "@apollo/client";
import {useSnackbar} from "notistack";
import {FormSubmitButtonWithTooltip, ErrorBoundary} from "@synaptix/ui";
import {projectContributionFormDefinition} from "../ProjectContribution/form/ProjectContribution.form";
import {gqlCreateProjectContribution} from "../ProjectContribution/gql/CreateProjectContribution.gql";

export function LinkToNewProjectContributionDialog(props) {
  return (
    <ErrorBoundary>
      <LinkToNewProjectContributionDialogCode {...props} />
    </ErrorBoundary>
  );
}

function LinkToNewProjectContributionDialogCode({open, onClose, selectedResources, selectedProject} = {}) {
  const {t} = useTranslation();
  const apolloClient = useApolloClient();
  const {enqueueSnackbar} = useSnackbar();

  const [createProjectContribution, {loading: savingMutation}] = useMutation(
    gqlCreateProjectContribution,
    {
      onCompleted: async () => {
        enqueueSnackbar(t("BIN.ACTIONS.LINK_TO_PROJECT_CONTRIBUTION.SUCCESS"), {variant: "success"});
        await apolloClient.reFetchObservableQueries();
      }
    }
  );

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth={"md"}>
      <Formik
        validationSchema={projectContributionFormDefinition.getValidationSchema({t})}
        initialValues={projectContributionFormDefinition.getInitialValues({})}
        onSubmit={(values) => {
          handleProcessAction(values);
        }}
        validateOnChange={true}
        validateOnBlur={true}>
        {({errors}) => {
          return (
            <Form>
              <DialogTitle>{t(`BIN.ACTIONS.LINK_TO_NEW_PROJECT_CONTRIBUTION.TITLE`)}</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  {t(`BIN.ACTIONS.LINK_TO_NEW_PROJECT_CONTRIBUTION.DESCRIPTION`, {project: selectedProject?.title})}
                </DialogContentText>
                <ProjectContributionFormContent />
              </DialogContent>
              <DialogActions>
                <MuiButton onClick={onClose} color="primary">
                  {t("ACTIONS.CANCEL")}
                </MuiButton>
                <FormSubmitButtonWithTooltip
                  saving={savingMutation}
                  label={t("ACTIONS.PROCEED")}
                />
              </DialogActions>
            </Form>
          );
        }}
      </Formik>
    </Dialog>
  );

  async function handleProcessAction(projectContributionInput) {
    await createProjectContribution({
      variables: {
        input: {
          objectInput: {
            ...projectContributionInput,
            attachmentInputs: selectedResources.map((resource) => ({resourceInput: {id: resource.id}})),
            projectInput: {
              id: selectedProject.id
            }
          }
        }
      }
    });
    onClose();
  }
}
