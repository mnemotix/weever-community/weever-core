/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Button as MuiButton, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import {ProjectsAutocomplete,Button} from '@synaptix/ui';
import {useTranslation} from "react-i18next";
import {useMutation,gql} from "@apollo/client";
import {useSnackbar} from "notistack";
import {ErrorBoundary} from "@synaptix/ui";

/**
 * @param open
 * @param onClose
 * @param selectedResources
 * @param selectedProject
 * @param onSelectProject
 * @return {*}
 * @constructor
 */
export function LinkToProjectDialog(props) {
  return (
    <ErrorBoundary>
      <LinkToProjectDialogCode {...props} />
    </ErrorBoundary>
  );
}
function LinkToProjectDialogCode({open, onClose, selectedResources, selectedProject, onSelectProject} = {}) {
  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();

  const [linkResourcesToProject, {loading: savingMutation}] = useMutation(
    generateLinkResourcesToProjectMutation({
      resources: selectedResources,
      project: selectedProject
    }),
    {
      onCompleted: () => {
        enqueueSnackbar(t("BIN.ACTIONS.LINK_TO_PROJECT.SUCCESS"), {variant: "success"});
      }
    }
  );

  return (
    <Dialog open={open} onClose={onClose}>
      <DialogTitle>{t(`BIN.ACTIONS.LINK_TO_PROJECT.TITLE`)}</DialogTitle>
      <DialogContent>
        <DialogContentText>{t(`BIN.ACTIONS.LINK_TO_PROJECT.DESCRIPTION`)}</DialogContentText>
        <ProjectsAutocomplete onSelect={onSelectProject} selectedObject={selectedProject} />
      </DialogContent>
      <DialogActions>
        <MuiButton onClick={onClose} color="primary">
          {t("ACTIONS.CANCEL")}
        </MuiButton>
        <Button onClick={handleProcessAction} color="primary" autoFocus loading={savingMutation}>
          {t("ACTIONS.PROCEED")}
        </Button>
      </DialogActions>
    </Dialog>
  );

  async function handleProcessAction() {
    await linkResourcesToProject();
    onClose();
  }

  /**
   * @param resources
   * @param project
   */
  function generateLinkResourcesToProjectMutation({resources, project}) {
    return gql`
      mutation LinkResourcesToProject{
        ${
          !selectedProject || resources.length === 0
            ? "description"
            : resources.map(
                (resource, index) => `
        updateFile${index}: updateFile(input: {objectId: "${resource.id}" objectInput: {projectInput: {id: "${project.id}"}}}){
          updatedObject{
            id
            project{
              id
              title
            }
          }
        }
      `
              )
        }
      }
    `;
  }
}
