import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, string} from "yup";
import {gqlOrganizationFragment} from "../gql/Organization.gql";

export function getOrganizationValidationSchema({t}) {
  return object().shape({
    name: string().required(t("ORGANIZATION.FIELD_ERRORS.NAME_REQUIRED"))
  });
}

export const organizationFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["name", "avatar", "shortDescription", "description"],
    gqlFragment: gqlOrganizationFragment
  }),
  validationSchema: getOrganizationValidationSchema
});
