import {gql} from "@apollo/client";

export const gqlCreateOrganization = gql`
  mutation CreateOrganization($input: CreateOrganizationInput!) {
    createOrganization(input: $input) {
      createdObject {
        id
      }
    }
  }
`;
