import {gql} from "@apollo/client";

export const gqlUpdateOrganization = gql`
  mutation UpdateOrganization($input: UpdateOrganizationInput!) {
    updateOrganization(input: $input) {
      updatedObject {
        id
      }
    }
  }
`;
