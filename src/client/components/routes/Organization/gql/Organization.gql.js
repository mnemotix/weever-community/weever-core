import {gql} from "@apollo/client";

export const gqlOrganizationBasicProps = ["id", "avatar", "name", "description", "shortDescription"];

export const gqlOrganizationFragment = gql`
  fragment OrganizationFragment on Organization {
    id
    avatar
    name
    nameTranslated
    description
    descriptionTranslated
    shortDescription
    shortDescriptionTranslated
  }
`;

export const gqlOrganization = gql`
  query Organization($id: ID!) {
    organization(id: $id) {
      ...OrganizationFragment
      createdAt
      createdBy
      creatorPerson {
        id
      }
      seeAlso
      permissions {
        update
        grant
      }
    }
  }

  ${gqlOrganizationFragment}
`;
