/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Typography, Grid} from "@mui/material";
import {useTranslation} from "react-i18next";

import {AgentPhonesEdit} from "../Agent/Phone/AgentPhonesEdit";
import {AgentEmailsEdit} from "../Agent/Email/AgentEmailsEdit";
import {AgentAddressesEdit} from "../Agent/Address/AgentAddressesEdit";
import {AgentAffiliationsEdit} from "../Agent/Affiliation/AgentAffiliationsEdit";
import {OrganizationFormContent} from "./OrganizationFormContent";
import {DynamicForm, TaggingsField, ExternalLinksEdit, AccessPolicyField} from "@synaptix/ui";
import {organizationFormDefinition} from "./form/Organization.form";

/**
 * @return {*}
 * @constructor
 */
export function OrganizationDynamicForm({organization, fullForm, mutateFunction, saving} = {}) {
  const {t} = useTranslation();

  return (
    <DynamicForm
      object={organization}
      formDefinition={organizationFormDefinition}
      mutateFunction={mutateFunction}
      saving={saving}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <OrganizationFormContent id={organization?.id} />
        </Grid>

        <If condition={fullForm}>
          <Grid item container spacing={2}>
            <Grid item xs={12}>
              <AccessPolicyField entityId={organization?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("AGENT.EMAILS")}</Typography>

              <AgentEmailsEdit agentId={organization?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("AGENT.PHONES")}</Typography>

              <AgentPhonesEdit agentId={organization?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("AGENT.ADDRESSES")}</Typography>

              <AgentAddressesEdit agentId={organization?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ENTITY.EXTERNAL_LINKS")}</Typography>

              <ExternalLinksEdit entityId={organization?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ORGANIZATION.AFFILIATIONS")}</Typography>

              <AgentAffiliationsEdit agentId={organization?.id} agentType={"Organization"} />
            </Grid>

            <Grid item xs={12}>
              <TaggingsField entityId={organization?.id} />
            </Grid>
          </Grid>
        </If>
      </Grid>
    </DynamicForm>
  );
}
