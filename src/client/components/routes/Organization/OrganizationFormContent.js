import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import * as Yup from "yup";
import {TranslatableField, TextField, RichTextAreaField, ImagePickerField} from "@synaptix/ui";
import {useUploadService} from "../Import/BinWorkflow/UploadService";
import {FileUploadDashboard} from "../Import/BinWorkflow/FileUploadDashboard";

export const formikValidationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Too Short!")
    .required("Required")
});

export function OrganizationFormContent() {
  const {t} = useTranslation();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <TranslatableField FieldComponent={TextField} required={true} name="name" label={t("ORGANIZATION.NAME")} />
      </Grid>
      <Grid item xs={12}>
        <TranslatableField
          FieldComponent={RichTextAreaField}
          name="shortDescription"
          label={t("ORGANIZATION.SHORT_DESCRIPTION")}
        />
      </Grid>

      <Grid item xs={12}>
        <TranslatableField
          FieldComponent={RichTextAreaField}
          name="description"
          label={t("ORGANIZATION.DESCRIPTION")}
        />
      </Grid>

      <Grid item xs={12}>
        <ImagePickerField
          name={"avatar"}
          label={t("AGENT.AVATAR")}
          useUploadService={useUploadService}
          FileUploadDashboard={FileUploadDashboard}
          smallPreview={true}
        />
      </Grid>
    </Grid>
  );
}
