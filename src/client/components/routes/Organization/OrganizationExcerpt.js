import {LoadingSplashScreen, Excerpt} from "@synaptix/ui";
import {useQuery} from "@apollo/client";
import {gqlOrganization} from "./gql/Organization.gql";

export function OrganizationExcerpt({id, creatorHidden}) {
  const {data, loading} = useQuery(gqlOrganization, {
    variables: {
      id
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <Excerpt
      picture={data?.organization?.avatar}
      title={data?.organization?.name}
      description={data?.organization?.shortDescription || data?.organization?.description}
      createdAt={data?.organization?.createdAt}
      createdBy={data?.organization?.createdBy}
      creatorHidden={creatorHidden}
    />
  );
}
