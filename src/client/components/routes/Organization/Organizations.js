/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";

import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../routes";
import {Link} from "react-router-dom";
import {Avatar, Typography, Box} from "@mui/material";
import {gqlOrganizations} from "./gql/Organizations.gql";

import {
  CollectionDataProvider,
  CollectionView,
  Button,
  useLoggedUser,
  useResponsive,
  createLink,
  getImageThumbnail,
  generateCollectionViewGenericColumns,
  EntityFormContentBatch,
  FloatingButton
} from "@synaptix/ui";

import {organizationFormDefinition} from "./form/Organization.form";
import {gqlBatchUpdateOrganizations} from "./gql/BatchUpdateOrganization.gql";

export function getColumns({t}) {
  return [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    {
      name: "avatar",
      label: " ",
      altLabel: t("SEARCH.COLUMNS.COMMON.IMAGE"),
      options: {
        filter: false,
        sort: false,
        customBodyRender: (image, {row: organization}) => {
          return (
            <Box
              sx={{
                my: -1
              }}>
              <Link style={{textDecoration: "none"}} to={formatRoute(ROUTES.ORGANIZATION, {id: organization.id})}>
                <Avatar
                  src={getImageThumbnail({
                    imageUrl: image,
                    size: "small"
                  })}>
                  {(organization.name || "").charAt(0)}
                </Avatar>
              </Link>
            </Box>
          );
        },
        setCellProps: () => ({
          sx: (theme) => ({width: theme.spacing(8)})
        })
      }
    },
    {
      name: "name",
      label: t("SEARCH.COLUMNS.ORGANIZATIONS.NAME"),
      // displayAsMainTitle:true,
      options: {
        filter: false,
        sort: true,
        customBodyRender: (name, {row}) => {
          return (
            <Typography variant="subtitle1">
              {createLink({
                to: formatRoute(ROUTES.ORGANIZATION, {id: row.id}),
                text: name
              })}
            </Typography>
          );
        }
      }
    },
    ...generateCollectionViewGenericColumns({t})
  ];
}

export default function Organizations({qs} = {}) {
  const {t} = useTranslation();

  const {isDesktop} = useResponsive();
  const {isEditor, isContributor} = useLoggedUser();

  const columns = getColumns({t});

  return (
    <>
      <CollectionDataProvider gqlConnectionPath={"organizations"} gqlQuery={gqlOrganizations} qs={qs}>
        <CollectionView
          collectionKey={"OrganizationsList"}
          availableDisplayModes={{table: {}}}
          columns={columns}
          renderRightSideActions={() => {
            return isDesktop ? (
              <Button disabled={!isContributor} to={formatRoute(ROUTES.ORGANIZATION_CREATE)}>
                {t("ORGANIZATION.CREATE")}
              </Button>
            ) : (
              <FloatingButton locked={!isContributor} to={formatRoute(ROUTES.ORGANIZATION_CREATE)} name="AddIcon" />
            );
          }}
          batchEditEnabled={isEditor}
          batchEditButtonProps={{
            gqlUpdateMutation: gqlBatchUpdateOrganizations,
            entityFormDefinition: organizationFormDefinition,
            entityFormContent: <EntityFormContentBatch formContentFragmentPrefix="Organization" />,
            title: ({entityTotalCount}) => t("ORGANIZATION.ACTIONS.BATCH_UPDATE.TITLE", {count: entityTotalCount}),
            confirmMessage: ({entityTotalCount}) =>
              t("ORGANIZATION.ACTIONS.BATCH_UPDATE.CONFIRM", {count: entityTotalCount}),
            successMessage: ({entityTotalCount}) =>
              t("ORGANIZATION.ACTIONS.BATCH_UPDATE.SUCCESS", {count: entityTotalCount})
          }}
        />
      </CollectionDataProvider>
    </>
  );
}

/*    
 <CollectionView
  collectionKey={"ResourcesList"}
  columns={columns}
  gqlConnectionPath={"organizations"}
  gqlCountPath={"organizationsCount"}
  gqlQuery={gqlOrganizations}
  qs={qs}
  availableDisplayModes={{table: {}}}
  batchEditEnabled={isEditor}
  batchEditButtonProps={{
    gqlUpdateMutation: gqlBatchUpdateOrganizations,
    entityFormDefinition: organizationFormDefinition,
    entityFormContent: <EntityFormContentBatch formContentFragmentPrefix="Organization" />,
    title: ({entityTotalCount}) => t("ORGANIZATION.ACTIONS.BATCH_UPDATE.TITLE", {count: entityTotalCount}),
    confirmMessage: ({entityTotalCount}) =>
      t("ORGANIZATION.ACTIONS.BATCH_UPDATE.CONFIRM", {count: entityTotalCount}),
    successMessage: ({entityTotalCount}) =>
      t("ORGANIZATION.ACTIONS.BATCH_UPDATE.SUCCESS", {count: entityTotalCount})
  }}
  renderRightSideActions={() => {
    return isDesktop ? (
      <Button disabled={!isContributor} to={formatRoute(ROUTES.ORGANIZATION_CREATE)}>
        {t("ORGANIZATION.CREATE")}
      </Button>
    ) : (
      <FloatingButton locked={!isContributor} to={formatRoute(ROUTES.ORGANIZATION_CREATE)} name="AddIcon" />
    );
  }}
/>
*/
