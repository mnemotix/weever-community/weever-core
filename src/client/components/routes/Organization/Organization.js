/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  createLink,
  TranslatableDisplay,
  PageNotFound,
  useResponsive,
  Button,
  LoadingSplashScreen,
  ExternalLinks,
  FloatingButton,
  Taggings,
  EntityAccessPolicy
} from "@synaptix/ui";
import {useParams} from "react-router-dom";
import {Box, Breadcrumbs, Grid, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {SanitizedHtml} from "@mnemotix/synaptix-client-toolkit";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../routes";
import {AgentPhones} from "../Agent/Phone/AgentPhones";
import {AgentEmails} from "../Agent/Email/AgentEmails";
import {AgentAddresses} from "../Agent/Address/AgentAddresses";
import {AgentAffiliations} from "../Agent/Affiliation/AgentAffiliations";
import {gqlOrganization} from "./gql/Organization.gql";
import {OrganizationAvatar} from "./OrganizationAvatar";
import {GlobalGraphButton} from "../Visualizations/GlobalGraphButton";
import {EntityActivity} from "../Activity/EntityActivity";

const classes = {
  empty: {
    color: "text.emptyHint"
  },
  action: {
    textAlign: "center",
    alignSelf: "center"
  }
};

export default function Organization({} = {}) {
  const {t} = useTranslation();
  let {id} = useParams();
  id = decodeURIComponent(id);
  const {isDesktop} = useResponsive();

  const {data: {organization} = {}, loading} = useQuery(gqlOrganization, {
    variables: {
      id
    }
  });

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (id && !organization) {
    return <PageNotFound />;
  }

  return (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <Breadcrumbs>
          {createLink({to: ROUTES.ORGANIZATIONS, text: t("BREADCRUMB.ORGANIZATIONS")})}
          <Typography color="textPrimary">{organization?.name}</Typography>
        </Breadcrumbs>
      </Grid>
      <Grid item xs={12} container columnSpacing={2}>
        <Grid item xs={12} md={9}>
          <div>
            <Typography variant="h5" gutterBottom>
              {organization?.name}
            </Typography>

            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ORGANIZATION.SHORT_DESCRIPTION")}</Typography>

                <Box sx={[!organization.shortDescription && classes.empty]}>
                  <TranslatableDisplay isTranslated={organization.shortDescriptionTranslated}>
                    <SanitizedHtml html={organization.shortDescription || t("ORGANIZATION.NO_DESCRIPTION")} />
                  </TranslatableDisplay>
                </Box>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ORGANIZATION.DESCRIPTION")}</Typography>
                <Box sx={[!organization.description && classes.empty]}>
                  <TranslatableDisplay isTranslated={organization.descriptionTranslated}>
                    <SanitizedHtml html={organization.description || t("ORGANIZATION.NO_DESCRIPTION")} />
                  </TranslatableDisplay>
                </Box>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ENTITY.ACCESS_POLICY")}</Typography>
                <EntityAccessPolicy entityId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ENTITY.TAGGINGS")}</Typography>
                <Taggings entityId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("AGENT.PHONES")}</Typography>

                <AgentPhones agentId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("AGENT.EMAILS")}</Typography>

                <AgentEmails agentId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("AGENT.ADDRESSES")}</Typography>

                <AgentAddresses agentId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ENTITY.EXTERNAL_LINKS")}</Typography>

                <ExternalLinks entityId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ORGANIZATION.AFFILIATIONS")}</Typography>

                <AgentAffiliations agentId={id} />
              </Grid>
            </Grid>
          </div>
        </Grid>

        <Grid item xs={12} md={3}>
          <Grid container spacing={2} direction="column" justifyContent="space-evenly" alignItems="center">
            <Grid item xs={12}>
              <OrganizationAvatar
                organization={organization}
                classSx={(theme) => ({
                  width: theme.spacing(40),
                  height: theme.spacing(40),
                  fontSize: theme.typography.fontSize * 5
                })}
              />
            </Grid>

            <Grid item xs={12}>
              {t("AGENT.CREATED_AT_BY", {date: dayjs(organization.createdAt).format("LL")})}

              {createLink({
                to: formatRoute(ROUTES.PERSON, {id: organization?.creatorPerson?.id}),
                text: organization?.createdBy
              })}
            </Grid>
            {isDesktop ? (
              <Grid item xs={12} container direction="row" justifyContent="center" alignItems="center" spacing={4}>
                <Grid item sx={classes.action}>
                  <Button disabled={!organization?.permissions.update} to={formatRoute(ROUTES.ORGANIZATION_EDIT, {id})}>
                    {t("ACTIONS.UPDATE")}
                  </Button>
                </Grid>

                <Grid item sx={classes.action}>
                  <GlobalGraphButton entityId={id} />
                </Grid>
              </Grid>
            ) : (
              <FloatingButton
                disabled={!organization?.permissions.update}
                to={formatRoute(ROUTES.ORGANIZATION_EDIT, {id})}
                name="EditIcon"
              />
            )}
          </Grid>
        </Grid>
      </Grid>
      <If condition={organization?.permissions?.grant}>
        <Grid item xs={12}>
          <EntityActivity entityId={id} />
        </Grid>
      </If>
    </Grid>
  );
}
