/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {createLink, LoadingSplashScreen, PageNotFound} from "@synaptix/ui";
import {useParams, useHistory} from "react-router-dom";
import {Typography, Breadcrumbs, Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useMutation, useLazyQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../routes";
import {gqlOrganization} from "./gql/Organization.gql";
import {gqlUpdateOrganization} from "./gql/UpdateOrganization.gql";
import {gqlCreateOrganization} from "./gql/CreateOrganization.gql";
import {useSnackbar} from "notistack";
import {OrganizationDynamicForm} from "./OrganizationDynamicForm";

/**
 * @return {*}
 * @constructor
 */
export default function OrganizationEdit({id, hideBreadcrumb, disableRedirect} = {}) {
  let params = useParams();
  const {t} = useTranslation();
  const history = useHistory();
  let {enqueueSnackbar} = useSnackbar();

  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const [getOrganization, {data: {organization} = {}, loading, called}] = useLazyQuery(gqlOrganization);

  useEffect(() => {
    if (id) {
      getOrganization({
        variables: {
          id
        }
      });
    }
  }, [id]);

  const [mutateOrganization, {loading: saving}] = useMutation(id ? gqlUpdateOrganization : gqlCreateOrganization, {
    onCompleted: (data) => {
      if (data.createOrganization?.createdObject?.id) {
        id = data.createOrganization.createdObject.id;
      }
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});

      if (!disableRedirect) {
        history.push(formatRoute(ROUTES.ORGANIZATION, {id}));
      }
    }
  });

  if (id) {
    if (loading || !called) {
      return <LoadingSplashScreen />;
    }

    if (!organization?.permissions?.update) {
      return <PageNotFound />;
    }
  }

  return (
    <Grid container rowSpacing={4}>
      <If condition={!hideBreadcrumb}>
        <Grid item xs={12}>
          <Breadcrumbs>
            {createLink({to: ROUTES.ORGANIZATIONS, text: t("BREADCRUMB.ORGANIZATIONS")})}
            <Typography color="textPrimary">{id ? organization.name : t("ORGANIZATION.CREATE_TITLE")}</Typography>
          </Breadcrumbs>
        </Grid>
      </If>

      <Grid item xs={12}>
        <OrganizationDynamicForm
          organization={organization}
          mutateFunction={mutateOrganization}
          fullForm
          saving={saving}
        />
      </Grid>
    </Grid>
  );
}
