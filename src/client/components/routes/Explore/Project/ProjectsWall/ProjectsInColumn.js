import {Fragment} from "react";
import {Grid, Divider} from "@mui/material";
import {ImageOnSide} from "./ImageOnSide";

/**
 * Display items in row direction ( so many items on same line )
 * @param {array} projects : items to display,
 * each item of data need to like  {id, image, color,title, shortDescription, description, createdAt, updatedAt ,creatorPerson :{fullName ,id, avatar }} , if not use dataForView to rename fields in parent
 * @param {function} generateProjectRoute : generate route for each project
 * @param {boolean} hideCreator : hide or not the creator
 */
export function ProjectsInColumn({projects = [], generateProjectRoute, hideCreator = false}) {


  return (
    <Grid container direction="column" justifyContent="space-between" alignItems="stretch" spacing={3}>
      {projects.map((project, index) => (
        <Fragment key={project.id}>
          <Grid item xs={12}>
            <ImageOnSide
              project={project}
              linkProps={{to: generateProjectRoute(project)}}
              hideCreator={hideCreator}
              imageOnRightSide
              dense
            />
          </Grid>

          <If condition={index < projects.length - 1}>
            <Grid item xs={12}>
              <Divider />
            </Grid>
          </If>
        </Fragment>
      ))}
    </Grid>
  );
}

