import {purifyHtml} from "@synaptix/ui";
import Box from '@mui/material/Box';

export function TitleDescription({title, shortDescription, description}) {
  return (
    <Box sx={{marginTop: "5px"}}>
      <Box sx={(theme) => ({
        fontSize: theme.typography.pxToRem(40),
        lineHeight: "1 !important",
        letterSpacing: "0!important",
        fontWeight: "700!important",
        fontStyle: "normal!important",
        "&$small": {
          fontSize: theme.typography.pxToRem(22)
        }
      })}>
        {title}
      </Box>
      <Box sx={(theme) => ({
        paddingTop: theme.spacing(1),
        fontSize: "17px !important",
        lineHeight: "1.3 !important",
        letterSpacing: "0!important",
        fontWeight: "300!important",
        fontStyle: "normal!important",
        color: "rgba(0,0,0,.54)!important",
        fill: "rgba(0,0,0,.54)!important",
        display: "box", // @see https://caniuse.com/#search=line-clamp
        lineClamp: 3,
        boxOrient: "vertical",
        overflow: "hidden",
        textOverflow: "ellipsis"
      })}>
        {purifyHtml(shortDescription || description || "", [])}
      </Box>
    </Box>
  );
}
