import React from "react";
import {Grid} from "@mui/material";
import {ImageOnTop} from "./ImageOnTop";

/**
 * Display items in row direction ( many items on same line )
 * @param {array} projects : items to display,
 * each item of data need to have : {id, image, color,title, shortDescription, description, createdAt, updatedAt ,creatorPerson :{fullName ,id, avatar }} , if not use dataForView to rename fields in parent
 *
 *
 * @param {function} generateProjectRoute : generate route for each project 
 * @param {boolean} [hideImage] : if true don't show the image of each element
 * @param {boolean} [hideCreator] : hide or not the creator
 *
 * sm = 6, md = 4, lg = 3
 */
export function ProjectsInRow({
  projects = [],
  generateProjectRoute,
  hideImage = false,
  hideCreator = false,
  sm = 6,
  md = 4,
  lg = 4
}) {

  return (
    <Grid container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={3}>      
      {projects.map((project) => {
        return (
          <Grid item xs={10} sm={sm} md={md} lg={lg} key={project.id} sx={{position: "relative"}}>
            <ImageOnTop
              project={project}
              linkProps={{to: generateProjectRoute(project)}}
              hideImage={hideImage}
              hideCreator={hideCreator}
            />
          </Grid>
        );
      })}
    </Grid>
  );
}

