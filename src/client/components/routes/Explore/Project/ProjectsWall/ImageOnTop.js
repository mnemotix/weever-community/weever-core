import {Link} from "react-router-dom";
import {Grid} from "@mui/material";
import {TitleDescription} from "./TitleDescription";
import {AgentAvatar} from "../../widgets/AgentAvatar";
import {ResponsiveImageBG} from "@synaptix/ui";


/**
 * display on the top : Image and on bottom : TitleDescription + CreationInformation
 */
export function ImageOnTop({project, linkProps, hideImage = false, hideCreator = false}) {
  const {id, image, color, creatorPerson, title, description, shortDescription} = project;

  let imageUrl = image;

  return (
    <Link style={{textDecoration: "none", height: "100%", flex: 1, display: "flex", color: "black"}} key={id} {...linkProps}>
      <Grid container direction={"column"} spacing={2}>
        <If condition={!hideImage}>
          <Grid item sx={(theme) => ({height: theme.spacing(22), width: "100%"})}>
            <ResponsiveImageBG url={imageUrl} color={color} size="medium" />
          </Grid>
        </If>
        <Grid item xs>
          <TitleDescription 
            title={title}
            description={description}
            shortDescription={shortDescription}
          />
        </Grid>
        <If condition={!hideCreator && creatorPerson}>
          <Grid item>
            <AgentAvatar agent={creatorPerson} />
          </Grid>
        </If>
      </Grid>
    </Link>
  );
}
