import {Link} from "react-router-dom";
import {Grid, Box} from "@mui/material";
import {TitleDescription} from "./TitleDescription";
import {AgentAvatar} from "../../widgets/AgentAvatar";
import {ResponsiveImageBG} from "@synaptix/ui";

const classes = {
  link: {
    textDecoration: "none",
    color: "black"
  }
};

const imageSx = (theme) => ({
  height: theme.spacing(42),
  [theme.breakpoints.between("xs", "sm")]: {
    height: theme.spacing(35)
  },
  [theme.breakpoints.down("xs")]: {
    height: theme.spacing(27)
  },
  width: "100%",

  "$dense &": {
    height: theme.spacing(20)
  }
});

/**
 * @param project
 * @param linkProps
 * @param hideCreator
 * @param imageOnRightSide
 * @param dense
 * @returns {JSX.Element}
 * @constructor
 */
export function ImageOnSide({project, linkProps, hideCreator = false, imageOnRightSide = false, dense = false}) {
  const {creatorPerson, image, id, title, description, shortDescription, color} = project;

  return (
    <Link style={classes.link} {...linkProps} key={id}>
      <Grid
        container
        direction={imageOnRightSide ? "row-reverse" : "row"}
        justifyContent={"space-between"}
        alignItems="stretch"
        spacing={2}
      >
        <Grid item xs={12} md={dense ? 4 : 7}>
          <Box sx={imageSx}>
            <ResponsiveImageBG url={image} color={color} size="medium" />
          </Box>
        </Grid>
        <Grid container item xs={12} md={dense ? 8 : 5} direction={"column"} spacing={2}>
          <Grid item xs>
            <TitleDescription
              title={title}
              description={description}
              shortDescription={shortDescription}
              size={dense ? "small" : "medium"}
            />
          </Grid>

          <Grid item>
            <If condition={!hideCreator && creatorPerson}>
              <AgentAvatar agent={creatorPerson} />
            </If>
          </Grid>
        </Grid>
      </Grid>
    </Link>
  );
}
