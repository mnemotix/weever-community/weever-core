/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {LoadingSplashScreen} from "@synaptix/ui";
import {CategoryTitle} from "../../widgets/CategoryTitle";
import {ROUTES} from "../../../../../routes";
import {gqlProjectInvolvements} from "./gql/ProjectInvolvements.gql.js";
import {AgentAvatar} from "../../widgets/AgentAvatar";
import {Grid} from "@mui/material";

/**
 * Display project Involvements and gatheredInvolvedAgents (with role)
 */
export function ProjectInvolvements({projectId, containerClassName} = {}) {
  const {t} = useTranslation();

  const {data: {project} = {}, loading} = useQuery(gqlProjectInvolvements, {variables: {projectId}});

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (project.involvements.edges.length < 1) {
    return null;
  }

  return (
    <div className={containerClassName}>
      <CategoryTitle title={t("PROJECT.INVOLVEMENTS")} showBorder={false} />
      <Grid container spacing={2}>
        {project.involvements.edges.map(({node: involvement}, index) => (
          <Grid item key={involvement.id} lg={3} md={6} xs={12}>
            <AgentAvatar
              key={involvement.id}
              agent={involvement.agent}
              subheader={involvement.role}
              size="large"
              popoverEnabled
            />
          </Grid>
        ))}
      </Grid>
    </div>
  );
}
