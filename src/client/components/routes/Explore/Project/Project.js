/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  getImageThumbnail, MaxWidth, useExtensionFragment, RichTextViewer, LoadingSplashScreen
} from "@synaptix/ui";
import dayjs from "dayjs";
import {useParams} from "react-router-dom";
import {Box, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {gqlProject} from "./gql/Project.gql";

import {globalSx} from "../../../layouts/styles";
import {AgentAvatar} from "../widgets/AgentAvatar";

import {RelatedProjects} from "./SeeAlso/RelatedProjects/RelatedProjects";
import {ProjectInvolvements} from "./ProjectInvolvements/ProjectInvolvements";
import {RelatedAgents} from "./SeeAlso/ProjectInvolvedAgents/RelatedAgents";
import {ProjectTimeline} from "./ProjectTimeline/ProjectTimeline";
import {gqlExploreCrossFilters} from "../gql/Explore.gql";
import {CategoryTitle} from "../widgets/CategoryTitle";

const rootSx = (theme) => ({
  flex: 1,
  display: "flex",
  flexDirection: "column",
  wordBreak: "break-word",
  wordWrap: "break-word",
  color: "rgba(61, 62, 62, 1)",
  letterSpacing: "-0.011em",
  fontStyle: "normal",
  fontSize: "1.8rem",
  [theme.breakpoints.down("lg")]: {
    fontSize: "1.6rem"
  }
});
const titleSx = (theme) => ({
  fontWeight: "700",
  fontSize: theme.typography.pxToRem(46)
});
const subTitleSx = (theme) => ({
  fontWeight: "500",
  fontSize: theme.typography.pxToRem(26)
});
const seeAlsoSx = (theme) => ({
  width: "100%",
  background: theme.palette.grey["50"],
  padding: theme.spacing(2, 0)
});
const seeAlsoTextSx = (theme) => ({
  width: "100%",
  fontSize: theme.typography.pxToRem(22),
  borderBottom: `1px solid ${theme.palette.grey["200"]}`
});


const classes = {
  mb15: {
    mb: 4
  },
  flex: {
    flex: 1,
    display: "flex",
    flexDirection: "column"
  },
  description: {
    textAlign: "justify",
    textJustify: "inter-word",
    letterSpacing: "-0.003em",
    lineHeight: "32px",
    fontSize: "21px",
    whiteSpace: "normal !important" // RichTextViewer add white-space: wrap who break the  justify
  },
  spacer: {
    width: "100%",
    paddingTop: 4,
    paddingBottom: 4
  },
}

export default function Project() {


  const {t} = useTranslation();
  let {id} = useParams();
  id = decodeURIComponent(id);

  const {data, loading} = useQuery(gqlProject, {
    variables: {
      id,
      filters: gqlExploreCrossFilters
    }
  });

  const ProjectExtensions = useExtensionFragment("Project", {projectId: id, publicMode: true});

  if (loading || !data?.project) {
    return <LoadingSplashScreen />;
  }

  const {
    createdAt,
    updatedAt,
    creatorPerson,
    image,
    title,
    shortDescription,
    involvementsCount = 0,
    gatheredInvolvedAgentsCount = 0,
    projectContributionsCount = 0
  } = data?.project;

  return (
    <Box sx={rootSx}>
      <MaxWidth size="lg" classSx={classes.flex}>
        <Box sx={classes.mb15} />
        <Typography component="h2" variant="h2" gutterBottom sx={titleSx}>
          {title}
        </Typography>
        {shortDescription && <RichTextViewer content={shortDescription} sx={subTitleSx} />}

        <AgentAvatar agent={creatorPerson} subheader={dayjs(updatedAt || createdAt).format("L")} size="large" />

        {image && (
          <Box sx={(theme) => ({
            ...globalSx.flexCenter,
            width: "100%",
            flexBasis: theme.spacing(75),
            [theme.breakpoints.between("xs", "sm")]: {
              flexBasis: theme.spacing(50)
            },
            [theme.breakpoints.down("xs")]: {
              flexBasis: theme.spacing(30)
            },
            backgroundSize: "contain",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
            backgroundColor: theme.palette.grey["100"],
            margin: theme.spacing(2, 0),
            backgroundImage: `url(${getImageThumbnail({imageUrl: image, size: "big"})})`
          })} />
        )}
      </MaxWidth>

      <MaxWidth size="lg" classSx={classes.flex}>
        {data?.project?.description && (
          <Box sx={classes.spacer}>
            <RichTextViewer content={data.project.description} editableClassSx={classes.description} />
          </Box>
        )}
      </MaxWidth>

      <If condition={projectContributionsCount > 0}>
        <MaxWidth size="lg" classSx={classes.flex}>
          <CategoryTitle title={"Ligne de temps des événements :"} />
          <ProjectTimeline projectId={id} />
        </MaxWidth>
      </If>

      <If condition={involvementsCount + gatheredInvolvedAgentsCount > 0}>
        <MaxWidth size="lg" classSx={classes.spacer}>
          <ProjectInvolvements projectId={id} />
        </MaxWidth>
      </If>

      <Box sx={seeAlsoSx}>
        <MaxWidth size="lg">
          <Typography variant="subtitle1" gutterBottom sx={seeAlsoTextSx}>
            Voir aussi:
          </Typography>

          {ProjectExtensions}

          <RelatedProjects projectId={id} sx={classes.spacer} />
          <RelatedAgents projectId={id} />
        </MaxWidth>
      </Box>
    </Box>
  );
}
