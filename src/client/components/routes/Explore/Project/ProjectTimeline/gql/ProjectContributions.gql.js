import {gql} from "@apollo/client";

export const gqlProjectContributions = gql`
  query ProjectContributions(
    $projectId: ID!
    $qs: String
    $first: Int
    $after: String
    $filters: [String]
    $sortings: [SortingInput]
  ) {
    project(id: $projectId) {
      id
      projectContributionsCount(qs: $qs, filters: $filters)
      projectContributions(qs: $qs, first: $first, after: $after, sortings: $sortings, filters: $filters) {
        edges {
          node {
            id
            startDate
            endDate
            createdAt
            title
            description
            shortDescription

            creatorPerson {
              fullName
            }

            attachments(filters: $filters) {
              edges {
                node {
                  id
                  resource {
                    id
                    publicUrl
                    ... on File {
                      mime
                      filename
                    }
                  }
                }
              }
            }
            involvements(filters: $filters) {
              edges {
                node {
                  id
                  agent {
                    id
                    avatar
                    __typename
                    ... on Person {
                      fullName
                      bio
                    }
                    ... on Organization {
                      name
                      description
                    }
                  }
                }
              }
            }
            replies(filters: $filters) {
              edges {
                node {
                  id
                  description
                  title
                  creatorPerson {
                    avatar
                    fullName
                  }
                }
              }
            }
          }
          cursor
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;
