import {useEffect} from "react";
import {RichTextViewer, ResponsiveImageBG, Pdf} from "@synaptix/ui";
import dayjs from "dayjs";
import {useTranslation} from "react-i18next";
import {
  Box,
  Grid,
  Card,
  CardHeader,
  CardContent,
  Avatar,
  Typography,
  Divider,
  ImageList,
  ImageListItem,
  Accordion,
  AccordionSummary,
  AccordionDetails
} from "@mui/material";
import {
  Timelapse as TimelapseIcon,
  QueryBuilder as InstantIcon,
  ExpandMore as ExpandMoreIcon
} from "@mui/icons-material";
import {Skeleton} from '@mui/material';
import {usePaginatedQuery} from "@mnemotix/synaptix-client-toolkit";
import {useInView} from "react-intersection-observer";

import {gqlExploreCrossFilters} from "../../gql/Explore.gql";
import {gqlProjectContributions} from "./gql/ProjectContributions.gql";
import {AgentAvatar} from "../../widgets/AgentAvatar";



const imageListHeight = 350;

const classes = {
  root: {
    paddingLeft: 5
  },
  event: {
    position: "relative",
    marginBottom: 6
  },
  imageList: {
    width: "100%",
    transform: "translateZ(0)",
    flexWrap: "nowrap"
  }
};

/**
 * @param {string} projectId
 * @param {array}  [gqlFilters]
 * @param {array}  [gqlSortings]
 * @param {object} [gqlVariables]
 * @param {int}    [pageSize=6]
 * @returns {JSX.Element}
 * @constructor
 */
export function ProjectTimeline({
  projectId,
  gqlFilters = gqlExploreCrossFilters,
  gqlSortings = [{sortBy: "startDate"}],
  gqlVariables,
  pageSize = 6
} = {}) {
  const {t} = useTranslation();
  const {ref, inView} = useInView({
    threshold: 0
  });

  const {
    data: {project: {projectContributions, projectContributionsCount} = {}} = {},
    loading,
    loadNextPage,
    hasNextPage
  } = usePaginatedQuery(gqlProjectContributions, {
    connectionPath: "project.projectContributions",
    pageSize,
    variables: {
      projectId,
      sortings: gqlSortings,
      filters: gqlFilters,
      ...gqlVariables
    }
  });

  useEffect(() => {
    if (!loading && projectContributions) {
      loadNextPage();
    }
  }, [inView]);

  return loading || projectContributionsCount > 0 ? (
    <Box sx={classes.root}>
      <Grid container spacing={2} wrap={"nowrap"} direction={"column"} sx={(theme) => ({
        paddingLeft: theme.spacing(4),
        borderLeft: `3px solid ${theme.palette.grey[400]}`,
        marginTop: theme.spacing(3)
      })}>
        {projectContributions?.edges.map(({node: projectContribution}) => {
          const startDate = projectContribution.startDate ? dayjs(projectContribution.startDate).format("LL") : null;
          const endDate = projectContribution.endDate ? dayjs(projectContribution.endDate).format("LL") : null;

          const [images, pdfs, videos] = projectContribution.attachments.edges.reduce(
            ([images, pdfs, videos], {node: {resource}}) => {
              if (resource.mime.includes("image/")) {
                if (!images.some(({id}) => id === resource.id)) {
                  images.push(resource);
                }
              } else if (resource.mime.includes("video/")) {
                videos.push(resource);
              } else if (resource.mime === "application/pdf") {
                pdfs.push(resource);
              }

              return [images, pdfs, videos];
            },
            [[], [], []]
          );

          return (
            <Grid item xs key={projectContribution.id} sx={classes.event}>
              <Avatar sx={(theme) => ({left: theme.spacing(-6.6), position: "absolute"})}>
                <Choose>
                  <When condition={endDate && endDate !== startDate}>
                    <TimelapseIcon />
                  </When>
                  <Otherwise>
                    <InstantIcon />
                  </Otherwise>
                </Choose>
              </Avatar>

              <Card variant="outlined" sx={(theme) => ({marginTop: theme.spacing(-3)})}>
                <CardHeader
                  title={projectContribution.title}
                  subheader={
                    <>
                      {startDate}
                      <If condition={endDate && endDate !== startDate}>
                        {" - "} {endDate}
                      </If>
                    </>
                  }
                />

                <If condition={images.length > 0}>
                  <ImageList
                    sx={classes.imageList}
                    rowHeight={imageListHeight}
                    style={{height: imageListHeight + 10}}>
                    {images.map((image) => (
                      <ImageListItem key={image.id} cols={images.length === 1 ? 2 : 1} rows={1}>
                        <ResponsiveImageBG url={image.publicUrl} />
                      </ImageListItem>
                    ))}
                    {pdfs.map((pdf) => (
                      <ImageListItem key={pdf.id} cols={1} rows={1}>
                        <Pdf resource={pdf} />
                      </ImageListItem>
                    ))}
                  </ImageList>
                </If>

                <If condition={projectContribution.involvements.edges.length > 0}>
                  <CardContent>
                    <Typography gutterBottom>{t("PROJECT_CONTRIBUTION.INVOLVEMENTS")}</Typography>
                    <Grid container spacing={2}>
                      {(projectContribution.involvements.edges || []).map(({node: {agent}}) => (
                        <Grid item xs={3} key={agent.id}>
                          <AgentAvatar agent={agent} popoverEnabled />
                        </Grid>
                      ))}
                    </Grid>
                  </CardContent>
                  <Divider />
                </If>

                <If condition={projectContribution.description}>
                  <CardContent>
                    <Typography variant="body1" color="textSecondary" component="div">
                      <RichTextViewer content={projectContribution.description} />
                    </Typography>
                  </CardContent>
                </If>

                <If condition={projectContribution.replies.edges.length > 0}>
                  {(projectContribution.replies.edges || []).map(({node: memo}, index) => {
                    return (
                      <Accordion key={memo.id}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                          <AgentAvatar
                            agent={memo.creatorPerson}
                            subheader={
                              <Choose>
                                <When condition={memo.title}>{memo.title}</When>
                                <When condition={projectContribution.replies.edges.length > 1}>Mémo n°{index + 1}</When>
                                <Otherwise>Mémo</Otherwise>
                              </Choose>
                            }
                          />
                        </AccordionSummary>
                        <AccordionDetails style={{display: "block"}}>
                          <Typography variant="body1" color="textSecondary" component="div" style={{width: "100%"}}>
                            <RichTextViewer key={memo.id} content={memo.description} />
                          </Typography>
                        </AccordionDetails>
                      </Accordion>
                    );
                  })}
                </If>
              </Card>
            </Grid>
          );
        })}

        <Choose>
          <When condition={loading}>
            {[...Array(pageSize)].map((_, i) => (
              <Grid item xs key={i}>
                <Card variant="outlined">
                  <CardHeader
                    title={<Skeleton animation="wave" height={10} width="80%" style={{marginBottom: 6}} />}
                    subheader={<Skeleton animation="wave" height={10} width="40%" />}
                  />
                  <CardContent>
                    <Skeleton animation="wave" variant="rect" height={250} width="100%" />
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </When>
          <When condition={hasNextPage}>
            <div ref={ref}>Chargement des suivants....</div>
          </When>
        </Choose>
      </Grid>
    </Box>
  ) : null;
}
