/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Fragment, useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../../routes";
import {MaxWidth, SearchBar} from "@synaptix/ui";

import {Grid, Divider, Box} from "@mui/material";
import {Skeleton} from '@mui/material';
import {gqlProjects} from "./gql/Projects.gql";
import {chunkArray} from "../../../utilities/tools";
import {ImageOnSide} from "./ProjectsWall/ImageOnSide";
import {ImageOnTop} from "./ProjectsWall/ImageOnTop";
import {ProjectsInColumn} from "./ProjectsWall/ProjectsInColumn";
import {ProjectsInRow} from "./ProjectsWall/ProjectsInRow";
import {usePaginatedQuery} from "@mnemotix/synaptix-client-toolkit";
import {gqlExploreCrossFilters} from "../gql/Explore.gql";

import {useInView} from "react-intersection-observer";

const classes = {
  root: {
    paddingTop: 2
  },
  searchBar: {
    display: "flex",
    height: "auto",
    width: "100%",
    border: 0,
    borderBottom: "2px solid grey",
    borderRadius: 0
  },
  searchBarContainer: {},
  searchBarInput: {
    fontSize: "2rem"
  }
}

/**
 * Return list of all Projets
 *
 * @param {string} qs query string to filter data
 * @returns
 */
export default function Projects({ } = {}) {

  const {t} = useTranslation();
  const [qs, setQs] = useState();

  const {ref, inView, entry} = useInView({
    threshold: 0
  });

  const {
    data: {projectsCount, projects: projectsConnection} = {},
    loading,
    loadNextPage,
    hasNextPage
  } = usePaginatedQuery(gqlProjects, {
    pageSize: 20,
    connectionPath: "projects",
    variables: {
      filters: gqlExploreCrossFilters,
      qs
    }
  });

  useEffect(() => {
    if (!loading && projectsConnection) {
      loadNextPage();
    }
  }, [inView]);

  const projects = (projectsConnection?.edges || []).map((p) => p.node);

  const splittedProjects = {
    main: projects.slice(0, 1),
    second: projects.slice(1, 4),
    rest: chunkArray(projects.slice(4), 3, true)
  };

  return (
    <MaxWidth size="lg" classSx={classes.root}>
      <Grid container direction="column" wrap={"nowrap"} spacing={4}>
        <Grid item>
          <SearchBar
            value={qs}
            onRequestSearch={setQs}
            onChange={setQs}
            onCancelSearch={() => setQs(null)}
            loading={loading}
            placeholder={t("REMOTE_TABLE.TOOLBAR.SEARCH")}
            variant={"outlined"}
            overridenSxClasses={{
              root: classes.searchBar,
              container: classes.searchBarContainer,
              input: classes.searchBarInput
            }}
          />
        </Grid>

        <If condition={projectsCount === 0}>
          <Grid item>
            <Box sx={(theme) => ({
              textAlign: "center",
              color: theme.palette.text.emptyHint,
              fontSize: theme.typography.pxToRem(24),
              padding: theme.spacing(20, 0)
            })}>
              Aucun projet trouvé...
            </Box>
          </Grid>
        </If>
        {splittedProjects?.main.map((project, index) => {
          return (
            <Grid item key={"main" + index}>
              <ImageOnSide project={project} linkProps={{to: generateProjectRoute(project)}} />
            </Grid>
          );
        })}

        <If condition={splittedProjects?.second.length > 0}>
          <Grid item>
            <Divider />
          </Grid>

          <Grid item container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={3}>
            {splittedProjects?.second.map((project, index) => {
              return (
                <Grid item container xs={12} sm={6} md={4} key={"second" + index}>
                  <ImageOnTop project={project} linkProps={{to: generateProjectRoute(project)}} />
                </Grid>
              );
            })}
          </Grid>
        </If>

        {splittedProjects?.rest.map((projects, index) => (
          <Fragment key={index}>
            <Grid item>
              <Divider />
            </Grid>

            <Grid item>
              {index % 2 === 0 ? displayInColumn(projects, "rest" + index) : displayInRow(projects)}
            </Grid>
          </Fragment>
        ))}
      </Grid>

      <Choose>
        <When condition={loading}>
          <Grid container direction={"row"} justifyContent={"space-between"} alignItems="stretch" spacing={3}>
            <Grid item container spacing={2}>
              <Grid item xs={12} md={7}>
                <Skeleton animation="wave" variant="rect" height={300} width="100%" />
              </Grid>
              <Grid container item xs={12} md={5} direction={"column"} spacing={2}>
                <Grid item xs>
                  <Skeleton animation="wave" height={40} width="80%" />
                  <Skeleton animation="wave" height={20} width="40%" />
                </Grid>
              </Grid>
            </Grid>

            <Grid item>
              <Divider />
            </Grid>

            <Grid item container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={3}>
              {[...Array(3)].map((_, i) => (
                <Grid item container xs={12} sm={6} md={4} key={"second" + i}>
                  <Grid container direction={"column"} spacing={2}>
                    <Grid item  >
                      <Skeleton animation="wave" variant="rect" height={150} width="100%" />
                    </Grid>
                    <Grid item xs>
                      <Skeleton animation="wave" height={40} width="80%" />
                      <Skeleton animation="wave" height={20} width="40%" />
                    </Grid>
                  </Grid>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </When>
        <When condition={hasNextPage}>
          <div ref={ref}>Chargement des suivants....</div>
        </When>
      </Choose>
    </MaxWidth>
  );

  function displayInColumn(projects, index) {
    return <ProjectsInColumn projects={projects} generateProjectRoute={generateProjectRoute} />;
  }

  function displayInRow(projects) {
    return (
      <ProjectsInRow
        projects={projects}
        generateProjectRoute={generateProjectRoute}
        hideCreator={false}
      />
    );
  }

  function generateProjectRoute(project) {
    return formatRoute(ROUTES.EXPLORE_PROJECT, {id: project.id});
  }
}
