/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {Grid} from "@mui/material";
import {LoadingSplashScreen} from "@synaptix/ui";
import {CategoryTitle} from "../../../widgets/CategoryTitle";
import {gqlRelatedAgents} from "./gql/RelatedAgents.gql.js";
import {AgentAvatar} from "../../../widgets/AgentAvatar";


/**
 * Display project Involvements and gatheredInvolvedAgents (with role)
 */
export function RelatedAgents({projectId} = {}) {

  const {t} = useTranslation();

  const {data: {project} = {}, loading} = useQuery(gqlRelatedAgents, {variables: {projectId}});

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (project.gatheredInvolvedAgents.edges.length < 1) {
    return null;
  }

  return (
    <div>
      <CategoryTitle title={t("EXPLORE.PROJECT.SEE_ALSO.ASSOCIATED_AGENTS")} showBorder={false} />
      <Grid container spacing={2} sx={{width: "100%"}}>
        {project.gatheredInvolvedAgents.edges.map(({node: agent}, index) => (
          <Grid item xs={3} key={agent.id}>
            <AgentAvatar agent={agent} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
}
