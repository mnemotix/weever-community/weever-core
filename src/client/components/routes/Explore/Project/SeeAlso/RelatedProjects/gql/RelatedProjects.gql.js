import {gql} from "@apollo/client";
import {gqlProjectFragment} from "../../../gql/Project.gql";
import {gqlExploreCrossFilters} from "../../../../gql/Explore.gql";

export const gqlRelatedProjects = gql`
  query RelatedProjects($projectId: ID!) {
    project(id: $projectId) {
      id
      parentProject {
        ...ProjectFragment
        subProjects(filters: ${JSON.stringify(gqlExploreCrossFilters)}) {
          edges{
            node{
              ...ProjectFragment
            }
          }
        }
      }
      subProjects(filters: ${JSON.stringify(gqlExploreCrossFilters)}) {
        edges{
          node{
            ...ProjectFragment
          }
        }
      }
    }
  }
  ${gqlProjectFragment}
`;
