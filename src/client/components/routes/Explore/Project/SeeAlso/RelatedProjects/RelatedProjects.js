import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../../../../routes";
import {ProjectsInRow} from "../../ProjectsWall/ProjectsInRow";
import {CategoryTitle} from "../../../widgets/CategoryTitle";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {gqlRelatedProjects} from "./gql/RelatedProjects.gql";

/**
 * display all related projects
 * @param {string} projectId
 * @param {string} className
 * @returns
 */
export function RelatedProjects({projectId, className}) {
  const {t} = useTranslation();

  const {data: {project} = {}, loading} = useQuery(gqlRelatedProjects, {variables: {projectId}});

  // gather projects
  let relatedProjects = [];

  if (project) {
    if (project.parentProject) {
      relatedProjects.push({
        id: project.parentProject.id,
        image: project.parentProject.image,
        title: project.parentProject.title,
        nodeType: "parent"
      });
    }

    if (project.parentProject?.subProjects) {
      relatedProjects = [
        ...relatedProjects,
        ...project.parentProject?.subProjects?.edges
          ?.filter((item) => item?.node?.id !== projectId)
          .map((item) => {
            return {...item.node, nodeType: "sibling"};
          })
      ];
    }

    if (project.subProjects) {
      relatedProjects = [
        ...relatedProjects,
        ...project.subProjects?.edges?.map((item) => {
          return {...item.node, nodeType: "sub"};
        })
      ];
    }
  }

  if (relatedProjects.length < 1) {
    return null;
  }

  function generateProjectRoute(node) {
    return formatRoute(ROUTES.EXPLORE_PROJECT, {id: node.id});
  }

  return (
    <div className={className}>
      <CategoryTitle title={t("EXPLORE.PROJECT.SEE_ALSO.ASSOCIATED_PROJECTS")} />
      <ProjectsInRow
        projects={relatedProjects}
        uniqkey={"ProjectsRelated"}
        generateProjectRoute={generateProjectRoute}
        md={4}
        lg={4}
      />
    </div>
  );
}
