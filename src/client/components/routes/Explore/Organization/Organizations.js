/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../../routes";
import {CollectionDataProvider, CollectionView, MaxWidth} from "@synaptix/ui";

import {gqlOrganizations} from "./gql/Organizations.gql";
import {getColumns} from "../../Organization/Organizations";

/**
 * Return list of all Organizations
 *
 * @param {string} qs query string to filter data
 * @returns
 */
export default function Organizations({qs}) {
  const {t} = useTranslation();
  const gatherSubProjects = false; // required even if false
  let gqlVariables = {gatherSubProjects};
  const columns = getColumns({classes, t});

  const _isFiltred = qs && qs.length > 0;
  const availableDisplayModes = _isFiltred
    ? {
        blog: {dataForView: (node) => node}
      }
    : {
        blog: {dataForView: (node) => node},
        masonry: {dataForView: (node) => node}
      };

  if (_isFiltred) {
    // if filtred get only first 9 items
    gqlVariables["first"] = 6;
  }

  return (
    <MaxWidth size="md">
      <CollectionDataProvider
        pageSize={20}
        gqlVariables={gqlVariables}
        gqlConnectionPath={"organizations"}
        gqlQuery={gqlOrganizations}
        qs={qs}
        gqlSortings={!!qs ? null : [{sortBy: "updatedAt", isSortDescending: true}]}>
        <CollectionView
          collectionKey={"OrganizationsList"}
          availableDisplayModes={availableDisplayModes}
          columns={columns}
          showTopMenu={!_isFiltred}
          generateItemRoute={(node) => formatRoute(ROUTES.EXPLORE_ORGANIZATION, {id: node.id})}
        />
      </CollectionDataProvider>
    </MaxWidth>
  );
}
