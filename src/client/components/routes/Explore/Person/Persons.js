/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";

import {ROUTES} from "../../../../routes";
import {MaxWidth, CollectionDataProvider, CollectionView} from "@synaptix/ui";

import {gqlPersons} from "./gql/Persons.gql";
import {getColumns} from "../../Person/Persons";



/**
 * Return list of all Persons
 *
 * @param {string} qs query string to filter data
 * @returns
 */
export default function Persons({qs}) {

  const {t} = useTranslation();
  const gatherSubProjects = false; // required even if false
  let gqlVariables = {gatherSubProjects};
  const columns = getColumns({t, routeOnClick: ROUTES.EXPLORE_PERSON});

  const _isFiltred = qs && qs.length > 0;
  const availableDisplayModes = _isFiltred
    ? {
      blog: {dataForView: (node) => node, props: {hideCreator: true}}
    }
    : {
      blog: {dataForView: (node) => node, props: {hideCreator: true}},
      masonry: {dataForView: (node) => node}
    };

  if (_isFiltred) {
    // if filtred get only first 9 items
    gqlVariables["first"] = 6;
  }

  return (
    <MaxWidth size="md">
      <CollectionDataProvider
        gqlConnectionPath={"persons"}
        pageSize={20}
        gqlQuery={gqlPersons}
        gqlVariables={gqlVariables}
        gqlFilters={[]}
        qs={qs}
        gqlSortings={!!qs ? null : [{sortBy: "updatedAt", isSortDescending: true}]}
      >
        <CollectionView
          collectionKey={"PersonsList"}
          displayMode={"blog"}
          availableDisplayModes={availableDisplayModes}
          columns={columns}
          showTopMenu={!_isFiltred}
        />
      </CollectionDataProvider>
    </MaxWidth>
  );
}
