import {Box} from "@mui/material";



/**
 * display elements with flex wrap, used for a project's Exhibitions for example
 */
export function FlexWrap({children}) {

  return <Box sx={{
    paddingLeft: "15px",
    lineHeight: "32px",
    fontSize: "21px",
    color: "text.emptyHint",
    display: "flex",
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    flexWrap: "wrap"
  }}>
    {children}
  </Box>;
}
