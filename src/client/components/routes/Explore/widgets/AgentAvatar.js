import {useState} from "react";
import {PersonExcerpt} from "@synaptix/ui";
import {CardHeader, Popover, ClickAwayListener} from "@mui/material";
import {PersonAvatar} from "../../Person";
import {OrganizationAvatar, OrganizationExcerpt} from "../../Organization";
import {pxToRem} from "../../../../components/utilities/tools";


const classes = {
  root: {
    padding: 0
  },
  avatarHeader: {
    marginRight: "8px" // 2 
  },
  largeTitle: {
    fontSize: pxToRem(22)
  },
  mediumTitle: {
    fontSize: pxToRem(20)
  },
  LargeSubHeader: {
    fontSize: pxToRem(20)
  },
  MediumSubHeader: {
    fontSize: pxToRem(18)
  },
  clickable: {
    cursor: "pointer"
  },
  paper: {
    width: "40vw !important",
    padding: "16px" // 2 
  },
  largeAvatar: {
    fontSize: "1.05rem",
    width: "80px",
    height: "80px"
  },
  mediumAvatar: {
    fontSize: "1.05rem",
    width: "40px",
    height: "40px"
  }
};

/**
 * display information about creation, date and person name with link
 * @returns
 */
export function AgentAvatar({agent, subheader, size = "medium", sxClass, popoverEnabled} = {}) {

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  if (!agent) {
    agent = {
      fullName: "Auteur anonyme"
    };
  }

  const Excerpt = agent.__typename === "Person" ? PersonExcerpt : OrganizationExcerpt;

  return (
    <>
      <CardHeader
        title={agent?.fullName || agent?.name}
        subheader={subheader}
        avatar={
          agent.__typename === "Person" ? (
            <PersonAvatar person={agent} classSx={classes?.[size + "Avatar"]} />
          ) : (
            <OrganizationAvatar organization={agent} classSx={classes?.[size + "Avatar"]} />
          )
        }
        sx={{
          root: [classes.root, sxClass, popoverEnabled && classes.clickable],
          title: classes?.[size + "Title"],
          subheader: classes?.[size + "Subheader"],
          avatar: classes.avatarHeader
        }}
        onMouseOver={handlePopoverOpen}
      />
      <If condition={popoverEnabled}>
        <ClickAwayListener onClickAway={handlePopoverClose}>
          <Popover
            sx={{
              paper: classes.paper
            }}
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "left"
            }}
            onClose={handlePopoverClose}
            disableRestoreFocus>
            <Excerpt id={agent.id} creatorHidden />
          </Popover>
        </ClickAwayListener>
      </If>
    </>
  );

  function handlePopoverOpen(event) {
    if (popoverEnabled) {
      setAnchorEl(event.currentTarget);
    }
  }

  function handlePopoverClose() {
    if (popoverEnabled) {
      setAnchorEl(null);
    }
  }
}
