import {Typography} from "@mui/material";


/**
 * display a title of a category, so all use same style
 */
export function CategoryTitle({title}) {

  return (
    <Typography component={"div"} variant="subtitle1"
      sx={(theme) => ({borderBottom: `1px solid ${theme.palette.grey["200"]}`, marginBottom: theme.spacing(4)})}
    >
      {title}
    </Typography>
  );
}
