/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect, useState} from "react";
import {LanguageChooserMenu, createLink, useResponsive, useLoggedUser, FlexboxWrap, FragmentSwitch} from "@synaptix/ui";
import {useTranslation} from "react-i18next";

import {formatRoute} from "react-router-named-routes";
import {Link, Route, useHistory, matchPath} from "react-router-dom";
import {grey} from '@mui/material/colors';
import {
  AppBar, Box, Menu, MenuItem, IconButton, Paper, Slide,
  Toolbar, Typography, useScrollTrigger, Divider, Button as MuiButton
} from "@mui/material";
import {AccountCircle as AccountCircleIcon, Mood as MoodIcon} from "@mui/icons-material";
import loadable from "@loadable/component";

import MobileCategories from "../../routes/Search/MobileCategories.js";

const ExplorePersons = loadable(() => import("./Person/Persons"));
const ExploreOrganisations = loadable(() => import("./Organization/Organizations"));
const ExploreProjects = loadable(() => import("./Project/Projects"));
const ExploreProject = loadable(() => import("./Project/Project"));

import Config from "../../../Config";
import {ROUTES} from "../../../routes";

import {AdminMenuLink} from "../Admin/AdminMenuLink";

const appBarSx = (theme) => ({
  backgroundColor: "rgba(255,255,255,0.75)",
  zIndex: theme.zIndex.drawer + 1,
  color: Config.colors.darkBlue,
  boxShadow: "none",
  borderBottom: `1px solid ${theme.palette.grey[300]}`
});

const classes = {
  exploreRoot: {
    fontFamily: "'Rubik', sans-serif"
  },
  toolbar: {
    backgroundColor: "rgba(255,255,255,0.75)",
    padding: "0 24px",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1,
    width: "100%"
  },
  title: {
    flexGrow: 1,
    margin: "0 10px"
  },
  titleLink: {
    color: "inherit",
    textDecoration: "none"
  },
};

const TabsSettings = [
  {
    route: ROUTES.EXPLORE_PROJECTS,
    generateLabel: ({t}) => t("SEARCH.TABS.PROJECTS")
  }
  // {
  //   route: ROUTES.EXPLORE_ORGANIZATIONS,
  //   generateLabel: ({t}) => t("SEARCH.TABS.ORGANIZATIONS")
  // },
  // {
  //   route: ROUTES.EXPLORE_PERSONS,
  //   generateLabel: ({t}) => t("SEARCH.TABS.PERSONS")
  // }
];

/**
 * @param {Component} [TitleComponent]
 * @param children
 * @return {*}
 * @constructor
 */
export default function ExploreLayout({TitleComponent}) {

  const {t, i18n} = useTranslation();
  const [qs, setQs] = useState("");
  const [anchorEl, setAnchorEl] = useState();
  const {isDesktop} = useResponsive();
  const trigger = useScrollTrigger({
    threshold: 0
  });
  const {user, isAdmin} = useLoggedUser();

  useEffect(() => {
    let tab = getTabValueFromHistory();
    if (tab === -1) {
      history.push(TabsSettings[0].route);
    }
  });
  let history = useHistory();

  return (
    <Box sx={classes.exploreRoot}>
      <Slide appear={false} direction="down" in={!trigger}>
        <AppBar sx={appBarSx}>
          <Toolbar disableGutters={true} sx={classes.toolbar}>
            <Typography component="h1" variant="h6" color="inherit" noWrap sx={classes.title}>
              <Link to={ROUTES.EXPLORE} style={classes.titleLink}>
                {TitleComponent || "Exploration des données ouvertes"}
              </Link>
            </Typography>

            {/*renderTabs()*/}

            <LanguageChooserMenu i18n={i18n} />

            <Choose>
              <When condition={user}>
                <div>
                  <IconButton color="inherit" onClick={(e) => setAnchorEl(e.currentTarget)}>
                    <AccountCircleIcon />
                  </IconButton>
                  <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    keepMounted
                    open={!!anchorEl}
                    onClose={() => setAnchorEl(null)}>
                    <MenuItem key="WCEMenuItem1" disabled>
                      {t("APP_BAR.MENU.HELLO")} {user?.firstName} <MoodIcon sx={(theme) => ({
                        margin: [[0, theme.spacing(1)]]
                      })} /> !
                    </MenuItem>

                    <MenuItem key="WCEMenuItem2" component={Link} to={formatRoute(ROUTES.INDEX)}>
                      {t("APP_BAR.MENU.MY_PROFILE")}
                    </MenuItem>

                    <MenuItem key="WCEMenuItem3" disabled>
                      <Box style={{width: "100%"}}>
                        <Divider />
                      </Box>
                    </MenuItem>

                    <If condition={isAdmin}>
                      <MenuItem key="WCEMenuItem4">
                        <AdminMenuLink />
                      </MenuItem>
                      <MenuItem key="WCEMenuItem5" disabled>
                        <Box style={{width: "100%"}}>
                          <Divider />
                        </Box>
                      </MenuItem>
                    </If>

                    <MenuItem key="WCEMenuItem6" onClick={() => {logout();}}>
                      {t("SIGN_IN.TITLE")}
                    </MenuItem>
                  </Menu>
                </div>
              </When>
              <Otherwise>{createLink({to: ROUTES.INDEX, text: t("SIGN_IN.TITLE")})}</Otherwise>
            </Choose>
          </Toolbar>
        </AppBar>
      </Slide>
      <Box component="main" sx={(theme) => ({backgroundColor: theme.palette.grey["50"], minHeight: "90vh", p: theme.spacing(3, 0)})}>
        <Box sx={(theme) => (theme.mixins.toolbar)} />

        <FragmentSwitch>
          <Route exact path={ROUTES.EXPLORE_PROJECT}>
            <ExploreProject />
          </Route>
          <Route exact path={ROUTES.EXPLORE_PERSONS}>
            <ExplorePersons qs={qs} />
          </Route>
          <Route exact path={ROUTES.EXPLORE_ORGANIZATIONS}>
            <ExploreOrganisations />
          </Route>
          <Route path={ROUTES.EXPLORE_PROJECTS}>
            <ExploreProjects />
          </Route>
        </FragmentSwitch>
      </Box>
      <Box component='footer' sx={(theme) => ({
        width: "100%",
        padding: theme.spacing(8, 2),
        background: grey["200"],
        color: "white",
      })}
      />
    </Box>
  );

  function getTabValueFromHistory() {
    return TabsSettings.findIndex(({route}) =>
      matchPath(history.location.pathname, {
        path: route
      })
    );
  }

  function renderTabs() {
    if (isDesktop) {
      return (
        <FlexboxWrap>
          {TabsSettings.map((item, index) => {
            const {route, generateLabel} = item;
            return (
              <MuiButton
                component={Link}
                className={classes.toolbarLink}
                to={formatRoute(route)}
                key={index}
                onClick={handleCancelSearch}>
                {generateLabel({t})}
              </MuiButton>
            );
          })}
        </FlexboxWrap>
      );
    } else {
      return (
        <Paper className={classes.smallTabs}>
          <MobileCategories
            value={Math.max(0, getTabValueFromHistory())}
            onClick={(route) => history.push(route)}
            TabsSettings={TabsSettings}
            data={null}
          />
        </Paper>
      );
    }
  }

  function handleRequestSearch(qs) {
    setQs(qs);
  }

  function handleCancelSearch() {
    setQs("");
  }
}
