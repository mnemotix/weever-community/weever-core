/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {CollectionDataProvider, CollectionView} from "@synaptix/ui";

import {ROUTES} from "../../../../routes";
import {gqlResources, ErrorBoundary} from "@synaptix/ui";
import {generateColumns, generateTimeLine} from "../../../../components/routes/ProjectContribution/ProjectContributions";
import {gqlProjectContributions} from "./gql/ProjectContributions.gql";

export function ProjectContributions(props) {
  return (
    <ErrorBoundary>
      <ProjectContributionsCode {...props} />
    </ErrorBoundary>
  );
}
/**
 * display the contributions of a given project
 * @param {string} qs query string
 * @param {string} projectId
 * @returns
 */
export function ProjectContributionsCode({qs, projectId} = {}) {
  const gqlSortings = [{sortBy: "startDate", isSortDescending: true}];
  const columns = generateColumns({
    gqlResources,
    gqlSortings,
    routes: {
      PROJECT_CONTRIBUTION: ROUTES.PROJECT_CONTRIBUTION,
      PROJECT_CONTRIBUTION_MEMOS: ROUTES.PROJECT_CONTRIBUTION_MEMOS
    }
  });

  return (
    <CollectionDataProvider
      gqlConnectionPath={"project.projectContributions"}
      gqlQuery={gqlProjectContributions}
      //  gqlCountPath={"project.projectContributionsCount"}
      gqlVariables={{projectId}}
      gqlSortings={gqlSortings}
      qs={qs}
      searchEnabled={searchEnabled}
    >
      <CollectionView
        collectionKey={"ProjectsList"}
        columns={columns}
        removalEnabled={false}
        hideIfNoResults={true}
        batchEditEnabled={false}
        availableDisplayModes={{
          publicTimeline: {
            key: "publicTimeline",
            renderComponent: (props) =>
              generateTimeLine({
                gqlProjectContributions,
                projectId,
                gqlResources,
                gqlSortings,
                props,
                publicMode: true
              })
          }
        }}
      />
    </CollectionDataProvider>

  );
}
