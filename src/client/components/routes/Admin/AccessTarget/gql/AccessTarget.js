import {gql} from "@apollo/client";
import {gqlUserAccountFragment} from "../../UserAccount/gql/UserAccount.gql";
import {gqlUserGroupFragment} from "../../UserGroup/gql/Group.gql";

export const gqlAccessTargetFragment = gql`
  fragment AccessTargetFragment on AccessTargetInterface {
    id
    ... on UserAccount {
      fullName
    }
    ... on UserGroup {
      label
      asGlobalRole
      userAccounts {
        edges {
          node {
            id
          }
        }
      }
    }
  }
`;

export const gqlAccessTargets = gql`
  query AccessTargets($qs: String, $first: Int) {
    accessTargets(qs: $qs, first: $first) {
      edges {
        node {
          ...AccessTargetFragment
        }
      }
    }
  }
  ${gqlAccessTargetFragment}
`;
