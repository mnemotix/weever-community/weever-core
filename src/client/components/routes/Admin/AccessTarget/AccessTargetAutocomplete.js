/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ListItemText} from "@mui/material";
import {useTranslation} from "react-i18next";
import {gqlAccessTargets} from "./gql/AccessTarget";
import {Autocomplete} from "@synaptix/ui";

/**
 * @param {function} onSelect
 * @param {string} placeholder
 * @param {object} selectedObjects
 * @param {string} error
 * @param {boolean} [multiple]
 */
export function AccessTargetAutocomplete({onSelect, placeholder, selectedObjects, error, multiple} = {}) {
  const {t} = useTranslation();
  return (
    <Autocomplete
      placeholder={placeholder || t("ACCESS_TARGET.AUTOCOMPLETE.CHOOSE")}
      gqlQuery={gqlAccessTargets}
      gqlDataConnectionPath={"accessTargets"}
      gqlVariables={{first: 10}}
      onSelect={onSelect}
      selectedObjects={selectedObjects}
      multiple={multiple}
      AutocompleteProps={{
        noOptionsText: t("ACCESS_TARGET.AUTOCOMPLETE.NO_RESULT")
      }}
      TextFieldProps={{
        variant: "standard",
        error: !!error,
        helperText: error
      }}
      getOptionLabel={(accessTarget) => accessTarget.fullName || accessTarget.label}
      renderOption={(accessTarget) => {
        return (
          <ListItemText
            primary={accessTarget.fullName || accessTarget.label}
            secondary={t(`ACCESS_TARGET.CATEGORY.${accessTarget.__typename.toUpperCase()}`)}
          />
        );
      }}
    />
  );
}
