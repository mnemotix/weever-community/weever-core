/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as Yup from "yup";
import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {gqlUserAccountFragment} from "../gql/UserAccount.gql";

export const getCreateUserAccountValidationSchema = ({t}) =>
  Yup.object().shape({
    email: Yup.string().email().required(t("FORM_ERRORS.FIELD_ERRORS.EMAIL_REQUIRED")),
    password: Yup.string()
      .min(6, t("FORM_ERRORS.FIELD_ERRORS.PASSWORD_TOO_SHORT"))
      .required(t("FORM_ERRORS.FIELD_ERRORS.PASSWORD_REQUIRED")),
    passwordConfirmation: Yup.string()
      .when("password", {
        is: (val) => !!(val && val.length > 0),
        then: Yup.string().oneOf([Yup.ref("password")], t("FORM_ERRORS.FIELD_ERRORS.PASSWORDS_DO_NOT_MATCH"))
      })
      .required(t("FORM_ERRORS.FIELD_ERRORS.PASSWORD_CONFIRMATION_REQUIRED")),
    lastName: Yup.string().required(t("FORM_ERRORS.FIELD_ERRORS.LAST_NAME_REQUIRED"))
  });

export const createUserAccountFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["firstName", "lastName", "password", "email"],
    gqlFragment: gqlUserAccountFragment,
    gqlFragmentName: "UserAccountFragment"
  }),
  postProcessInitialValues: (object) => ({
    ...object,
    passwordConfirmation: ""
  }),
  validationSchema: getCreateUserAccountValidationSchema
});
