/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Link} from "react-router-dom";
import {Typography, Breadcrumbs, Paper, Button as MuiButton} from "@mui/material";
import {formatRoute} from "react-router-named-routes";
import {useTranslation} from "react-i18next";
import {ROUTES} from "../../../../routes";
import Persons from "../../Person/Persons";



export default function UserAccounts({ } = {}) {

  const {t} = useTranslation();

  return (
    <>
      <Breadcrumbs sx={{mb: 2}}>
        <Typography color="textPrimary">{t("ADMIN.USERS.TITLE")}</Typography>
      </Breadcrumbs>

      <Paper>
        <Persons
          qs={""}
          filterExistingUserAccounts={true}
          searchEnabled={true}
          routeOnClick={ROUTES.PERSON_EDIT}
          renderRightSideActions={
            <MuiButton variant="contained" color="primary" component={Link} to={formatRoute(ROUTES.ADMIN_USER_CREATE)}>
              {t("ADMIN.MENU.NEW_USER")}
            </MuiButton>
          }
        />
      </Paper>
    </>
  );
}
