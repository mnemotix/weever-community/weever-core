/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {useSnackbar} from "notistack";
import {Grid, Typography, Breadcrumbs, ListItem, ListItemText, ListItemAvatar} from "@mui/material";
import {formatRoute} from "react-router-named-routes";
import {useHistory, useLocation} from "react-router-dom";
import {useMutation} from "@apollo/client";
import {ROUTES} from "../../../../routes";
import {gqlRegisterUserAccount, gqlRegisterUserAccountWithNewId} from "./gql/RegisterUserAccount.gql";
import {createUserAccountFormDefinition} from "./form/CreateUserAccount.form";
import {DynamicForm, TextField} from "@synaptix/ui";

import {PersonAvatar} from "../../Person/PersonAvatar";

/**
 * Display a button to create a account from a person,
 * On click open a dialog with confirmation and form to fulfill good information
 * @param {*} props  {id, firstName, lastName,  email}
 */
export default function UserAccountCreate() {
  const {enqueueSnackbar} = useSnackbar();  
  const {t} = useTranslation();
  const history = useHistory();
  const location = useLocation();
  const person = location?.state?.person;

  const [registerUserAccount, {loading: saving}] = useMutation(
    person ? gqlRegisterUserAccount : gqlRegisterUserAccountWithNewId,
    {
      onCompleted(data) {
        if (data?.registerUserAccount?.success) {
          enqueueSnackbar(t("PERSON.ADMIN_PANEL.ACCOUNT_CREATED"), {variant: "success"});
          history.push(
            formatRoute(ROUTES.PERSON, {id: person?.id || data?.registerUserAccount?.userAccount?.person?.id})
          );
        }
      }
    }
  );

  return (
    <>
      <Breadcrumbs sx={(theme) => ({marginBottom: theme.spacing(2)})}>
        <Typography color="textPrimary">{t("PERSON.ADMIN_PANEL.CREATE_ACCOUNT")}</Typography>
      </Breadcrumbs>

      <DynamicForm
        object={
          person
            ? {
              ...person,
              email: person.mainEmail?.email
            }
            : null
        }
        validateOnMount
        formDefinition={createUserAccountFormDefinition}
        mutateFunction={registerUserAccount}
        saving={saving}
        formatMutationInput={({objectInput}) => {
          let formattedInput = Object.assign(
            person
              ? {
                firstName: person.firstName || "NC",
                lastName: person.lastName,
                email: person.mainEmail?.email,
                personInput: {
                  id: person.id
                }
              }
              : {},
            objectInput
          );

          return formattedInput;
        }}>
        <Grid container spacing={2}>
          <Choose>
            <When condition={person}>
              <Grid item xs={12}>
                <ListItem disableGutters>
                  <ListItemAvatar>
                    <PersonAvatar person={person} />
                  </ListItemAvatar>
                  <ListItemText primary={[person.firstName, person.lastName].join(" ")} />
                </ListItem>
              </Grid>
            </When>
            <Otherwise>
              <Grid item xs={12} sx={{mt:2, mb: 2}}>
                <TextField name="firstName" label={t("PERSON.FIRST_NAME")} />
              </Grid>
              <Grid item xs={12} sx={{mt:2, mb: 2}}>
                <TextField name="lastName" label={t("PERSON.LAST_NAME")} />
              </Grid>
            </Otherwise>
          </Choose>

          <Grid item xs={12} sx={{mt:2, mb: 2}}>
            <TextField name="email" label={t("SIGN_IN.EMAIL")} />
          </Grid>
          <Grid item xs={12} sx={{mt:2, mb: 2}}>
            <TextField type="password" name="password" label={t("SIGN_IN.PASSWORD")} />
          </Grid>

          <Grid item xs={12} sx={{mt:2, mb: 2}}>
            <TextField
              type="password"
              name="passwordConfirmation"
              label={t("PROFIL.CHANGE_PASSWORD.CONFIRM_NEW_PASSWORD")}
            />
          </Grid>
        </Grid>
      </DynamicForm>
    </>
  );
}
