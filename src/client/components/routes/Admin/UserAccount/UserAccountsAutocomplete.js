/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {Autocomplete} from "@synaptix/ui";
import {gqlUserAccounts} from "./gql/UserAccounts.gql";

/**
 * @param {function} onSelect
 * @param {string} placeholder
 * @param {object} selectedProject
 * @param {string} error
 * @param {boolean} creationEnabled
 * @param {function} onCreate
 */
export function UserAccountsAutocomplete({onSelect, placeholder, selectedObject, error, creationEnabled, onCreate} = {}) {
  const {t} = useTranslation();
  return (

    <Autocomplete
      placeholder={placeholder || t("PERSON.AUTOCOMPLETE.CHOOSE")}
      gqlQuery={gqlUserAccounts}
      gqlDataConnectionPath={"userAccounts"}
      getOptionLabel={(userAccount) => `${userAccount?.firstName} ${userAccount?.lastName}`}
      onSelect={onSelect}
      selectedObject={selectedObject}
      AutocompleteProps={{
        noOptionsText: t("PERSON.AUTOCOMPLETE.NO_RESULT")
      }}
      TextFieldProps={{
        variant: "standard",
        error: !!error,
        helperText: error
      }}
    />
  );
}
