import {gql} from "@apollo/client";
import {gqlUserAccountFragment} from "./UserAccount.gql";

// we get list of agents, casted to type person
// it works only because type "Person" extends "AgentInterface" interface
export const gqlUsersAccount = gql`
  query UserAccounts($qs: String, $first: Int) {
    userAccounts(qs: $qs, first: $first) {
      edges {
        node {
          ...UserAccountFragment
        }
      }
    }
  }
  ${gqlUserAccountFragment}
`;
