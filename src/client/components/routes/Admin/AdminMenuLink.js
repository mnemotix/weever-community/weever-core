/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {useTranslation} from "react-i18next";
import {Box, MenuItem} from "@mui/material";
import {formatRoute} from "react-router-named-routes";
import {Link} from "react-router-dom";
import {ROUTES} from "../../../routes";

const classes = {
  fullWidth: {
    width: "100%"
  }
};

export function AdminMenuLink() {
  const [showLink, setShowLink] = useState(false);
  let {t} = useTranslation();

  return (
    <Box sx={classes.fullWidth}>
      <div onClick={() => {setShowLink(!showLink);}}>
        <Box sx={classes.fullWidth}>{t("ADMIN.MENU.LABEL")}</Box>
      </div>
      {showLink && (
        <div>
          <MenuItem component={Link} to={formatRoute(ROUTES.ADMIN_USER_CREATE)} key="AdminMenuLink1">
            {t("ADMIN.MENU.NEW_USER")}
          </MenuItem>
          <MenuItem component={Link} to={formatRoute(ROUTES.ADMIN_USERS)} key="AdminMenuLink2">
            {t("ADMIN.MENU.USERS")}
          </MenuItem>
          {/* <MenuItem component={Link} to={formatRoute(ROUTES.ADMIN_GROUP_CREATE)} key="AdminMenuLink3">
            {t("ADMIN.MENU.NEW_GROUP")}
          </MenuItem> */}
          <MenuItem component={Link} to={formatRoute(ROUTES.ADMIN_GROUPS)} key="AdminMenuLink4">
            {t("ADMIN.MENU.GROUPS")}
          </MenuItem>
        </div>
      )}
    </Box>
  );
}
