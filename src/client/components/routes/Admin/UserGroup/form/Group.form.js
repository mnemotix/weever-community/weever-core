import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, string} from "yup";
import {gqlUserGroupFragment} from "../gql/Group.gql";

export function getGroupValidationSchema({t}) {
  return object().shape({
    label: string().required(t("FORM_ERRORS.FIELD_ERRORS.LABEL_REQUIRED"))
  });
}

export const groupFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["label", "description"],
    gqlFragment: gqlUserGroupFragment
  }),
  validationSchema: getGroupValidationSchema
});
