import {object, string} from "yup";
import {DynamicFormDefinition, MutationConfig, LinkInputDefinition} from "@synaptix/ui";
import {gqlUserGroupFragment} from "../gql/Group.gql";

export function getUserGroupValidationSchema({t}) {
  return object().shape({
   /* label: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    description: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED"))*/
  });
}

export const userGroupFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["label", "description"],
    gqlFragment: gqlUserGroupFragment,
    gqlFragmentName: "UserGroupFragment"
  }),
  validationSchema: getUserGroupValidationSchema
});

export const userGroupLinkInputDefinition = new LinkInputDefinition({
  name: "userAccounts",
  isPlural: true,
  inputName: "userAccountInputs",
  targetObjectFormDefinition: userGroupFormDefinition
});
