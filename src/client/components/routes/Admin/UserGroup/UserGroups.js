/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Typography,
  Breadcrumbs,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Grid,
  Card,
  CardActionArea,
  CardContent
} from "@mui/material";
import {ExpandMore as ExpandMoreIcon} from "@mui/icons-material";
import {useTranslation} from "react-i18next";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../../routes";

import {gqlGroups} from "./gql/Groups.gql.js";
import {useQuery} from "@apollo/client";
import {useHistory} from "react-router-dom";
import {CollectionDataProvider, CollectionView, Button, useLoggedUser, createLink} from "@synaptix/ui";

const headingClass = (theme) => ({
  fontSize: theme.typography.pxToRem(20),
  flexBasis: "25%",
  flexShrink: 0
})

const secondaryHeadingClass = (theme) => ({
  fontSize: theme.typography.pxToRem(16),
  color: theme.palette.text.secondary
});

const classes = {
  root: {
    paddingTop: 4,
    paddingBottom: 4
  },
  breadcrumbs: {
    marginBottom: 2
  },
  globalRoleCard: {
    "&, & > button, & > button > div": {
      height: "100%"
    }
  }
};

export default function UserGroups({ } = {}) {

  const {t} = useTranslation();
  const history = useHistory();
  const {isEditor} = useLoggedUser();
  const columns = [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    {
      label: t("ADMIN.GROUP.NAME"),
      name: "label",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (title, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.ADMIN_GROUP, {id: row.id}),
            text: title
          });
        }
      }
    },
    {
      name: "description",
      label: t("ADMIN.GROUP.DESCRIPTION"),
      options: {
        filter: true,
        sort: true
      }
    }
  ];

  const {data: {userGroups: globalRoles} = {}, loading} = useQuery(gqlGroups, {
    variables: {
      filters: ["asGlobalRole = true"]
    }
  });

  const orderedGlobalRoles = (globalRoles?.edges || [])
    .map(({node: globalRole}) => globalRole)
    .sort((roleA, roleB) => {
      if (!roleA.subGroupOf.edges.some(({node: {id}}) => id === roleB.id)) {
        return -1;
      }

      return 0;
    });

  return (
    <>
      <Breadcrumbs sx={classes.breadcrumbs}>
        <Typography color="textPrimary">{t("BREADCRUMB.GROUPS")}</Typography>
      </Breadcrumbs>

      <Accordion defaultExpanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography sx={headingClass}>{t("ADMIN.GROUPS.GLOBAL_ROLES.TITLE")}</Typography>
          <Typography sx={secondaryHeadingClass}>{t("ADMIN.GROUPS.GLOBAL_ROLES.DESCRIPTION")}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container spacing={2}>
            {orderedGlobalRoles.map((globalRole) => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={globalRole.id}>
                <Card variant="outlined" sx={classes.globalRoleCard}>
                  <CardActionArea onClick={() => history.push(formatRoute(ROUTES.ADMIN_GROUP, {id: globalRole.id}))}>
                    <CardContent>
                      <Typography variant="h5" component="h2">
                        {globalRole.label}
                      </Typography>
                      <Typography color="textSecondary">
                        {globalRole.description}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            ))}
          </Grid>
        </AccordionDetails>
      </Accordion>

      <Accordion defaultExpanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography sx={headingClass}>{t("ADMIN.GROUPS.SPECIFIC_GROUPS.TITLE")}</Typography>
          <Typography sx={secondaryHeadingClass}>{t("ADMIN.GROUPS.SPECIFIC_GROUPS.DESCRIPTION")}</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <CollectionDataProvider
            gqlConnectionPath={"userGroups"}
            gqlQuery={gqlGroups}
            qs={""}
            gqlFilters={["asGlobalRole != true"]}
            gqlSortings={[{sortBy: "label"}]}
            searchEnabled={true}
          >
            <CollectionView
              collectionKey={"UserList"}
              availableDisplayModes={{table: {}}}
              columns={columns}
              renderRightSideActions={() => (
                <Button disabled={!isEditor} to={formatRoute(ROUTES.ADMIN_GROUP_CREATE)}>
                  {t("ADMIN.GROUPS.CREATE")}
                </Button>
              )}
            />
          </CollectionDataProvider>

        </AccordionDetails>
      </Accordion>
    </>
  );
}
