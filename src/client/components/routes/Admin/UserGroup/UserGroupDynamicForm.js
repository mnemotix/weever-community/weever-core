import {DynamicForm, RemoveEntityPanel} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {Grid} from "@mui/material";
import {Alert} from '@mui/material';

import {UserGroupUsersEdit} from "./UserGroupUsersEdit";
import {groupFormDefinition} from "./form/Group.form";
import {UserGroupFormContent} from "./UserGroupFormContent";
import {ROUTES} from "../../../../routes";
import {useHistory} from "react-router-dom";



export function UserGroupDynamicForm({userGroup, mutateFunction, saving} = {}) {
  const {t} = useTranslation();
  const history = useHistory();

  return (
    <DynamicForm
      object={userGroup}
      formDefinition={groupFormDefinition}
      mutateFunction={mutateFunction}
      saving={saving}
      renderFormContent={renderFormContent}
    />
  );

  function renderFormContent({renderActions}) {
    return (
      <Grid container spacing={4} direction="row" justifyContent="flex-start" alignItems="stretch" sx={{p: 2}}>
        <Grid item xs={8} container spacing={4}>
          <UserGroupFormContent group={userGroup} />

          <If condition={userGroup?.isInformativeGroup}>
            <Grid item xs={12}>
              <Alert severity="warning">{t("ADMIN.GROUP.INFORMATIVE_GROUP_MESSAGE")}</Alert>
            </Grid>
          </If>

          <Grid item xs={12}>
            <UserGroupUsersEdit groupId={userGroup?.id} isInformativeGroup={userGroup?.isInformativeGroup} />
          </Grid>
        </Grid>

        <Grid item xs={4} container alignContent={"flex-start"} spacing={10}>
          <Grid item xs={12} justifyContent={"center"} container>
            {renderActions()}
          </Grid>

          <If condition={userGroup && !userGroup.asGlobalRole}>
            <Grid item xs={12}>
              <RemoveEntityPanel
                entity={userGroup}
                panelTitle={t("ADMIN.GROUP.REMOVE.TITLE")}
                panelWarnMessage={t("ADMIN.GROUP.REMOVE.WARN_MESSAGE")}
                confirmMessage={t("ADMIN.GROUP.REMOVE.CONFIRM_MESSAGE", {group: userGroup?.label})}
                removeButtonLabel={t("ADMIN.GROUP.REMOVE.TITLE")}
                refetchQueriesAfterSuccess={["Groups"]}
                onSuccess={() => history.push(ROUTES.ADMIN_GROUPS)}
              />
            </Grid>
          </If>
        </Grid>
      </Grid>
    );
  }
}
