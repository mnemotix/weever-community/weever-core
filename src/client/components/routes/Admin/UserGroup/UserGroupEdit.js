/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {createLink, LoadingSplashScreen} from "@synaptix/ui";

import {Typography, Breadcrumbs} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useLazyQuery, useMutation} from "@apollo/client";

import {useSnackbar} from "notistack";
import {useParams, useHistory} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {UserGroupDynamicForm} from "./UserGroupDynamicForm";

import {ROUTES} from "../../../../routes";

import {gqlGroup} from "./gql/Group.gql.js";
import {gqlUpdateGroup} from "./gql/UpdateGroup.gql";
import {gqlCreateGroup} from "./gql/CreateGroup.gql";


export default function UserGroupEdit({ } = {}) {

  let {enqueueSnackbar} = useSnackbar();
  const {t} = useTranslation();
  const history = useHistory();
  let {id} = useParams();
  if (id) {
    id = decodeURIComponent(id);
  }

  const [getGroup, {data: {userGroup} = {}, loading}] = useLazyQuery(gqlGroup);

  useEffect(() => {
    if (id) {
      getGroup({variables: {id}});
    }
  }, [id]);

  // noinspection JSUnusedLocalSymbols
  const [mutateGroup, {loading: saving}] = useMutation(userGroup ? gqlUpdateGroup : gqlCreateGroup, {
    onCompleted: (data) => {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
      history.push(formatRoute(ROUTES.ADMIN_GROUPS));
    }
  });

  if (loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <>
      <Breadcrumbs sx={{mb: 2}}>
        {createLink({
          to: ROUTES.ADMIN_GROUPS,
          text: t("BREADCRUMB.GROUPS")
        })}
        <Typography color="textPrimary">{userGroup ? userGroup.label : t("ADMIN.GROUPS.CREATE")}</Typography>
      </Breadcrumbs>

      <UserGroupDynamicForm userGroup={userGroup} mutateFunction={mutateGroup} saving={saving} />
    </>
  );
}
