import {gql} from "@apollo/client";

export const gqlUpdateGroup = gql`
  mutation UpdateUserGroup($input: UpdateUserGroupInput!) {
    updateUserGroup(input: $input) {
      updatedObject {
        id
      }
    }
  }
`;
