import {gql} from "@apollo/client";

export const gqlCreateGroup = gql`
  mutation CreateUserGroup($input: CreateUserGroupInput!) {
    createUserGroup(input: $input) {
      createdObject {
        id
      }
    }
  }
`;
