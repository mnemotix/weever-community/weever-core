import {gql} from "@apollo/client";

export const gqlGroups = gql`
  query UserGroups($qs: String, $first: Int, $after: String, $sortings: [SortingInput], $filters: [String]) {
    userGroups(qs: $qs, first: $first, after: $after, sortings: $sortings, filters: $filters) {
      totalCount
      edges {
        node {
          id
          label
          description
          subGroupOf {
            edges {
              node {
                id
              }
            }
          }
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
`;
