import {gql} from "@apollo/client";
import {gqlUserAccountFragment} from "../../UserAccount/gql/UserAccount.gql";

// user's fragment
export const gqlUserGroupFragment = gql`
  fragment UserGroupFragment on UserGroup {
    id
    uri
    label
    description
    asGlobalRole
  }
`;

// info of a group
export const gqlGroup = gql`
  query UserGroup($id: ID!) {
    userGroup(id: $id) {
      ...UserGroupFragment
      isInformativeGroup
    }
  }
  ${gqlUserGroupFragment}
`;

// users of a given group
export const gqlGroupUsers = gql`
  query UserGroupUserAccounts($id: ID!) {
    userGroup(id: $id) {
      id
      userAccounts {
        edges {
          node {
            ...UserAccountFragment
            userGroups(filters: ["asGlobalRole = true"]) {
              totalCount
            }
          }
        }
      }
    }
  }
  ${gqlUserAccountFragment}
`;
