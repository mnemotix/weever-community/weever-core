/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Grid} from "@mui/material";
import {TextAreaField, TextField} from "@synaptix/ui";
import {useTranslation} from "react-i18next";

/**
 */
export function UserGroupFormContent({group} = {}) {
  const {t} = useTranslation();

  return (
    <>
      <Grid item xs={12} sx={{mt:2, mb: 2}}>
        <TextField required name="label" label={t("ADMIN.GROUP.NAME")} />
      </Grid>

      <Grid item xs={12}>
        <TextAreaField name="description" label={t("ADMIN.GROUP.DESCRIPTION")} />
      </Grid>
    </>
  );
}
