import {useTranslation} from "react-i18next";
import {Autocomplete} from "@synaptix/ui";

import {gqlUsersAccount} from "../UserAccount/gql/UserAccounts.gql";

export function UserGroupAutocomplete({onSelect, placeholder, selectedObject, error} = {}) {
  const {t} = useTranslation();
  return (
    <Autocomplete
      placeholder={placeholder || t("ADMIN.AUTOCOMPLETE.CHOOSE")}
      gqlQuery={gqlUsersAccount}
      gqlDataConnectionPath={"userAccounts"}
      optionLabelPath={"person.fullName"}
      gqlVariables={{first: 10}}
      onSelect={onSelect}
      selectedObject={selectedObject}
      AutocompleteProps={{
        noOptionsText: t("ADMIN.AUTOCOMPLETE.NO_RESULT")
      }}
      TextFieldProps={{
        variant: "standard",
        error: !!error,
        helperText: error
      }}
      creationEnabled={false}
    />
  );
}
