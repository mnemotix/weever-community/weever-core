import {useEffect, useState} from "react";
import {PluralLinkField, LoadingSplashScreen, ErrorBoundary, } from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useLazyQuery} from "@apollo/client";
import {ListItemText, ListItemIcon} from "@mui/material";
import {gqlGroupUsers} from "./gql/Group.gql";
import {userGroupLinkInputDefinition} from "./form/UserGroup.form.js";
import {UserGroupAutocomplete} from "./UserGroupAutocomplete";
import {PersonAvatar} from "../../Person";

export function UserGroupUsersEdit(props) {
  return (
    <ErrorBoundary>
      <GroupUsersEditCode {...props} />
    </ErrorBoundary>
  );
}

export function GroupUsersEditCode({groupId, isInformativeGroup} = {}) {
  const {t} = useTranslation();
  const [getGroupUsers, {data: {userGroup} = {}, loading}] = useLazyQuery(gqlGroupUsers);
  const [normalizedUserGroup, setNormalizedUserGroup] = useState();

  useEffect(() => {
    if (groupId) {
      getGroupUsers({
        variables: {
          id: groupId
        }
      });
    }
  }, [groupId]);

  useEffect(() => {
    if (userGroup) {
      setNormalizedUserGroup(normalizeUserGroup(userGroup));
    }
  }, [userGroup]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <PluralLinkField
      data={normalizedUserGroup}
      disabled={isInformativeGroup}
      linkInputDefinition={userGroupLinkInputDefinition}
      renderObjectContent={(userAccount) => (
        <>
          <ListItemIcon>
            <PersonAvatar person={userAccount?.person} />
          </ListItemIcon>
          <ListItemText
            primary={[userAccount?.person?.firstName, userAccount?.person?.lastName].join(" ")}
            secondary={null}
          />
        </>
      )}
      renderObjectAutocomplete={({...props}) => {
        return <UserGroupAutocomplete {...props} />;
      }}
      label={t("ADMIN.GROUP.MEMBERS")}
      addButtonLabel={t("ADMIN.GROUP.LINKEDIT_ADD_LABEL")}
    />
  );

  function normalizeUserGroup(userGroup) {
    if (!userGroup) {
      return;
    }

    let normalizedGroup = {...userGroup};

    normalizedGroup.userAccounts = {
      edges: (userGroup?.userAccounts?.edges || []).filter(({node: userAccount}) => {
        // This case of informative group is particular and must be handled here.
        // Take VisitorGroup or Contributor that are global roles where users
        // are gathered after an automatic processing (by inferring).
        // The problem consists on separating those who belong to another group and only display those who belong to
        // this group only.
        // For example, user that belongs to AdministratorGroup is automatically append to ContributorGroup (because get write access to at least one project)
        // but we don't want to see it appear in ContributorGroup users list.
        return isInformativeGroup ? userAccount.userGroups.totalCount === 1 : true;
      })
    };

    return normalizedGroup;
  }
}
