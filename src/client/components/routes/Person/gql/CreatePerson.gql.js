import {gql} from "@apollo/client";

export const gqlCreatePerson = gql`
  mutation CreatePerson($input: CreatePersonInput!) {
    createPerson(input: $input) {
      createdObject {
        id
      }
    }
  }
`;
