import {gql} from "@apollo/client";

export const gqlPersonExcerpt = gql`
  query PersonExcerpt($id: ID!) {
    person(id: $id) {
      id
      fullName
      avatar
    }
  }
`;
