/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {prepareMutation} from "@mnemotix/synaptix-client-toolkit";
import {Typography, Grid} from "@mui/material";
import {useTranslation} from "react-i18next";

import {AgentPhonesEdit} from "../Agent/Phone/AgentPhonesEdit";
import {AgentEmailsEdit} from "../Agent/Email/AgentEmailsEdit";
import {AgentAddressesEdit} from "../Agent/Address/AgentAddressesEdit";

import {AgentAffiliationsEdit} from "../Agent/Affiliation/AgentAffiliationsEdit";
import {PersonFormContent} from "./PersonFormContent";
import {DynamicForm, ExternalLinksEdit, AccessPolicyField, TaggingsField} from "@synaptix/ui";
import {personFormDefinition} from "./form/Person.form";
/**
 * @return {*}
 * @constructor
 */
export function PersonDynamicForm({person, fullForm, mutateFunction, saving, renderExtraActions} = {}) {
  const {t} = useTranslation();

  return (
    <DynamicForm
      object={person}
      formDefinition={personFormDefinition}
      mutateFunction={mutateFunction}
      saving={saving}
      renderExtraActions={renderExtraActions}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <PersonFormContent id={person?.id} />
        </Grid>

        <If condition={fullForm}>
          <>
            <Grid item xs={12}>
              <AccessPolicyField entityId={person?.id} />
            </Grid>

            <Grid item xs={12}>
              <AgentEmailsEdit agentId={person?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("AGENT.PHONES")}</Typography>

              <AgentPhonesEdit agentId={person?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("AGENT.ADDRESSES")}</Typography>

              <AgentAddressesEdit agentId={person?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ENTITY.EXTERNAL_LINKS")}</Typography>

              <ExternalLinksEdit entityId={person?.id} />
            </Grid>

            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("PERSON.AFFILIATIONS")}</Typography>

              <AgentAffiliationsEdit agentId={person?.id} agentType={"Person"} />
            </Grid>

            <Grid item xs={12}>
              <TaggingsField entityId={person?.id} />
            </Grid>
          </>
        </If>
      </Grid>
    </DynamicForm>
  );
}
