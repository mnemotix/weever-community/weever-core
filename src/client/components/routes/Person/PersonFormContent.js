import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {TranslatableField, TextField, DatePickerField, ImagePickerField, RichTextAreaField} from "@synaptix/ui";
import {useUploadService} from "../Import/BinWorkflow/UploadService";
import {FileUploadDashboard} from "../Import/BinWorkflow/FileUploadDashboard";

export function PersonFormContent({children} = {}) {
  const {t} = useTranslation();

  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TextField name="firstName" required={true} label={t("PERSON.FIRST_NAME")} />
      </Grid>
      <Grid item xs={12}>
        <TextField name="lastName" required={true} label={t("PERSON.LAST_NAME")} />
      </Grid>

      <Grid item xs={12}>
        <DatePickerField required={true} name="birthday" label={t("PERSON.BORN")} />
      </Grid>
      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="shortBio" label={t("PERSON.SHORT_BIO")} />
      </Grid>

      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="bio" label={t("PERSON.BIO")} />
      </Grid>

      <Grid item xs={12}>
        <ImagePickerField
          name={"avatar"}
          label={t("AGENT.AVATAR")}
          useUploadService={useUploadService}
          FileUploadDashboard={FileUploadDashboard}
        />
      </Grid>

      {children}
    </Grid>
  );
}
