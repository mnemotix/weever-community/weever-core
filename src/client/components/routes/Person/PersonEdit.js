/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {createLink, RemoveEntityPanel, useLoggedUser, LoadingSplashScreen, PageNotFound, gqlPerson} from "@synaptix/ui";
import {useParams, useHistory} from "react-router-dom";
import {Typography, Breadcrumbs, Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useMutation, useLazyQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../routes";
import {gqlUpdatePerson} from "./gql/UpdatePerson.gql";
import {gqlCreatePerson} from "./gql/CreatePerson.gql";
import {useSnackbar} from "notistack";
import {PersonAdminPanel} from "./PersonAdminPanel/PersonAdminPanel";
import {PersonDynamicForm} from "./PersonDynamicForm";

/**
 * @return {*}
 * @constructor
 */
export default function PersonEdit({id, hideBreadcrumb, disableRedirect} = {}) {
  const params = useParams();

  const {t} = useTranslation();
  const history = useHistory();
  const {enqueueSnackbar} = useSnackbar();

  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const {isAdmin} = useLoggedUser();
  const [getPerson, {data: {person} = {}, loading, called}] = useLazyQuery(gqlPerson);

  useEffect(() => {
    if (id) {
      getPerson({
        variables: {
          id,
          isAdmin: !!isAdmin
        }
      });
    }
  }, [id, isAdmin]);

  const [mutatePerson, {loading: saving}] = useMutation(id ? gqlUpdatePerson : gqlCreatePerson, {
    onCompleted: (data) => {
      if (data.createPerson?.createdObject?.id) {
        id = data.createPerson.createdObject.id;
      }
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
      if (!disableRedirect) {
        history.push(formatRoute(ROUTES.PERSON, {id}));
      }
    }
  });

  if (id) {
    if (loading || !called) {
      return <LoadingSplashScreen />;
    }

    if (!person?.permissions?.update) {
      return <PageNotFound />;
    }
  }

  return (
    <Grid container rowSpacing={4}>
      <If condition={!hideBreadcrumb}>
        <Grid item xs={12}>
          <Breadcrumbs>
            {createLink({to: ROUTES.PERSONS, text: t("BREADCRUMB.PERSONS")})}
            <Typography color="textPrimary">
              {id ? [person?.firstName, person?.lastName].join(" ") : t("PERSON.CREATE_TITLE")}
            </Typography>
          </Breadcrumbs>
        </Grid>
      </If>

      <Grid item xs={12}>
        <PersonDynamicForm
          person={person}
          mutateFunction={mutatePerson}
          fullForm
          saving={saving}
          renderExtraActions={renderExtraActions}
        />
      </Grid>
    </Grid>
  );

  function renderExtraActions() {
    if (person?.permissions.delete) {
      return (
        <Grid container item spacing={3}>
          <Grid item xs={12}>
            <RemoveEntityPanel
              entity={person}
              panelTitle={t("PERSON.ACTIONS.REMOVE.TITLE")}
              panelWarnMessage={t("PERSON.ACTIONS.REMOVE.WARN_MESSAGE")}
              confirmMessage={t("PERSON.ACTIONS.REMOVE.CONFIRM_MESSAGE", {person: person.fullName})}
              removeButtonLabel={t("PERSON.ACTIONS.REMOVE.TITLE")}
              onSuccess={() => history.go(-2)}
              refetchQueriesAfterSuccess={["Persons"]}
            />
          </Grid>

          <If condition={person}>
            <Grid item xs={12}>
              <PersonAdminPanel personId={person.id} />
            </Grid>
          </If>
        </Grid>
      );
    }
  }
}
