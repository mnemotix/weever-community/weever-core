/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  TranslatableDisplay,
  Button,
  gqlPerson,
  EntityAccessPolicy,
  ExternalLinks,
  Taggings,
  FloatingButton,
  useResponsive,
  createLink,
  PageNotFound,
  LoadingSplashScreen,
  RichTextViewer
} from "@synaptix/ui";
import {useParams} from "react-router-dom";
import {Box, Grid, Typography, Breadcrumbs, Divider, Chip} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../routes";
import {AgentPhones} from "../Agent/Phone/AgentPhones";
import {AgentEmails} from "../Agent/Email/AgentEmails";
import {AgentAddresses} from "../Agent/Address/AgentAddresses";
import {AgentAffiliations} from "../Agent/Affiliation/AgentAffiliations";

import {PersonAvatar} from "./PersonAvatar";
import {GlobalGraphButton} from "../Visualizations/GlobalGraphButton";
import {EntityActivity} from "../Activity/EntityActivity";

const classes = {
  avatar: (theme) => ({
    width: theme.spacing(40),
    height: theme.spacing(40),
    fontSize: theme.typography.fontSize * 5
  }),
  breadcrumbs: {
    marginBottom: 2
  },
  toolbar: {
    marginTop: 2,
    textAlign: "right"
  },
  birthday: {
    marginBottom: 2
  },
  empty: {
    color: "text.emptyHint"
  },
  action: {
    textAlign: "center",
    alignSelf: "center"
  }
};

export default function Person({id} = {}) {
  const {t} = useTranslation();
  const {isDesktop, isMobile} = useResponsive();

  if (!id) {
    id = decodeURIComponent(useParams().id);
  }

  const {data: {person} = {}, loading} = useQuery(gqlPerson, {
    variables: {
      id
    }
  });

  const fullName = [person?.firstName, person?.lastName].join(" ");

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (id && !person) {
    return <PageNotFound />;
  }

  return (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <Breadcrumbs>
          {createLink({to: ROUTES.PERSONS, text: t("BREADCRUMB.PERSONS")})}
          <Typography color="textPrimary">{fullName}</Typography>
        </Breadcrumbs>
      </Grid>
      <Grid item xs={12} container columnSpacing={2}>
        <Grid item xs={12} md={8}>
          <Box>
            <Typography variant="h5" gutterBottom>
              {fullName}
            </Typography>

            <If condition={person.birthday}>
              <Box sx={classes.birthday}>{t("PERSON.BORN_ON", {date: dayjs(person.birthday).format("LL")})}</Box>
            </If>

            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("PERSON.SHORT_BIO")}</Typography>

                <Box sx={[!person.shortBio && classes.empty]}>
                  <TranslatableDisplay isTranslated={person.shortBioTranslated}>
                    <RichTextViewer content={person.shortBio || t("PERSON.NO_BIOGRAPHY")} />
                  </TranslatableDisplay>
                </Box>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("PERSON.BIO")}</Typography>

                <Box sx={[!person.bio && classes.empty]}>
                  <TranslatableDisplay isTranslated={person.bioTranslated}>
                    <RichTextViewer content={person.bio || t("PERSON.NO_BIOGRAPHY")} />
                  </TranslatableDisplay>
                </Box>
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ENTITY.ACCESS_POLICY")}</Typography>
                <EntityAccessPolicy entityId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ENTITY.TAGGINGS")}</Typography>
                <Taggings entityId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("AGENT.PHONES")}</Typography>

                <AgentPhones agentId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("AGENT.EMAILS")}</Typography>

                <AgentEmails agentId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("AGENT.ADDRESSES")}</Typography>

                <AgentAddresses agentId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("ENTITY.EXTERNAL_LINKS")}</Typography>

                <ExternalLinks entityId={id} />
              </Grid>

              <Grid item xs={12}>
                <Typography variant="subtitle1">{t("PERSON.AFFILIATIONS")}</Typography>

                <AgentAffiliations agentId={id} isPerson />
              </Grid>
            </Grid>
          </Box>
        </Grid>

        <Grid item xs={12} md={4}>
          <Grid container spacing={2} direction="column" justifyContent="space-evenly" alignItems="center">
            <Grid item xs={12}>
              <PersonAvatar person={person} classSx={classes.avatar} />
            </Grid>

            <Grid item xs={12}>
              {t("AGENT.CREATED_AT_BY", {date: dayjs(person.createdAt).format("LL")})}
              {createLink({
                to: formatRoute(ROUTES.PERSON, {id: person?.creatorPerson?.id}),
                text: person?.createdBy
              })}
            </Grid>
            {isDesktop && (
              <Grid item xs={12} container direction="row" justifyContent="center" alignItems="center" spacing={4}>
                <Grid item sx={classes.action}>
                  <Button disabled={!person?.permissions?.update} to={formatRoute(ROUTES.PERSON_EDIT, {id})}>
                    {t("ACTIONS.UPDATE")}
                  </Button>
                </Grid>
                <Grid item sx={classes.action}>
                  <GlobalGraphButton entityId={id} />
                </Grid>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>

      <If condition={person?.permissions?.grant}>
        <Grid item xs={12}>
          <EntityActivity entityId={id} />
        </Grid>
      </If>

      <If condition={isMobile}>
        <FloatingButton
          locked={!person?.permissions?.update}
          to={formatRoute(ROUTES.PERSON_EDIT, {id})}
          name="EditIcon"
        />
      </If>
    </Grid>
  );
}
