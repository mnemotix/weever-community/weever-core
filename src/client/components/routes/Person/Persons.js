/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useTranslation} from "react-i18next";
import {Box, Avatar, Typography} from "@mui/material";
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../routes";
import {
  CollectionDataProvider,
  CollectionView,
  Button,
  useLoggedUser,
  useResponsive,
  createLink,
  getImageThumbnail,
  generateCollectionViewGenericColumns,
  EntityFormContentBatch,
  FloatingButton
} from "@synaptix/ui";
import {gqlPersons} from "./gql/Persons.gql";
import {personFormDefinition} from "./form/Person.form";
import {gqlBatchUpdatePersons} from "./gql/BatchUpdatePerson.gql";

export function getColumns({t, routeOnClick}) {
  return [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    {
      name: "avatar",
      label: " ",
      altLabel: t("SEARCH.COLUMNS.COMMON.IMAGE"),
      options: {
        filter: false,
        sort: false,
        customBodyRender: (image, {row: person}) => {
          return (
            <Box
              sx={{
                my: -1
              }}>
              <Link style={{textDecoration: "none"}} to={formatRoute(routeOnClick, {id: person.id})}>
                <Avatar
                  src={getImageThumbnail({
                    imageUrl: image,
                    size: "small"
                  })}>
                  {(person.firstName || "").charAt(0)}
                  {(person.lastName || "").charAt(0)}
                </Avatar>
              </Link>
            </Box>
          );
        },
        setCellProps: () => ({
          sx: (theme) => ({width: theme.spacing(8)})
        })
      }
    },
    {
      name: "fullName",
      label: `${t("SEARCH.COLUMNS.PERSONS.FULL_NAME")}`,
      // displayAsMainTitle:true,
      options: {
        filter: true,
        sort: true,
        customBodyRender: (title, {row: person}) => {
          return (
            <Typography variant="subtitle1">
              {" "}
              {createLink({
                to: formatRoute(routeOnClick, {id: person.id}),
                text: person.fullName
              })}{" "}
            </Typography>
          );
        }
      }
    },
    ...generateCollectionViewGenericColumns({t})
  ];
}

export default function Persons({
  qs,
  filterExistingUserAccounts,
  searchEnabled,
  routeOnClick = ROUTES.PERSON,
  renderRightSideActions = null
} = {}) {
  const {t} = useTranslation();
  const {isEditor, isContributor} = useLoggedUser();

  const {isDesktop} = useResponsive();

  let gqlFilters = [];
  if (filterExistingUserAccounts) {
    gqlFilters = ["hasUserAccount:*"];
  }

  const columns = getColumns({t, routeOnClick});

  return (
    <>
      <CollectionDataProvider
        gqlConnectionPath={"persons"}
        gqlQuery={gqlPersons}
        gqlFilters={gqlFilters}
        qs={qs}
        searchEnabled={searchEnabled}>
        <CollectionView
          collectionKey={"PersonsList"}
          availableDisplayModes={{table: {}}}
          columns={columns}
          renderRightSideActions={() => {
            if (renderRightSideActions) {
              return renderRightSideActions;
            } else {
              return isDesktop ? (
                <Button disabled={!isContributor} to={formatRoute(ROUTES.PERSON_CREATE)}>
                  {t("PERSON.CREATE")}
                </Button>
              ) : (
                <FloatingButton locked={!isContributor} to={formatRoute(ROUTES.PERSON_CREATE)} name="AddIcon" />
              );
            }
          }}
          batchEditEnabled={isEditor}
          batchEditButtonProps={{
            gqlUpdateMutation: gqlBatchUpdatePersons,
            entityFormDefinition: personFormDefinition,
            entityFormContent: <EntityFormContentBatch formContentFragmentPrefix="Person" />,
            title: ({entityTotalCount}) => t("PERSON.ACTIONS.BATCH_UPDATE.TITLE", {count: entityTotalCount}),
            confirmMessage: ({entityTotalCount}) => t("PERSON.ACTIONS.BATCH_UPDATE.CONFIRM", {count: entityTotalCount}),
            successMessage: ({entityTotalCount}) => t("PERSON.ACTIONS.BATCH_UPDATE.SUCCESS", {count: entityTotalCount})
          }}
        />
      </CollectionDataProvider>
    </>
  );
}

/*
<CollectionView
  collectionKey={"PersonsList"}
  columns={columns}
  gqlConnectionPath={"persons"}
  gqlCountPath={"personsCount"}
  gqlQuery={gqlPersons}
  qs={qs}
  gqlFilters={gqlFilters}
  availableDisplayModes={{table: {}}}
  searchEnabled={searchEnabled}
  batchEditEnabled={isEditor}
  batchEditButtonProps={{
    gqlUpdateMutation: gqlBatchUpdatePersons,
    entityFormDefinition: personFormDefinition,
    entityFormContent: <EntityFormContentBatch formContentFragmentPrefix="Person" />,
    title: ({entityTotalCount}) => t("PERSON.ACTIONS.BATCH_UPDATE.TITLE", {count: entityTotalCount}),
    confirmMessage: ({entityTotalCount}) => t("PERSON.ACTIONS.BATCH_UPDATE.CONFIRM", {count: entityTotalCount}),
    successMessage: ({entityTotalCount}) => t("PERSON.ACTIONS.BATCH_UPDATE.SUCCESS", {count: entityTotalCount})
  }}
  renderRightSideActions={() => {
    if (renderRightSideActions) {
      return renderRightSideActions;
    } else {
      return isDesktop ? (
        <Button disabled={!isContributor} to={formatRoute(ROUTES.PERSON_CREATE)}>
          {t("PERSON.CREATE")}
        </Button>
      ) : (
        <FloatingButton locked={!isContributor} to={formatRoute(ROUTES.PERSON_CREATE)} name="AddIcon" />
      );
    }
  }}
/>
*/
