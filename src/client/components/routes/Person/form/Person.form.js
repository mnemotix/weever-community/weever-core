import {object, string, addMethod} from "yup";
import without from "lodash/without";
import {DynamicFormDefinition, MutationConfig, gqlPersonFragment} from "@synaptix/ui";


addMethod(object, 'atLeastOneOf', function (list, message) {
  return this.shape(list.reduce((acc, field) => ({
    ...acc,
    [field]: this.fields[field].when(without(list, field), {
      is: (...values) => !values.some((item) => item),
      then: this.fields[field].required(message),
    }),
  }), {}), list.reduce((acc, item, idx, all) => [...acc, ...all.slice(idx + 1).map((i) => [item, i])], []));
})

export function getPersonValidationSchema({t}) {
  return object().shape({
    lastName: string(),
    firstName: string()
  }).atLeastOneOf(["lastName", "firstName"], t("PERSON.FIELD_ERRORS.LAST_NAME_REQUIRED") );
}

export const personFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["avatar", "firstName", "lastName", "bio", "shortBio", "birthday"],
    gqlFragment: gqlPersonFragment
  }),
  postProcessInitialValues: object => ({
    ...object,
    birthday: object.birthday === "" ? null : object.birthday // Replace "" by null to make DatePickerField working
  }),
  validationSchema: getPersonValidationSchema
});
