/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState, useEffect} from "react";
import {useTranslation} from "react-i18next";
import {useSnackbar} from "notistack";
import {Typography, TextField, Box, Chip, Button as MuiButton, Switch, FormGroup, FormControlLabel, Grid} from "@mui/material";
import {useQuery, useMutation} from "@apollo/client";

import {gqlResetUserAccountPasswordByMail} from "../../../../components/routes/Profil/ProfilResetPassword";
import {globalSx} from "../../../../components/layouts/styles";
import {gqlDisableUserAccount} from "../../Admin/UserAccount/gql/DisableUserAccount.gql";
import {gqlEnableUserAccount} from "../../Admin/UserAccount/gql/EnableUserAccount.gql";
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../../routes";
import {useLoggedUser} from "@synaptix/ui";
import {gqlPersonAdmin} from "./gql/PersonAdmin.gql";
import {Accordion, ConfirmDialog} from '@synaptix/ui';

const classes = {
  noAccountContainer: {
    textAlign: "center"
  },
  noAccountMessage: {
    marginBottom: 2
  }
};

/**
 * return the panel to edit a person settings, used by admin only
 * @param string id id is used to deletePerson
 * @param string userAccount.userId is used to enable/disable userAccount
 */
export function PersonAdminPanel({personId} = {}) {
  let {enqueueSnackbar} = useSnackbar();

  const {t} = useTranslation();
  const user = useLoggedUser();


  const onMyOwnAccount = user?.id === personId;
  // boolean to change Enable/disable button after mutation, page is not reloaded
  const [userAccountIsDisabled, setUserAccountIsDisabled] = useState(false);
  const [sendEmailNotifIsDisabled, setSendEmailNotifIsDisabled] = useState(false);

  // show or not the reset password confirm
  const [showResetPasswordConfirm, setShowResetPasswordConfirm] = useState(false);
  const [personEmail, setPersonEmail] = useState(null);
  const [userAccount, setUserAccount] = useState({});

  const {data: {person} = {}} = useQuery(gqlPersonAdmin, {
    variables: {
      id: personId
    }
  });

  useEffect(() => {
    if (person) {
      setUserAccount(person?.userAccount || {});
      setUserAccountIsDisabled(person?.userAccount?.isDisabled);
      setPersonEmail(person?.mainEmail?.email);
    }
  }, [person]);

  const haveAccount = userAccount?.id && !userAccount?.isUnregistered;

  // to disable or enable user account ( not deleting it )
  const [disableUserAccount] = useMutation(gqlDisableUserAccount, {
    onCompleted(data) {
      if (data?.disableUserAccount?.success) {
        setUserAccountIsDisabled(true);
        enqueueSnackbar(t("PERSON.ADMIN_PANEL.ACCOUNT_DISABLED"), {
          variant: "warning"
        });
      }
    }
  });

  const [enableUserAccount] = useMutation(gqlEnableUserAccount, {
    onCompleted(data) {
      if (data?.enableUserAccount?.success) {
        setUserAccountIsDisabled(false);
        enqueueSnackbar(t("PERSON.ADMIN_PANEL.ACCOUNT_ENABLED"), {
          variant: "success"
        });
      }
    }
  });

  // to reset account with email
  const [resetUserAccount] = useMutation(gqlResetUserAccountPasswordByMail, {
    onCompleted() {
      enqueueSnackbar(t("RESET_PASSWORD.CONFIRMATION"), {
        variant: "success"
      });
    }
  });

  return (
    <Accordion title={t("PERSON.ADMIN_PANEL.TITLE")} defaultExpanded className={{textAlign: "left"}}>
      <Grid spacing={2} container>
        {renderAccountStatus()}
      </Grid>
    </Accordion>
  );

  // render account status, account created or not, with role , disable/enable email notification, reset password by email
  function renderAccountStatus() {
    if (haveAccount) {
      return (
        <>
          <Grid item xs={12}>
            <Typography variant="subtitle2" gutterBottom>
              {" "}
              {t("PERSON.ADMIN_PANEL.ROLES")}
            </Typography>
            <Box sx={globalSx.chipsContainer}>
              {userAccount?.userGroups?.edges?.length > 0 ? (
                userAccount?.userGroups?.edges?.map(({node}, index) => (
                  <Chip size="small" key={node.label + index} onClick={null} label={node.label} variant="outlined" />
                ))
              ) : (
                <div>{t("PERSON.ADMIN_PANEL.NO_ROLE")}</div>
              )}
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="subtitle2" gutterBottom>
              {" "}
              {t("PERSON.ADMIN_PANEL.SETTINGS")}
            </Typography>

            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    checked={!sendEmailNotifIsDisabled}
                    onChange={() => setSendEmailNotifIsDisabled(!sendEmailNotifIsDisabled)}
                  />
                }
                label={
                  sendEmailNotifIsDisabled
                    ? t("PERSON.ADMIN_PANEL.DONT_SEND_EMAIL_NOTIFICATION")
                    : t("PERSON.ADMIN_PANEL.SEND_EMAIL_NOTIFICATION")
                }
              />
            </FormGroup>

            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    disabled={onMyOwnAccount}
                    checked={!userAccountIsDisabled}
                    onChange={() =>
                      userAccountIsDisabled
                        ? enableUserAccount({
                          variables: {input: {userId: userAccount.userId}}
                        })
                        : disableUserAccount({
                          variables: {input: {userId: userAccount.userId}}
                        })
                    }
                  />
                }
                label={
                  userAccountIsDisabled
                    ? t("PERSON.ADMIN_PANEL.ACCOUNT_DISABLED")
                    : t("PERSON.ADMIN_PANEL.ACCOUNT_ENABLED")
                }
              />
            </FormGroup>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="subtitle2" gutterBottom>
              {" "}
              {t("PERSON.ADMIN_PANEL.PASSWORD")}
            </Typography>
            <MuiButton
              onClick={() => setShowResetPasswordConfirm(true)}
              variant="contained"
              color="secondary"
              size="small">
              {t("RESET_PASSWORD.SEND_RESET_TO_EMAIL")}
            </MuiButton>
            <ConfirmDialog
              title={t("RESET_PASSWORD.SEND_RESET_TO_EMAIL")}
              open={showResetPasswordConfirm}
              onClose={() => setShowResetPasswordConfirm(false)}
              onConfirm={handleSendResetEmail}>

              <TextField
                margin="dense"
                required
                label={t("SIGN_IN.EMAIL")}
                name="email"
                autoComplete="email"
                autoFocus
                fullWidth
                value={personEmail}
                onChange={(event) => setPersonEmail(event.target.value)}
              />

            </ConfirmDialog>
          </Grid>
        </>
      );
    } else {
      return (
        <Grid item xs={12} sx={classes.noAccountContainer}>
          <Typography variant="subtitle1" gutterBottom sx={classes.noAccountMessage}>
            {userAccount?.id && userAccount?.isUnregistered
              ? t("PERSON.ADMIN_PANEL.DELETED_ACCOUNT")
              : t("PERSON.ADMIN_PANEL.NO_ACCOUNT_YET")}
          </Typography>

          <MuiButton
            component={Link}
            to={{
              pathname: formatRoute(ROUTES.ADMIN_USER_CREATE, {id: personId}),
              state: {
                person
              }
            }}
            color="secondary"
            variant="contained"
            size="small">
            {t("PERSON.ADMIN_PANEL.CREATE_ACCOUNT")}
          </MuiButton>
        </Grid>
      );
    }
  }

  function handleSendResetEmail() {
    resetUserAccount({
      variables: {
        input: {
          email: personEmail
        }
      }
    });
  }
}
