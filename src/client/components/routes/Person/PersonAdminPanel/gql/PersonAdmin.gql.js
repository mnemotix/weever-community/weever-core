import {gql} from "@apollo/client";

export const gqlPersonAdmin = gql`
  query PersonAdmin($id: ID!) {
    person(id: $id) {
      id
      firstName
      lastName
      avatar
      mainEmail {
        email
      }
      emails {
        edges {
          node {
            id
          }
        }
      }
      userAccount {
        id
        isDisabled
        isUnregistered
        userId
        userGroups(filters: ["asGlobalRole = true"]) {
          edges {
            node {
              id
              label
            }
          }
        }
      }
    }
  }
`;
