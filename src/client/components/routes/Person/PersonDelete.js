import {useState} from "react";
import {Button as MuiButton} from "@mui/material";

import {useTranslation} from "react-i18next";
import DeleteIcon from "@mui/icons-material/Delete";

import {useMutation, gql} from "@apollo/client";
import {useSnackbar} from "notistack";
import {ConfirmDeleteDialog} from "../Profil/ProfilDelete";

const gqlDeletePerson = gql`
  mutation RemoveEntity($input: RemoveEntityInput!) {
    removeEntity(input: $input) {
      deletedId
    }
  } 
`;

export function PersonDelete({id}) {
  const {t} = useTranslation();
  let {enqueueSnackbar} = useSnackbar();

  const [confirmOpen, setConfirmOpen] = useState(false);

  const [deletePerson] = useMutation(gqlDeletePerson, {
    onCompleted() {
      enqueueSnackbar(t("PERSON.DELETE.DONE_CONFIRMATION"), {variant: "success"});
      history.push(formatRoute(ROUTES.ADMIN_USERS));
    }
  });

  const handleConfirm = async () => {
    await deletePerson({
      variables: {
        input: {objectId: id}
      }
    });
  };

  return (
    <div>
      <MuiButton
        onClick={() => setConfirmOpen(true)}
        variant="contained"
        color="secondary"
        style={{backgroundColor: "red"}}>
        <DeleteIcon /> {t("PERSON.DELETE.DELETE_DEFINITELY")}
      </MuiButton>
      <ConfirmDeleteDialog
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={handleConfirm}
        title={t("PERSON.DELETE.DELETE_DEFINITELY")}
        buttonLabel={t("PERSON.DELETE.I_UNDERSTAND")}
      />
    </div>
  );
}
