/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {gql} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {Autocomplete} from "@synaptix/ui";

const gqlPersons = gql`
  query Persons($qs: String, $first: Int, $filters: [String], $sortings: [SortingInput]) {
    persons(qs: $qs, first: $first, sortings: $sortings, filters: $filters) {
      edges {
        node {
          id
          firstName
          lastName
          fullName
        }
      }
    }
  }
`;

/**
 * @param {function} onSelect
 * @param {string} placeholder
 * @param {object} selectedProject
 * @param {string} error
 * @param {boolean} creationEnabled
 * @param {function} onCreate
 */
export function PersonsAutocomplete({onSelect, placeholder, selectedObject, error, creationEnabled, onCreate} = {}) {
  const {t} = useTranslation();
  return (
    <Autocomplete
      placeholder={placeholder || t("PERSON.AUTOCOMPLETE.CHOOSE")}
      gqlQuery={gqlPersons}
      gqlDataConnectionPath={"persons"}
      optionLabelPath={"fullName"}
      onSelect={onSelect}
      selectedObject={selectedObject}
      creationEnabled={creationEnabled}
      onCreate={object => {
        // Transform object.fullName from autocomplete to object.firstName/lastName
        const spittedName = (object.fullName || "").split(" ");
        return onCreate({
          fullName: object.fullName,
          firstName: spittedName.length > 1 ? spittedName[0] || "" : "",
          lastName: spittedName[1] || spittedName[0] || ""
        });
      }}
      AutocompleteProps={{
        noOptionsText: t("PERSON.AUTOCOMPLETE.NO_RESULT")
      }}
      TextFieldProps={{
        variant: "standard",
        error: !!error,
        helperText: error
      }}
    />
  );
}
