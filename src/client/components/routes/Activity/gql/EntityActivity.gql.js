/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {gql} from "@apollo/client";

export const gqlEntityActivity = gql`
  query EntityActivity($entityId: ID!, $first: Int, $after: String, $filters: [String], $sortings: [SortingInput]) {
    entity(id: $entityId) {
      id
      types
      __typename
      ... on Person {
        fullName
        avatar
      }
      actionsCount(filters: $filters)
      actions(first: $first, after: $after, sortings: $sortings, filters: $filters) {
        edges {
          node {
            id
            startedAtTime
            snapshot(pretty: true)
            person {
              id
              avatar
              fullName
            }
          }
        }
        pageInfo {
          startCursor
          endCursor
          hasNextPage
        }
      }
    }
  }
`;
