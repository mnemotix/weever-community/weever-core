/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Trans} from "react-i18next";

export function getLinkedObjectsTranslations({
  linkedObjects,
  i18nPrefix,
  LinkedObjectComponent,
  i18nLinkedComponentTag = "linkedComponent"
}) {
  if (!Array.isArray(linkedObjects)) {
    linkedObjects = [linkedObjects];
  }

  linkedObjects = linkedObjects.filter((linkedObject) => !!linkedObject?.id);

  const addedLinkedObjects = linkedObjects.filter((linkedObject) => !linkedObject["@deletion"]);
  const deletedLinkedObjects = linkedObjects.filter((linkedObject) => linkedObject["@deletion"]);

  return (
    <>
      {[
        ["ADDED", addedLinkedObjects],
        ["REMOVED", deletedLinkedObjects]
      ].map(([i18nPostfix, linkedObjects]) => (
        <If condition={linkedObjects.length > 0}>
          <div key={i18nPostfix}>
            <Trans
              i18nKey={`${i18nPrefix}.${i18nPostfix}`}
              values={{count: linkedObjects.length}}
              components={{
                [i18nLinkedComponentTag]: (
                  <>
                    {linkedObjects.map(({id}, index) => {
                      return id ? (
                        <>
                          <LinkedObjectComponent key={id} id={id} />
                          {index < linkedObjects.length - 1 && ", "}
                        </>
                      ) : (
                        <></>
                      );
                    })}
                  </>
                )
              }}
            />
          </div>
        </If>
      ))}
    </>
  );
}
