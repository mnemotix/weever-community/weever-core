import {Fragment, useEffect, useState} from "react";
import {Trans, useTranslation} from "react-i18next";
import {usePaginatedQuery} from "@mnemotix/synaptix-client-toolkit";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";
import snakeCase from "lodash/snakeCase";
import {
  Chip,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Skeleton,
  Typography,
  Box,
  Link,
  Paper
} from "@mui/material";
import {ROUTES} from "../../../routes";
import {createLink, useExtensionSettings} from "@synaptix/ui";
import {PersonAvatar} from "../Person";
import {useInView} from "react-intersection-observer";
import "react-json-pretty/themes/monikai.css";
import {gqlEntityActivity} from "./gql/EntityActivity.gql";
import {AccessPolicyExcerpt} from "../Common/AccessPolicy/AccessPolicyExcerpt";
import {AccessTargetExcerpt} from "../Common/Entity/AccessTargets/AccessTargetExcerpt";
import {TaggingExcerpt} from "../Common/Tagging/TaggingExcerpt";
import {getLinkedObjectsTranslations} from "./getLinkedObjectsTranslations";
import JSONPretty from "react-json-pretty";

const pageSize = 15;

const classes = {
  personAvatar: {
    width: ({spacing}) => spacing(5),
    height: ({spacing}) => spacing(5)
  }
};

const entityTypenameMapping = {
  File: "Resource"
};

/**
 * @param {string} entityId
 * @param {string} [i18nPrefix]
 * @param {object} [linkedObjectsComponents]
 * @returns {JSX.Element}
 * @constructor
 */
export function EntityActivity({entityId, i18nPrefix, linkedObjectsComponentMapping = {}} = {}) {
  const {t, i18n} = useTranslation();
  const {data: {entity} = {}, loading, loadNextPage, hasNextPage} = usePaginatedQuery(gqlEntityActivity, {
    pageSize,
    connectionPath: "entity.actions",
    variables: {
      sortings: [{sortBy: "startedAtTime", isSortDescending: true}],
      entityId
    }
  });

  const {ref, inView, entry} = useInView({
    threshold: 0
  });

  useEffect(() => {
    if (!loading && entity?.actions) {
      loadNextPage();
    }
  }, [inView]);

  if (!i18nPrefix && entity) {
    i18nPrefix = `${formatWord(entityTypenameMapping[entity.__typename] || entity.__typename)}.ACTIVITY`;
  }

  const extensionLinkedObjectsComponentMapping = useExtensionSettings(`${i18nPrefix}.COMPONENT_MAPPING`);

  linkedObjectsComponentMapping = {
    "ACTIVITY.ENTITY.ACCESS_POLICY": AccessPolicyExcerpt,
    "ACTIVITY.ENTITY.READ_ONLY_ACCESS_TARGETS": AccessTargetExcerpt,
    "ACTIVITY.ENTITY.READ_WRITE_ACCESS_TARGETS": AccessTargetExcerpt,
    "ACTIVITY.ENTITY.TAGGINGS": TaggingExcerpt,
    ...linkedObjectsComponentMapping
  };

  if (extensionLinkedObjectsComponentMapping) {
    Object.entries(extensionLinkedObjectsComponentMapping).map(([key, value]) => {
      linkedObjectsComponentMapping[`${i18nPrefix}.${key}`] = value;
      return linkedObjectsComponentMapping;
    });
  }

  return (
    <>
      <br />
      <Divider textAlign={"center"}>
        <Chip label={t("ACTIVITY.ENTITY_ACTIVITY")} />
      </Divider>
      <List>
        {buildActivity(entity?.actions?.edges || []).map((action, index) => renderActionListItem(action, index))}

        <Choose>
          <When condition={loading}>
            {[...Array(pageSize)].map((_, i) => (
              <Fragment key={i}>
                <ListItem sx={{paddingTop: 0}}>
                  <ListItemAvatar>
                    <Skeleton variant="circle" width={40} height={40} />
                  </ListItemAvatar>
                  <ListItemText
                    primary={<Skeleton variant="rect" width={210} />}
                    secondary={
                      <>
                        <div>
                          <Typography component="span" variant="body2" color="textPrimary">
                            <Skeleton variant="rect" width={210} />
                          </Typography>
                          {" — "}
                          <Skeleton variant="rect" width={400} />
                        </div>
                      </>
                    }
                    secondaryTypographyProps={{component: "div"}}
                  />
                </ListItem>
                <Divider variant="inset" component="li" />
              </Fragment>
            ))}
          </When>
          <When condition={hasNextPage}>
            <div ref={ref}>Chargement des suivants....</div>
          </When>
        </Choose>
      </List>
    </>
  );

  function renderActionListItem(action) {
    return (
      <Fragment key={action.id}>
        <ListItem alignItems={action.__typename === "Update" ? "flex-start" : "center"} sx={{padding: 0}}>
          <ListItemAvatar>
            <PersonAvatar person={action.person || entity} classSx={classes.personAvatar} />
          </ListItemAvatar>
          <Choose>
            <When condition={action.__typename === "Creation"}>{renderCreationActionBody(action)}</When>
            <When condition={action.__typename === "Update"}>{renderUpdateActionBody(action)}</When>
          </Choose>
        </ListItem>

        {/*<JSONPretty data={action.snapshot} />*/}
      </Fragment>
    );
  }

  function renderCreationActionBody(action) {
    let i18nKey = `ACTIVITY.ACTION.CREATION.${formatWord(entity.__typename)}`;

    // Case of an auto-created account (with related person)
    if (!action.person && entity.__typename === "Person") {
      i18nKey += `_SELF_CREATED`;
    }

    return (
      <ListItemText
        primary={
          <Typography component="span" variant="body2" color="textPrimary">
            <Trans
              i18nKey={i18nKey}
              values={{label: entity.label, person: action.person?.fullName}}
              components={{
                person: action.person ? (
                  createLink({
                    to: formatRoute(ROUTES.PERSON, {id: action.person.id}),
                    text: action.person.fullName
                  })
                ) : (
                  <></>
                ),
                entity: <></>
              }}
            />
            {" - "} {dayjs(action.startedAtTime).format("LLL")}
          </Typography>
        }
      />
    );
  }

  function renderUpdateActionBody(action) {
    return (
      <ListItemText
        primary={
          <Typography component="span" variant="body2" color="textPrimary">
            <If condition={action.person}>
              {createLink({
                to: formatRoute(ROUTES.PERSON, {id: action.person.id}),
                text: action.person.fullName
              })}
            </If>
            {" - "} {dayjs(action.startedAtTime).format("LLL")}
          </Typography>
        }
        secondary={
          <List dense sx={{paddingTop: 0}}>
            {action.fragments.map((fragment) => (
              <ListItem key={fragment.id} dense sx={{padding: 0}}>
                <ListItemText primary={fragment.description} />
              </ListItem>
            ))}
          </List>
        }
        secondaryTypographyProps={{component: "div"}}
      />
    );
  }

  function formatWord(word) {
    return snakeCase(word).toUpperCase();
  }

  function buildActivity(actions) {
    return actions.reduce((activity, {node: action}) => {
      const fragments = getActionFragments(action);

      if (action.__typename === "Creation" || fragments.length > 0) {
        activity.push({
          ...action,
          snapshot: JSON.parse(action.snapshot),
          fragments
        });
      }

      return activity;
    }, []);
  }

  function getActionFragments(action) {
    const entitySnapshot = action.snapshot ? JSON.parse(action.snapshot)?.[0] : null;
    let fragments = [];

    if (entitySnapshot && entity.__typename === entitySnapshot.type) {
      const typenameI18nKeys = [
        ...entity.types.map((type) => {
          const lastIndexOfSlash = type.lastIndexOf("/");
          if (lastIndexOfSlash !== 1) {
            return type.slice(lastIndexOfSlash + 1);
          }
          return type;
        }),
        entity.__typename
      ].map((type) => formatWord(type));
      const [fields, links] = Object.entries(entitySnapshot).reduce(
        ([fields, links], [property, values]) => {
          if (!["id", "uri", "@ref", "type"].includes(property)) {
            let mainI18nKey = formatWord(property);
            let altI18nKeys = typenameI18nKeys.map((typenameI18nKey) => `${typenameI18nKey}.${mainI18nKey}`);
            let defaultLabel = t(altI18nKeys);

            if (typeof values === "string" || (values?.[0]?.lang && values?.[0]?.value)) {
              fields.push({
                label: defaultLabel,
                value: values?.[0]?.value || values
              });
            } else {
              links.push({
                mainI18nKey,
                altI18nKeys,
                defaultLabel,
                linkedObjects: values
              });
            }
          }

          return [fields, links];
        },
        [[], []]
      );

      if (fields.length > 0) {
        fragments.push({
          id: `${action.id}_${entitySnapshot.id}_properties`,
          description: (
            <>
              <Trans
                i18nKey={`ACTIVITY.ENTITY.FIELDS_UPDATE`}
                values={{fields: fields.map(({label}) => label).join(", "), count: fields.length}}
                components={{
                  fields: <strong />
                }}
              />{" "}
              <FieldsDetail fields={fields} />
            </>
          )
        });
      }

      for (const {mainI18nKey, altI18nKeys, linkedObjects, defaultLabel} of links) {
        fragments.push({
          id: `${action.id}_${entitySnapshot.id}_${mainI18nKey}`,
          description: getLinkTranslation({mainI18nKey, altI18nKeys, defaultLabel, linkedObjects})
        });
      }
    }

    return fragments;
  }

  function getLinkTranslation({mainI18nKey, altI18nKeys, linkedObjects, defaultLabel}) {
    let [linkedObjectsI18nPrefix, LinkedObjectComponent] =
      Object.entries(linkedObjectsComponentMapping).find(([key]) =>
        [`ACTIVITY.ENTITY.${mainI18nKey}`, `${i18nPrefix}.${mainI18nKey}`].includes(key)
      ) || [];

    if (LinkedObjectComponent) {
      return getLinkedObjectsTranslations({
        linkedObjects,
        i18nPrefix: linkedObjectsI18nPrefix,
        LinkedObjectComponent
      });
    } else {
      return getLinkedObjectsTranslations({
        linkedObjects: Array.isArray(linkedObjects) ? linkedObjects[0] : linkedObjects,
        i18nPrefix: "ACTIVITY.ENTITY.LINKS",
        LinkedObjectComponent: () => (
          <Box sx={{fontWeight: "bold", display: "inline"}}>
            {i18n.exists([...altI18nKeys, mainI18nKey]) ? t([...altI18nKeys, mainI18nKey]) : defaultLabel}
          </Box>
        )
      });
    }
  }
}

function FieldsDetail({fields}) {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <Link sx={{textDecoration: "underline", cursor: "pointer"}} onClick={() => setVisible(!visible)}>
        {visible ? "Masquer le détail" : "Afficher le détail"}
      </Link>
      <If condition={visible}>
        <Paper square variant="outlined" sx={{p: 1, m: 1}}>
          <Choose>
            <When condition={fields.length === 1}>{renderField({field: fields[0], hideLabel: true})}</When>
            <Otherwise>
              <List>
                {fields.map((field) => (
                  <ListItem key={field.label}>{renderField({field})}</ListItem>
                ))}
              </List>
            </Otherwise>
          </Choose>
        </Paper>
      </If>
    </>
  );

  function renderField({field, hideLabel}) {
    return (
      <>
        <If condition={!hideLabel}>
          <Box sx={{fontWeight: "bold", mr: 1}}>{field.label} : </Box>{" "}
        </If>
        {field.value}
      </>
    );
  }
}
