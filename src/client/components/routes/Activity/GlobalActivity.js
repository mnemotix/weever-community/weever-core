import {Fragment, useEffect} from "react";
import {useTranslation, Trans} from "react-i18next";
import {usePaginatedQuery} from "@mnemotix/synaptix-client-toolkit";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";
import snakeCase from "lodash/snakeCase";
import {gqlActivity} from "./gql/Activity.gql";
import {List, ListItem, ListItemText, ListItemAvatar, Typography, Divider} from "@mui/material";
import {Skeleton} from "@mui/material";
import {ROUTES} from "../../../routes";
import {createLink} from "@synaptix/ui";
import {PersonAvatar} from "../Person";
import {useInView} from "react-intersection-observer";
import JSONPretty from "react-json-pretty";
import "react-json-pretty/themes/monikai.css";

const pageSize = 15;

const classes = {
  personAvatar: {
    width: ({spacing}) => spacing(5),
    height: ({spacing}) => spacing(5)
  }
};
/**
 *
 */
export default function GlobalActivity({} = {}) {
  const {t} = useTranslation();
  const {data: {actions} = {}, loading, loadNextPage, hasNextPage} = usePaginatedQuery(gqlActivity, {
    pageSize,
    connectionPath: "actions",
    variables: {
      sortings: [{sortBy: "startedAtTime", isSortDescending: true}]
    }
  });

  const {ref, inView, entry} = useInView({
    threshold: 0
  });

  useEffect(() => {
    if (!loading && actions) {
      loadNextPage();
    }
  }, [inView]);

  return (
    <>
      <List>
        {buildActivity(actions?.edges || []).map((event) => {
          return (
            <Fragment key={event.id}>
              <ListItem>
                <ListItemAvatar>
                  <PersonAvatar person={event.from || {}} classSx={classes.personAvatar} />
                </ListItemAvatar>
                <ListItemText
                  primary={
                    <Typography component="span" variant="body2" color="textPrimary">
                      {dayjs(event.time).format("LLL")} - {renderEventContent({event, onlyFirstOne: true})}
                    </Typography>
                  }
                />
              </ListItem>
              {/*<JSONPretty data={event.snapshot} />*/}
              <Divider variant="inset" component="li" />
            </Fragment>
          );
        })}

        <Choose>
          <When condition={loading}>
            {[...Array(pageSize)].map((_, i) => (
              <Fragment key={i}>
                <ListItem>
                  <ListItemAvatar>
                    <Skeleton variant="circle" width={40} height={40} />
                  </ListItemAvatar>
                  <ListItemText
                    primary={<Skeleton variant="rect" width={210} />}
                    secondary={
                      <>
                        <div>
                          <Typography component="span" variant="body2" color="textPrimary">
                            <Skeleton variant="rect" width={210} />
                          </Typography>
                          {" — "}
                          <Skeleton variant="rect" width={400} />
                        </div>
                      </>
                    }
                    secondaryTypographyProps={{component: "div"}}
                  />
                </ListItem>
                <Divider variant="inset" component="li" />
              </Fragment>
            ))}
          </When>
          <When condition={hasNextPage}>
            <div ref={ref}>Chargement des suivants....</div>
          </When>
        </Choose>
      </List>
    </>
  );

  function renderEventContent({event, onlyFirstOne}) {
    let body;

    if (onlyFirstOne) {
      body = event.fragments[0].description;
    } else {
      body = (
        <ul>
          {event.fragments.map((fragment) => (
            <li key={fragment.id}>
              <div>{fragment.description}</div>
            </li>
          ))}
        </ul>
      );
    }

    return body;
  }

  function formatWord(word) {
    return snakeCase(word).toUpperCase();
  }

  function buildActivity(actions) {
    let lastAction;
    let mutatedActions = [...actions];
    const activity = Object.entries(mutatedActions).reduce((activity, [index, {node: action}]) => {
      if (lastAction !== action) {
        let snapedAction = {...action};

        let event = {
          id: action.id,
          time: action.startedAtTime,
          from: action.person || action.entities.edges.find(({node}) => node.__typename === "Person")?.node
        };

        snapedAction.snapshot = JSON.parse(action.snapshot);

        const nextAction = actions[parseInt(index) + 1]?.node;

        if (
          nextAction &&
          nextAction.startedAtTime === action.startedAtTime &&
          nextAction.person?.id === action.person?.id
        ) {
          lastAction = nextAction;
          snapedAction.snapshot = snapedAction.snapshot.concat(JSON.parse(nextAction.snapshot));
          snapedAction.entities = {edges: [...snapedAction.entities.edges, ...nextAction.entities.edges]};
        } else {
          lastAction = action;
        }

        event.fragments = getActionFragments(snapedAction);
        event.snapshot = snapedAction.snapshot;

        if (event.fragments.length > 0) {
          activity.push(event);
        }
      }

      return activity;
    }, []);

    return activity;
  }

  function getActionFragments(action) {
    let filterOnType = action.snapshot[0]?.id === "?node" && action.snapshot[0]?.type;
    let userCreation = !action.person && action.entities.edges.some(({node}) => node.__typename === "UserAccount");

    if (userCreation) {
      filterOnType = "Person";
    }

    return action.entities.edges.reduce((fragments, {node: entity}) => {
      let fragment = {
        id: `${action.id}_${entity.id}`
      };

      if ((!filterOnType || filterOnType === entity.__typename) && entity.label) {
        fragment.i18nKey = userCreation
          ? `ACTIVITY.ACTION.CREATION.USER_ACCOUNT`
          : `ACTIVITY.ACTION.${formatWord(action.__typename)}.${formatWord(entity.__typename)}`;
        fragment.description = getEntityFragmentDescription({entity, fragment, action});
        fragments.push(fragment);
      }

      return fragments;
    }, []);
  }

  function getEntityRoute(entity) {
    return {
      ...ROUTES,
      USER_GROUP: ROUTES.ADMIN_GROUP,
      FILE: ROUTES.RESOURCE
    }[formatWord(entity.__typename)];
  }

  function getEntityFragmentDescription({entity, fragment, action}) {
    const entityRoute = getEntityRoute(entity);
    const entityUrl = entityRoute && formatRoute(entityRoute, {id: entity.id});
    let i18nKey = fragment.i18nKey;
    let extraValues = {};
    let extraComponents = {};

    switch (entity.__typename) {
      case "ProjectContribution":
        if (entity.project) {
          extraValues.projectTitle = entity.project.title;
          extraComponents.project = createLink({
            to: formatRoute(ROUTES.PROJECT, {id: entity.project.id}),
            text: entity.label
          });
          i18nKey += "_WITH_PROJECT";
        }
        break;
    }

    return (
      <Trans
        i18nKey={i18nKey}
        values={{label: entity.label, person: action.person?.fullName, ...extraValues}}
        components={{
          person: action.person ? (
            createLink({to: formatRoute(ROUTES.PERSON, {id: action.person.id}), text: action.person.fullName})
          ) : (
            <></>
          ),
          entity: entityUrl ? createLink({to: entityUrl, text: entity.label}) : <></>,
          ...extraComponents
        }}
      />
    );
  }
}
