import {useState} from "react";
import {Dialog, DialogContent, DialogActions, Button} from "@mui/material";
import {useTranslation} from "react-i18next";
import {string, bool} from "prop-types";
import GlobalGraph from "./GlobalGraph";


export function GlobalGraphButton({
  entityId = [],
  variant = "outlined",
  color,
  size,
} = {}) {
  const {t} = useTranslation();
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <Button
        onClick={() => setDialogOpen(true)}
        variant={variant}
        color={color}
        size={size}>
        {t("ACTIONS.SEE_GRAPH")}
      </Button>

      <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)} fullScreen>
        <DialogContent>
          <GlobalGraph filterOnIds={[entityId]} />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setDialogOpen(false)} color="primary">
            {t("ACTIONS.CLOSE")}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

GlobalGraphButton.propTypes = {
  entityId: string.isRequired,
  variant: string,
  color: string,
  size: bool
};
