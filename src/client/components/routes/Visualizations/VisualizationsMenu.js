/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {useTranslation} from "react-i18next";
import {MenuItem, Box} from "@mui/material";
import {formatRoute} from "react-router-named-routes";
import {Link} from "react-router-dom";
import {ROUTES} from "../../../routes";

const classes = {
  fullWidth: {
    width: "100%"
  }
};

export function VisualizationsMenu() {
  const [show, setShow] = useState(false);

  let {t} = useTranslation();

  return (
    <Box className={classes.fullWidth}>
      <div onClick={() => setShow(!show)}>
        <Box className={classes.fullWidth}>{t("APP_BAR.MENU.VISUALIZATIONS.TITLE")}</Box>
      </div>
      {show && (
        <MenuItem component={Link} to={formatRoute(ROUTES.VISUALIZATIONS_GLOBAL_GRAPH)} key="VisualizationsMenu">
          {t("APP_BAR.MENU.VISUALIZATIONS.GLOBAL_GRAPH_ITEM")}
        </MenuItem>
      )}
    </Box>
  );
}
