import {ErrorBoundary} from "@synaptix/ui";

export default function GlobalGraph(props) {
  return (
    <ErrorBoundary>
      <GlobalGraphCode {...props} />
    </ErrorBoundary>
  );
}

import {useRef, useEffect, useState} from "react";
import {render} from "react-dom";
import {useQuery} from "@apollo/client";
import {useTranslation} from "react-i18next";
import {gqlGlobalGraph} from "./gql/GlobalGraph.gql";
import ForceGraph3D from "react-force-graph-3d";
import {CSS2DRenderer, CSS2DObject} from "./utils/ThreeCss";
import {
  Chip, Slider, FormControlLabel, Switch, Typography, Paper, Grid, TextField,
  Checkbox, Menu, MenuItem, Button, Autocomplete, Box
} from "@mui/material";

import {useTheme} from "@mui/material/styles";

import {AgentAvatar} from "../Explore/widgets/AgentAvatar";
import {ProjectAvatar} from "../Project/ProjectAvatar";
import clsx from "clsx";


const classes = {
  controls: (theme) => ({
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }),
  graphContainer: {
    position: "absolute",
    left: 0
  },
  node: {
    transition: "opacity 0.25s, box-shadow 0.25s"
  },
  defaultNode: (theme) => ({
    padding: theme.spacing(0.1),
    borderadius: "4px",
    backgroundColor: theme.palette.background.default,
    width: "10vw",
    textAlign: "center",
    border: "1px solid rgba(0,0,0,0.3)",
    borderRadius: 5
  }),
  conceptNode: {
    background: "white"
  },
  agentNode: {
    background: "white",
    // background: theme.palette.background.default,
    px: 0.5,
    py: 1,
    border: "1px solid rgba(0,0,0,0.3)",
    borderRadius: 5
  },
  projectNode: {
    width: "10vw",
    background: "white"
  },
  highlighted: (theme) => ({
    border: `1px solid ${theme.palette.secondary.main}`,
    cursor: "pointer",
    transition: "border 0.25s"
  }),
  faded: {
    opacity: 0.4,
    transition: "opacity 0.25s"
  },
  hovered: (theme) => ({
    boxShadow: theme.shadows[6],
    opacity: 1,
    transition: "opacity 0.25s, box-shadow 0.25s"
  })
};


/**
 *
 */
const extraRenderers = [new CSS2DRenderer()];

function GlobalGraphCode({filterOnIds} = {}) {

  const {t} = useTranslation();
  const theme = useTheme();
  const fgRef = useRef();
  const availableTypenames = {
    "Person": t("TYPENAME.PERSON"),
    "Organization": t("TYPENAME.ORGANIZATION"),
    "Concept": t("TYPENAME.CONCEPT"),
    "Project": t("TYPENAME.PROJECT"),
    "ProjectContribution": t("TYPENAME.PROJECT_CONTRIBUTION"),
  }
  const [visibleTypenamesMenuEl, setVisibleTypenamesMenuEl] = useState();
  const [visibleTypenames, setVisibleTypenames] = useState(new Set(["Person", "Organization", "Concept", "Project"]));
  const [graphData, setGraphData] = useState();
  const [linkDistance, setLinkDistance] = useState(150);
  const [projectContributionsVisible, setProjectContributionsVisible] = useState(false);
  const [canvasSize, setCanvasSize] = useState([window.innerWidth, window.innerHeight]);
  const [orbiting, setOrbiting] = useState(false);
  let orbitingDistance = 500;
  let orbitingInterval = useRef();

  const [highlightNodes, setHighlightNodes] = useState(new Map());
  const [highlightLinks, setHighlightLinks] = useState(new Map());
  const [selectedNode, setSelectedNode] = useState(null);
  const [hoverNode, setHoverNode] = useState(null);

  let gqlFilters = [];

  if (!filterOnIds) {
    gqlFilters.push("hasMention:*");
    gqlFilters.push("types: http://ns.mnemotix.com/ontologies/2019/8/generic-model/Project");
  } else {
    gqlFilters.push(`hasMention.id:${JSON.stringify(filterOnIds)}`);
  }

  const {data: {entities} = {}, loading, loadNextPage, hasNextPage} = useQuery(gqlGlobalGraph, {
    variables: {
      first: 500,
      filters: gqlFilters
    }
  });

  useEffect(() => {
    fgRef.current?.cameraPosition?.({z: orbitingDistance});

    window.addEventListener("resize", handeResize);

    return function cleanup() {
      window.removeEventListener("resize", handeResize);
    };
  }, []);

  useEffect(() => {
    toggleOrbiting();
  }, [orbiting]);

  useEffect(() => {
    if (entities) {
      function processEntity({entity, graphData}) {
        const node = graphData.nodes.get(entity.id) || {...entity, neighbors: new Map(), links: new Set()};
        graphData.nodes.set(entity.id, node);
        return node;
      }
      let linkId = 0;
      const {nodes, links} = entities.edges.reduce(
        (graphData, {node: entity}) => {
          const sourceNode = processEntity({entity, graphData});

          graphData.links = graphData.links.concat(
            entity.mentions.edges.reduce((links, {node: target}) => {
              const targetNode = processEntity({entity: target, graphData});
              const link = {
                id: linkId++,
                source: entity.id,
                target: target.id,
                sourceTypename: entity.__typename,
                targetTypename: target.__typename
              };

              targetNode.neighbors.set(entity.id, sourceNode);
              sourceNode.neighbors.set(targetNode.id, targetNode);
              targetNode.links.add(link);
              sourceNode.links.add(link);
              links.push(link);

              return links;
            }, [])
          );
          return graphData;
        },
        {
          nodes: new Map(),
          links: []
        }
      );
      setGraphData({
        nodes: Array.from(nodes.values()),
        links
      });
    }
  }, [entities, projectContributionsVisible]);

  useEffect(() => {
    if (fgRef.current?.d3Force) {
      fgRef.current.d3Force("link").distance(() => linkDistance);
      setTimeout(() => {
        fgRef.current.d3ReheatSimulation();
      }, 100);
    }
  }, [linkDistance]);

  useEffect(() => {
    setTimeout(() => {
      if (graphData?.nodes && filterOnIds?.length === 1) {
        const filteredNode = graphData?.nodes?.find(({id}) => id === filterOnIds[0]);
        if (filteredNode) {
          handleNodeClick(filteredNode);
        }
      }
    }, 500)
  }, [graphData?.nodes])
  return (
    <>
      <Paper sx={classes.controls}>
        <Grid container spacing={4}>
          <Grid item xs container alignItems={"center"} spacing={2}>
            <Grid item>
              <Typography id="continuous-slider" gutterBottom>
                Distance des liens
              </Typography>
            </Grid>

            <Grid item xs>
              <Slider
                value={linkDistance}
                onChange={(_, linkDistance) => setLinkDistance(linkDistance)}
                min={10}
                max={1000}
              />
            </Grid>
          </Grid>
          <Grid item>
            <Autocomplete
              options={(graphData?.nodes || [])
                .filter(node => visibleTypenames.has(node.__typename))
                .sort((a, b) => {
                  const typenameCompare = -b.__typename.localeCompare(a.__typename);
                  if (typenameCompare !== 0) {
                    return typenameCompare;
                  }
                  return -(b.label || "").localeCompare(a.label || "");
                })}
              groupBy={node => availableTypenames[node.__typename]}
              getOptionLabel={option => option.label || ""}
              style={{width: 300}}
              renderInput={params => (
                <TextField {...params} label={"Noeuds du graphe"} variant="outlined" size={"small"} />
              )}
              onChange={(e, node) => handleNodeClick(node, true)}
            />
          </Grid>
          <Grid item xs>
            <Button variant="outlined" onClick={(e) => setVisibleTypenamesMenuEl(e.currentTarget)}>
              Types de noeuds visibles
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={visibleTypenamesMenuEl}
              keepMounted
              open={Boolean(visibleTypenamesMenuEl)}
              onClose={() => setVisibleTypenamesMenuEl(null)}
            >
              {Object.entries(availableTypenames).map(([typename, label]) => (
                <MenuItem dense key={typename}>
                  <FormControlLabel
                    control={<Checkbox checked={visibleTypenames.has(typename)} onChange={() => {
                      let mutatedVisibleTypenames = new Set(visibleTypenames);

                      if (mutatedVisibleTypenames.has(typename)) {
                        mutatedVisibleTypenames.delete(typename);
                      } else {
                        mutatedVisibleTypenames.add(typename)
                      }
                      setVisibleTypenames(mutatedVisibleTypenames);
                    }} />}
                    label={label}
                  />
                </MenuItem>
              ))}
            </Menu>
          </Grid>
          <Grid item xs style={{textAlign: "right"}}>
            <FormControlLabel
              control={<Switch checked={orbiting} onChange={() => setOrbiting(!orbiting)} color="primary" />}
              label="Mettre en orbite"
            />
          </Grid>
        </Grid>
      </Paper>
      <Box sx={classes.graphContainer} style={{width: "100%"}}>
        <ForceGraph3D
          width={canvasSize[0]}
          height={canvasSize[1]}
          ref={fgRef}
          extraRenderers={extraRenderers}
          graphData={graphData}
          backgroundColor={theme.palette.background.default}
          nodeResolution={50}
          nodeRelSize={25}
          nodeVal={1}
          nodeOpacity={0.1}
          nodeColor={() => theme.palette.background.default}
          nodeThreeObject={renderNode}
          nodeThreeObjectExtend={true}
          nodeVisibility={node =>
            visibleTypenames.has(node.__typename) &&
            (!selectedNode || highlightNodes.has(node.id))
          }
          linkVisibility={link => {
            return (
              visibleTypenames.has(link.sourceTypename) &&
              visibleTypenames.has(link.targetTypename) &&
              (!selectedNode || highlightLinks.has(link.id))
            );
          }}
          linkOpacity={1}
          linkColor={link => (highlightLinks.has(link.id) && highlightLinks.get(link.id)?.color === "primary" ? theme.palette.secondary.main : "rgba(0,0,0, 0.2)")}
          linkWidth={link => (highlightLinks.has(link.id) && highlightLinks.get(link.id)?.color === "primary" ? 0.5 : 0.2)}
          linkResolution={20}
          onNodeClick={handleNodeClick}
          onNodeHover={handleNodeHover}
          onBackgroundClick={() => handleNodeClick(null)}
        />
      </Box>
    </>
  );

  function toggleOrbiting() {
    if (orbiting) {
      if (!orbitingInterval.current) {
        centerCameraOnNode({
          node: selectedNode
        });

        // camera orbit
        let angle = 0;
        orbitingInterval.current = setInterval(() => {
          centerCameraOnNode({
            node: selectedNode,
            angle
          });
          angle += Math.PI / 2000;
        }, 10);
      }
    } else {
      clearInterval(orbitingInterval.current);
      orbitingInterval.current = null;
    }
  }

  function centerCameraOnNode({node, duration, angle = 0}) {
    let position = fgRef.current.cameraPosition();

    if (node) {
      if (node?.x && node?.y && node?.z) {
        const distRatio = 1 + orbitingDistance / Math.hypot(node.x, node.y, node.z);
        position = {
          x: node.x * distRatio * Math.sin(angle),
          y: node.y * distRatio,
          z: node.z * distRatio * Math.cos(angle)
        };
      }

      fgRef.current.cameraPosition(
        position, // new position
        node, // lookAt ({ x, y, z })
        duration // ms transition duration
      );
    }

    return position;
  }

  function computeSelectionArounNode(node, depth = 0) {
    if (!highlightNodes.has(node.id)) {
      highlightNodes.set(node.id, {
        color: depth === 0 ? "primary" : "secondary"
      });
    }

    node.neighbors?.forEach(neighbor => {
      if (!highlightNodes.has(neighbor.id)) {
        highlightNodes.set(neighbor.id, {
          color: depth === 0 ? "primary" : "secondary"
        });
      }

      if (depth === 0) {
        computeSelectionArounNode(neighbor, 1);
      }
    });

    node.links?.forEach(link => highlightLinks.set(link.id, {
      color: depth === 0 ? "primary" : "secondary"
    }));
  }

  function handleNodeClick(node) {
    centerCameraOnNode({node, duration: 3000});

    highlightNodes.clear();
    highlightLinks.clear();

    if (node) {
      computeSelectionArounNode(node)
    }

    setSelectedNode(node || null);
    setHighlightNodes(highlightNodes);
    setHighlightLinks(highlightLinks);
  }

  function handleNodeHover(node) {
    setHoverNode(node);
  }

  function renderNode(node) {
    const nodeEl = document.createElement("div");
    const highlighted = highlightNodes.has(node.id) && highlightNodes.get(node.id)?.color === "primary";
    const faded = selectedNode && (!highlightNodes.has(node.id) || highlightNodes.get(node.id)?.color !== "primary");
    const hovered = hoverNode && hoverNode === node;
    const extraClassnames = {
      [classes.node]: true,
      [classes.highlighted]: highlighted,
      [classes.faded]: faded,
      [classes.hovered]: hovered
    };

    if (node.__typename === "Concept") {
      render(
        <Chip sx={[classes.conceptNode, extraClassnames]} label={node.prefLabel} variant={"outlined"} />,
        nodeEl
      );
    } else if (["Person", "Organization"].includes(node.__typename)) {
      render(<AgentAvatar sxClass={[classes.agentNode, extraClassnames]} agent={node} />, nodeEl);
    } else if (node.__typename === "Project") {
      render(
        <ProjectAvatar project={node} sxClass={[classes.projectNode, extraClassnames]} titleVariant={"body1"} />,
        nodeEl
      );
    } else {
      nodeEl.textContent = node.label || node.id;
      nodeEl.style.color = node.color;
      nodeEl.className = clsx(classes.defaultNode, extraClassnames);
    }

    return new CSS2DObject(nodeEl);
  }

  function handeResize() {
    setCanvasSize([window.innerWidth, window.innerHeight]);
  }
}
