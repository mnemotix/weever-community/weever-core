/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {gql} from "@apollo/client";
import {gqlEntityInListFragment} from "@synaptix/ui";

export const gqlProjectContributions = gql`
  query ProjectContributions($qs: String, $first: Int, $after: String, $sortings: [SortingInput], $filters: [String]) {
    projectContributionsCount(qs: $qs, filters: $filters)
    projectContributions(qs: $qs, first: $first, after: $after, sortings: $sortings, filters: $filters) {
      totalCount
      edges {
        node {
          id
          startDate
          endDate
          title
          description
          attachmentsCount
          repliesCount
          attachments(first: 5) {
            edges {
              node {
                id
                resource {
                  id
                  publicUrl
                  ... on File {
                    mime
                  }
                }
              }
            }
          }
          ...EntityInListFragment
        }
      }
      aggregations {
        name
        buckets {
          count
          key
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
  ${gqlEntityInListFragment}
`;

export const gqlProjectContributionsDates = gql`
  query ProjectContributionsDates($projectId: ID!, $sortings: [SortingInput]) {
    project(id: $projectId) {
      id
      projectContributionsCount
      projectContributions(sortings: $sortings) {
        edges {
          node {
            id
            startDate
            createdAt
          }
        }
      }
    }
  }
`;
