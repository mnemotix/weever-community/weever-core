/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {PageNotFound, LoadingSplashScreen} from "@synaptix/ui";
import {useParams, useHistory} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {useSnackbar} from "notistack";
import {useMutation, useLazyQuery} from "@apollo/client";
import {ROUTES} from "../../../routes";

import {formatRoute} from "react-router-named-routes";

import {projectContributionGql} from "./gql/ProjectContribution.gql";
import {gqlUpdateProjectContribution} from "./gql/UpdateProjectContribution.gql";
import {ProjectContributionDynamicForm} from "./ProjectContributionDynamicForm";
import {useEffect} from "react";
import {ProjetContributionBreadcrumb} from "./ProjetContributionBreadcrumb";
import {Grid} from "@mui/material";

export default function ProjectContributionEdit({id} = {}) {
  const history = useHistory();
  const {t} = useTranslation();
  const params = useParams();
  const {enqueueSnackbar} = useSnackbar();

  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const [getProjectContribution, {data: {projectContribution} = {}, loading, called} = {}] = useLazyQuery(
    projectContributionGql
  );
  const [mutateProjectContribution, {loading: saving}] = useMutation(gqlUpdateProjectContribution, {
    onCompleted() {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
      history.push(formatRoute(ROUTES.PROJECT_CONTRIBUTION, {id}));
    }
  });

  useEffect(() => {
    if (id) {
      getProjectContribution({
        variables: {
          id
        }
      });
    }
  }, [id]);

  if (loading || !called) {
    return <LoadingSplashScreen />;
  }

  if ((id && !projectContribution) || !projectContribution?.permissions?.update) {
    return <PageNotFound />;
  }

  return (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <ProjetContributionBreadcrumb projectContribution={projectContribution} />
      </Grid>
      <Grid item xs={12}>
        <ProjectContributionDynamicForm
          mutateFunction={mutateProjectContribution}
          saving={saving}
          projectContribution={projectContribution}
          fullForm
        />
      </Grid>
    </Grid>
  );
}
