/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {LoadingSplashScreen, createLink} from "@synaptix/ui";
import {useParams, useHistory} from "react-router-dom";
import {Typography, Breadcrumbs} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useSnackbar} from "notistack";
import {useQuery, useMutation} from "@apollo/client";
import {ROUTES} from "../../../routes";
import {formatRoute} from "react-router-named-routes";
import {gqlProject} from "../../../components/routes/Project/gql/Project.gql";
import {ProjectContributionDynamicForm} from "./ProjectContributionDynamicForm";
import {gqlCreateProjectContribution} from "./gql/CreateProjectContribution.gql";

const classes = {
  breadcrumbs: {
    marginBottom: 2
  }
};

export default function ProjectContributionCreate({ } = {}) {

  let history = useHistory();
  const {t} = useTranslation();
  let {id} = useParams();
  id = decodeURIComponent(id);
  let {enqueueSnackbar} = useSnackbar();

  const {data, loading} = useQuery(gqlProject, {
    variables: {id}
  });

  const [mutateProjectContribution, {loading: saving}] = useMutation(gqlCreateProjectContribution, {
    onCompleted(data) {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});

      if (data?.createProjectContribution?.createdObject?.id) {
        setTimeout(
          history.push(
            formatRoute(ROUTES.PROJECT_CONTRIBUTION, {id: data?.createProjectContribution?.createdObject?.id})
          ),
          2000 // tres vilain. sans timeout j'obtiens 'data.projectContribution?.project = null' dans ProjectContribution si rajoute des liens vers personne et organisation.
        );
      } else {
        history.push(formatRoute(ROUTES.PROJECT, {id}));
      }
    }
  });

  const saveForm = async (mutparam) => {
    if (mutparam?.variables?.input?.objectInput) {
      mutparam.variables.input.objectInput["projectInput"] = {
        id
      };
    }
    await mutateProjectContribution(mutparam);
  };

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <>
      <Breadcrumbs sx={classes.breadcrumbs}>
        {createLink({to: ROUTES.PROJECTS, text: t("BREADCRUMB.PROJECTS")})}
        {createLink({
          to: formatRoute(ROUTES.PROJECT, {id}),
          text: data?.project.title
        })}
        <Typography color="textPrimary"> {t("PROJECT_CONTRIBUTION.CREATE")}</Typography>
      </Breadcrumbs>
      <ProjectContributionDynamicForm projectContribution={data} fullForm mutateFunction={saveForm} saving={saving} />
    </>
  );
}
