/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {createLink, LoadingSplashScreen, ErrorBoundary} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {Chip, Box} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import {globalSx} from "../../../layouts/styles";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../../routes";
import {gqlProjectContributionInvolvements} from "./gql/ProjectContributionInvolvements.gql";

export function ProjectContributionInvolvements(props) {
  return (
    <ErrorBoundary>
      <ProjectContributionInvolvementsCode {...props} />
    </ErrorBoundary>
  );
}
function ProjectContributionInvolvementsCode({projectContributionId} = {}) {
  const {t} = useTranslation();


  const {data, loading, fetchMore} = useQuery(gqlProjectContributionInvolvements, {
    variables: {
      projectContributionId
    }
  });

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <Choose>
      <When condition={data?.projectContribution?.involvements.edges.length > 0}>
        {data?.projectContribution.involvements.edges.map(({node: involvement}) => (
          <Box component="span" sx={globalSx.commaAfter} key={involvement.agent?.id}>
            <If condition={involvement.agent.__typename === "Organization"}>
              {createLink({
                text: involvement.agent.name,
                to: formatRoute(ROUTES.ORGANIZATION, {
                  id: involvement.agent.id
                })
              })}
            </If>

            <If condition={involvement.agent.__typename === "Person"}>
              {createLink({
                text: involvement.agent.fullName,
                to: formatRoute(ROUTES.PERSON, {id: involvement.agent.id})
              })}
            </If>
          </Box>
        ))}
      </When>
      <Otherwise>
        <Box sx={globalSx.empty}>{t("PROJECT_CONTRIBUTION.NO_RELATED_INVOLVEMENTS")}</Box>
      </Otherwise>
    </Choose>
  );
}

export function ProjectContributionInvolvementsEdit(props) {
  return (
    <ErrorBoundary>
      <ProjectContributionInvolvementsEditCode {...props} />
    </ErrorBoundary>
  );
}
function ProjectContributionInvolvementsEditCode({projectContributionId} = {}) {
  const {t} = useTranslation();


  const {data, loading, fetchMore} = useQuery(gqlProjectContributionInvolvements, {
    variables: {
      projectContributionId
    }
  });

  const addNew = () => {
  };
  const deleteOrganization = (id) => {
  };

  const deletePerson = (id) => {
  };
  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <Box sx={globalSx.chipsContainer}>
      {data?.projectContribution.involvements.edges.map(({node: involvement}) => (
        <span key={involvement.agent?.id}>
          <If condition={involvement.agent.__typename === "Organization"}>
            {/*createLink({
                text: involvement.agent.name,
                to: formatRoute(ROUTES.ORGANIZATION, {
                  id: involvement.agent.id
                })
              })*/}
            <Chip label={involvement.title} onDelete={() => deleteOrganization(involvement.agent.id)} variant="outlined" />
          </If>

          <If condition={involvement.agent.__typename === "Person"}>
            {/*createLink({
                text: involvement.agent.fullName,
                to: formatRoute(ROUTES.PERSON, {id: involvement.agent.id})
              })*/}
            <Chip label={involvement.title} onDelete={() => deletePerson(involvement.agent.id)} variant="outlined" />
          </If>
        </span>
      ))}

      <span key={"add"}>
        <Chip icon={<AddIcon />} label={t("ACTIONS.ADD")} variant="outlined" onClick={addNew} />
      </span>
    </Box>
  );
}
