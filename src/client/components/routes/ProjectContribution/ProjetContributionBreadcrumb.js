import {formatRoute} from "react-router-named-routes";
import {Breadcrumbs, Box, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {createLink} from "@synaptix/ui";
import {ROUTES} from "../../../routes";
import {Fragment} from "react";

const classes = {
  projects: {
    "& > a": {
      display: "block"
    }
  }
};

/**
 *
 */
export function ProjetContributionBreadcrumb({projectContribution, children} = {}) {
  const {t} = useTranslation();

  return (
    <Breadcrumbs>
      {createLink({to: ROUTES.PROJECTS, text: t("BREADCRUMB.PROJECTS")})}
      <Box sx={classes.projects}>
        {(projectContribution?.projects.edges || []).map(({node: project}) => (
          <Fragment key={project.id}>{renderProjectLink({project})}</Fragment>
        ))}
      </Box>
      <Typography color="textPrimary">{projectContribution.title || t("PROJECT.CREATE_TITLE")}</Typography>
      {children}
    </Breadcrumbs>
  );

  function renderProjectLink({project}) {
    return createLink({
      to: formatRoute(ROUTES.PROJECT, {
        id: project.id
      }),
      text: project.title
    });
  }
}
