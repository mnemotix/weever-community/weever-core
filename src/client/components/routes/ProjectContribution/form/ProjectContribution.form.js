import {DynamicFormDefinition, MutationConfig} from "@synaptix/ui";
import {object, date, string} from "yup";
import {gqlProjectContributionFragment} from "../gql/ProjectContribution.gql";

export function getProjectContributionValidationSchema({t}) {
  return object().shape({
    title: string().required(t("FORM_ERRORS.FIELD_ERRORS.TITLE_REQUIRED")),
    startDate: date().nullable().required(t("FORM_ERRORS.FIELD_ERRORS.START_DATE_REQUIRED")),
    endDate: date().nullable().default(undefined),
  });
}

export const projectContributionFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["title", "description", "startDate", "endDate"],
    gqlFragment: gqlProjectContributionFragment,
    gqlFragmentName: "ProjectContributionFragment"
  }),
  postProcessInitialValues: (object) => ({
    ...object,
    startDate: object.startDate === "" ? null : object.startDate, // Replace "" by null to make DatePickerField working
    endDate: object.endDate === "" ? null : object.endDate // Replace "" by null to make DatePickerField working
  }),
  validationSchema: getProjectContributionValidationSchema
});
