/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect} from "react";
import {
  Resources,
  createLink,
  LoadingSplashScreen,
  RichTextViewer,
  useLoggedUser,
  Button,
  useResponsive,
  Taggings,
  EntityAccessPolicy,
  useFloatingButtonDialContext,
  FloatingButtonDial,
  TranslatableDisplay,
  PageNotFound
} from "@synaptix/ui";
import {matchPath, Route, Switch, useHistory, useParams} from "react-router-dom";
import {Box, Grid, Paper, Tab, Tabs, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../routes";
import {ProjectContributionInvolvements} from "./ProjectContributionInvolvements/ProjectContributionInvolvements";
import {projectContributionGql} from "./gql/ProjectContribution.gql";
import {ProjectContributionMemos} from "./ProjectContributionMemos/ProjectContributionMemos";
import {ProjetContributionBreadcrumb} from "./ProjetContributionBreadcrumb";
import {ImportDialogContainer} from "../Import/ImportDialog";
import {createGqlFilters} from "../../utilities/tools";
import ResourceEdit from "../Resource/ResourceEdit";
import {EntityActivity} from "../Activity/EntityActivity";

const classes = {
  headingSection: {
    marginBottom: 2
  },
  projectContributions: {},
  noDate: {
    color: "text.emptyHint"
  }
};

export default function ProjectContribution() {
  const {t} = useTranslation();
  const history = useHistory();
  const {isContributor, isEditor} = useLoggedUser();
  let {id} = useParams();
  id = decodeURIComponent(id);
  const {isDesktop} = useResponsive();

  const {contextFloatingButtonDialSetValues} = useFloatingButtonDialContext();
  useEffect(() => {
    if (!isDesktop) {
      contextFloatingButtonDialSetValues([
        {
          icon: "EditIcon",
          locked: !isContributor,
          link: formatRoute(ROUTES.PROJECT_CONTRIBUTION_EDIT, {id}),
          display: "ACTIONS.UPDATE"
        }
      ]);
    }
  }, [isContributor, isDesktop]);

  const tabs = [
    {
      label: t("PROJECT_CONTRIBUTION.TABS.RESOURCES"),
      route: ROUTES.PROJECT_CONTRIBUTION
    },
    {
      label: t("PROJECT_CONTRIBUTION.TABS.MEMOS"),
      route: ROUTES.PROJECT_CONTRIBUTION_MEMOS
    }
  ];

  const {data: {projectContribution} = {}, loading} = useQuery(projectContributionGql, {
    variables: {
      id
    }
  });

  function getTabValueFromHistory() {
    return (
      tabs.filter(({route}) =>
        matchPath(history.location.pathname, {
          path: route
        })
      ).length - 1
    );
  }

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (id && !projectContribution) {
    return <PageNotFound />;
  }

  const gqlFilters = createGqlFilters({filterOnProjectContribution: projectContribution});

  return (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <ProjetContributionBreadcrumb projectContribution={projectContribution} />
      </Grid>

      <Grid item xs={12} container columnSpacing={2} sx={classes.headingSection} alignItems="stretch">
        <Grid item xs={12} md={8}>
          <Grid container spacing={2} sx={classes.headingSection}>
            <Grid item xs={12}>
              <Typography variant="h5" gutterBottom>
                <TranslatableDisplay isTranslated={projectContribution.titleTranslated}>
                  {projectContribution.title}
                </TranslatableDisplay>
              </Typography>

              {projectContribution?.description && (
                <TranslatableDisplay isTranslated={projectContribution.descriptionTranslated}>
                  <RichTextViewer content={projectContribution.description} />
                </TranslatableDisplay>
              )}
            </Grid>

            <Grid item xs={12} md={6}>
              <Typography variant="subtitle1">{t("PROJECT_CONTRIBUTION.START_DATE")}</Typography>
              <Box sx={[!projectContribution.startDate && classes.noDate]}>
                {projectContribution.startDate
                  ? dayjs(projectContribution.startDate).format("LL")
                  : t("PROJECT_CONTRIBUTION.NO_DATE")}
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Typography variant="subtitle1">{t("PROJECT_CONTRIBUTION.END_DATE")}</Typography>
              <Box sx={[!projectContribution.endDate && classes.noDate]}>
                {projectContribution.endDate
                  ? dayjs(projectContribution.endDate).format("LL")
                  : t("PROJECT_CONTRIBUTION.NO_DATE")}
              </Box>
            </Grid>

            <Grid item xs={12} md={6}>
              <Typography variant="subtitle1">{t("PROJECT_CONTRIBUTION.INVOLVEMENTS")}</Typography>
              <ProjectContributionInvolvements projectContributionId={id} />
            </Grid>

            <Grid item xs={12} md={6}>
              <Typography variant="subtitle1">{t("ENTITY.TAGGINGS")}</Typography>
              <Taggings entityId={id} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} md={4} style={{alignSelf: "flex-end"}}>
          <Grid container spacing={2} sx={classes.headingSection}>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{t("ENTITY.ACCESS_POLICY")}</Typography>
              <EntityAccessPolicy entityId={id} />
            </Grid>

            <Grid item xs={12} md={8}>
              <div>
                {t("PROJECT_CONTRIBUTION.CREATED_AT_BY", {
                  date: dayjs(projectContribution.createdAt).format("LL")
                })}
              </div>
              <div>
                {createLink({
                  to: formatRoute(ROUTES.PERSON, {
                    id: projectContribution?.creatorPerson?.id
                  }),
                  text: projectContribution?.createdBy
                })}
              </div>
            </Grid>
            {isDesktop && (
              <Grid item xs={12} md={4} align="right">
                <Button
                  disabled={!projectContribution?.permissions?.grant}
                  to={formatRoute(ROUTES.PROJECT_CONTRIBUTION_EDIT, {id})}>
                  {t("ACTIONS.UPDATE")}
                </Button>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={12}>
        <Paper sx={classes.projectContributions}>
          <Tabs
            value={Math.max(getTabValueFromHistory(), 0)}
            onChange={(e, index) => {
              history.push(formatRoute(tabs[index].route, {id}));
            }}
            variant="fullWidth"
            indicatorColor="primary"
            textColor="primary"
            centered>
            {tabs.map(({route, label}, index) => (
              <Tab key={index} label={label} />
            ))}
          </Tabs>

          <Switch>
            <Route
              exact
              path={ROUTES.PROJECT_CONTRIBUTION_MEMOS}
              render={() => (
                <ProjectContributionMemos
                  projectContributionId={projectContribution.id}
                  creationEnabled={projectContribution?.permissions?.update}
                />
              )}
            />
            <Route
              render={() => (
                <>
                  <Resources
                    gqlFilters={gqlFilters}
                    removalEnabled={projectContribution?.permissions?.delete}
                    batchEditEnabled={projectContribution?.permissions?.update}
                    carouselEnabled={true}
                    dense={true}
                    generateItemRoute={(node) => formatRoute(ROUTES.RESOURCE, {id: node.id})}
                    projectColumnExcluded={true}
                    renderActions={() => (
                      <ImportDialogContainer
                        disabled={!projectContribution?.permissions?.update}
                        filterOnProject={{}}
                        filterOnProjectContribution={projectContribution}                        
                      />
                    )}
                    ComponentForEditResource={ResourceEdit}
                  />
                </>
              )}
            />
          </Switch>
        </Paper>
      </Grid>

      <If condition={projectContribution?.permissions?.grant}>
        <Grid item xs={12}>
          <EntityActivity entityId={id} />
        </Grid>
      </If>

      {!isDesktop && <FloatingButtonDial />}
    </Grid>
  );
}
