/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useTranslation} from "react-i18next";
import {Grid} from "@mui/material";

import {TranslatableField, DatePickerField, TextField, RichTextAreaField} from "@synaptix/ui";

/**
 * @return {*}
 * @constructor
 */
export function ProjectContributionFormContent({} = {}) {
  const {t} = useTranslation();
  return (
    <Grid container rowSpacing={3}>
      <Grid item xs={12}>
        <TranslatableField required FieldComponent={TextField} name="title" label={t("PROJECT.TITLE")} />
      </Grid>
      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="description" label={t("PROJECT.DESCRIPTION")} />
      </Grid>
      <Grid item xs={12}>
        <Grid container colSpacing={2}>
          <Grid item xs={12} md={6}>
            <DatePickerField name="startDate" required={true} label={t("PROJECT_CONTRIBUTION.START_DATE")} pickTime />
          </Grid>
          <Grid item xs={12} md={6}>
            <DatePickerField name="endDate" label={t("PROJECT_CONTRIBUTION.END_DATE")} pickTime />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
