/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";

import {ProjectContributionFormContent} from "./ProjectContributionFormContent";
import {DynamicForm, TaggingsField, AccessPolicyField} from "@synaptix/ui";

import {ProjectContributionInvolvementsEdit} from "./ProjectContributionInvolvements/ProjectContributionInvolvementsEdit";
import {projectContributionFormDefinition} from "./form/ProjectContribution.form";
import Typography from "@mui/material/Typography";

/**
 * @param {object} [projectContribution]
 * @param {boolean} fullForm
 * @param {function} [mutateFunction]
 * @param {boolean} [saving]
 * @param {object} [initialTouched]
 * @param {boolean} [validateOnMount]
 * @param {boolean} [asDialog]
 * @param {boolean} [dialogOpen]
 * @param {*} [dialogTitle]
 * @param {function} [onDialogClose]
 * @param {function} [formatMutationInput]
 * @return {*}
 */
export function ProjectContributionDynamicForm({
  projectContribution,
  fullForm,
  mutateFunction,
  saving,
  initialTouched,
  validateOnMount,
  asDialog = false,
  dialogOpen = false,
  dialogTitle,
  onDialogClose = () => {},
  formatMutationInput
} = {}) {
  const {t} = useTranslation();

  return (
    <DynamicForm
      object={projectContribution}
      formDefinition={projectContributionFormDefinition}
      mutateFunction={mutateFunction}
      saving={saving}
      initialTouched={initialTouched}
      validateOnMount={validateOnMount}
      asDialog={asDialog}
      dialogOpen={dialogOpen}
      dialogTitle={dialogTitle}
      onDialogClose={onDialogClose}
      formatMutationInput={formatMutationInput}>
      <Grid container rowSpacing={3}>
        <Grid item xs={12}>
          <ProjectContributionFormContent />
        </Grid>

        <If condition={fullForm}>
          <Grid item xs={12}>
            <AccessPolicyField entityId={projectContribution?.id} />
          </Grid>

          <Grid item xs={12}>
            <TaggingsField entityId={projectContribution?.id} />
          </Grid>

          <Grid item xs={12}>
            <Typography variant="subtitle1">{t("PROJECT_CONTRIBUTION.INVOLVEMENTS")}</Typography>

            <ProjectContributionInvolvementsEdit projectContributionId={projectContribution?.id} />
          </Grid>
        </If>
      </Grid>
    </DynamicForm>
  );
}
