/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  CollectionDataProvider,
  CollectionView,
  Button,
  useLoggedUser,
  useResponsive,
  gqlResources,
  ThumbResourcesWithCarousel,
  createLink,
  generateCollectionViewGenericColumns,
  EntityFormContentBatch,
  FloatingButton
} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import dayjs from "dayjs";
import {Link} from "react-router-dom";
import {formatRoute} from "react-router-named-routes";
import ResourceEdit from "../Resource/ResourceEdit";
import {Box, Avatar, Container} from "@mui/material";

import {ROUTES} from "../../../routes";
import {gqlProjectContributions} from "./gql/ProjectContributions.gql";
import {ProjectTimeline} from "../Explore/Project/ProjectTimeline/ProjectTimeline";
import {projectContributionFormDefinition} from "./form/ProjectContribution.form";
import {gqlBatchUpdateProjectContributions} from "./gql/BatchUpdateProjectContribution.gql";

export function generateColumns({gqlResources, gqlSortings, routes}) {
  const {isDesktop} = useResponsive();
  const {t} = useTranslation();
  const generatedCollectionViewGenericColumns = generateCollectionViewGenericColumns({t});

  let columns = [
    {
      name: "title",
      displayAsMainTitle: true,
      label: t("PROJECT.EVENTS.COLUMNS.TITLE"),
      options: {
        sort: true,
        customBodyRender: (title, {row: projectContribution}) => {
          return createLink({
            to: formatRoute(routes.PROJECT_CONTRIBUTION, {id: projectContribution.id}),
            text: title,
            sx: (theme) => ({
              textDecoration: "none",
              fontSize: theme.typography.pxToRem(16)
            })
          });
        },
        setCellProps: () => ({
          sx: (theme) => ({width: theme.spacing(8)})
        })
      }
    },
    {
      name: "attachments",
      label: t("PROJECT.EVENTS.COLUMNS.RESOURCES"),
      options: {
        customBodyRender: (title, {row: projectContribution}) => (
          <ThumbResourcesWithCarousel
            resources={projectContribution?.attachments?.edges}
            count={projectContribution.attachmentsCount}
            gqlQuery={gqlResources}
            filters={[`hasProjectContribution.id:${projectContribution.id}`]}
            sortings={gqlSortings}
            dataPath="resources"
            ComponentForEdit={ResourceEdit}
          />
        ),
        setCellProps: () => ({
          sx: {width: "25vw"}
        })
      }
    }
  ];

  if (isDesktop) {
    columns = [
      ...columns,

      {
        name: "repliesCount",
        label: t("PROJECT.EVENTS.COLUMNS.MEMOS"),
        options: {
          customBodyRender: (memoCount, {row: projectContribution}) => {
            if (!memoCount) {
              return null;
            }
            return (
              <Link to={formatRoute(routes.PROJECT_CONTRIBUTION_MEMOS, {id: projectContribution.id})}>
                <Avatar variant="rounded" sx={{color: "black", fontSize: "1.25rem"}}>
                  {memoCount}
                </Avatar>
              </Link>
            );
          }
        }
      },
      {
        name: "startDate",
        label: t("PROJECT.EVENTS.COLUMNS.START_DATE"),
        transformValue: (value) => {
          return value ? dayjs(value).format("L") : "";
        },
        options: {
          sort: true,
          setCellProps: () => ({
            sx: (theme) => ({width: theme.spacing(15)})
          })
        }
      },
      {
        name: "endDate",
        label: t("PROJECT.EVENTS.COLUMNS.END_DATE"),
        transformValue: (value) => (value ? dayjs(value).format("L") : ""),
        options: {
          sort: true,
          setCellProps: () => ({
            sx: (theme) => ({width: theme.spacing(15)})
          })
        }
      },
      ...generatedCollectionViewGenericColumns
    ];
  }

  return columns;
}

/**
 * Display the project contributions list of a given project
 *
 * @param {string} projectId
 * @param {string} [qs] query string
 * @param {boolean} [searchEnabled=true]
 * @param {boolean} [creationEnabled=false]
 * @param {boolean} [batchEditEnabled=false]
 * @param {boolean} [showFloatingButton] show or not the floating button in mobile mode,
 */
export function ProjectContributions({
  projectId,
  qs,
  searchEnabled = true,
  creationEnabled = false,
  batchEditEnabled = false,
  showFloatingButton = false
} = {}) {
  const {isEditor} = useLoggedUser();
  const {isDesktop} = useResponsive();
  const {t} = useTranslation();

  const gqlSortings = [{sortBy: "startDate", isSortDescending: true}];
  const columns = generateColumns({
    gqlResources,
    gqlSortings,
    routes: {
      PROJECT_CONTRIBUTION: ROUTES.PROJECT_CONTRIBUTION,
      PROJECT_CONTRIBUTION_MEMOS: ROUTES.PROJECT_CONTRIBUTION_MEMOS
    }
  });
  const gqlVariables = {filters: [`hasProject:${projectId}`]};
  const availableDisplayModes = {
    table: {default: true},
    timeline: {
      key: "timeline",
      renderComponent: ({qs} = {}) => {
        return (
          <Box
            sx={(theme) => ({
              background: theme.palette.background.default,
              paddingTop: theme.spacing(3),
              marginBottom: theme.spacing(-5)
            })}>
            <Container maxWidth={"lg"}>
              <ProjectTimeline
                projectId={projectId}
                gqlVariables={{qs}}
                gqlFilters={null}
                gqlSortings={[
                  {
                    sortBy: "startDate",
                    isSortDescending: true
                  }
                ]}
              />
            </Container>
          </Box>
        );
      }
    }
  };

  return (
    <CollectionDataProvider
      gqlConnectionPath={"projectContributions"}
      gqlQuery={gqlProjectContributions}
      gqlVariables={gqlVariables}
      gqlSortings={gqlSortings}
      qs={qs}
      searchEnabled={searchEnabled}>
      <CollectionView
        collectionKey={"ProjectContributionsList"}
        columns={columns}
        availableDisplayModes={availableDisplayModes}
        removalEnabled={isEditor && isDesktop}
        renderRightSideActions={() => {
          return isDesktop ? (
            <Button disabled={!creationEnabled} to={formatRoute(ROUTES.PROJECT_CONTRIBUTION_CREATE, {id: projectId})}>
              {t("PROJECT_CONTRIBUTION.CREATE")}
            </Button>
          ) : showFloatingButton ? (
            <FloatingButton
              locked={!isContributor}
              to={formatRoute(ROUTES.PROJECT_CONTRIBUTION_CREATE, {id: projectId})}
              name="AddIcon"
            />
          ) : null;
        }}
        batchEditEnabled={batchEditEnabled}
        batchEditButtonProps={{
          gqlUpdateMutation: gqlBatchUpdateProjectContributions,
          entityFormDefinition: projectContributionFormDefinition,
          entityFormContent: <EntityFormContentBatch formContentFragmentPrefix="ProjectContribution" />,
          title: ({entityTotalCount}) =>
            t("PROJECT_CONTRIBUTION.ACTIONS.BATCH_UPDATE.TITLE", {count: entityTotalCount}),
          confirmMessage: ({entityTotalCount}) =>
            t("PROJECT_CONTRIBUTION.ACTIONS.BATCH_UPDATE.CONFIRM", {count: entityTotalCount}),
          successMessage: ({entityTotalCount}) =>
            t("PROJECT_CONTRIBUTION.ACTIONS.BATCH_UPDATE.SUCCESS", {count: entityTotalCount})
        }}
      />
    </CollectionDataProvider>
  );
}
