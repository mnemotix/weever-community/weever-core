/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {AccessPolicyField, RichTextAreaField, TextField} from "@synaptix/ui";


/**
 * @return {*}
 * @constructor
 */
export function MemoFormContent({memo} = {}) {
  const {t} = useTranslation();
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <AccessPolicyField entityId={memo?.id} />
      </Grid>
      <Grid item xs={12}>
        <TextField name={"title"} label={t("PROJECT.TITLE")} />
      </Grid>
      <Grid item xs={12}>
        <RichTextAreaField
          label={t("PROJECT_CONTRIBUTION.MEMOS.COLUMNS.CONTENT")}
          name={"description"}
          editableClassSx={{height: "320px"}}
        />
      </Grid>
    </Grid>
  );
}
