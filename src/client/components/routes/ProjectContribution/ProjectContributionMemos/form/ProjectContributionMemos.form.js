import {DynamicFormDefinition, LinkInputDefinition, MutationConfig, entityAccessPolicyInputDefinition} from "@synaptix/ui";
import {memoFormDefinition} from "./Memo.form";
import {gqlProjectContributionFragment} from "../../gql/ProjectContribution.gql";

/**
 * @type {LinkInputDefinition}
 */
export const projectContributionMemoInputDefinition = new LinkInputDefinition({
  name: "replies",
  isPlural: true,
  inputName: "replyInputs",
  targetObjectFormDefinition: memoFormDefinition,
  forceUpdateTarget: false,
  nestedLinks: [
    entityAccessPolicyInputDefinition
  ]
});

export const projectContributionFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: [],
    gqlFragment: gqlProjectContributionFragment,
    gqlFragmentName: "ProjectContributionFragment"
  })
});