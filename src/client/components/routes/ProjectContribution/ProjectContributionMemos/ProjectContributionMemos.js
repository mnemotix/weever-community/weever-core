/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {
  LoadingSplashScreen, ErrorBoundary,
  PluralLinkField, RichTextViewer, useLoggedUser, DynamicForm
} from "@synaptix/ui";
import {useTranslation} from "react-i18next";
import {useSnackbar} from "notistack";
import {useLazyQuery, useMutation} from "@apollo/client";
import {ListItemText, ListItemAvatar, Typography} from "@mui/material";
import dayjs from "dayjs";
import {MemoFormContent} from "./MemoFormContent";
import {gqlUpdateProjectContribution} from "../gql/UpdateProjectContribution.gql";
import {gqlProjectContributionMemos} from "./gql/ProjectContributionMemos";
import {
  projectContributionMemoInputDefinition,
  projectContributionFormDefinition
} from "./form/ProjectContributionMemos.form";
import {PersonAvatar} from "../../Person/PersonAvatar";



export function ProjectContributionMemos({projectContributionId, creationEnabled} = {}) {
  return (
    <ErrorBoundary>
      <ProjectProjectContributionMemosEditCode
        projectContributionId={projectContributionId}
        creationEnabled={creationEnabled}
      />
    </ErrorBoundary>
  );
}

function ProjectProjectContributionMemosEditCode({projectContributionId, creationEnabled} = {}) {
  let {enqueueSnackbar} = useSnackbar();
  const {t} = useTranslation();
  const {user} = useLoggedUser();

  const [mutateProjectContribution, {loading: saving}] = useMutation(gqlUpdateProjectContribution, {
    onCompleted() {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
    }
  });
  const [getProjectContributionMemos, {data: {projectContribution} = {}, loading} = {}] = useLazyQuery(
    gqlProjectContributionMemos
  );

  useEffect(() => {
    if (projectContributionId) {
      getProjectContributionMemos({
        variables: {
          projectContributionId,
          sortings: [
            {
              sortBy: "createdAt"
            }
          ]
        }
      });
    }
  }, [projectContributionId]);

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <DynamicForm
      object={projectContribution}
      formDefinition={projectContributionFormDefinition}
      mutateFunction={mutateProjectContribution}
      saving={saving}
      onTheFly>
      <PluralLinkField
        data={projectContribution}
        linkInputDefinition={projectContributionMemoInputDefinition}
        creationEnabled={creationEnabled}
        isObjectEditable={memo => memo.permissions?.update}
        renderObjectContent={memo => {
          const creatorPerson = memo.creatorPerson || user;
          return (
            <>
              <ListItemAvatar>
                <PersonAvatar person={creatorPerson} />
              </ListItemAvatar>
              <ListItemText
                primary={
                  <>
                    <Typography component="span" variant="body1" display={"inline"} color="textPrimary">
                      {`${creatorPerson.firstName} ${creatorPerson.lastName}`}
                    </Typography>
                    {" - "}
                    <Typography component="span" variant="body2" display={"inline"} color="textSecondary">
                      {dayjs(memo?.createdAt).format("L")}
                    </Typography>
                    {" - "}
                    <Typography component="span" variant="overline" display={"inline"} color="secondary">
                      {memo.accessPolicy?.label || t("ENTITY.NO_ACCESS_POLICY")}
                    </Typography>
                  </>
                }
                secondary={
                  <>
                    <If condition={memo?.title}>
                      <Typography variant="subtitle1">{memo.title}</Typography>
                    </If>
                    <RichTextViewer content={memo?.description} />
                  </>
                }
                secondaryTypographyProps={{
                  component: "div"
                }}
              />
            </>
          );
        }}
        renderObjectForm={({object: memo}) => <MemoFormContent memo={memo} />}
        addButtonLabel={t("ACTIONS.ADD_MEMO")}
        fullWidth={true}
      />
    </DynamicForm>
  );
}
