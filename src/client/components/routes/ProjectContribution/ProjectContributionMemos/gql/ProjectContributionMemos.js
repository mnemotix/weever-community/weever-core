import {gql} from "@apollo/client";
import {gqlMemoFragment} from "./Memo.gql";
import {gqlEntityFragment} from "@synaptix/ui";

export const gqlProjectContributionMemosFragment = gql`
  fragment ProjectContributionMemosFragment on ProjectContribution {
    replies (qs: $qs, first: $first, after: $after, sortings: $sortings) {
      edges {
        node {
          ...MemoFragment
          ...EntityFragment
        }
      }
      pageInfo {
        startCursor
        endCursor
      }
    }
  }
  
  ${gqlMemoFragment}
  ${gqlEntityFragment}
`;

export const gqlProjectContributionMemos = gql`
  query ProjectContributionMemos($projectContributionId: ID! $qs: String, $first: Int, $after: String, $sortings: [SortingInput]) {
    projectContribution(id: $projectContributionId) {
      id
      ...ProjectContributionMemosFragment
    }
  }

  ${gqlProjectContributionMemosFragment}
`;
