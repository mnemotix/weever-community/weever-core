import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {
  TranslatableField, TextField, RichTextAreaField, AccessPolicyField,
  TaggingsField
} from "@synaptix/ui";

export function ResourceFormContent({resource} = {}) {
  const {t} = useTranslation();

  return (
    <Grid container item spacing={4}>
      <Grid item xs={12}>
        <TextField required={true} name="filename" label={t("RESOURCE.FILE_NAME")} />
      </Grid>
      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="description" label={t("RESOURCE.DESCRIPTION")} />
      </Grid>
      <Grid item xs={12}>
        <TaggingsField entityId={resource?.id} />
      </Grid>
      <Grid item xs={12}>
        <TextField name="credits" label={t("RESOURCE.CREDITS")} />
      </Grid>
      <Grid item xs={12}>
        <AccessPolicyField entityId={resource?.id} />
      </Grid>
    </Grid>
  );
}
