/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  createLink,
  Taggings,
  LoadingSplashScreen,
  EntityAccessPolicy,
  Button,
  FloatingButton,
  useLoggedUser,
  useResponsive,
  ResourceViewer,
  RichTextViewer,
  gqlResource,
  PageNotFound
} from "@synaptix/ui";
import {useParams} from "react-router-dom";
import {Grid, Box, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";
import numeraljs from "numeraljs";

import {ROUTES} from "../../../routes";
import {ResourceBreadcrumb} from "./ResourceBreadcrumb";
import {EntityActivity} from "../Activity/EntityActivity";

const classes = {
  resourceViewerContainer: {
    textAlign: "center"
  },
  grow: {
    height: "95%"
  },
  empty: {
    color: "text.emptyHint"
  },
  filename: {
    wordBreak: "break-all"
  }
};

export default function Resource(props) {
  const {t} = useTranslation();
  const {isContributor} = useLoggedUser();
  const {isDesktop} = useResponsive();

  let {id} = useParams();
  id = decodeURIComponent(id);

  const {data: {resource} = {}, loading} = useQuery(gqlResource, {
    variables: {
      id
    }
  });

  if (loading) {
    return <LoadingSplashScreen />;
  }

  if (id && !resource) {
    return <PageNotFound />;
  }

  // const headingExtension = useExtension("resourceHeading", {resourceId: id});
  return (
    <Grid container rowSpacing={4}>
      <Grid item xs={12}>
        <ResourceBreadcrumb resource={resource} />
      </Grid>

      <Grid item xs={12} container columnSpacing={4} alignItems="flex-start">
        <Grid item xs={12} md={6}>
          <Box sx={classes.resourceViewerContainer}>
            <ResourceViewer resource={resource} />
          </Box>
        </Grid>

        <Grid item xs={12} md={6}>
          <Grid container spacing={4} sx={classes.grow}>
            <Grid item xs={12} md={4}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.FILE_NAME")}
              </Typography>
              <Box sx={classes.filename}>{resource?.filename}</Box>
            </Grid>
            <Grid item xs={12} md={4}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.SIZE")}
              </Typography>
              <div>{numeraljs(resource?.size).format("0b")}</div>
            </Grid>
            <Grid item xs={12} md={12}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.MIME")}
              </Typography>
              <div>{resource?.mime}</div>
            </Grid>
            <Grid item xs={12} md={12}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.URL")}
              </Typography>
              <a href={resource?.publicUrl} target="_blank">
                {resource?.publicUrl}
              </a>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="subtitle1">{t("ENTITY.TAGGINGS")}</Typography>
              <Taggings entityId={id} />
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="subtitle1">{t("ENTITY.ACCESS_POLICY")}</Typography>
              <EntityAccessPolicy entityId={id} />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.DESCRIPTION")}
              </Typography>
              <Box sx={[!resource?.credits && classes.empty]}>
                <RichTextViewer content={resource?.description || t("RESOURCE.NO_DESCRIPTION")} />
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Typography variant="subtitle1" gutterBottom>
                {t("RESOURCE.CREDITS")}
              </Typography>

              <Box sx={[!resource?.credits && classes.empty]}>{resource?.credits || t("RESOURCE.NO_CREDITS")}</Box>
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={12} md={9}>
                  <div>
                    {t("RESOURCE.CREATED_AT_BY", {
                      date: dayjs(resource?.createdAt).format("LL")
                    })}

                    {createLink({
                      to: formatRoute(ROUTES.PERSON, {
                        id: resource?.creatorPerson?.id
                      }),
                      text: resource?.createdBy
                    })}
                  </div>
                </Grid>
                {isDesktop && (
                  <Grid item xs={12} md={3}>
                    <Button disabled={!resource?.permissions?.update} to={formatRoute(ROUTES.RESOURCE_EDIT, {id})}>
                      {t("ACTIONS.UPDATE")}
                    </Button>
                  </Grid>
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <If condition={resource?.permissions?.grant}>
          <Grid item xs={12}>
            <EntityActivity entityId={id} />
          </Grid>
        </If>
      </Grid>

      <If condition={resource?.permissions?.delete}>
        <Grid item xs={12}>
          <EntityActivity entityId={id} />
        </Grid>
      </If>

      {!isDesktop && (
        <FloatingButton locked={!isContributor} to={formatRoute(ROUTES.RESOURCE_EDIT, {id})} name="EditIcon" />
      )}
    </Grid>
  );
}
