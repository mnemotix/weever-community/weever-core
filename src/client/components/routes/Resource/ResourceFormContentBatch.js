import {Grid} from "@mui/material";
import {useTranslation} from "react-i18next";
import {TranslatableField, TextField, RichTextAreaField} from "@synaptix/ui";

export default function ResourceFormContentBatch({} = {}) {
  const {t} = useTranslation();

  return (
    <>
      <Grid item xs={12}>
        <TranslatableField FieldComponent={RichTextAreaField} name="description" label={t("RESOURCE.DESCRIPTION")} />
      </Grid>
      <Grid item xs={12}>
        <TextField name="credits" label={t("RESOURCE.CREDITS")} />
      </Grid>
    </>
  );
}
