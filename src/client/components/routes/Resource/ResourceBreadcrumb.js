import {formatRoute} from "react-router-named-routes";
import {Box, Breadcrumbs, Typography} from "@mui/material";
import {useTranslation} from "react-i18next";
import {createLink} from "@synaptix/ui";
import {ROUTES} from "../../../routes";
import {Fragment} from "react";

const classes = {
  projects: {
    "& > a": {
      display: "block"
    }
  }
}
/**
 *
 */
export function ResourceBreadcrumb({resource, classSx, children} = {}) {

  const {t} = useTranslation();

  const [projects, projectContributions] = (resource?.attachments.edges || []).reduce(
    ([projects, projectContributions], {node: attachment}) => {
      if (attachment.projectContribution) {
        projectContributions.push(attachment.projectContribution);
        (attachment.projectContribution.projects?.edges || []).map(({node: project}) => projects.push(project));
      }
      return [projects, projectContributions];
    },
    [[], []]
  );

  return (
    <Breadcrumbs sx={classSx}>
      <Choose>
        <When condition={projectContributions.length > 0}>
          {createLink({to: ROUTES.PROJECTS, text: t("BREADCRUMB.PROJECTS"), key: 1})}

          {projects.length > 0 &&
            <Box sx={classes.projects} key="projects">
              {projects.map((project) => (
                <Fragment key={project.id}>{renderProjectLink({project})}</Fragment>
              ))}
            </Box>
          }

          <Box sx={classes.projects} key={"projectContributions"}>
            {projectContributions.map((projectContribution) => {
              if (projectContribution?.id) {
                return (
                  <Fragment key={projectContribution.id}>{renderProjectContributionLink({projectContribution})}</Fragment>
                )
              }
            })}
          </Box>
        </When>

        <Otherwise>
          {createLink({
            to: ROUTES.RESOURCES,
            text: t("BREADCRUMB.RESOURCES")
          })}
        </Otherwise>
      </Choose>

      <Typography color="textPrimary">{resource?.filename}</Typography>
      {children}
    </Breadcrumbs>
  );

  function renderProjectLink({project}) {
    return createLink({
      to: formatRoute(ROUTES.PROJECT, {
        id: project.id
      }),
      text: project.title
    });
  }

  function renderProjectContributionLink({projectContribution}) {
    return createLink({
      to: formatRoute(ROUTES.PROJECT_CONTRIBUTION, {
        id: projectContribution.id
      }),
      text: projectContribution.title
    });
  }
}
