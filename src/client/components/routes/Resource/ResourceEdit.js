/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect} from "react";
import {RemoveEntityPanel, gqlResource, LoadingSplashScreen, PageNotFound, } from "@synaptix/ui";
import {useSnackbar} from "notistack";
import {useHistory, useParams} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {useMutation, useLazyQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";

import {ROUTES} from "../../../routes";
import {gqlUpdateResource} from "./gql/UpdateResource.gql";
import {ResourceDynamicForm} from "./ResourceDynamicForm";
import {ResourceBreadcrumb} from "./ResourceBreadcrumb";


const classes = { 
  breadcrumbs: {
    marginBottom: 2
  },
};

/**
 *
 * @param {string} id of resource to edit
 * @param {bool} breadcrumbDisabled, set to true to hide breadcrumb, for edit in slider for example
 * @param {bool} preventChangeRoute, set to true to avoid redirect after mutation success, for edit in slider for example
 */
export default function ResourceEdit({id, breadcrumbDisabled = false, preventChangeRoute = false} = {}) {
  let params = useParams(); 
  const {t} = useTranslation();
  const history = useHistory();
  let {enqueueSnackbar} = useSnackbar();

  if (!id && params.id) {
    id = decodeURIComponent(params.id);
  }

  const [getResource, {data: {resource} = {}, loading, called} = {}] = useLazyQuery(gqlResource);
  const [mutateResource, {loading: saving}] = useMutation(gqlUpdateResource, {
    onCompleted() {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});

      if (!preventChangeRoute && history) {
        history.push(formatRoute(ROUTES.RESOURCE, {id}));
      }
    }
  });

  useEffect(() => {
    if (id) {
      getResource({
        variables: {
          id
        }
      });
    }
  }, [id]);

  if (loading || !called) {
    return <LoadingSplashScreen />;
  }

  if ((id && !resource) || !resource?.permissions?.update) {
    return <PageNotFound />;
  }

  return (
    <>
      <If condition={!breadcrumbDisabled}>
        <ResourceBreadcrumb classSx={classes.breadcrumbs} resource={resource} />
      </If>

      <ResourceDynamicForm
        resource={resource}
        mutateFunction={mutateResource}
        saving={saving}
        renderExtraActions={renderExtraActions}
      />
    </>
  );

  function renderExtraActions() {
    if (resource?.permissions.delete) {
      return (
        <RemoveEntityPanel
          entity={resource}
          panelTitle={t("RESOURCE.ACTIONS.REMOVE.TITLE")}
          panelWarnMessage={t("RESOURCE.ACTIONS.REMOVE.WARN_MESSAGE")}
          confirmMessage={t("RESOURCE.ACTIONS.REMOVE.CONFIRM_MESSAGE", {resource: resource.filename})}
          removeButtonLabel={t("RESOURCE.ACTIONS.REMOVE.TITLE")}
          onSuccess={() => history.go(-2)}
          refetchQueriesAfterSuccess={["Resources"]}
        />
      );
    }
  }
}
