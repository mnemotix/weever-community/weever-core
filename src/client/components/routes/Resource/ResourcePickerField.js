/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useEffect, useState} from "react";
import {getImageThumbnail, Resources, useFormController} from "@synaptix/ui";
import {useField, useFormikContext} from "formik";
import {formatRoute} from "react-router-named-routes";
import {
  Box,
  Button as MuiButton,
  Dialog,
  DialogActions,
  DialogContent,
  Fab,
  FormControl,
  Grid,
  InputLabel
} from "@mui/material";
import RemoveIcon from "@mui/icons-material/DeleteOutline";
import {useTranslation} from "react-i18next";
import {ImportDialogContainer} from "../Import/ImportDialog";
import {ROUTES} from "../../../routes";
import ResourceEdit from "../Resource/ResourceEdit";

const classes = {
  root: {
    width: "100%",
    paddingTop: 3
  },
  previewContainer: {
    width: "100%"
  },
  changeButton: {
    marginRight: 2
  },
  removeButton: (theme) => ({
    position: "absolute",
    bottom: theme.spacing(1),
    right: theme.spacing(1)
  })
};

/**
 *  UNUSED
 *
 * @param {string} [uploadEndpoint]
 * @param {string} [label]
 * @param {boolean} [multiple=false]
 * @param {string}name
 * @param {string} toDeleteInputName
 * @param {object} props
 * @param {LinkInputDefinition} linkInputDefinition
 * @return {*}
 * @constructor
 */
export const ResourcePickerField = ({
  uploadEndpoint,
  label,
  multiple = false,
  name,
  toDeleteInputName,
  linkInputDefinition,
  ...props
} = {}) => {
  if (linkInputDefinition) {
    name = linkInputDefinition.name;
    toDeleteInputName = linkInputDefinition.deleteInputName;
  }

  const [field] = useField({name, ...props});
  const formikContext = useFormikContext();
  const formController = useFormController();
  const {t} = useTranslation();
  const [open, setOpen] = useState(false);
  const [selectedResources, setSelectedResources] = useState([]);
  const [resources, setResources] = useState([]);
  const [deletedResources, setDeletedResources] = useState([]);

  useEffect(() => {
    if (linkInputDefinition) {
      formController.alterMutationConfig({
        linkInputDefinitions: [linkInputDefinition]
      });
    }
  }, []);

  useEffect(() => {
    if (!!field.value) {
      setResources(getResourcesFromField());
    }
  }, []);

  //
  // Update formik field value
  //
  useEffect(() => {
    if (field.value) {
      const storedResources = getResourcesFromField();

      if (multiple) {
        if (resources.length !== storedResources.length) {
          formikContext.setFieldValue(name, formatResourceToField(resources));
        }
        if (deletedResources.length > 0) {
          formikContext.setFieldValue(
            toDeleteInputName,
            deletedResources.map(({id}) => id)
          );
        }
      } else {
        if (resources.length > 0) {
          formikContext.setFieldValue(name, {id: resources[0]?.id});
        } else {
          formikContext.setFieldValue(name, null);
        }
      }
    }
  }, [resources.length]);

  return (
    <FormControl sx={classes.root}>
      <InputLabel shrink={true}>{label}</InputLabel>
      <Grid container spacing={2}>
        <If condition={resources.length > 0}>
          <Grid item xs={12}>
            <Box sx={classes.previewContainer}>
              {resources.map((resource, index) => (
                <Box
                  key={resource?.id || index}
                  sx={(theme) => ({
                    position: "relative",
                    display: "inline-block",
                    width: theme.spacing(30),
                    height: theme.spacing(20),
                    marginRight: theme.spacing(1),
                    backgroundPosition: "center",
                    backgroundSize: "cover",
                    backgroundColor: theme.palette.grey[300],
                    backgroundImage: `url(${getImageThumbnail({imageUrl: resource.publicUrl, size: "small"})})`
                  })}>
                  <If condition={multiple}>
                    <Fab size={"small"} sx={classes.removeButton} onClick={() => handleRemoveImage(resource)}>
                      <RemoveIcon />
                    </Fab>
                  </If>
                </Box>
              ))}
            </Box>
          </Grid>
        </If>
        <Grid item xs={12}>
          <MuiButton size={"small"} variant="contained" onClick={() => setOpen(true)} sx={classes.changeButton}>
            {multiple ? t("ACTIONS.ADD_IMAGES") : t("ACTIONS.CHANGE_IMAGE")}
          </MuiButton>
          <If condition={!multiple}>
            <MuiButton size={"small"} variant="outlined" onClick={handleRemoveImage}>
              {t("ACTIONS.REMOVE_IMAGE")}
            </MuiButton>
          </If>
        </Grid>
      </Grid>

      <Dialog open={open} onClose={handleClose} maxWidth={"lg"} fullWidth={true}>
        <DialogContent>
          <Resources
            gqlFilters={[]}
            onSelectResources={handleSelectResources}
            removalEnabled={false}
            uploadEnabled={true}
            searchEnabled={true}
            generateItemRoute={(node) => formatRoute(ROUTES.RESOURCE, {id: node.id})}
            pageSize={8}
            dense={true}
            renderActions={() => (
              <ImportDialogContainer
                disabled={!projectContribution?.permissions?.update}
                filterOnProject={{}}
                filterOnProjectContribution={projectContribution}             
              />
            )}
            ComponentForEditResource={ResourceEdit}
          />
        </DialogContent>
        <DialogActions>
          <MuiButton onClick={handleClose}>{t("ACTIONS.CANCEL")}</MuiButton>
          <MuiButton onClick={handleProceed} disabled={selectedResources.length === 0}>
            {t("ACTIONS.PROCEED")}
          </MuiButton>
        </DialogActions>
      </Dialog>
    </FormControl>
  );

  function handleSelectResources(resources) {
    setSelectedResources(resources);
  }

  function handleRemoveImage(resource) {
    if (!multiple) {
      setResources([]);
    } else {
      setDeletedResources(
        [].concat(
          deletedResources,
          resources.splice(
            resources.findIndex(({id}) => resource.id === id),
            1
          )
        )
      );
      setResources([].concat(resources));
    }
  }

  async function handleProceed() {
    setResources([].concat(resources, selectedResources));
    handleClose();
  }

  function handleClose() {
    setOpen(false);
  }

  function getResourcesFromField() {
    if (typeof field.value === "string") {
      return [field.value];
    }

    if (field.value?.edges) {
      return field.value?.edges.map(({node}) => node);
    }

    if (field.value?.length) {
      return field.value;
    }

    return [];
  }

  function formatResourceToField(resources) {
    if (linkInputDefinition?.isPlural) {
      return {
        edges: resources.map((resource) => ({node: resource}))
      };
    } else {
      return resources.map(({id}) => ({id}));
    }
  }
};
