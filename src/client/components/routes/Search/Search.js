/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect, useState} from "react";
import {
  SearchBar,
  useResponsive,
  Resources,
  useLoggedUser,
  useExtensionFragment,
  useExtensionSettings,
  FragmentSwitch
} from "@synaptix/ui";
import {matchPath, Route, useHistory, useLocation} from "react-router-dom";

import Paper from "@mui/material/Paper";
import {formatRoute} from "react-router-named-routes";

import {useQuery} from "@apollo/client";
import URLSearchParams from "@ungap/url-search-params";
import loadable from "@loadable/component";
import DesktopCategories from "./DesktopCategories";
import MobileCategories from "./MobileCategories";
import {useTranslation} from "react-i18next";
import {ImportDialogContainer} from "../Import/ImportDialog";

const Projects = loadable(() => import("../Project/Projects"));
const Persons = loadable(() => import("../Person/Persons"));
const Organizations = loadable(() => import("../Organization/Organizations"));

import {ROUTES} from "../../../routes";
import {gqlSearch} from "./gql/Search.gql";

const LS_QS_KEY = "searchQs";

const DefaultTabsSettings = [
  {
    route: ROUTES.PROJECTS,
    generateLabel: ({t, data}) => t("SEARCH.TABS.PROJECTS") + (data?.projectsCount ? ` (${data.projectsCount})` : "")
  },
  {
    route: ROUTES.RESOURCES,
    generateLabel: ({t, data}) => t("SEARCH.TABS.RESOURCES") + (data?.resourcesCount ? ` (${data.resourcesCount})` : "")
  },
  {
    route: ROUTES.PERSONS,
    generateLabel: ({t, data}) => t("SEARCH.TABS.PERSONS") + (data?.personsCount ? ` (${data.personsCount})` : "")
  },
  {
    route: ROUTES.ORGANIZATIONS,
    generateLabel: ({t, data}) =>
      t("SEARCH.TABS.ORGANIZATIONS") + (data?.organizationsCount ? ` (${data.organizationsCount})` : "")
  }
];

/**
 * @return {*}
 * @constructor
 */
export default function Search({ } = {}) {
  const {t} = useTranslation();
  const [qs, setQs] = useState(localStorage.getItem(LS_QS_KEY) || "");
  const {data, loading} = useQuery(gqlSearch, {
    variables: {
      qs
    }
  });
  const {search, pathname} = useLocation();
  const params = new URLSearchParams(search);
  const {isDesktop} = useResponsive();
  const {isEditor} = useLoggedUser();

  useEffect(() => {
    if (qs !== params.get("qs")) {
      if (!!qs) {
        params.set("qs", qs);
      } else {
        params.delete("qs");
      }
      history.replace({search: params.toString()});
    }
  }, [qs, pathname]);

  const ExtensionsTabsSettings = useExtensionSettings("Search.SearchTabsSettings", true);

  const TabsSettings = [...DefaultTabsSettings, ...ExtensionsTabsSettings];

  useEffect(() => {
    let tab = getTabValueFromHistory();
    if (tab === -1) {
      history.push(TabsSettings[0].route);
    }
  });

  const history = useHistory();

  function getTabValueFromHistory() {
    return TabsSettings.findIndex(({route}) =>
      matchPath(history.location.pathname, {
        path: route
      })
    );
  }

  const ExtensionsTabs = useExtensionFragment("Search.Tabs", {qs}, true);
  const ExtensionsRoutes = useExtensionFragment("Search.Routes", {qs});

  return (
    <>
      <SearchBar
        value={qs}
        onRequestSearch={handleRequestSearch}
        onChange={handleRequestSearch}
        onCancelSearch={handleCancelSearch}
        loading={loading}
        placeholder={t("REMOTE_TABLE.TOOLBAR.SEARCH")}
        sx={{height: "48px"}}
      />
      {renderTabs()}

      <Paper sx={(theme) => ({marginTop: theme.spacing(4)})}>
        <FragmentSwitch>
          <Route path={ROUTES.PROJECTS} render={() => <Projects qs={qs} />} />
          <Route
            path={ROUTES.RESOURCES}
            render={() => (
              <Resources
                removalEnabled={false}
                uploadEnabled={true}
                qs={qs}
                gqlFilters={[]}
                batchEditEnabled={isEditor}
                generateItemRoute={(node) => formatRoute(ROUTES.RESOURCE, {id: node?.id})}
                renderActions={() => (
                  <>
                    <ImportDialogContainer
                      disabled={false}
                      filterOnProject={{}}
                      filterOnProjectContribution={{}}
                    />
                  </>
                )}
              />
            )}
          />
          <Route path={ROUTES.PERSONS} render={() => <Persons qs={qs} />} />
          <Route path={ROUTES.ORGANIZATIONS} render={() => <Organizations qs={qs} />} />
          {ExtensionsRoutes}
        </FragmentSwitch>
      </Paper>
    </>
  );

  function renderTabs() {
    if (isDesktop) {
      return (
        <Paper sx={(theme) => ({marginTop: theme.spacing(4)})}>
          <DesktopCategories
            value={Math.max(0, getTabValueFromHistory())}
            onClick={(route) => history.push(route)}
            DefaultTabsSettings={DefaultTabsSettings}
            ExtensionsTabs={ExtensionsTabs}
            ExtensionsTabsSettings={ExtensionsTabsSettings}
            data={data}
          />
        </Paper>
      );
    } else {
      return (
        <Paper sx={(theme) => ({marginTop: theme.spacing(1)})}>
          <MobileCategories
            value={Math.max(0, getTabValueFromHistory())}
            onClick={(route) => history.push(route)}
            TabsSettings={TabsSettings}
            data={data}
          />
        </Paper>
      );
    }
  }

  /**
   * In this callback called after a SearchBar update, we need to :
   *  - Update local state
   *  - Save this state in local storage to restore it after a goback routing
   *
   * @param qs
   */
  function handleRequestSearch(qs = "") {
    setQs(qs);
    localStorage.setItem(LS_QS_KEY, qs);
  }

  /**
   * In this callback called after a SearchBar cancel, we need to :
   */
  function handleCancelSearch() {
    setQs("");
    localStorage.removeItem(LS_QS_KEY);
  }
}
