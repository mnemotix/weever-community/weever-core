import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import {useTranslation} from "react-i18next";

/***
 * Show tabs for desktop display
 */
export default function DesktopCategories({
  DefaultTabsSettings,
  ExtensionsTabs,
  ExtensionsTabsSettings,
  value,
  onClick,
  data
}) {
  const {t} = useTranslation();

  return (
    // orientation="vertical"
    <Tabs value={value} variant="fullWidth" indicatorColor="primary" textColor="primary" centered>
      {DefaultTabsSettings.map(({route, generateLabel}, key) => (
        <Tab key={key} value={key} label={generateLabel({t, data})} route={route} onClick={() => onClick(route)} />
      ))}
      {ExtensionsTabs}
      {/*
       * Ugly hack to circumvent MaterialUI Tabs component limitation that doesn't accept fragment children.
       * @see https://github.com/mui-org/material-ui/issues/10932 tagged as "wontfix".
       *
       * The workaround here is to force ExtensionsTabs to be an array of logic React.Component that returns <Tab /> array.
       * But doing this won't be sufficient. Clicking on last tab would procude the following error :
       *
       * "Material-UI: The value provided to the Tabs component is invalid.". None of the Tabs' children match with `...`.
       *
       * The reason is that Material UI only relies to Tabs DOM children count. The following line mocks that.
       */}
      {ExtensionsTabsSettings.map(({ }, key) => (
        <Tab key={key} style={{display: "none"}} />
      ))}
    </Tabs>
  );
}
