import {useState} from "react";

import {Button as MuiButton, Menu, MenuItem} from "@mui/material";
import {useTranslation} from "react-i18next";

/**
 * Show categories with menu to change current value for mobile display
 */
export default function MobileCategories({TabsSettings, value, onClick, data}) {
  const {t} = useTranslation();
  const [anchorEl, setAnchorEl] = useState(null);

  return (
    <div style={{flex: 1}}>
      <MuiButton color="inherit" onClick={(e) => setAnchorEl(e.currentTarget)} style={{flex: 1}}>
        {TabsSettings[value].generateLabel({t, data})}
      </MuiButton>

      <Menu id="menu-appbarlang" anchorEl={anchorEl} keepMounted open={!!anchorEl} onClose={() => setAnchorEl(null)}>
        {TabsSettings.map(({route, generateLabel}, key) => (
          <MenuItem
            key={key}
            onClick={() => {
              setAnchorEl(null);
              onClick(route);
            }}>
            {generateLabel({t, data})}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}
