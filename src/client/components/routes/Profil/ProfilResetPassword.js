/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {Copyright, TextField} from "@synaptix/ui";
import Avatar from "@mui/material/Avatar";
import {Grid, Box, Button as MuiButton, CircularProgress, Container, Typography} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import {Form, Formik} from "formik";
import {useTranslation} from "react-i18next";
import {gql, useMutation} from "@apollo/client";
import {useSnackbar} from "notistack";
import * as Yup from "yup";

const classes = {
  paper: {
    marginTop: 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: 1,
    backgroundColor: "secondary.main"
  },
  form: {
    width: "100%",
    marginTop: 1
  },
  submit: (theme) => ({
    margin: theme.spacing(3, 0, 2)
  }),
  error: (theme) => ({
    margin: theme.spacing(1, 0, 1),
    color: theme.palette.error.dark
  })
};

function formikValidationSchema(t) {
  return Yup.object().shape({
    email: Yup.string()
      .email(t("FORM_ERRORS.FIELD_ERRORS.INVALID_EMAIL"))
      .required(t("FORM_ERRORS.FIELD_ERRORS.EMAIL_REQUIRED")),
  });
}


export const gqlResetUserAccountPasswordByMail = gql`
  mutation ResetUserAccountPasswordByMail($input: ResetUserAccountPasswordByMailInput!) {
    resetUserAccountPasswordByMail(input: $input) {
      success
    }
  }
`;

export default function ProfilResetPassword() {

  const {t} = useTranslation();
  let {enqueueSnackbar} = useSnackbar();

  const [resetUserAccount, {loading: savingMutation}] = useMutation(gqlResetUserAccountPasswordByMail, {
    onCompleted() {
      enqueueSnackbar(t("RESET_PASSWORD.CONFIRMATION"), {variant: "success"});
    }
  });

  const submitForm = async ({email}) => {
    await resetUserAccount({
      variables: {
        input: {
          email: email
        }
      }
    });
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box sx={classes.paper}>
        <Avatar sx={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {t("SIGN_IN.PASSWORD_FORGOTTEN")}
        </Typography>
        <Formik
          initialValues={{
            email: localStorage.getItem("rememberMe") || ""
          }}
          validationSchema={formikValidationSchema(t)}
          validateOnChange={true}
          validateOnBlur={true}
          onSubmit={submitForm}>
          {({isSubmitting}) => {
            return (
              <Form style={classes.form} noValidate>
                <Grid container spacing={2}>
                  <Grid item xs={12}></Grid>
                  <Grid item xs={12}>
                    <TextField
                      margin="normal"
                      required
                      label={t("SIGN_IN.EMAIL")}
                      name="email"
                      autoComplete="email"
                      autoFocus
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <MuiButton type="submit" fullWidth variant="contained" color="primary" sx={classes.submit}>
                      {t("RESET_PASSWORD.CONFIRM")}
                    </MuiButton>
                  </Grid>
                  {isSubmitting && <CircularProgress size={24} sx={classes.buttonProgress} />}
                </Grid>
              </Form>
            );
          }}
        </Formik>
      </Box>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
