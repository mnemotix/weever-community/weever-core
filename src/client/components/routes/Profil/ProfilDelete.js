/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useState} from "react";
import {Box, Button as MuiButton, Dialog, DialogActions, DialogContent, TextField, DialogTitle} from "@mui/material";

import {useTranslation} from "react-i18next";
import DeleteIcon from "@mui/icons-material/Delete";
import {gql, useMutation} from "@apollo/client";
import {useSnackbar} from "notistack";

const gqlUnregisterUserAccount = gql`
  mutation UnregisterUserAccount($input: UnregisterUserAccountInput!) {
    unregisterUserAccount(input: $input) {
      success
    }
  }
`;

export function ConfirmDeleteDialog({open, setOpen, onConfirm, title, buttonLabel}) {
  const [checkInput, setCheckInput] = useState("");
  const [disableButton, setDisableButton] = useState(true);
  const {t} = useTranslation();
  const checkString = t("PROFIL.DELETE_ACCOUNT.CHECKSTRING") || "DELETE";

  function handleChange(event) {
    setCheckInput(event.target.value);
    if (event.target.value === checkString) {
      setDisableButton(false);
    }
  }

  return (
    <Dialog open={open} onClose={() => setOpen(false)} aria-labelledby="confirm-dialog">
      <DialogTitle id="confirm-dialog">{title}</DialogTitle>
      <DialogContent>
        <Box sx={{padding: 10}}>
          <b>{t("PROFIL.DELETE_ACCOUNT.WARN_MESSAGE")}</b>
          <br />
          <br />
          {t("PROFIL.DELETE_ACCOUNT.PLEASE_INPUT_A")} <b>{checkString}</b> {t("PROFIL.DELETE_ACCOUNT.PLEASE_INPUT_B")}
          <br />
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={false}
            fullWidth
            value={checkInput}
            onChange={handleChange}
          />
        </Box>
      </DialogContent>
      <DialogActions>
        <MuiButton variant="outlined" onClick={() => setOpen(false)} color="secondary">
          {t("ACTIONS.CANCEL")}
        </MuiButton>

        <MuiButton
          disabled={disableButton}
          variant="contained"
          onClick={() => {
            setOpen(false);
            onConfirm();
          }}
          color={"primary"}
        >
          {buttonLabel}
        </MuiButton>
      </DialogActions>
    </Dialog>
  );
}

export function ProfilDelete({userId, checkString}) {
  const {t} = useTranslation();
  let {enqueueSnackbar} = useSnackbar();
  const [confirmOpen, setConfirmOpen] = useState(false);

  const [changeAccountPassword] = useMutation(gqlUnregisterUserAccount, {
    onCompleted() {
      enqueueSnackbar(t("PROFIL.DELETE_ACCOUNT.DONE_CONFIRMATION"), {variant: "success"});
      history.push(formatRoute(ROUTES.INDEX));
    }
  });

  const deleteAccount = async () => {
    await changeAccountPassword({
      variables: {
        input: {userId, permanent: false}
      }
    });
  };

  return (
    <div>
      <ConfirmDeleteDialog
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={deleteAccount}
        title={t("PROFIL.DELETE_ACCOUNT.DELETE_DEFINITELY")}
        buttonLabel={t("PROFIL.DELETE_ACCOUNT.I_UNDERSTAND")}
      />

      <MuiButton onClick={() => setConfirmOpen(true)} variant="outlined" style={{backgroundColor: "orange"}}>
        <DeleteIcon /> {t("PROFIL.DELETE_ACCOUNT.DELETE_DEFINITELY")}
      </MuiButton>
    </div>
  );
}
