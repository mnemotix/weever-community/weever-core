/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {useState} from "react";
import {gqlMeQuery, LoadingSplashScreen} from "@synaptix/ui";
import {Box, Typography, Breadcrumbs, Accordion, AccordionDetails, AccordionSummary} from "@mui/material";

import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {useTranslation} from "react-i18next";
import {useQuery} from "@apollo/client";
import {ProfilChangePassword} from "./ProfilChangePassword";
import {ProfilDelete} from "./ProfilDelete";
import PersonEdit from "../../../components/routes/Person/PersonEdit.js";


const classes = {
  breadcrumbs: {
    marginBottom: 2
  },
  paddingTop30: {
    paddingTop: "30px"
  },
  heading: (theme) => ({
    fontSize: theme.typography.pxToRem(15),
    //flexBasis: "15%",
    flexShrink: 0,
    marginRight: 5
  }),
  secondaryHeading: (theme) => ({
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  })
};

export default function ProfilEdit({ } = {}) {

  const {t} = useTranslation();

  const [expanded, setExpanded] = useState(false);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const {data, loading} = useQuery(gqlMeQuery, {});
  const myId = (!loading && data?.me?.id) || false;
  const myUserId = (!loading && data?.me?.userAccount?.userId) || false;

  return loading ? (
    <LoadingSplashScreen />
  ) : (
    <>
      <Breadcrumbs sx={classes.breadcrumbs}>
        <Typography color="textPrimary">{t("PROFIL.EDIT_MY_PROFIL")}</Typography>
      </Breadcrumbs>

      <Box sx={classes.paddingTop30}>
        <Accordion expanded={expanded === "panel1"} onChange={handleChange("panel1")}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1bh-content" id="panel1bh-header">
            <Typography sx={classes.heading}>{t("ACTIONS.EDIT")}</Typography>
            <Typography sx={classes.secondaryHeading}>{t("PROFIL.MY_PERSONAL_INFO")}</Typography>
          </AccordionSummary>
          <AccordionDetails >
            <PersonEdit id={myId} hideBreadcrumb={true} disableRedirect={true} />
          </AccordionDetails>
        </Accordion>
        <Accordion expanded={expanded === "panel2"} onChange={handleChange("panel2")}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel2bh-content" id="panel2bh-header">
            <Typography sx={classes.heading}>{t("ACTIONS.CHANGE")}</Typography>
            <Typography sx={classes.secondaryHeading}>{t("PROFIL.MY_PASSWORD")}</Typography>
          </AccordionSummary>
          <AccordionDetails >
            <ProfilChangePassword />
          </AccordionDetails>
        </Accordion>
        <Accordion expanded={expanded === "panel3"} onChange={handleChange("panel3")}>
          <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel3bh-content" id="panel3bh-header">
            <Typography sx={classes.heading}>{t("ACTIONS.DELETE")}</Typography>
            <Typography sx={classes.secondaryHeading}>{t("PROFIL.MY_ACCOUNT")}</Typography>
          </AccordionSummary>
          <AccordionDetails >
            <ProfilDelete userId={myUserId} />
          </AccordionDetails>
        </Accordion>
      </Box>
    </>
  );
}
