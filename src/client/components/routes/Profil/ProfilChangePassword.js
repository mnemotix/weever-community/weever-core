/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {FormSubmitButtonWithTooltip, TextField} from "@synaptix/ui";
import {Form, Formik} from "formik";
import {useTranslation} from "react-i18next";
import {gql, useMutation} from "@apollo/client";
import {Grid} from "@mui/material";
import {useSnackbar} from "notistack";
import * as Yup from "yup";

function formikValidationSchema(t) {
  return Yup.object().shape({
    oldPassword: Yup.string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    newPassword: Yup.string().min(6, t("FORM_ERRORS.FIELD_ERRORS.TOO_SHORTS")).required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    newPasswordConfirm: Yup.string().min(6, t("FORM_ERRORS.FIELD_ERRORS.TOO_SHORTS")).when("newPassword", {
      is: (val) => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf([Yup.ref("newPassword")], t("FORM_ERRORS.FIELD_ERRORS.PASSWORDS_DO_NOT_MATCH"))
    }).required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED"))
  })
};

const gqlResetUserAccountPassword = gql`
  mutation ResetUserAccountPassword($input: ResetUserAccountPasswordInput!) {
    resetUserAccountPassword(input: $input) {
      success
    }
  }
`;


export function ProfilChangePassword() {

  const {t} = useTranslation();
  let {enqueueSnackbar} = useSnackbar();

  const [changeAccountPassword, {loading: savingMutation}] = useMutation(gqlResetUserAccountPassword, {
    onCompleted() {
      enqueueSnackbar(t("PROFIL.CHANGE_PASSWORD.DONE_CONFIRMATION"), {variant: "success"});
    }
  });

  const submitForm = async ({oldPassword, newPassword, newPasswordConfirm}) => {
    await changeAccountPassword({
      variables: {
        input: {
          oldPassword,
          newPassword,
          newPasswordConfirm
        }
      }
    });
  };

  return (
    <Formik
      initialValues={{
        oldPassword: "",
        newPassword: "",
        newPasswordConfirm: ""
      }}
      validationSchema={formikValidationSchema(t)}
      validateOnChange={true}
      validateOnBlur={true}
      onSubmit={submitForm}>
      {({errors, touched, isValid, dirty, ...props}) => {
        return (
          <Form style={{width: "100%", mt: 1}} noValidate>
            <Grid container spacing={4}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="oldPassword"
                  label={t("PROFIL.CHANGE_PASSWORD.OLD_PASSWORD")}
                  type="password"
                  id="oldPassword"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="newPassword"
                  label={t("PROFIL.CHANGE_PASSWORD.NEW_PASSWORD")}
                  type="password"
                  id="newPassword"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="newPasswordConfirm"
                  label={t("PROFIL.CHANGE_PASSWORD.CONFIRM_NEW_PASSWORD")}
                  type="password"
                  id="newPasswordConfirm"
                />
              </Grid>
              <Grid item xs={12}>
                <FormSubmitButtonWithTooltip saving={savingMutation} />
              </Grid>
            </Grid>


          </Form>
        );
      }}
    </Formik>
  );
}


