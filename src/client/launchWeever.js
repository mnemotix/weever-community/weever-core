/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {Suspense} from "react";
import ReactDOM from "react-dom";
import {BrowserRouter} from "react-router-dom";
import {ApolloProvider} from "@apollo/client";
import loadable from "@loadable/component";
import {SnackbarProvider, useSnackbar} from "notistack";

import {i18nInitialize} from "../locales/i18nInitialize";
import {SplashScreen, ErrorBoundary, getApolloClient} from "@synaptix/ui";
import {theme as defaultTheme} from "./theme";
import {CssBaseline} from "@mui/material";
import {ThemeProvider, StyledEngineProvider} from "@mui/material/styles";
import {coreExtension} from "./components/CoreExtension";
import merge from "lodash/merge";
import {createTheme} from "@mui/material/styles";

const Weever = loadable(() => import(/* webpackChunkName: "Weever" */ "./Weever"));

let reactRootElement = document.getElementById("react-root");

const ApolloContainer = ({i18n, children, possibleTypes}) => {
  const {enqueueSnackbar} = useSnackbar();
  return <ApolloProvider client={getApolloClient({i18n, enqueueSnackbar, possibleTypes})}>{children}</ApolloProvider>;
};

/**
 * @param {object} possibleTypes
 * @param {[Extension]} [extensions]
 * @param {Theme} [theme]
 * @param {[MentionDefinition]} [extraMentionsDefinitions]
 * @param {Component} [AppBarTitleComponent]
 * @param {Component} [SplashScreenComponent]
 */
export function launchWeever({
  possibleTypes,
  extensions = [],
  theme = {},
  extraMentionsDefinitions = [],
  AppBarTitleComponent,
  SplashScreenComponent = SplashScreen
} = {}) {
  extensions.unshift(coreExtension);

  i18nInitialize().then((i18n) => {
    ReactDOM.render(
      <BrowserRouter>
        <StyledEngineProvider injectFirst>
          <ThemeProvider theme={createTheme(merge(defaultTheme, theme))}>
            <CssBaseline />
            <SnackbarProvider maxSnack={3}>
              <ApolloContainer i18n={i18n} possibleTypes={possibleTypes}>
                <Suspense fallback={<SplashScreenComponent />}>
                  <ErrorBoundary>
                    <Weever
                      extensions={extensions}
                      theme={theme}
                      extraMentionsDefinitions={extraMentionsDefinitions}
                      AppBarTitleComponent={AppBarTitleComponent}
                      SplashScreenComponent={SplashScreenComponent}
                    />
                  </ErrorBoundary>
                </Suspense>
              </ApolloContainer>
            </SnackbarProvider>
          </ThemeProvider>
        </StyledEngineProvider>
      </BrowserRouter>,
      reactRootElement
    );
  });
}
