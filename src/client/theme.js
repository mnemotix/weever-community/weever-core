/**
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {blueGrey} from "@mui/material/colors";

export const theme = {
  palette: {
    mode: "light",
    primary: blueGrey,
    secondary: blueGrey,
    text: {
      emptyHint: "rgba(0, 0, 0, 0.54)"
    },
    background: {
      dark: "#F4F6F8",
      default: "#fafafa"
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 700,
      lg: 1080,
      xl: 1920
    }
  },
  typography: {
    fontSize: 14
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          backgroundColor: "#fafafa",
          fontSize: "0.875rem"
        }
      }
    },
    MuiTypography: {
      styleOverrides: {
        subtitle1: {
          fontSize: "1rem"
        }
      }
    },
    MuiDialogActions: {
      styleOverrides: {
        spacing: {
          "&>*": {
            marginLeft: 8
          }
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: {
          textDecoration: "none"
        }
      }
    },
    MuiTimelineItem: {
      styleOverrides: {
        missingOppositeContent: {
          "&:before": {
            display: "none"
          }
        }
      }
    },
    MuiInputBase: {},
    MuiInputLabel: {
      styleOverrides: {
        shrink: {transform: "translate(0, -5px)"},
        outlined: {
          top: "-2px",
          fontSize: "0.75rem",
          lineHeight: 1.43
        },
        notchedOutline: {
          top: "-2px",
          fontSize: "0.75rem",
          lineHeight: 1.43
        }
      }
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {color: "inherit", fontSize: "1rem"}
      }
    }
  }
};
