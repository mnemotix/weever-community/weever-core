# Règles de code à respecter

## Javascript

- Indentation à 2 espaces. Pas de tab.
- Pas de "_" devant les noms de variables.
- Utiliser "const" quand les variables sont immutables. "let" sinon. Jamais de "var".
- Les tests égalités c'est `===` et pas seulement `==` qui n'est pas strict.

## CSS (Ou JSS)

- Pas besoin de fichier Config.js. Utiliser le fichier theme.js pour ça.
- Pas de "px" dans les unités de distance. Utiliser `theme.spacing(unité)`
