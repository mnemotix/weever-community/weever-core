import {ApolloClient, HttpLink} from 'apollo-boost';
 
import {InMemoryCache } from '@apollo/client/cache';
import possibleTypes from "./gql/possibleTypes"; // mauvaix path , il est dans weever ,pas weevercore, passage par param 


export const CacheWithPossibleTypes = new InMemoryCache({
  possibleTypes,
  dataIdFromObject: (o) => {
    return o.id;
  }
});

export const apolloClient = new ApolloClient({
  link: new HttpLink({
    uri: `${process.env.APP_URL}/graphql`,
    fetch: (uri, options) => {
      options.credentials = "omit";
      options.referrer = process.env.APP_URL;
      return global.originalFetch(uri, options);
    }
  }),
  cache: CacheWithPossibleTypes
});
