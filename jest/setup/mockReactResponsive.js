import React from 'react';

// We have to rename window to `mockWindow` so that `jest.mock` doesn't
// complain.
const mockWindow = window;

export function setupMockReactResponsive() {
  jest.mock("react-responsive", () => {
    const MediaQuery = require.requireActual("react-responsive").default;

    const MockMediaQuery = (props) => {
      const defaultWidth = mockWindow.innerWidth;
      const defaultHeight = mockWindow.innerHeight;
      const values = Object.assign({}, { width: defaultWidth, height: defaultHeight }, props.values);

      return <MediaQuery {...props} values={values} />;
    }

    return MockMediaQuery;
  })
}
